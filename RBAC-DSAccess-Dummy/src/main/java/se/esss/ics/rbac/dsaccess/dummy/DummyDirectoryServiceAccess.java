/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.dummy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ejb.Singleton;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;

/**
 * 
 * <code>DummyDirectoryServiceAccess</code> provides a dummy implementation of directory service access. This
 * implementation provides a set of hard coded user info objects.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Singleton
public class DummyDirectoryServiceAccess implements DirectoryServiceAccess {

    private static final long serialVersionUID = 3495804220915196481L;

    private List<DummyUserInfo> users;

    /**
     * Constructs a new dummy directory service access.
     */
    public DummyDirectoryServiceAccess() {
        users = new ArrayList<>();
        users.add(new DummyUserInfo("mihanovak", "Novak", "Miha", null, "GR17", "mn@somewhere.brbr", "888-888-887",
                "B1 r13"));
        users.add(new DummyUserInfo("jakabobnar", "Bobnar", "Jaka", null, "GR16", "jb@somewhere.brbr", "888-888-886",
                "B1 r12"));
        users.add(new DummyUserInfo("jakobbattelino", "Battelino Prelog", "Jakob", null, "GR14", "jbp@somewhere.brbr",
                "888-888-880", "B1 r11"));
        users.add(new DummyUserInfo("miroslavpavleski", "Pavleski", "Miroslav", null, "GR13", "mp@somewhere.brbr",
                "888-888-881", "B1 r10"));
        users.add(new DummyUserInfo("sunilsah", "Sah", "Sunil", null, "GR12", "ssah@somewhere.brbr", "888-888-885",
                "B1 r2"));
        users.add(new DummyUserInfo("andrazpozar", "Pozar", "Andraz", null, "GR11", "ap@somewhere.brbr", "888-888-884",
                "B1 r3"));
        users.add(new DummyUserInfo("jurekrasna", "Krasna", "Jure", null, "GR10", "jk@somewhere.brbr", "888-888-883",
                "B1 r4"));
        users.add(new DummyUserInfo("mihavitorovic", "Vitorovic", "Miha", null, "GR6", "mv@somewhere.brbr",
                "888-888-882", "B1 r12"));
        users.add(new DummyUserInfo("markokolar", "Kolar", "Marko", null, "GR2", "mk@somewhere.brbr", "888-888-088",
                "B1 r5"));
        users.add(new DummyUserInfo("andrejbabic", "Babic", "Andrej", null, "GR1", "ab@somewhere.brbr", "888-880-888",
                "B1 r6"));
        users.add(new DummyUserInfo("sgysin", "Gysin", "Suzanne", null, "GR2", "sg@somewhere.brbr", "808-888-888",
                "B1 r7"));
        users.add(new DummyUserInfo("testuser", "Test", "User", null, "TGR5", "user@test.com", "123-456-189", 
                "test4"));
        users.add(new DummyUserInfo("testuser2", "Test", "User", "2", "TGR4", "user2@test.com", "123-456-289", 
                "test3"));
        users.add(new DummyUserInfo("rbactester1", "RBAC", "Tester", "1", "TGR2", "user2@test.com", "123-256-789",
                "test1"));
        users.add(new DummyUserInfo("rbactester2", "RBAC", "Tester", "2", "TGR3", "user2@test.com", "113-456-789",
                "test2"));

        for (int i = 0; i < 2000; i++) {
            users.add(new DummyUserInfo("user"+i,"last"+i,"first"+i,"","mcs","","","room"));
        }
        Collections.sort(users);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#login(char[], char[])
     */
    @Override
    public UserInfo login(char[] username, char[] password) throws DirectoryServiceAccessException {
        if (Arrays.equals(username, password)) {
            String name = new String(username);
            for (DummyUserInfo i : users) {
                if (i.getUsername().equals(name)) {
                    return i;
                }
            }
        }
        if (Arrays.equals("rbactester1".toCharArray(), username) 
                && Arrays.equals("Changeit!".toCharArray(), password)) {
            for (DummyUserInfo info : users) {
                if ("rbactester1".equals(info.getUsername())) {
                    return info;
                }
            }
        } else if (Arrays.equals("rbactester2".toCharArray(), username)
                && Arrays.equals("Changeit!".toCharArray(), password)) {
            for (DummyUserInfo info : users) {
                if ("rbactester2".equals(info.getUsername())) {
                    return info;
                }
            }
        }

        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#logout(char[])
     */
    @Override
    public boolean logout(char[] username) {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#getUserInfo(char[])
     */
    @Override
    public UserInfo getUserInfo(char[] username) {
        String userId = new String(username);
        for (UserInfo user : users) {
            if (user.getUsername().equalsIgnoreCase(userId)) {
                return user;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#getUserField(char[], java.lang.String)
     */
    @Override
    public String getUserField(char[] username, String fieldname) {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#getUsers(java.lang.String, boolean, boolean, boolean)
     */
    @Override
    public UserInfo[] getUsers(String wildcard, boolean searchByUsername, boolean searchByFirstName,
            boolean searchByLastName) throws DirectoryServiceAccessException {
        if (!searchByUsername && !searchByFirstName && !searchByLastName) {
            return new UserInfo[0];
        }
        if ("*".equals(wildcard)) {
            return users.toArray(new DummyUserInfo[users.size()]);
        } else {
            List<DummyUserInfo> result = new ArrayList<>();
            for (DummyUserInfo userInfo : users) {
                String wc = wildcard.toLowerCase();
                if ((searchByFirstName && userInfo.getFirstName().toLowerCase().startsWith(wc))
                        || (searchByLastName && userInfo.getLastName().toLowerCase().startsWith(wc))
                        || (searchByUsername && userInfo.getUsername().toLowerCase().startsWith(wc))) {
                    result.add(userInfo);
                }
            }
            return result.toArray(new DummyUserInfo[result.size()]);
        }
    }
}
