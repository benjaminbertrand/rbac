/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import se.esss.ics.rbac.jaxb.util.NameValueAdapter;

/**
 * 
 * <code>PermissionsInfo</code> is a wrapper object for response data when asking for permission. It defines XML
 * structure of the data by using JAXB annotations.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 * 
 */
@XmlRootElement(name = "resource")
@XmlType(propOrder = { "resourceName", "permissions" })
@XmlAccessorType(XmlAccessType.FIELD)
public class PermissionsInfo implements Serializable {

    private static final long serialVersionUID = 6105304652068060883L;

    @XmlElement(name = "name")
    private String resourceName;
    @XmlElement(name = "permissions")
    @XmlJavaTypeAdapter(NameValueAdapter.class)
    private Map<String, Boolean> permissions;

    /**
     * Constructs a new PermissionInfo.
     */
    public PermissionsInfo() {
    }

    /**
     * Constructs a new PermissionInfo.
     * 
     * @param resourceName the name of the resource that owns the permissions in this wrapper
     * @param permissions the map of key value pairs, where keys are permission names and values are grant and deny
     *            responses
     */
    public PermissionsInfo(String resourceName, Map<String, Boolean> permissions) {
        this.resourceName = resourceName;
        this.permissions = permissions;
    }

    /**
     * @return the name of the resource
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Sets the resource name for this wrapper.
     * 
     * @param name the name of the resource
     */
    public void setResourceName(String name) {
        this.resourceName = name;
    }

    /**
     * @return the map of permissions, where the keys are permission names, and values are grant and deny states
     */
    public Map<String, Boolean> getPermissions() {
        return permissions;
    }

    /**
     * Sets the permissions.
     * 
     * @param permissions the map of permissions, where the keys are the permission names and the values are grant and
     *            deny states.
     */
    public void setPermissions(Map<String, Boolean> permissions) {
        this.permissions = permissions;
    }

    /**
     * Returns true if the permission to the action described by the given name is granted, or false if denied. If this
     * permission info does not contain the specified permission name, false is returned.
     * 
     * @param permissionName the name of the permission that is being checked
     * @return true if the permission is granted or false if denied
     */
    @Transient
    public boolean isPermissionGranted(String permissionName) {
        Boolean val = permissions.get(permissionName);
        if (val != null) {
            return val.booleanValue();
        }
        return false;
    }
}
