/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <code>RolesInfo</code> is a wrapper for the roles. It can be used to define the list of roles
 * for a specific user or for all the roles in the system (userID is null). This class defines the 
 * XML structure used by the RBAC.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@XmlRootElement(name = "user")
@XmlType(propOrder = { "userID", "roleNames" })
public class RolesInfo implements Serializable {

    private static final long serialVersionUID = 2265876784766082120L;

    private String userID;
    private List<String> roleNames;

    /**
     * Constructs a new RolesInfo.
     */
    public RolesInfo() {
    }

    /**
     * Constructs a new RolesInfo.
     * 
     * @param userID the id of the user that is the owner of the roles.
     * @param roles the list of roles of the user
     */
    public RolesInfo(String userID, List<String> roles) {
        this.userID = userID;
        roleNames = new ArrayList<>(roles);
    }

    /**
     * @return the username
     */
    @XmlElement(name = "username", required = false)
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the username.
     * 
     * @param userID the username
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * @return the list of roles
     */
    @XmlElement(name = "role")
    @XmlElementWrapper(name = "roles")
    public List<String> getRoleNames() {
        return roleNames;
    }

    /**
     * Sets the list of roles.
     * 
     * @param roleNames the roles
     */
    public void setRoleNames(List<String> roleNames) {
        this.roleNames = roleNames;
    }
}
