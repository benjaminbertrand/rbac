/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * <code>ResourcesInfo</code> is a wrapper for the resources list. This class defines the 
 * XML structure used by the RBAC.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@XmlRootElement(name = "resources")
public class ResourcesInfo implements Serializable {

    private static final long serialVersionUID = 2265876784766082120L;

    private List<String> resourceNames;

    /**
     * Constructs a new ResourcesInfo.
     */
    public ResourcesInfo() {
    }

    /**
     * Constructs a new ResourcesInfo.
     * 
     * @param resources the list of resources
     */
    public ResourcesInfo(List<String> resources) {
        resourceNames = new ArrayList<>(resources);
    }

    /**
     * @return the list of roles
     */
    @XmlElement(name = "resource")
  //  @XmlElementWrapper(name = "resources")
    public List<String> getResourceNames() {
        return resourceNames;
    }

    /**
     * Sets the list of resources.
     * 
     * @param resourceNames the resources
     */
    public void setResourceNames(List<String> resourceNames) {
        this.resourceNames = resourceNames;
    }
}
