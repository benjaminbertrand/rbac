/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb.util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Date;

import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * 
 * <code>TokenUtil</code> provides utility methods to transform token data to byte array that is used for signature
 * generation.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class TokenUtil {

    /** Delimiter for roles */
    public static final char DELIMITER_ROLES = ',';

    private static final Charset CHARSET = Charset.forName("UTF-8");
    private static final String EMPTY_STRING = "";

    private TokenUtil() {
    }

    private static byte[] generateTokenData(String username, String firstName, String lastName, String rolesString,
            String ip, Date creationDate) {
        byte[] usernameData = username == null ? new byte[0] : username.getBytes(CHARSET);
        byte[] firstNameData = firstName == null ? new byte[0] : firstName.getBytes(CHARSET);
        byte[] lastNameData = lastName == null ? new byte[0] : lastName.getBytes(CHARSET);
        byte[] rolesData = rolesString == null ? new byte[0] : rolesString.getBytes(CHARSET);
        byte[] ipData = ip == null ? new byte[0] : ip.getBytes(CHARSET);
        byte[] creationData = creationDate == null ? new byte[0] : ByteBuffer.allocate(8)
                .putLong(creationDate.getTime()).array();
        byte[] data = new byte[usernameData.length + firstNameData.length + lastNameData.length + rolesData.length
                + ipData.length + creationData.length];
        int index = 0;
        System.arraycopy(usernameData, 0, data, 0, usernameData.length);
        index += usernameData.length;
        System.arraycopy(firstNameData, 0, data, 0, firstNameData.length);
        index += firstNameData.length;
        System.arraycopy(lastNameData, 0, data, 0, lastNameData.length);
        index += lastNameData.length;
        System.arraycopy(rolesData, 0, data, index, rolesData.length);
        index += rolesData.length;
        System.arraycopy(ipData, 0, data, index, ipData.length);
        index += ipData.length;
        System.arraycopy(creationData, 0, data, index, creationData.length);
        return data;
    }

    /**
     * Generates a byte array that represents the given parameters. The array is created by transforming all the string
     * values to bytes (using the UTF-8 character set) and combining the arrays together in the order the parameters are
     * given. The date is transformed from long to 8 bytes.
     * 
     * @param username the username
     * @param firstName the first name of the token owner
     * @param lastName the last name of the tokenOwner
     * @param roles the list of roles (first transformed by {@link #rolesToString(Collection)})
     * @param ip the IP
     * @param creationDate the creation time
     * @return the byte array representing all the parameters
     */
    public static byte[] generateTokenData(String username, String firstName, String lastName,
            Collection<String> roles, String ip, long creationDate) {
        return generateTokenData(username, firstName, lastName, rolesToString(roles), ip, new Date(creationDate));
    }

    /**
     * Generates a byte array that represents the given parameters. The array is created by transforming all the string
     * values to bytes (using the UTF-8 character set) and combining the arrays together in the order the parameters are
     * given. The date is first transformed to long and then to 8 bytes.
     * 
     * @param username the username
     * @param firstName the first name of the token owner
     * @param lastName the last name of the tokenOwner
     * @param roles the list of roles (first transformed by {@link #rolesToString(String[])})
     * @param ip the IP
     * @param creationDate the creation time
     * @return the byte array representing all the parameters
     */
    public static byte[] generateTokenData(String username, String firstName, String lastName, String[] roles,
            String ip, Date creationDate) {
        return generateTokenData(username, firstName, lastName, rolesToString(roles), ip, creationDate);
    }

    /**
     * Generates a byte array that represents the given info. The array is created by transforming the username, roles,
     * ip to bytes (using the UTF-8 charset) and combining the arrays together in the order the parameters are given.
     * The creation date is transformed to 8 bytes and appended at the end.
     * 
     * @param info the token info that data is generated for
     * @return the byte array representing the token info
     */
    public static byte[] generateTokenData(TokenInfo info) {
        return generateTokenData(info.getUserID(), info.getFirstName(), info.getLastName(), info.getRoleNames(),
                info.getIp(), info.getCreationTime());
    }

    /**
     * Transforms the array to a string representing the content of the array. The string is composed of all the entries
     * concatenated together and delimited by {@value #DELIMITER_ROLES}.
     * 
     * @param roles the array to transform
     * @return a string representing the array
     */
    public static String rolesToString(String[] roles) {
        if (roles == null) {
            return EMPTY_STRING;
        } else {
            StringBuilder sb = new StringBuilder();
            for (String r : roles) {
                sb.append(r).append(DELIMITER_ROLES);
            }
            return sb.length() > 0 ? sb.substring(0, sb.length() - 1) : EMPTY_STRING;
        }
    }

    /**
     * Transforms the collection to a string representing the content of the collection. The string is composed of all
     * the entries concatenated together and delimited by {@value #DELIMITER_ROLES}.
     * 
     * @param roles the collection to transform
     * @return a string representing the collection
     */
    public static String rolesToString(Collection<String> roles) {
        if (roles == null) {
            return EMPTY_STRING;
        } else {
            StringBuilder sb = new StringBuilder();
            for (String r : roles) {
                sb.append(r).append(DELIMITER_ROLES);
            }
            return sb.length() > 0 ? sb.substring(0, sb.length() - 1) : EMPTY_STRING;
        }
    }

}
