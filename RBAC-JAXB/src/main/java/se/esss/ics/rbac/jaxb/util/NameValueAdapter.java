/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * 
 * <code>NameValueAdapter</code> is the adapter for transforming the {@link Permissions} container to a map.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class NameValueAdapter extends XmlAdapter<Permissions, Map<String, Boolean>> {

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    @Override
    public Map<String, Boolean> unmarshal(Permissions v) throws Exception {
        Map<String, Boolean> permissions = new HashMap<>();
        for (NameValuePair p : v.getPermissions()) {
            permissions.put(p.getName(), Boolean.valueOf(p.getValue()));
        }
        return permissions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    @Override
    public Permissions marshal(Map<String, Boolean> v) throws Exception {
        List<NameValuePair> elements = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : v.entrySet()) {
            elements.add(new NameValuePair(entry.getKey(), String.valueOf(entry.getValue())));
        }
        return new Permissions(elements);
    }
}
