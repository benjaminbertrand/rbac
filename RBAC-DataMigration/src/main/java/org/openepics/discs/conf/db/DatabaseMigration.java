package org.openepics.discs.conf.db;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;

@Singleton
@Startup
@TransactionManagement(value = TransactionManagementType.BEAN)
public class DatabaseMigration {

	@Resource(lookup = "java:/rbac")
	private DataSource dataSource;

	@PostConstruct
	private void onStartup() {

		if (dataSource == null) {
			throw new EJBException("DataSource could not be found!");
		}

		Flyway flyway = new Flyway((new FluentConfiguration()).envVars().dataSource(dataSource));
		flyway.migrate();

	}
}
