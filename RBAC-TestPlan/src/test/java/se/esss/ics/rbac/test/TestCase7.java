/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase7: Multiple Tokens</code> verifies if multiple tokens for the same user on the same IP exist.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase7 extends Base {

    private static final String PREFERRED_ROLE = "RBACAdministrator";

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
    }

    /**
     * Login twice from the same IP and check that both tokens exist at the same time.
     */
    @Test
    public void rbac207() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token firstToken = facade.authenticate();

        SecurityFacade secondaryFacade = new SecurityFacade();
        secondaryFacade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password, PREFERRED_ROLE);
            }
        });
        Token secondToken = secondaryFacade.authenticate();

        assertNotEquals("Tokens are generally different", firstToken, secondToken);
        assertEquals("Tokens belong to the same user", firstToken.getUsername(), secondToken.getUsername());
        assertEquals("Tokens belong to the same user", firstToken.getFirstName(), secondToken.getFirstName());
        assertEquals("Tokens belong to the same user", firstToken.getLastName(), secondToken.getLastName());
        assertEquals("Tokens are valid for the same IP", firstToken.getIP(), secondToken.getIP());
        assertNotEquals("Tokens have different IDs", new String(firstToken.getTokenID()),
                new String(secondToken.getTokenID()));

        // check that the token really exists on the remote system

        SecurityFacade checkFacade = new SecurityFacade();
        checkFacade.setToken(firstToken.getTokenID());
        Token checkFirstToken = checkFacade.getToken();
        assertEquals("First token and refetched first token should be the equal", firstToken, checkFirstToken);
        boolean valid = checkFacade.isTokenValid();
        assertTrue("Token " + new String(firstToken.getTokenID()) + " is valid", valid);

        checkFacade.logout();
        checkFacade.setToken(secondToken.getTokenID());
        Token checkSecondToken = checkFacade.getToken();
        assertEquals("Second token and refetched second token should be the equal", secondToken, checkSecondToken);
        valid = checkFacade.isTokenValid();
        assertTrue("Token " + new String(secondToken.getTokenID()) + " is valid", valid);

    }
}
