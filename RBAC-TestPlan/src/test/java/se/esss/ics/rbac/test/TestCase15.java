/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.dsaccess.ldap.LDAPDirectoryServiceAccess;

/**
 * 
 * <code>TestCase15: Username and password</code> checks that the same credentials are used for authentication on RBAC
 * as they are used for authentication on LDAP.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase15 implements UsernameAndPassword {

    /**
     * Test if the same credentials can be used to login to RBAC as they are used for login to LDAP. This test assumes
     * that LDAP is available and that the library is already configured to point to the correct server.
     */
    @Test
    public void rbac010() throws SecurityFacadeException, DirectoryServiceAccessException {
        // login to RBAC
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        Token token = facade.authenticate();
        assertNotNull("Login should be successful", token);
        assertEquals("Token should belong to the specified user", username, token.getUsername());

        // login to LDAP
        try (LDAPDirectoryServiceAccess ldap = new LDAPDirectoryServiceAccess()) {
            UserInfo info = ldap.login(username.toCharArray(), password);
            assertNotNull("Login to LDAP should be successful", info);
            assertEquals("Info should belong to the specified user", username, info.getUsername());

            assertEquals("The first name should match", token.getFirstName(), info.getFirstName());
            assertEquals("The last name should match", token.getLastName(), info.getLastName());
        }
    }

}
