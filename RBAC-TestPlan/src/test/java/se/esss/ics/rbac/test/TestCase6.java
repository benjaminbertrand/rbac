/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.swing.JOptionPane;

import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase6: Login Dialog</code> verifies that standard login dialog is provided and that custom dialog can be
 * used as well.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase6 extends Base {

    /**
     * Test that a standard login dialog is provided by default.
     */
    @Test
    public void rbac200() throws SecurityFacadeException {
        Token token = SecurityFacade.getDefaultInstance().authenticate();
        assertNotNull("Token should be created", token);
        JOptionPane.showMessageDialog(null, "Successfully signed in:\n" + token.toString(), "Sign Successful",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Test if a custom dialog can be provided for authentication.
     */
    @Test
    public void rbac203() {
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                JOptionPane.showMessageDialog(null,
                        "This is a sample custom dialog, which uses\npredefined username 'foo' and password 'bar'",
                        "Custom Login Dialog", JOptionPane.WARNING_MESSAGE);
                return new Credentials("foo", "bar".toCharArray());
            }
        });
        try {
            SecurityFacade.getDefaultInstance().authenticate();
            fail("Exception should occur due to incorrect username and password");
        } catch (SecurityFacadeException e) {
            assertTrue("Exception expected", e.getMessage().contains("Incorrect username or password."));
            JOptionPane.showMessageDialog(null, "Test successful: Sign in unsuccessful. There is no user foo.",
                    "Sign In Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
