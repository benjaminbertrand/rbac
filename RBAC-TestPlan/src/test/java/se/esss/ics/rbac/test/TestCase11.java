/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ExclusiveAccess;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase11: Exclusive Access</code> tests that exclusive access for a permission can be requested and released.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase11 extends Base {

    private SecurityFacade facade;
    private SecurityFacade secondFacade;

    private String secondaryUsername = null;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        secondaryUsername = null;
        facade = new SecurityFacade();
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        facade.authenticate();
        secondFacade = new SecurityFacade();
        secondFacade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                Credentials c = super.getCredentials();
                if (c == null)
                    return c;
                secondaryUsername = c.getUsername();
                return c;
            }
        });
        secondFacade.authenticate();
    }

    @Override
    @After
    public void tearDown() throws Exception {
        facade.releaseExclusiveAccess("RBACDemo", "Add");
        secondFacade.releaseExclusiveAccess("RBACDemo", "Add");
        facade.logout();
        secondFacade.logout();
        facade.destroy();
        secondFacade.destroy();
        super.tearDown();
    }

    /**
     * Test if user can request exclusive access for a permission and that other users are denied the permission when
     * exclusive access is active. It also checks that you can only reqeust exclusive access for permissions that you do
     * have.
     * 
     * @throws SecurityFacadeException
     */
    @Test
    public void rbac210() throws SecurityFacadeException {
        // check permissions for both users
        boolean hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

        // request exclusive access for first user
        ExclusiveAccess granted = facade.requestExclusiveAccess("RBACDemo", "Add", 5);
        assertNotNull("Exclusive access for Add was granted", granted);
        assertEquals("Exclusive access granted for resource RBACDemo", "RBACDemo", granted.getResource());
        assertEquals("Exclusive access granted for permissions Add", "Add", granted.getPermission());
        long diff = Math.abs(System.currentTimeMillis() - granted.getExpirationDate().getTime()) / 1000;
        assertTrue("Exclusive access granted for about 5 minutes", diff > 295 && diff < 301);

        // check permissions for both users
        hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertFalse("Second user does not have permission for Add", hasPermission);

        // second user should not be allowed to request exclusive access.
        try {
            granted = secondFacade.requestExclusiveAccess("RBACDemo", "Add", 5);
            fail("Exception should occur, because first user already has exclusive access.");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected",
                    "Service responded unexpectedly: Exclusive access for permission 'Add' for resource 'RBACDemo' is owned by user '"
                            + username + "'.", e.getMessage().trim());
        }
        // release exclusive access
        facade.releaseExclusiveAccess("RBACDemo", "Add");

        // check permissions for both users
        hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

        // check request exclusive access for a permission you do not have
        hasPermission = secondFacade.hasPermission("RBACManagementStudio", "ManageRole");
        assertFalse("Second user should not have permission for ManageRole", hasPermission);
        try {
            granted = secondFacade.requestExclusiveAccess("RBACManagementStudio", "ManageRole", 5);
        } catch (SecurityFacadeException e) {
            assertEquals(
                    "Exception expected",
                    "Service responded unexpectedly: Cannot request exclusive access for a permission 'ManageRole' for resource 'RBACManagementStudio'. You do not have this permission.",
                    e.getMessage().trim());
        }
    }

    /**
     * Test if the exclusive access is automatically removed after it expires.
     * 
     * @throws SecurityFacadeException
     * @throws InterruptedException
     */
    @Test
    public void rbac220() throws SecurityFacadeException, InterruptedException {
        // check permissions for both users
        boolean hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

        // request exclusive access for first user for 1 minute
        ExclusiveAccess granted = facade.requestExclusiveAccess("RBACDemo", "Add", 1);
        assertNotNull("Exclusive access for Add was granted", granted);

        // check permissions for both users
        hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertFalse("Second user does not have permission for Add", hasPermission);

        synchronized (this) {
            for (int i = 0; i < 65; i += 5) {
                System.out.println("Waiting for exclusive access to expire. " + (65 - i) + " seconds left.");
                wait(5000);
            }
        }

        // check permissions for both users
        hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

    }

    /**
     * Check if the default exclusive access duration is 1 hour.
     * 
     * @throws SecurityFacadeException
     */
    @Test
    public void rbac221() throws SecurityFacadeException {
        // check permissions for both users
        boolean hasPermission = facade.hasPermission("RBACDemo", "DefaultDuration");
        assertTrue("User should have permission for DefaultDuration", hasPermission);
        // request exclusive access for default duration
        ExclusiveAccess granted = facade.requestExclusiveAccess("RBACDemo", "DefaultDuration", 0);
        assertNotNull("Exclusive access for DefaultDuration was granted", granted);
        long diff = Math.abs(System.currentTimeMillis() - granted.getExpirationDate().getTime()) / 1000;
        assertTrue("Exclusive access granted for about 1 hour", diff > 3595 && diff < 3602);
    }

    /**
     * Test if RBACAdministrator can forcefully release exclusive access of another user.
     * 
     * @throws SecurityFacadeException
     */
    @Test
    public void rbac230() throws SecurityFacadeException {
        // check permissions for both users
        boolean hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

        // request exclusive access for second user
        ExclusiveAccess granted = secondFacade.requestExclusiveAccess("RBACDemo", "Add", 1);
        assertNotNull("Exclusive access for Add was granted", granted);

        // check permissions for both users
        hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertFalse("First user does not have permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

        // first user forces exclusive access release
        boolean release = facade.releaseExclusiveAccess("RBACDemo", "Add");
        assertTrue(
                "First user should be able to release exclusive access owned by the second user, because he is RBAC Administrator",
                release);

        // check permissions
        hasPermission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("First user has permission for Add", hasPermission);
        hasPermission = secondFacade.hasPermission("RBACDemo", "Add");
        assertTrue("Second user has permission for Add", hasPermission);

        // request exclusive access
        granted = facade.requestExclusiveAccess("RBACDemo", "Add", 1);
        assertNotNull("Exclusive access for Add was granted", granted);

        try {
            secondFacade.releaseExclusiveAccess("RBACDemo", "Add");
            fail("Exception should occur, because second user does not have permission to release exclusive access of other users.");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Service responded unexpectedly: Token user '" + secondaryUsername
                    + "' does not match the active exclusive access owner '" + username + "'.", e.getMessage());
        }

        facade.releaseExclusiveAccess("RBACDemo", "Add");

    }
}
