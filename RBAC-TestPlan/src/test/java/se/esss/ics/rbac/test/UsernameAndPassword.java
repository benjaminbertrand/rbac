/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

/**
 * 
 * <code>UsernameAndPassword</code> specifies the username and password which are used by the tester during the
 * execution of individual test cases. The username and password need to be valid and recognised by the directory
 * service used by RBAC. For details see the Test Plan document.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public interface UsernameAndPassword {

    /** The username that is used for authentication */
    public static final String username = "rbactester1";
    /** The password that matches the above username */
    public static final char[] password = "Changeit!".toCharArray();
}
