/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing;

import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;

import javax.swing.SwingUtilities;

import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.ISecurityFacade;

/**
 * 
 * <code>SwingAutoLogout</code> is an implementation of the auto logout, which listens to AWT input events to update the
 * action time.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SwingAutoLogout extends AutoLogout implements AWTEventListener {

    private boolean registered = false;

    /**
     * Constructs a new auto logout, which listens to AWT events to reset the last action time.
     * 
     * @param facade the facade to work with
     */
    public SwingAutoLogout(ISecurityFacade facade) {
        super(facade);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.AbstractAutoLogout#registerForInputEvents()
     */
    @Override
    protected void registerForInputEvents() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (registered) {
                    return;
                }
                Toolkit.getDefaultToolkit().addAWTEventListener(
                        SwingAutoLogout.this,
                        AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK
                                | AWTEvent.MOUSE_WHEEL_EVENT_MASK);
                registered = true;
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.AbstractAutoLogout#unRegisterForInputEvents()
     */
    @Override
    protected void unRegisterForInputEvents() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (!registered) {
                    return;
                }
                Toolkit.getDefaultToolkit().removeAWTEventListener(SwingAutoLogout.this);
                registered = false;
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.AWTEventListener#eventDispatched(java.awt.AWTEvent)
     */
    @Override
    public void eventDispatched(AWTEvent event) {
        update();
    }
}
