/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.xml.bind.DatatypeConverter;

/**
 * 
 * <code>CertificatePane</code> is a pane that displays the the certificate issued by a remote host and offers user to
 * accept or reject the certificate.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class CertificatePane extends JOptionPane {
    private static final long serialVersionUID = -832188649399294844L;

    private static final String MESSAGE = "Host %s has issued the certificate %s.\n"
            + "Would you like to accept this certificate (required for any further action)?";
    private static final String DETAILS = "Details >>";

    private final JButton detailsButton;
    private final JTextArea detailsArea;

    private JDialog dialog;

    /**
     * Constructs a new certificate pane.
     * 
     * @param host the name of the host that issued the certificate
     * @param certificate the certificate that needs to be confirmed
     */
    protected CertificatePane(String host, X509Certificate certificate) {
        super("", JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
        JTextArea messageLabel = new JTextArea();
        messageLabel.setText(String.format(MESSAGE, host, certificate.getSubjectX500Principal().getName()));
        messageLabel.setBackground(getBackground());
        messageLabel.setLineWrap(false);
        messageLabel.setFont(new JLabel().getFont());
        int width = (int) messageLabel.getPreferredSize().getWidth();
        detailsArea = new JTextArea(getCertificateInformation(certificate, (width - 3 * 7) / (7 * 3)));
        detailsArea.setBackground(Color.WHITE);
        detailsArea.setEditable(false);
        detailsArea.setLineWrap(true);
        detailsArea.setFont(new Font("Courier New", Font.PLAIN, 12));
        JPanel p = new JPanel(new GridBagLayout());
        p.add(messageLabel, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        p.add(detailsArea, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.BOTH,
                new Insets(5, 0, 0, 0), 0, 0));

        setMessage(p);

        JPanel buttonPanel = (JPanel) getComponent(1);
        detailsButton = new JButton(DETAILS);
        detailsButton.setToolTipText("Show Certificate Details");
        detailsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (DETAILS.equals(detailsButton.getText())) {
                    expand();
                } else {
                    shrink();
                }

            }
        });
        buttonPanel.add(detailsButton, 0);
    }

    private void expand() {
        detailsButton.setText("<< Details");
        detailsButton.setToolTipText("Hide Certificate Details");
        detailsArea.setVisible(true);
        dialog.pack();
        dialog.setLocationRelativeTo(dialog.getParent());
    }

    private void shrink() {
        detailsButton.setText(DETAILS);
        detailsButton.setToolTipText("Show Certificate Details");
        detailsArea.setVisible(false);
        dialog.pack();
        dialog.setLocationRelativeTo(dialog.getParent());
    }

    /**
     * Creates a dialog that contains this pane.
     * 
     * @param parent the parent of this dialog
     * @return the dialog
     * @throws HeadlessException if <code>GraphicsEnvironment.isHeadless</code> returns <code>true</code>
     */
    public JDialog createDialog(Component parent) throws HeadlessException {
        dialog = super.createDialog(parent, "Confirm Certificate");
        shrink();
        dialog.setLocationRelativeTo(parent);
        return dialog;
    }

    /**
     * Returns <code>true</code> if the user accepted the certificate <code>false</code> if the user rejected it.
     * 
     * @return true if certificate was accepted or false if rejected
     */
    public boolean isConfirmed() {
        Object o = getValue();
        if (o == UNINITIALIZED_VALUE) {
            return false;
        }
        if (o instanceof Integer) {
            return ((Integer) o).intValue() == JOptionPane.YES_OPTION;
        }
        return false;
    }

    /**
     * Creates string with certificate data.
     * 
     * @param certificate the certificate
     * @param width the number of hex signature bytes in a row that fit into view
     * @return String with certificate data.
     */
    private static String getCertificateInformation(X509Certificate certificate, int width) {
        if (certificate == null) {
            return "Certificate not available";
        }
        PublicKey key = certificate.getPublicKey();
        StringBuilder keyStr = new StringBuilder(500);
        byte[] data = key.getEncoded();
        keyStr.append(key.getAlgorithm() + " " + key.getFormat() + ", " + data.length * 8 + " bits");
        for (int i = 0; i < data.length; i++) {
            if (i % width == 0) {
                keyStr.append("\n   ");
            }
            keyStr.append(DatatypeConverter.printHexBinary(new byte[] { data[i] })).append(' ');
        }
        if (key instanceof RSAPublicKey) {
            keyStr.append("\n   Public Exponent: ").append(((RSAPublicKey) key).getPublicExponent());
        }
        return new StringBuilder(1000).append(" Subject: ")
                .append(certificate.getSubjectX500Principal().getName())
                .append("\n Version: ").append(certificate.getVersion())
                .append("\n Serial number: ").append(certificate.getSerialNumber())
                .append("\n Signiture algorithm: ").append(certificate.getSigAlgName())
                .append(", OID: ").append(certificate.getSigAlgOID())
                .append("\n Issuer: ").append(certificate.getIssuerX500Principal().getName())
                .append("\n Valid from: ").append(certificate.getNotBefore())
                .append("\n Valid to: ").append(certificate.getNotAfter())
                .append("\n Public key: ").append(keyStr).toString();
    }
}
