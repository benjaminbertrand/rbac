/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing.test;

import java.security.cert.X509Certificate;

import se.esss.ics.rbac.access.swing.SecurityFacade;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.test.SecurityFacadeAuthenticationTest;

/**
 * 
 * <code>SwingSecurityFacadeAuthenticationTest</code> tests authentication methods using the swing security facade.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SwingSecurityFacadeAuthenticationTest extends SecurityFacadeAuthenticationTest {

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.test.AbstractSecurityFacadeTest#createFacade()
     */
    @Override
    protected ISecurityFacade createFacade() {
        ISecurityFacade facade = new SecurityFacade(super.createFacade());
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public boolean acceptCertificate(String host, X509Certificate certificate) {
                return true;
            }
        });
        return facade;
    }
}
