/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess;

/**
 * This exception is thrown if there is an error while authenticating a user, using the {@link DirectoryServiceAccess}.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public class DirectoryServiceAccessException extends Exception {
    private static final long serialVersionUID = 1961467758298758793L;

    /**
     * Constructs a new exception with <code>null</code> as its detail message. The cause is not initialised, and may
     * subsequently be initialised by a call to {@link #initCause(Throwable)}.
     */
    public DirectoryServiceAccessException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialised, and may subsequently
     * be initialised by a call to {@link #initCause(Throwable)}.
     * 
     * @param message detailed message of the exception
     */
    public DirectoryServiceAccessException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and cause's message as the detail message. This constructor
     * is useful for exceptions that are little more than wrappers for other throwables. A null value is permitted and
     * indicates that the cause is nonexistent or unknown.
     * 
     * @param cause the cause of the exception
     */
    public DirectoryServiceAccessException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause. Note that the detail message associated
     * with cause is not automatically incorporated in this exception's detail message.
     * 
     * @param message detailed message of the exception
     * @param cause the cause of the exception
     */
    public DirectoryServiceAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
