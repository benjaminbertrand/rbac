/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess;

import java.io.Serializable;

/**
 * DirectoryServiceAccess provides an interface to the directory service. It allows authentication of users and
 * retrieving their information as {@link UserInfo}. Authentication is done through {@link #login(char[], char[])} and
 * {@link #logout(char[])} methods, while information about users can be retrieved using other methods of this
 * interface.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public interface DirectoryServiceAccess extends Serializable {

    /**
     * Authenticates the user using the provided username and password. Upon successful authentication the
     * {@link UserInfo} is returned. If authentication is not successful, null is returned or an exception may be
     * thrown, if there was an error during authentication (e.g. directory service not available etc.). The returned
     * UserInfo may or may not already include the user data. If convenient, the data can be loaded at login; however,
     * it may also be loaded lazily, when an individual field in the UserInfo is accessed.
     * 
     * @param username <code>char[]</code> array containing the username to be used for authentication
     * @param password <code>char[]</code> array containing the password to be used for authentication
     * @return {@link UserInfo} containing the user's information or <code>null</code>, if the authentication has been
     *         unsuccessful
     * @throws DirectoryServiceAccessException if there was an error during authentication (e.g. user does not exist or
     *             any other kind of error)
     */
    UserInfo login(char[] username, char[] password) throws DirectoryServiceAccessException;

    /**
     * Logs the user out of the system and returns true if the user was logged out successfully or false otherwise. Some
     * directory services do not require users to logout. In such cases, this method should always return true.
     * 
     * @param username the username of the user that needs to be logged out
     * @return <code>true</code> if the user has been logged out successfully.
     */
    boolean logout(char[] username);

    /**
     * Returns the info for the provided username. The info in this case represents a place holder for the information
     * that belongs to the user. The {@link UserInfo} may include other properties if they are available, but they will
     * not be used or shown anywhere.
     * 
     * @param username <code>char[]</code> array containing the username of the user for whom the information should be
     *            returned
     * @return {@link UserInfo} information for the user with the provided username
     * @throws DirectoryServiceAccessException if there was an error while retrieving user info
     */
    UserInfo getUserInfo(char[] username) throws DirectoryServiceAccessException;

    /**
     * Returns the value of a single field of the user. The field may be a username, first name, last name etc.
     * 
     * @param username <code>char[]</code> array containing the username of the user for whom the field value should be
     *            returned
     * @param fieldname <code>String</code> that is the name of the field
     * @return <code>String</code> value of the requested field for the user with the provided username
     * @throws DirectoryServiceAccessException if there was an error while retrieving field value
     */
    String getUserField(char[] username, String fieldname) throws DirectoryServiceAccessException;

    /**
     * Returns the list of all users whose username, first or last name (depending on the provided parameters) matches
     * the given wildcard. If all three parameters are false, the service should return an empty array. The returned
     * UserInfo does not necessarily include all the fields, but it should include the username, first name and last
     * name.
     * 
     * @param wildcard <code>String</code> a string to search for in username, first name or last name
     * @param searchByUsername <code>true</code> if the search should be performed on the username field or
     *            <code>false</code> otherwise
     * @param searchByFirstName <code>true</code> if the search should be performed on the first name field or
     *            <code>false</code> otherwise
     * @param searchByLastName <code>true</code> if the search should be performed on the last name field or
     *            <code>false</code> otherwise
     * @return {@link UserInfo}<code>[]</code> array of users matching the provided name wildcard string
     * @throws DirectoryServiceAccessException if there was an error while retrieving users info
     */
    UserInfo[] getUsers(String wildcard, boolean searchByUsername, boolean searchByFirstName, boolean searchByLastName)
            throws DirectoryServiceAccessException;
}
