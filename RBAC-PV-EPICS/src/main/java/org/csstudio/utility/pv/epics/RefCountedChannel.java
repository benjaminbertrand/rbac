/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import gov.aps.jca.CAException;
import gov.aps.jca.Channel;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Channel with thread-safe reference count.
 * 
 * @author Kay Kasemir
 */
class RefCountedChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefCountedChannel.class);

    private Channel channel;

    private final AtomicInteger refs;

    /**
     * Construct a new reference channel.
     * 
     * @param channel the CA channel
     * @throws CAException when channel is <code>null</code>
     */
    public RefCountedChannel(final Channel channel) throws CAException {
        if (channel == null) {
            throw new CAException("Channel must not be null");
        }
        this.channel = channel;
        refs = new AtomicInteger(1);
    }

    /** Increment reference count. */
    public synchronized void incRefs() {
        refs.incrementAndGet();
    }

    /**
     * Decrement reference count.
     * 
     * @return Remaining references.
     */
    public synchronized int decRefs() {
        return refs.decrementAndGet();
    }

    /** @return ChannelAccess channel */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Must be called when all references are gone.
     * 
     * @throws CAException when channel is still references
     */
    public void dispose() throws CAException {
        if (refs.get() != 0) {
            throw new CAException("Channel destroyed while referenced " + refs + " times");
        }
        try {
            channel.destroy();
        } catch (CAException | IllegalStateException ex) {
            LOGGER.warn("Channel.destroy failed", ex);
        }
        channel = null;
    }
}
