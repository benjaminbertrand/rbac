/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.logic.AuthenticationEJB;
import se.esss.ics.rbac.logic.GeneralEJB;
import se.esss.ics.rbac.logic.SignatureEJB;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>AuthenticationEJBTest</code> tests methods of {@link AuthenticationEJB}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class AuthenticationEJBTest {

    private UserInfo johnDoe = new UserInfo() {
        private static final long serialVersionUID = 1L;

        @Override
        public int compareTo(UserInfo o) {
            return 0;
        }

        @Override
        public String getUsername() {
            return "johndoe";
        }

        @Override
        public String getPhoneNumber() {
            return "555-888-888";
        }

        @Override
        public String getMiddleName() {
            return null;
        }

        @Override
        public String getLocation() {
            return "office 2";
        }

        @Override
        public String getLastName() {
            return "Doe";
        }

        @Override
        public String getGroup() {
            return "John's group";
        }

        @Override
        public String getFirstName() {
            return "John";
        }

        @Override
        public String getEMail() {
            return "john.doe@esss.se";
        }
    };

    private AuthenticationEJB ejb;
    private EntityManager em;
    private Token token;
    private TypedQuery<Token> tokenQuery;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        SignatureEJB signature = mock(SignatureEJB.class);
        when(signature.verifySignature(any(byte[].class), any(byte[].class))).thenReturn(Boolean.TRUE);
        when(signature.generateSignature(any(byte[].class))).thenReturn(new byte[] { 1, 2, 3, 4, 5 });

        DirectoryServiceAccess dsAccess = mock(DirectoryServiceAccess.class);
        when(dsAccess.login("johndoe".toCharArray(), "pass".toCharArray())).thenReturn(johnDoe);
        when(dsAccess.login("janedoe".toCharArray(), "pass".toCharArray())).thenReturn(null);
        when(dsAccess.logout("johndoe".toCharArray())).thenReturn(Boolean.TRUE);
        when(dsAccess.logout("janedoe".toCharArray())).thenReturn(Boolean.FALSE);
        when(dsAccess.getUserInfo("johndoe".toCharArray())).thenReturn(johnDoe);
        when(dsAccess.getUserInfo("janedoe".toCharArray())).thenReturn(null);
        when(dsAccess.login("jenny".toCharArray(), "jenny".toCharArray())).thenThrow(
                new DirectoryServiceAccessException("Error"));
        when(dsAccess.getUserInfo("jenny".toCharArray())).thenThrow(new DirectoryServiceAccessException("Error"));

        ejb = new AuthenticationEJB();

        token = new Token();
        token.setCreationDate(new Timestamp(System.currentTimeMillis() - 10000));
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));
        token.setIp("192.168.1.1");
        token.setTokenId("id123");
        token.setUserId("johndoe");
        Role r = new Role();
        r.setName("Role");
        UserRole ur = new UserRole();
        ur.setUserId("johndoe");
        ur.setRole(r);
        token.setRole(new HashSet<>(Arrays.asList(new TokenRole(token, ur))));

        em = mock(EntityManager.class);
        tokenQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Token.findByTokenId", Token.class)).thenReturn(tokenQuery);
        when(tokenQuery.setParameter("tokenId", "id123")).thenReturn(tokenQuery);
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token));

        Role r2 = new Role();
        r2.setName("Role2");
        UserRole ur2 = new UserRole();
        ur2.setRole(r2);
        ur2.setUserId("johndoe");

        TypedQuery<UserRole> roleQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("UserRole.findValidByUserId", UserRole.class)).thenReturn(roleQuery);
        when(roleQuery.setParameter("userId", "johndoe")).thenReturn(roleQuery);
        when(roleQuery.getResultList()).thenReturn(Arrays.asList(ur, ur2));

        TypedQuery<UserRole> roleQuery2 = mock(TypedQuery.class);
        when(em.createNamedQuery("UserRole.findValidByNamesAndUserId", UserRole.class)).thenReturn(roleQuery2);
        when(roleQuery2.setParameter("userId", "johndoe")).thenReturn(roleQuery2);
        when(roleQuery2.setParameter(any(String.class), any(ArrayList.class))).thenReturn(roleQuery2);
        when(roleQuery2.getResultList()).thenReturn(new ArrayList<UserRole>());
        when(roleQuery2.getResultList()).thenReturn(Arrays.asList(ur));

        Field f = AuthenticationEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(ejb, em);

        f = AuthenticationEJB.class.getDeclaredField("signatureEJB");
        f.setAccessible(true);
        f.set(ejb, signature);

        f = AuthenticationEJB.class.getDeclaredField("directoryService");
        f.setAccessible(true);
        f.set(ejb, dsAccess);

        GeneralEJB general = new GeneralEJB();
        f = GeneralEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(general, em);

        f = AuthenticationEJB.class.getDeclaredField("generalEJB");
        f.setAccessible(true);
        f.set(ejb, general);

    }

    /**
     * Test {@link AuthenticationEJB#getToken(String, String)} with invalid parameters.
     */
    @Test
    public void testGetTokenWithInvalidParameters() {
        // test for null parameters
        try {
            ejb.getToken(null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        } catch (RBACException e) {
            assertEquals("Wrong exception", IllegalRBACArgumentException.class, e.getClass());
        }
        try {
            ejb.getToken("");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        } catch (RBACException e) {
            assertEquals("Wrong exception", IllegalRBACArgumentException.class, e.getClass());
        }
    }

    /**
     * Test {@link AuthenticationEJB#getToken(String, String)} if the providing or requested token has already expired
     * or is fishy.
     */
    @Test
    public void testGetTokenWithIncorrectToken() {
        // test for expired token
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 1000));
        try {
            ejb.getToken("id123");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            fail(e.getMessage());
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token owned by '" + token.getUserId() + "' has already expired.",
                    e.getMessage());
        } catch (RBACException e) {
            assertEquals("Wrong exception", TokenInvalidException.class, e.getClass());
        }
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));

        // test if the token does not exist
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        try {
            ejb.getToken("id123");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            fail(e.getMessage());
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token with ID '" + token.getTokenId() + "' could not be found.",
                    e.getMessage());
        } catch (RBACException e) {
            assertEquals("Wrong exception", InvalidResourceException.class, e.getClass());
        }

        // test if multiple tokens are returned
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.getToken("id123");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            fail(e.getMessage());
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        } catch (RBACException e) {
            assertEquals("Wrong exception", DataConsistencyException.class, e.getClass());
        }
    }

    /**
     * Test {@link AuthenticationEJB#getToken(String, String)} method.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testGetToken() throws RBACException {
        // Test if token is available
        TokenInfo info = ejb.getToken("id123");
        assertEquals("ID should match", token.getTokenId(), info.getTokenID());
        assertEquals("IP should match", token.getIp(), info.getIp());
        assertEquals("User ID should match", token.getUserId(), info.getUserID());
        assertEquals("Creation Date should match", token.getCreationDate().getTime(), info.getCreationTime());
        assertEquals("Expiration Date should match", token.getExpirationDate().getTime(), info.getExpirationTime());
    }

    /**
     * Test {@link AuthenticationEJB#getToken(String, String)} method if directory service throws exception.
     */
    @Test
    public void testGetTokenWithDSError() {
        // Test if token is available
        token.setUserId("jenny");
        try {
            ejb.getToken("id123");
            fail("Exception should occur");
        } catch (RBACException e) {
            assertEquals("Excpetion expected", "Information retrieval for user 'jenny' failed.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#isTokenValid(String)} method.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testValidateToken() throws RBACException {
        // Test if token is available
        TokenInfo info = ejb.isTokenValid("id123");
        assertEquals("ID should match", token.getTokenId(), info.getTokenID());
        assertEquals("IP should match", token.getIp(), info.getIp());
        assertEquals("User ID should match", token.getUserId(), info.getUserID());
        assertEquals("Creation Date should match", token.getCreationDate().getTime(), info.getCreationTime());
        assertEquals("Expiration Date should match", token.getExpirationDate().getTime(), info.getExpirationTime());
    }

    /**
     * Test {@link AuthenticationEJB#isTokenValid(String)} if the providing or requested token has already expired or is
     * fishy.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testValidateIncorrectToken() throws RBACException {
        // test for expired token
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 1000));
        try {
            ejb.isTokenValid("id123");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            throw e;
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token owned by '" + token.getUserId() + "' has already expired.",
                    e.getMessage());
        }

        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));

        // test if the token does not exist
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        try {
            ejb.isTokenValid("id123");
            fail("Exception should occur");
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token with ID '" + token.getTokenId() + "' could not be found.",
                    e.getMessage());
        }

        // test if multiple tokens are returned
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.isTokenValid("id123");
            fail("Exception should occur");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#isTokenValid(String)} with invalid parameters.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testValidateTokenWithInvalidParameters() throws RBACException {
        // test for null parameters
        try {
            ejb.isTokenValid(null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }

        try {
            ejb.isTokenValid("");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#login(char[], char[], String, String[])} with invalid parameters.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testLoginWithInvalidParameters() throws RBACException {
        try {
            ejb.login(null, "pass".toCharArray(), "192.168.1.1", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Username was not provided.", e.getMessage());
        }
        try {
            ejb.login(new char[0], "pass".toCharArray(), "192.168.1.1", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Username was not provided.", e.getMessage());
        }
        try {
            ejb.login("johndoe".toCharArray(), null, "192.168.1.1", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Password was not provided.", e.getMessage());
        }
        try {
            ejb.login("johndoe".toCharArray(), new char[0], "192.168.1.1", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Password was not provided.", e.getMessage());
        }
        try {
            ejb.login("johndoe".toCharArray(), "pass".toCharArray(), null, null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "IP address was not provided.", e.getMessage());
        }
        try {
            ejb.login("johndoe".toCharArray(), "pass".toCharArray(), "", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "IP address was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#login(char[], char[], String, String[])} with invalid credentials.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testLoginWithInvalidCredentials() throws RBACException {
        // test with invalid username
        try {
            ejb.login("janedoe".toCharArray(), "pass".toCharArray(), "192.168.1.1", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            fail(e.getMessage());
        } catch (AuthAndAuthException e) {
            assertEquals("Exception expected",
                    "Authentication for user 'janedoe' failed. Incorrect username or password.", e.getMessage());
        }

        // test with invalid password
        try {
            ejb.login("johndoe".toCharArray(), "bla".toCharArray(), "192.168.1.1", null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            fail(e.getMessage());
        } catch (AuthAndAuthException e) {
            assertEquals("Exception expected",
                    "Authentication for user 'johndoe' failed. Incorrect username or password.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#login(char[], char[], String, String[])} method.
     * 
     * @throws InterruptedException on error
     * @throws RBACException on error
     */
    @Test
    public void testLogin() throws InterruptedException, RBACException {
        // test with null roles requested
        TokenInfo info = ejb.login("johndoe".toCharArray(), "pass".toCharArray(), "192.168.1.1", null);
        // wait a little so the current time is later than info creation time
        Thread.sleep(5);
        assertEquals("First name should match", "John", info.getFirstName());
        assertEquals("Last name should match", "Doe", info.getLastName());
        assertEquals("Username name should match", "johndoe", info.getUserID());
        assertEquals("IP should match", "192.168.1.1", info.getIp());
        String[] roles = info.getRoleNames().toArray(new String[0]);
        Arrays.sort(roles);
        assertArrayEquals("Roles should match", new String[] { "Role", "Role2" }, roles);
        assertTrue("Creation date should be in the past", info.getCreationTime() < System.currentTimeMillis());
        assertTrue("Expiration date should be in the future", info.getExpirationTime() > System.currentTimeMillis());
        assertNotNull("TokenID should exist", info.getTokenID());
        assertNotNull("Signature should exist", info.getSignature());

        info = ejb.login("johndoe".toCharArray(), "pass".toCharArray(), "192.168.1.1", new String[] { "Role" });
        // wait a little so the current time is later than info creation time
        Thread.sleep(5);
        assertEquals("First name should match", "John", info.getFirstName());
        assertEquals("Last name should match", "Doe", info.getLastName());
        assertEquals("Username name should match", "johndoe", info.getUserID());
        assertEquals("IP should match", "192.168.1.1", info.getIp());
        assertArrayEquals("Roles should match", new String[] { "Role" }, info.getRoleNames().toArray(new String[0]));
        assertTrue("Creation date should be in the past", info.getCreationTime() < System.currentTimeMillis());
        assertTrue("Expiration date should be in the future", info.getExpirationTime() > System.currentTimeMillis());
        assertNotNull("TokenID should exist", info.getTokenID());
        assertNotNull("Signature should exist", info.getSignature());

        // test with role that we don't have
        TypedQuery<UserRole> query = em.createNamedQuery("UserRole.findValidByNamesAndUserId", UserRole.class);
        when(query.getResultList()).thenReturn(new ArrayList<UserRole>());
        info = ejb.login("johndoe".toCharArray(), "pass".toCharArray(), "192.168.1.1", new String[] { "Role2" });
        // wait a little so the current time is later than info creation time
        Thread.sleep(5);
        assertEquals("First name should match", "John", info.getFirstName());
        assertEquals("Last name should match", "Doe", info.getLastName());
        assertEquals("Username name should match", "johndoe", info.getUserID());
        assertEquals("IP should match", "192.168.1.1", info.getIp());
        assertArrayEquals("Roles should match", new String[0], info.getRoleNames().toArray(new String[0]));
        assertTrue("Creation date should be in the past", info.getCreationTime() < System.currentTimeMillis());
        assertTrue("Expiration date should be in the future", info.getExpirationTime() > System.currentTimeMillis());
        assertNotNull("TokenID should exist", info.getTokenID());
        assertNotNull("Signature should exist", info.getSignature());
    }

    /**
     * Test if directory service access errors are properly handled.
     */
    @Test
    public void testLoginWithDSAccessError() {
        try {
            ejb.login("jenny".toCharArray(), "jenny".toCharArray(), "192.168.1.1", null);
        } catch (RBACException e) {
            assertEquals("Exception expected", e.getClass(), RBACException.class);
            assertEquals("Message should match", "Authentication for user 'jenny' failed.", e.getMessage());
            assertEquals("Message should match", "Error", e.getCause().getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#logout(String, String)} when providing invalid parameters.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testLogoutWithInvalidParameters() throws RBACException {
        try {
            ejb.logout(null);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.logout("");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#logout(String, String)} method.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testLogout() throws RBACException {
        // test normal
        TokenInfo info = ejb.logout("id123");
        assertEquals("User id should match", token.getUserId(), info.getUserID());
        assertEquals("Token id should match", token.getTokenId(), info.getTokenID());
        assertEquals("IP should match", token.getIp(), info.getIp());
    }

    /**
     * Test {@link AuthenticationEJB#logout(String, String)} when the directory service fails to logout.
     */
    @Test
    public void testLogoutIfDSFails() {
        // test failed directory service logout
        token.setUserId("janedoe");
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token));
        try {
            ejb.logout("id123");
            fail("Exception should occur");
        } catch (RBACException e) {
            assertEquals("Exception expected", "Directory Service could not logout user 'janedoe'.", e.getMessage());
            assertEquals("Exception expected", RBACException.class, e.getClass());
        }
    }

    /**
     * Test {@link AuthenticationEJB#logout(String, String)} when using fishy token.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testLogoutWithIncorrectToken() throws RBACException {
        // test with non existing token
        when(tokenQuery.setParameter("tokenId", "id1234")).thenReturn(tokenQuery);
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        TokenInfo info = ejb.logout("id1234");
        assertNull("Token should be null", info);

        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token));
        // test with expired token
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 1000));
        info = ejb.logout("id123");
        assertEquals("User id should match", token.getUserId(), info.getUserID());
        assertEquals("Token id should match", token.getTokenId(), info.getTokenID());
        assertEquals("IP should match", token.getIp(), info.getIp());

        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));
        // test with multiple tokens
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.logout("id123");
            fail("Exception should occur");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#renewToken(String, String)} with invalid parameters.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRenewTokenWithInvalidParameters() throws RBACException {
        try {
            ejb.renewToken(null);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.renewToken("");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#renewToken(String, String)} method.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRenewToken() throws RBACException {
        // test normal
        TokenInfo info = ejb.getToken("id123");
        TokenInfo info2 = ejb.renewToken("id123");
        assertEquals("Username should match", info.getUserID(), info2.getUserID());
        assertEquals("First name should match", info.getFirstName(), info2.getFirstName());
        assertEquals("Last name should match", info.getLastName(), info2.getLastName());
        assertEquals("Token ID should match", info.getTokenID(), info2.getTokenID());
        assertEquals("Creation time should match", info.getCreationTime(), info2.getCreationTime());
        assertArrayEquals("Signature should match", info.getSignature(), info2.getSignature());
        assertArrayEquals("Roles should match", info.getRoleNames().toArray(new String[0]), info2.getRoleNames()
                .toArray(new String[0]));
        assertTrue("Expiration time of the renewed token should be greater",
                info2.getExpirationTime() > info.getExpirationTime());
    }

    /**
     * Test {@link AuthenticationEJB#renewToken(String, String)} method if directory service throws exception.
     */
    @Test
    public void testRenewTokenWithDSError() {
        // Test if token is available
        token.setUserId("jenny");
        try {
            ejb.renewToken("id123");
            fail("Exception should occur");
        } catch (RBACException e) {
            assertEquals("Excpetion expected", "Information retrieval for user 'jenny' failed.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#renewToken(String, String)} when the token is fishy.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRenewTokenWithIncorrectToken() throws RBACException {
        // test with expired token
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 10000));
        try {
            ejb.renewToken("id123");
            fail("Exception should occur");
        } catch (TokenInvalidException e) {
            assertEquals("Exception message should match", "Token owned by '" + token.getUserId()
                    + "' has already expired.", e.getMessage());
        }
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));

        // test with multiple tokens
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.renewToken("id123");
            fail("Exception should occur");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        }

        // test with no tokens
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        try {
            ejb.renewToken("id123");
            fail("Exception should occur");
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token with ID '" + token.getTokenId() + "' could not be found.",
                    e.getMessage());
        }
    }
}
