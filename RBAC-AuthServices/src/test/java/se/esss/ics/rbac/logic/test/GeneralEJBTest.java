/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.UsersInfo;
import se.esss.ics.rbac.logic.AuthenticationEJB;
import se.esss.ics.rbac.logic.GeneralEJB;
import se.esss.ics.rbac.logic.SignatureEJB;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>GeneralEJBTest</code> tests the methods of the {@link GeneralEJB}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class GeneralEJBTest {

    private GeneralEJB ejb;
    private TypedQuery<Role> query;
    private TypedQuery<Role> query2;
    private TypedQuery<Resource> query3;
    private TypedQuery<Permission> query4;
    private SignatureEJB signature;

    /**
     * Mock the common fields.
     * 
     * @throws Exception on error
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        ejb = new GeneralEJB();
        EntityManager em = mock(EntityManager.class);
        Role role1 = new Role();
        role1.setName("Role1");
        Role role2 = new Role();
        role2.setName("Role2");
        query = mock(TypedQuery.class);
        when(em.createNamedQuery("Role.findValidByUserId", Role.class)).thenReturn(query);
        when(query.setParameter("userId", "johndoe")).thenReturn(query);
        when(query.getResultList()).thenReturn(Arrays.asList(role1, role2));
        
        UserRole usr = new UserRole();
        usr.setRole(role1);
        usr.setUserId("johndoe");
        UserRole usr2 = new UserRole();
        usr2.setRole(role2);
        usr2.setUserId("janedoe");
        Field a = Role.class.getDeclaredField("users");
        a.setAccessible(true);
        a.set(role1, new LinkedHashSet<UserRole>(Arrays.asList(usr,usr2)));
        
        query2 = mock(TypedQuery.class);
        when(em.createNamedQuery("Role.findByName", Role.class)).thenReturn(query2);
        when(query2.setParameter("name", "admin")).thenReturn(query2);
        when(query2.getResultList()).thenReturn(Arrays.asList(role1));
        
        
        Resource resource = new Resource();
        resource.setName("resource");
        query3 = mock(TypedQuery.class);
        when(em.createNamedQuery("Resource.findByName", Resource.class)).thenReturn(query3);
        when(query3.setParameter("name", "resource")).thenReturn(query3);
        when(query3.getResultList()).thenReturn(Arrays.asList(resource));
        Permission permission = new Permission();
        permission.setRole(new LinkedHashSet<Role>(Arrays.asList(role1)));
        query4 = mock(TypedQuery.class);
        when(em.createNamedQuery("Permission.findByName", Permission.class)).thenReturn(query4);
        when(query4.setParameter("name", "permission")).thenReturn(query4);
        when(query4.setParameter("resource", resource)).thenReturn(query4);
        when(query4.getResultList()).thenReturn(Arrays.asList(permission));

        signature = mock(SignatureEJB.class);
        PublicKey key = mock(PublicKey.class);
        when(key.getEncoded()).thenReturn(new byte[] { 1, 2, 3, 4 });
        when(signature.getPublicKey()).thenReturn(key);

        Field f = GeneralEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(ejb, em);

        f = GeneralEJB.class.getDeclaredField("signatureEJB");
        f.setAccessible(true);
        f.set(ejb, signature);
    }
    
    /**
     * Test {@link GeneralEJB#getUsersWithRole(String)} method.
     * 
     * @throws IllegalRBACArgumentException on error
     * @throws InvalidResourceException on error
     */
    @Test
    public void testGetUsersWithRole() throws IllegalRBACArgumentException, InvalidResourceException {
        UsersInfo info = ejb.getUsersWithRole("admin");
        assertNull("Resource should be null",info.getResource());
        assertNull("Permission should be null",info.getPermission());
        assertEquals("Role name should match", "admin", info.getRole());
        assertArrayEquals("User names should match", new String[] { "johndoe", "janedoe" },
                info.getUsers().toArray(new String[0]));

        when(query2.getResultList()).thenReturn(new ArrayList<Role>());
        try {
            info = ejb.getUsersWithRole("admin");
            fail("Exception should occur");
        } catch (InvalidResourceException e) {
            assertEquals("Messages should be equal", "Role with name 'admin' could not be found.", e.getMessage());
        }
    }
    
    /**
     * Test {@link GeneralEJB#getUsersWithPermission(String, String)} method.
     *  
     * @throws IllegalRBACArgumentException on error
     * @throws InvalidResourceException on error
     */
    @Test
    public void testGetUsersWithPermission() throws IllegalRBACArgumentException, InvalidResourceException {
        UsersInfo info = ejb.getUsersWithPermission("resource","permission");
        assertNull("Role should be null", info.getRole());
        assertEquals("Resource names should match", "resource", info.getResource());
        assertEquals("Permission names should match", "permission", info.getPermission());
        assertArrayEquals("User names should match", new String[] { "johndoe", "janedoe" },
                info.getUsers().toArray(new String[0]));

        when(query4.getResultList()).thenReturn(new ArrayList<Permission>());
        try {
            info = ejb.getUsersWithPermission("resource","permission");
            fail("Exception should occur");
        } catch (InvalidResourceException e) {
            assertEquals("Messages should be equal",
                    "Permission 'permission' for resource 'resource' could not be found.", e.getMessage());
        }
        
        when(query3.getResultList()).thenReturn(new ArrayList<Resource>());
        try {
            info = ejb.getUsersWithPermission("resource","permission");
            fail("Exception should occur");
        } catch (InvalidResourceException e) {
            assertEquals("Messages should be equal", "Resource with name 'resource' could not be found.", 
                    e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#getUserRoles(String)} method.
     * 
     * @throws IllegalRBACArgumentException on error
     */
    @Test
    public void testGetUserRoles() throws IllegalRBACArgumentException {
        RolesInfo info = ejb.getUserRoles("johndoe");
        assertEquals("Username should match", "johndoe", info.getUserID());
        assertArrayEquals("Role names should match", new String[] { "Role1", "Role2" },
                info.getRoleNames().toArray(new String[0]));

        when(query.getResultList()).thenReturn(new ArrayList<Role>());
        info = ejb.getUserRoles("johndoe");
        assertEquals("Username should match", "johndoe", info.getUserID());
        assertArrayEquals("Role names should match", new String[0], info.getRoleNames().toArray(new String[0]));
    }

    /**
     * Test {@link AuthenticationEJB#getUserRoles(String)} when providing invalid parameters.
     * 
     * @throws IllegalRBACArgumentException
     */
    @Test
    public void testGetUserRolesWithInvalidParameters() {
        try {
            ejb.getUserRoles(null);
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Username was not provided.", e.getMessage());
        }
        try {
            ejb.getUserRoles("");
            fail("Exception should occur");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Username was not provided.", e.getMessage());
        }
    }

    /**
     * Test the {@link AuthenticationEJB#getPublicKeyData()} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetPublicKey() throws Exception {
        byte[] data = ejb.getPublicKeyData();
        assertArrayEquals("Received key data should match generated one", new byte[] { 1, 2, 3, 4 }, data);

        when(signature.getPublicKey()).thenThrow(new RBACException(null, "Error"));
        try {
            ejb.getPublicKeyData();
            fail("Exception expected");
        } catch (RBACException e) {
            assertEquals("Exception expected", "Error", e.getMessage());
        }

        verify(signature, times(2)).getPublicKey();

    }
}
