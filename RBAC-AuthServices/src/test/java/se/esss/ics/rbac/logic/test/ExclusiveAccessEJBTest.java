/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.jaxb.ExclusiveInfo;
import se.esss.ics.rbac.logic.AuthenticationEJB;
import se.esss.ics.rbac.logic.AuthorizationEJB;
import se.esss.ics.rbac.logic.ExclusiveAccessEJB;
import se.esss.ics.rbac.logic.GeneralEJB;
import se.esss.ics.rbac.logic.datatypes.ExclusiveInfoWrapper;
import se.esss.ics.rbac.logic.evaluation.RuleEvaluator;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>ExclusiveAccessEJBTest</code> tests the methods or {@link ExclusiveAccessEJB}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class ExclusiveAccessEJBTest {

    private ExclusiveAccessEJB ejb;
    private EntityManager em;
    private Token token;
    private TypedQuery<Token> tokenQuery;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        ejb = new ExclusiveAccessEJB();

        token = new Token();
        token.setCreationDate(new Timestamp(System.currentTimeMillis() - 10000));
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 10000));
        token.setIp("192.168.1.1");
        token.setTokenId("id123");
        token.setUserId("johndoe");
        Role r = new Role();
        r.setName("Role");
        UserRole ur = new UserRole();
        ur.setUserId("johndoe");
        ur.setRole(r);
        token.setRole(new HashSet<>(Arrays.asList(new TokenRole(token, ur))));

        em = mock(EntityManager.class);
        tokenQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Token.findByTokenId", Token.class)).thenReturn(tokenQuery);
        when(tokenQuery.setParameter("tokenId", "id123")).thenReturn(tokenQuery);
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token));

        Resource resource = new Resource();
        resource.setName(ExclusiveAccessEJB.RESOURCE_NAME);
        Resource resource2 = new Resource();
        resource2.setName("resource");
        TypedQuery<Resource> resourceQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Resource.findByName", Resource.class)).thenReturn(resourceQuery);
        when(resourceQuery.setParameter("name", ExclusiveAccessEJB.RESOURCE_NAME)).thenReturn(resourceQuery);
        when(resourceQuery.getResultList()).thenReturn(Arrays.asList(resource));
        TypedQuery<Resource> resourceQuery2 = mock(TypedQuery.class);
        when(resourceQuery.setParameter("name", "resource")).thenReturn(resourceQuery2);
        when(resourceQuery2.getResultList()).thenReturn(Arrays.asList(resource2));

        Role role = new Role();
        role.setName("Role");
        TypedQuery<Role> roleQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Role.findValidByUserId", Role.class)).thenReturn(roleQuery);
        when(roleQuery.setParameter("userId", "johndoe")).thenReturn(roleQuery);
        when(roleQuery.getResultList()).thenReturn(Arrays.asList(role));

        Permission permission1 = new Permission();
        permission1.setName("p1");
        permission1.setResource(resource);
        permission1.setRole(new HashSet<>(Arrays.asList(role)));
        permission1.setExclusiveAccessAllowed(Boolean.TRUE);
        Permission permission3 = new Permission();
        permission3.setName("p3");
        permission3.setResource(resource);
        permission3.setRole(new HashSet<Role>());
        permission3.setExclusiveAccessAllowed(Boolean.TRUE);
        Permission permission4 = new Permission();
        permission4.setName(ExclusiveAccessEJB.RELEASE_EXCLUSIVE_ACCESS);
        permission4.setResource(resource);
        permission4.setRole(new HashSet<>(Arrays.asList(role)));
        permission4.setExclusiveAccessAllowed(Boolean.FALSE);

        TypedQuery<Permission> permissionQuery = mock(TypedQuery.class);
        when(em.createNamedQuery("Permission.findByNames", Permission.class)).thenReturn(permissionQuery);
        when(permissionQuery.setParameter(any(String.class), any(String[].class))).thenReturn(permissionQuery);
        when(permissionQuery.setParameter("resource", "resource")).thenReturn(permissionQuery);
        when(permissionQuery.getResultList()).thenReturn(Arrays.asList(permission1, permission3, permission4));

        TypedQuery<Permission> permissionQuery2 = mock(TypedQuery.class);
        when(em.createNamedQuery("Permission.findByName", Permission.class)).thenReturn(permissionQuery2);
        when(permissionQuery2.setParameter("name", "p1")).thenReturn(permissionQuery2);
        when(permissionQuery2.setParameter("resource", resource2)).thenReturn(permissionQuery2);
        when(permissionQuery2.getResultList()).thenReturn(Arrays.asList(permission1));

        TypedQuery<Permission> permissionQuery3 = mock(TypedQuery.class);
        when(permissionQuery2.setParameter("name", "p3")).thenReturn(permissionQuery3);
        when(permissionQuery3.setParameter("resource", resource2)).thenReturn(permissionQuery3);
        when(permissionQuery3.getResultList()).thenReturn(Arrays.asList(permission3));

        Field f = ExclusiveAccessEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(ejb, em);

        GeneralEJB general = new GeneralEJB();
        f = GeneralEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(general, em);
        f = ExclusiveAccessEJB.class.getDeclaredField("generalEJB");
        f.setAccessible(true);
        f.set(ejb, general);

        // Use real authorizationEJB instead of Mock object. Invisible/internal methods
        // are called which are difficult to mock. Above mocks should take care of the calls
        // to the authorisation EJB, we just need to set the entity manager and the general EJB
        AuthorizationEJB authorization = new AuthorizationEJB();
        RuleEvaluator re = mock(RuleEvaluator.class);
        when(re.evaluate(any(Rule.class), any(Token.class))).thenReturn(true);
        
        f = AuthorizationEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(authorization, em);
        
        f = AuthorizationEJB.class.getDeclaredField("generalEJB");
        f.setAccessible(true);
        f.set(authorization, general);
        
        f = AuthorizationEJB.class.getDeclaredField("ruleEvaluatorEJB");
        f.setAccessible(true);
        f.set(authorization, re);

        f = ExclusiveAccessEJB.class.getDeclaredField("authorizationEJB");
        f.setAccessible(true);
        f.set(ejb, authorization);
    }

    /**
     * Test {@link ExclusiveAccessEJB#requestExclusiveAccess(String, String, String, String, long)} by providing invalid
     * parameters.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRequestWithInvalidParameters() throws RBACException {
        // test with null parameters
        try {
            ejb.requestExclusiveAccess(null, "resource", "permission", 10000);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.requestExclusiveAccess("", "resource", "permission", 10000);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.requestExclusiveAccess("id123", null, "permission", 10000);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Resource name was not provided.", e.getMessage());
        }
        try {
            ejb.requestExclusiveAccess("id123", "", "permission", 10000);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Resource name was not provided.", e.getMessage());
        }
        try {
            ejb.requestExclusiveAccess("id123", "resource", null, 10000);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Permission name was not provided.", e.getMessage());
        }
        try {
            ejb.requestExclusiveAccess("id123", "resource", "", 10000);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Permission name was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#requestExclusiveAccess(String, String, String, String, long)} method.
     * 
     * @throws Exception on error
     */
    @Test
    public void testRequest() throws Exception {
        TypedQuery<Permission> query = em.createNamedQuery("Permission.findByNames", Permission.class);
        Permission prm = query.getResultList().get(0);
        when(query.getResultList()).thenReturn(Arrays.asList(prm));

        ExclusiveInfo info = ejb.requestExclusiveAccess("id123", "resource", "p1", 10000).getInfo();
        assertEquals("Resource name should match", "resource", info.getResourceName());
        assertEquals("Permission name should match", "p1", info.getPermissionName());
        assertTrue("Expiration time should be in the future",
                info.getExpirationTime() > System.currentTimeMillis() + 8000);

        setUp();
        query = em.createNamedQuery("Permission.findByNames", Permission.class);
        prm = query.getResultList().get(0);
        when(query.getResultList()).thenReturn(Arrays.asList(prm));
        // test with default duration
        info = ejb.requestExclusiveAccess("id123", "resource", "p1", 0).getInfo();
        assertEquals("Resource name should match", "resource", info.getResourceName());
        assertEquals("Permission name should match", "p1", info.getPermissionName());
        assertTrue("Expiration time should be about one hour in the future",
                info.getExpirationTime() > System.currentTimeMillis() + 3599000);

        Permission p = em.createNamedQuery("Permission.findByName", Permission.class).getResultList().get(0);
        // test if exclusive access has expired
        ExclusiveAccess access = new ExclusiveAccess();
        access.setPermission(p);
        access.setStartTime(new Timestamp(System.currentTimeMillis() - 10000));
        access.setEndTime(new Timestamp(System.currentTimeMillis() + 10000));
        access.setUserId("janedoe");
        access.setEndTime(new Timestamp(System.currentTimeMillis() - 1000));
        p.setExclusiveAccess(access);
        info = ejb.requestExclusiveAccess("id123", "resource", "p1", 10000).getInfo();
        assertEquals("Resource name should match", "resource", info.getResourceName());
        assertEquals("Permission name should match", "p1", info.getPermissionName());
        assertTrue("Expiration time should be in the future",
                info.getExpirationTime() > System.currentTimeMillis() + 8000);
    }

    /**
     * Test {@link ExclusiveAccessEJB#requestExclusiveAccess(String, String, String, String, long)} if the general
     * conditions are not met.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRequestConditionsNotMet() throws RBACException {
        // test if exclusive access is not allowed
        Permission p = em.createNamedQuery("Permission.findByName", Permission.class).getResultList().get(0);
        TypedQuery<Permission> query = em.createNamedQuery("Permission.findByNames", Permission.class);
        when(query.getResultList()).thenReturn(Arrays.asList(p));

        p.setExclusiveAccessAllowed(Boolean.FALSE);
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
            fail("Exception should occur.");
        } catch (InvalidResourceException e) {
            assertEquals("Exception expected",
                    "Exclusive access is not allowed for permission 'p1' for resource 'resource'.", e.getMessage());
        }
        p.setExclusiveAccessAllowed(Boolean.TRUE);

        // test if exclusive access is taken
        ExclusiveAccess access = new ExclusiveAccess();
        access.setPermission(p);
        access.setStartTime(new Timestamp(System.currentTimeMillis() - 10000));
        access.setEndTime(new Timestamp(System.currentTimeMillis() + 10000));
        access.setUserId("janedoe");
        p.setExclusiveAccess(access);
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
            fail("Exception should occur.");
        } catch (AuthAndAuthException e) {
            assertEquals(
                    "Exception expected",
                    "Exclusive access for permission 'p1' for resource 'resource' is owned by user '"
                            + access.getUserId() + "'.", e.getMessage());
        }

        // test if user does not have the requested permission
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p3", 10000);
            fail("Exception should occur.");
        } catch (AuthAndAuthException e) {
            assertEquals(
                    "Exception expected",
                    "Cannot request exclusive access for a permission 'p3' for resource 'resource'. You do not have this permission.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#requestExclusiveAccess(String, String, String, String, long)} with invalid
     * permission.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRequestWithInvalidPermission() throws RBACException {
        // test if there are more than one permission for the name
        Permission p = em.createNamedQuery("Permission.findByName", Permission.class).getResultList().get(0);
        TypedQuery<Permission> q = em.createNamedQuery("Permission.findByName", Permission.class);
        when(q.getResultList()).thenReturn(Arrays.asList(p, p, p));
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
            fail("Exception should occur.");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple permissions named 'p1' exist for the resource 'resource'.",
                    e.getMessage());
        }
        // test if there is no such permission
        when(q.getResultList()).thenReturn(new ArrayList<Permission>());
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
            fail("Exception should occur.");
        } catch (InvalidResourceException e) {
            assertEquals("Exception expected", "Permission 'p1' for resource 'resource' could not be found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#requestExclusiveAccess(String, String, String, String, long)} with invalid
     * resource.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRequestWithInvalidResource() throws RBACException {
        // test if resource doesn't exist
        TypedQuery<Resource> res = em.createNamedQuery("Resource.findByName", Resource.class).setParameter("name",
                "resource");
        when(res.getResultList()).thenReturn(new ArrayList<Resource>());
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
        } catch (InvalidResourceException e) {
            assertEquals("Exception expected", "Resource with name 'resource' could not be found.", e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#requestExclusiveAccess(String, String, String, String, long)} if the token is
     * fishy.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRequestWithIncorrectToken() throws RBACException {
        // test with no token
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token with ID '" + token.getTokenId() + "' could not be found.",
                    e.getMessage());
        }
        // test with multiple tokens
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#requestExclusiveAccess(String, String, String, String, long)} if the token has
     * expired.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRequestWithExpiredToken() throws RBACException {
        // test with expired token
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 1000));
        try {
            ejb.requestExclusiveAccess("id123", "resource", "p1", 10000);
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token owned by '" + token.getUserId() + "' has already expired.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#releaseExclusiveAccess(String, String, String, String)} when providing invalid
     * parameters.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testReleaseWithInvalidParameters() throws RBACException {
        // test with null parameters
        try {
            ejb.releaseExclusiveAccess(null, "resource", "permission");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.releaseExclusiveAccess("", "resource", "permission");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Token ID was not provided.", e.getMessage());
        }
        try {
            ejb.releaseExclusiveAccess("id123", null, "permission");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Resource name was not provided.", e.getMessage());
        }
        try {
            ejb.releaseExclusiveAccess("id123", "", "permission");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Resource name was not provided.", e.getMessage());
        }
        try {
            ejb.releaseExclusiveAccess("id123", "resource", null);
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Permission name was not provided.", e.getMessage());
        }
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "");
            fail("Exception should occur.");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Permission name was not provided.", e.getMessage());
        }
    }

    /**
     * Test {@link AuthenticationEJB#releaseExclusiveAccess(String, String, String, String)} method.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testRelease() throws RBACException {
        Permission p = em.createNamedQuery("Permission.findByName", Permission.class).getResultList().get(0);
        ExclusiveAccess access = new ExclusiveAccess();
        access.setPermission(p);
        access.setStartTime(new Timestamp(System.currentTimeMillis() - 10000));
        access.setEndTime(new Timestamp(System.currentTimeMillis() + 10000));
        access.setUserId("johndoe");
        p.setExclusiveAccess(access);
        // test normal
        TypedQuery<Permission> query = em.createNamedQuery("Permission.findByNames", Permission.class);
        Permission prm = query.getResultList().get(2);
        when(query.getResultList()).thenReturn(Arrays.asList(prm));
        ExclusiveInfoWrapper release = ejb.releaseExclusiveAccess("id123", "resource", "p1");
        assertEquals("User id should match", token.getUserId(), release.getUser());
        assertEquals("Permission name should match", "p1", release.getInfo().getPermissionName());
        assertEquals("Resource name should match", "resource", release.getInfo().getResourceName());
        assertTrue("Release should be successful", release.isReleased());

        // test if exclusive access has expired
        access.setEndTime(new Timestamp(System.currentTimeMillis() - 1000));
        release = ejb.releaseExclusiveAccess("id123", "resource", "p1");
        assertEquals("User id should match", token.getUserId(), release.getUser());
        assertEquals("Permission name should match", "p1", release.getInfo().getPermissionName());
        assertEquals("Resource name should match", "resource", release.getInfo().getResourceName());

        // test if exclusive info does not exist
        p.setExclusiveAccess(null);
        release = ejb.releaseExclusiveAccess("id123", "resource", "p1");
        assertEquals("User id should match", token.getUserId(), release.getUser());
        assertEquals("Permission name should match", "p1", release.getInfo().getPermissionName());
        assertEquals("Resource name should match", "resource", release.getInfo().getResourceName());
        assertFalse("Release should no be successful", release.isReleased());
    }

    /**
     * Test {@link ExclusiveAccessEJB#releaseExclusiveAccess(String, String, String, String)} when the exclusive access
     * is taken by someone else.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testReleaseConditionsNotMet() throws RBACException {
        Permission p = em.createNamedQuery("Permission.findByName", Permission.class).getResultList().get(0);
        ExclusiveAccess access = new ExclusiveAccess();
        access.setPermission(p);
        access.setStartTime(new Timestamp(System.currentTimeMillis() - 10000));
        access.setEndTime(new Timestamp(System.currentTimeMillis() + 10000));
        access.setUserId("johndoe");
        p.setExclusiveAccess(access);
        // test if exclusive access is taken by someone else and you have permission to do that
        p.setExclusiveAccess(access);
        access.setUserId("janedoe");
        TypedQuery<Permission> query = em.createNamedQuery("Permission.findByNames", Permission.class);
        Permission prm = query.getResultList().get(2);
        when(query.getResultList()).thenReturn(Arrays.asList(prm));
        ExclusiveInfoWrapper release = ejb.releaseExclusiveAccess("id123", "resource", "p1");
        assertEquals("User id should match", token.getUserId(), release.getUser());
        assertEquals("Permission name should match", "p1", release.getInfo().getPermissionName());
        assertEquals("Resource name should match", "resource", release.getInfo().getResourceName());

        // test if taken by someone else and you do not have permission to do that
        prm.setRole(new HashSet<Role>());
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
            fail("Exception should occur.");
        } catch (AuthAndAuthException e) {
            assertEquals("Exception expected", "Token user '" + token.getUserId()
                    + "' does not match the active exclusive access owner '" + access.getUserId() + "'.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#releaseExclusiveAccess(String, String, String, String)} when the token is fishy.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testReleaseWithIncorrectToken() throws RBACException {
        // test with no token
        when(tokenQuery.getResultList()).thenReturn(new ArrayList<Token>());
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token with ID '" + token.getTokenId() + "' could not be found.",
                    e.getMessage());
        }
        // test with multiple tokens
        when(tokenQuery.getResultList()).thenReturn(Arrays.asList(token, token));
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple tokens with ID '" + token.getTokenId() + "' found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#releaseExclusiveAccess(String, String, String, String)} if the token has already
     * expired.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testReleaseWithExpiredToken() throws RBACException {
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() - 1000));
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
        } catch (TokenInvalidException e) {
            assertEquals("Exception expected", "Token owned by '" + token.getUserId() + "' has already expired.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#releaseExclusiveAccess(String, String, String, String)} if the permission does not
     * exist or the permission integrity is broken.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testReleaseWithInvalidPermission() throws RBACException {
        Permission p = em.createNamedQuery("Permission.findByName", Permission.class).getResultList().get(0);
        // test if there are more than one permission for the name
        TypedQuery<Permission> q = em.createNamedQuery("Permission.findByName", Permission.class);
        when(q.getResultList()).thenReturn(Arrays.asList(p, p, p));
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
            fail("Exception should occur.");
        } catch (DataConsistencyException e) {
            assertEquals("Exception expected", "Multiple permissions named 'p1' exist for the resource 'resource'.",
                    e.getMessage());
        }
        // test if there is no such permission
        when(q.getResultList()).thenReturn(new ArrayList<Permission>());
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
            fail("Exception should occur.");
        } catch (InvalidResourceException e) {
            assertEquals("Exception expected", "Permission 'p1' for resource 'resource' could not be found.",
                    e.getMessage());
        }
    }

    /**
     * Test {@link ExclusiveAccessEJB#releaseExclusiveAccess(String, String, String, String)} if the resource does not
     * exist.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testReleaseWithInvalidResource() throws RBACException {
        // test if resource doesn't exist
        TypedQuery<Resource> res = em.createNamedQuery("Resource.findByName", Resource.class).setParameter("name",
                "resource");
        when(res.getResultList()).thenReturn(new ArrayList<Resource>());
        try {
            ejb.releaseExclusiveAccess("id123", "resource", "p1");
        } catch (InvalidResourceException e) {
            assertEquals("Exception expected", "Resource with name 'resource' could not be found.", e.getMessage());
        }
    }
}
