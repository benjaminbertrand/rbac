/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.restservices.RequestUtilities;

/**
 * 
 * <code>RequestUtilitiesTest</code> tests the methods of the {@link RequestUtilities}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class RequestUtilitiesTest {

    /**
     * Test if exclusive duration is extracted properly.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetExclusiveDuration() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_EXCLUSIVE_DURATION)).thenReturn("60000");

        long duration = RequestUtilities.getExclusiveDuration(request);
        assertEquals("The duration of exclusive access should be 60000 milliseconds", 60000, duration);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_EXCLUSIVE_DURATION);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_EXCLUSIVE_DURATION)).thenReturn("10000");
        duration = RequestUtilities.getExclusiveDuration(request);
        assertEquals(
                "The duration of exclusive access should be 0 milliseconds - undefined. Values smaller than 60000 are not allowed.",
                0, duration);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_EXCLUSIVE_DURATION);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_EXCLUSIVE_DURATION)).thenReturn(null);

        duration = RequestUtilities.getExclusiveDuration(request);
        assertEquals("The duration of exclusive access should be 0 milliseconds - undefined.", 0, duration);
    }

    /**
     * Test if the IP of the caller is extracted properly.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetIP() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn("192.168.1.1");
        String ip = RequestUtilities.getIP(request);

        assertEquals("The ip should be 192.168.1.1", "192.168.1.1", ip);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_IP);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_IP)).thenReturn(null);
        when(request.getRemoteAddr()).thenReturn("192.168.1.2");

        ip = RequestUtilities.getIP(request);
        assertEquals("The ip should be 192.168.1.2", "192.168.1.2", ip);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_IP);
        verify(request, times(1)).getRemoteAddr();
    }

    /**
     * Test is roles are extracted properly.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetRoleNames() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_ROLES)).thenReturn("Role 1");
        String[] roles = RequestUtilities.getRoleNames(request);
        assertArrayEquals("There should be exaclty one role: Role 1", new String[] { "Role 1" }, roles);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_ROLES);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_ROLES)).thenReturn("Role 1; Role 2");
        roles = RequestUtilities.getRoleNames(request);
        assertArrayEquals("There should be exaclty one role: Role 1; Role 2", new String[] { "Role 1; Role 2" }, roles);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_ROLES);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_ROLES)).thenReturn("Role 1,Role 2,Role 3");
        roles = RequestUtilities.getRoleNames(request);
        assertArrayEquals("There should be exaclty three roles", new String[] { "Role 1", "Role 2", "Role 3" }, roles);
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_ROLES);

    }

    /**
     * Test if login data is extracted properly.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetLoginData() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION)).thenReturn(
                HttpServletRequest.BASIC_AUTH + " " + getUsernameAndPassword());

        char[][] loginData = RequestUtilities.getLoginData(request);
        assertEquals("Login arrays should have length 2", 2, loginData.length);
        assertEquals("The username should be \"username\"", "username", new String(loginData[0]));
        assertEquals("The password should be \"password\"", "password", new String(loginData[1]));
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION)).thenReturn(getUsernameAndPassword());
        try {
            RequestUtilities.getLoginData(request);
            fail("An exception should be thrown if BASIC tag is not provided");
        } catch (RBACException e) {
            assertEquals("Exception expected", "Invalid authentication type. '" + HttpServletRequest.BASIC_AUTH
                    + "' expected, but received '" + getUsernameAndPassword() + "'.", e.getMessage());
        }
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION);

        request = mock(HttpServletRequest.class);
        when(request.getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION)).thenReturn(null);
        try {
            RequestUtilities.getLoginData(request);
            fail("An exception should be thrown if authorization data is missing");
        } catch (RBACException e) {
            assertEquals("Exception expected", "Authorisation header was not provided.", e.getMessage());
        }
        verify(request, times(1)).getHeader(RequestUtilities.HEADER_NAME_AUTHORISATION);
    }

    private static String getUsernameAndPassword() throws CharacterCodingException {
        Charset utf8Charset = Charset.forName("UTF-8");
        byte[] usernameData = "username".getBytes(utf8Charset);
        ByteBuffer buffer = utf8Charset.newEncoder().encode(CharBuffer.wrap("password"));
        byte[] passwordData = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
        byte[] rawAuthData = new byte[usernameData.length + passwordData.length + 1];
        System.arraycopy(usernameData, 0, rawAuthData, 0, usernameData.length);
        rawAuthData[usernameData.length] = (byte) ':';
        System.arraycopy(passwordData, 0, rawAuthData, usernameData.length + 1, passwordData.length);
        return DatatypeConverter.printBase64Binary(rawAuthData);
    }
}
