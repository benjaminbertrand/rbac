/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.logic.datatypes.PermissionsInfoWrapper;
import se.esss.ics.rbac.logic.evaluation.EvaluationException;
import se.esss.ics.rbac.logic.evaluation.RuleEvaluator;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;

/**
 * 
 * <code>AuthorizationEJB</code> provides the logic for checking user permissions.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Stateless(description = "The bean provides the logic necessary to check user's permissions.")
public class AuthorizationEJB implements Serializable {

    private static final long serialVersionUID = -2331231249157330153L;

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;
    @EJB
    private GeneralEJB generalEJB;
    @EJB
    private RuleEvaluator ruleEvaluatorEJB;

    /**
     * Method checks, if a token with provided ID exists. It checks the available permissions for the resource against
     * the set of permissions provided as a parameter. A user is deemed to have a permission only if the permission
     * exists and if the permission is granted to the roles stored in the token. Permission can only be granted if
     * another user does not have exclusive access for that permission. Once all provided permissions are checked they
     * are mapped to booleans (where value <code>true</code> denotes, that user has the permission) and the checks are
     * returned in a {@link PermissionsInfo} wrapper.
     * 
     * @param tokenID id of the token containing available roles
     * @param resourceName name of the resource associated with permissions
     * @param permissionNames array of permission names to be checked
     * 
     * @return permissions information containing permission mapping. Permission is mapped to <code>true</code> if it's
     *         available, or <code>false</code> otherwise
     * 
     * @throws IllegalRBACArgumentException if any of the required parameters is null or empty
     * @throws TokenInvalidException if the token is invalid, or if the token could not be found
     * @throws InvalidResourceException if the resource or permission does not exist
     * @throws DataConsistencyException if multiple tokens or permissions exist or one of the rules could not be
     *          evaluated
     */
    public PermissionsInfoWrapper hasPermissions(String tokenID, String resourceName, String[] permissionNames)
            throws IllegalRBACArgumentException, DataConsistencyException, InvalidResourceException,
            TokenInvalidException {
        GeneralEJB.verifyInputParameters(tokenID, resourceName, permissionNames);
        Token token = generalEJB.fetchToken(tokenID);
        GeneralEJB.validateToken(token);
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + generalEJB.getExpirationPeriod()));
        return hasPermissionsForValidatedParameters(token, resourceName, permissionNames);
    }

    /**
     * Checks if the user with specified token and roles has specific permissions on the provided resource. Resource is
     * fetched from the database by its name and then passed as a parameter to
     * {@link #hasPermissionsForValidatedParameters(Token, Resource, String[], boolean)}. Permission checks are returned
     * wrapped in {@link PermissionsInfoWrapper}. Method assumes that token has already been validated.
     * 
     * @param token already validated token used for authorisation
     * @param resourceName the name of the resource associated with permissions
     * @param permissionNames names of permissions to be checked
     * @return permissions information containing permission mapping. Permission is mapped to <code>true</code> if it's
     *         available, or <code>false</code> otherwise.
     * 
     * @throws InvalidResourceException if the resource with provided name could not be found, or if one of the
     *             permissions could not be found
     * @throws DataConsistencyException if more permissions were found than requested or one of the rules could
     *              not be evaluated
     */
    public PermissionsInfoWrapper hasPermissionsForValidatedParameters(Token token, String resourceName,
            String[] permissionNames) throws InvalidResourceException, DataConsistencyException {
        List<Resource> resources = entityManager.createNamedQuery("Resource.findByName", Resource.class)
                .setParameter("name", resourceName).getResultList();
        if (resources.isEmpty()) {
            throw new InvalidResourceException(token.getUserId(), resourceName, Messages.getString(
                    Messages.NO_RESOURCE, resourceName));
        }

        Resource resource = resources.get(0);
        return hasPermissionsForValidatedParameters(token, resource, permissionNames, false);
    }

    /**
     * Checks if the user with specified token and roles has specific permissions on the provided resource. Resource is
     * fetched from the database by its name and then passed as a parameter to
     * {@link #hasPermissionsForValidatedParameters(Token, Resource, Collection, boolean)}. Permission checks are
     * returned wrapped in {@link PermissionsInfoWrapper}. Method assumes that token and resource parameters have
     * already been validated, the permissionNames will be validated.
     * 
     * @param token already validated token used for authorisation
     * @param resource the resource that owns the permission
     * @param permissionNames names of permissions to be checked
     * @param ignoreExclusiveAccess true if exclusive access should not be checked or false otherwise
     * 
     * @return permissions information containing permission mapping. Permission is mapped to <code>true</code> if it is
     *         available, or <code>false</code> otherwise.
     * 
     * @throws InvalidResourceException if the resource with provided name could not be found, or if one of the
     *             permissions could not be found
     * @throws DataConsistencyException if more permissions were found than requested or the rule could not be
     *              evaluated 
     */
    public PermissionsInfoWrapper hasPermissionsForValidatedParameters(Token token, Resource resource,
            String[] permissionNames, boolean ignoreExclusiveAccess) throws InvalidResourceException,
            DataConsistencyException {
        List<Permission> permissions = entityManager.createNamedQuery("Permission.findByNames", Permission.class)
                .setParameter("names", Arrays.asList(permissionNames)).setParameter("resource", resource)
                .getResultList();

        if (permissions.size() > permissionNames.length) {
            throw new DataConsistencyException(token.getUserId(), Messages.getString(Messages.MULTIPLE_PERMISSIONS,
                    Arrays.toString(permissionNames), resource.getName()));
        } else if (permissions.size() < permissionNames.length) {
            throw new InvalidResourceException(token.getUserId(), "Permission", Messages.getString(
                    Messages.PERMISSIONS_NOT_FOUND, resource.getName()));
        }

        return hasPermissionsForValidatedParameters(token, resource, permissions, ignoreExclusiveAccess);
    }

    /**
     * Checks if the user with specified token and roles has specific permissions on the provided resource. Permission
     * checks are returned wrapped in {@link PermissionsInfoWrapper}.
     * 
     * Method assumes that all input parameters have already been validated.
     * 
     * @param token already validated token used for authorisation
     * @param resource the resource associated with permissions
     * @param resourcePermissions names of permissions to be checked
     * @param ignoreExclusiveAccess if true exclusive access will not be checked
     * @return permissions information containing permission mapping. Permission is mapped to <code>true</code> if it's
     *         available, or <code>false</code> otherwise.
     * 
     * @throws DataConsistencyException if there was an error evaluating the rule on one of the permissions
     */
    private PermissionsInfoWrapper hasPermissionsForValidatedParameters(Token token, Resource resource,
            Collection<Permission> resourcePermissions, boolean ignoreExclusiveAccess) 
                    throws DataConsistencyException {

        Map<String, Boolean> permissionMapping = new HashMap<>();
        for (Permission permission : resourcePermissions) {
            permissionMapping.put(permission.getName(),
                    Boolean.valueOf(isPermissionGranted(permission, token, ignoreExclusiveAccess)));
        }

        return new PermissionsInfoWrapper(new PermissionsInfo(resource.getName(), permissionMapping), 
                token.getUserId());
    }

    /**
     * Checks if the permission is granted. Permission is granted if exclusive access owned by someone else does not
     * exist (the answer depends also on the <code>ignoreExclusiveAccess</code> parameter), the permission is 
     * included in one of the roles owned by the token and the permission's rule evaluates to true.
     * 
     * @param permission the permission that is being checked
     * @param token the token which is being authorised
     * @param ignoreExclusiveAccess if true exclusive access will not be checked
     * @return true if permission is granted or false otherwise
     * @throws DataConsistencyException if the rule could not be evaluated due to an error in the rule definition
     */
    private boolean isPermissionGranted(Permission permission, Token token, boolean ignoreExclusiveAccess) 
            throws DataConsistencyException {
        ExclusiveAccess exclusiveAccess = permission.getExclusiveAccess();
        String username = token.getUserId();
        // If active exclusive access is given to somebody else, restrict permission
        if (!ignoreExclusiveAccess && exclusiveAccess != null && !exclusiveAccess.getUserId().equals(username)
                && exclusiveAccess.getEndTime().after(new Date())) {
            return false;
        }
        Set<TokenRole> tokenRoles = token.getRole();
        // Allow only permissions tied to roles available with this token.
        try {
            for (Role role : permission.getRole()) {
                for (TokenRole tr : tokenRoles) {
                    if (tr.getRole().getRole().getId() == role.getId()) {
                        return ruleEvaluatorEJB.evaluate(permission.getRule(),token);
                    }
                }
            }
            return false;
        } catch (EvaluationException e) {
            throw new DataConsistencyException(username, e.getMessage(), e);
        }
    }
}
