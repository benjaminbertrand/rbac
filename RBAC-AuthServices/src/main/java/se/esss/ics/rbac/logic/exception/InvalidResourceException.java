/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.exception;

/**
 * 
 * <code>InvalidResourceException</code> represents an exception that happens due when a specific resource could not be
 * loaded.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class InvalidResourceException extends RBACException {

    private static final long serialVersionUID = 1L;
    private final String resource;

    /**
     * Constructs a new exception.
     * 
     * @param username the name of the user who triggered the exception
     * @param resource the resource that could not be found
     * @param message the message of the exception
     */
    public InvalidResourceException(String username, String resource, String message) {
        super(username, message);
        this.resource = resource;
    }

    /**
     * @return the tokenID that triggered this exception
     */
    public final String getResource() {
        return resource;
    }
}
