/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.restservices;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.ResourcesInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.jaxb.UsersInfo;
import se.esss.ics.rbac.logic.AuthenticationEJB;
import se.esss.ics.rbac.logic.AuthorizationEJB;
import se.esss.ics.rbac.logic.ExclusiveAccessEJB;
import se.esss.ics.rbac.logic.GeneralEJB;
import se.esss.ics.rbac.logic.ManifestEJB;
import se.esss.ics.rbac.logic.Messages;
import se.esss.ics.rbac.logic.PVAccessEJB;
import se.esss.ics.rbac.logic.RBACLogger;
import se.esss.ics.rbac.logic.datatypes.ExclusiveInfoWrapper;
import se.esss.ics.rbac.logic.datatypes.PermissionsInfoWrapper;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.RBACRuntimeException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.RBACLog.Action;
import se.esss.ics.rbac.datamodel.Role;

/**
 * <code>AuthResources</code> exposes ReST service methods for user authentication and authorisation.
 *
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 *
 */
@Stateless
@Path("/auth")
public class RBACResources implements Serializable {

    private static final long serialVersionUID = -7131042319760646898L;

    private static final String EMPTY_STRING = "";
    private static final String CHAR_ENCODING = "UTF-8";

    @EJB
    private RBACLogger rbacLogger;
    @EJB
    private AuthenticationEJB authenticationEJB;
    @EJB
    private AuthorizationEJB authorizationEJB;
    @EJB
    private ExclusiveAccessEJB exclusiveAccessEJB;
    @EJB
    private GeneralEJB generalEJB;
    @EJB
    private PVAccessEJB pvAccessEJB;
    @EJB
    private ManifestEJB manifestEJB;

    /**
     * Returns the build version number of the services. Version number is the build version declared in the manifest
     * file of the deployed web service's archive package. If the version can be read from the manifest it is returned
     * as plain text in a response with status code 200 (OK). If there is an error while reading the build version a
     * server error response is returned, containing more information about the problem.
     *
     * @return response containing version number of the services.
     */
    @GET
    @Path("/version")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getVersion() {
        try {
            return Response.ok().entity(manifestEJB.getVersion()).build();
        } catch (IOException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Returns the encoded public key that can be used to verify RBAC signature. The key should be decoded before it can
     * be used.
     *
     * @return response containing the encoded public key byte array
     */
    @GET
    @Path("/publickey")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getPublicKey() {
        try {
            return Response.ok().entity(generalEJB.getPublicKeyData()).build();
        } catch (RBACException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * Retrieves all users who have the specified role.
     * <p>
     * If the role exists and user retrieval is successful the users are returned as XML in a response with status code
     * 200 (OK). If there is an error while retrieving users, an error response with status code 400 (BAD REQUEST)
     * or 404 (NOT_FOUND) is returned, containing more information about the problem.
     * </p>
     *
     * @param role the name of the role for which owners are requested
     * @param request the HTTP request that triggered this call
     *
     * @return response containing all the user informations or a BAD_REQUEST or NOT_FOUND in case of an error
     */
    @GET
    @Path("/{role}/users")
    @Produces(MediaType.APPLICATION_XML)
    public Response getUsersWithRole(@PathParam("role") String role, @Context HttpServletRequest request) {
        try {
            UsersInfo users = generalEJB.getUsersWithRole(role);
            return Response.ok().entity(users).build();
        } catch (Exception e) {
            return handleException(e, Action.ROLES, null, request.getRemoteAddr());
        }
    }

    /**
     * Retrieves all users who have the specified permission.
     * <p>
     * If the permission exists and user retrieval is successful the users are returned as XML in a response with status
     * code 200 (OK). If there is an error while retrieving users, an error response with status code 400 (BAD REQUEST)
     * or 404 (NOT_FOUND) is returned, containing more information about the problem.
     * </p>
     *
     * @param resource the name of the resource that owns the permission
     * @param permission the name of permission for which owners are requested
     * @param request the HTTP request that triggered this call
     *
     * @return response containing all the user informations or a BAD_REQUEST or NOT_FOUND in case of an error
     */
    @GET
    @Path("/{resource}/{permission}/users")
    @Produces(MediaType.APPLICATION_XML)
    public Response getUsersWithPermission(@PathParam("resource") String resource,
            @PathParam("permission") String permission, @Context HttpServletRequest request) {
        try {
            UsersInfo users = generalEJB.getUsersWithPermission(resource, permission);
            return Response.ok().entity(users).build();
        } catch (Exception e) {
            return handleException(e, Action.ROLES, null, request.getRemoteAddr());
        }
    }

    /**
     * Retrieves all {@link Role}s available to the specified user. Authentication information is not checked, so this
     * method can be used to retrieve the user's roles before login, to allow the user to select a preferred role.
     * <p>
     * If the user exists and role retrieval is successful the roles are returned as XML in a response with status code
     * 200 (OK). If there is an error while retrieving roles, an error response with status code 400 (BAD REQUEST) is
     * returned, containing more information about the problem.
     * </p>
     *
     * @param username for which the roles should be returned
     * @param request the HTTP request that triggered this call
     *
     * @return response containing all the roles of the user or a BAD_REQUEST in case of an error
     */
    @GET
    @Path("/{username}/role")
    @Produces(MediaType.APPLICATION_XML)
    public Response getRoles(@PathParam("username") String username, @Context HttpServletRequest request) {
        try {
            RolesInfo roles = generalEJB.getUserRoles(username);
            return Response.ok().entity(roles).build();
        } catch (Exception e) {
            return handleException(e, Action.ROLES, null, request.getRemoteAddr());
        }
    }

    /**
     * Returns all the roles names currently available in the system.
     *
     * @param request the HTTP request that triggered this call
     *
     * @return response containing all the roles in the system
     */
    @GET
    @Path("/roles")
    @Produces(MediaType.APPLICATION_XML)
    public Response getRoles(@Context HttpServletRequest request) {
        try {
            RolesInfo roles = generalEJB.getRoles();
            return Response.ok().entity(roles).build();
        } catch (Exception e) {
            return handleException(e, Action.ROLES, null, request.getRemoteAddr());
        }
    }

    /**
     * Returns all the resource names currently available in the system.
     *
     * @param request the HTTP request that triggered this call
     *
     * @return response containing all the resources in the system
     */
    @GET
    @Path("/resources")
    @Produces(MediaType.APPLICATION_XML)
    public Response getResources(@Context HttpServletRequest request) {
        try {
            ResourcesInfo roles = generalEJB.getResources();
            return Response.ok().entity(roles).build();
        } catch (Exception e) {
            return handleException(e, Action.RESOURCES, null, request.getRemoteAddr());
        }
    }

    /**
     * Returns all permissions for the given resource currently available in the system.
     *
     * @param resourceName the name of the resource for which permissions are requested
     * @param request the HTTP request that triggered this call
     *
     * @return response containing all permissions in the system
     */
    @GET
    @Path("/{resourceName}/permissions")
    @Produces(MediaType.APPLICATION_XML)
    public Response getPermissions(@PathParam("resourceName") String resourceName,
            @Context HttpServletRequest request) {
        try {
            PermissionsInfo roles = generalEJB.getPermissions(resourceName);
            return Response.ok().entity(roles).build();
        } catch (Exception e) {
            return handleException(e, Action.PERMISSIONS, null, request.getRemoteAddr());
        }
    }

    /**
     * Logs the caller in, creates a token and returns it as XML. Caller's login data is retrieved from the
     * authorisation header of the request, which is expected to contain the basic authentication parameters.
     * <p>
     * IP and user's role data may also be provided using custom HTTP headers. Caller's IP should be stored in the
     * <code>RBACAddress</code> header, while preferred role names should be in the <code>RBACRoles</code> header, using
     * <code>','</code> as delimiter.
     * </p>
     * <p>
     * On successful login a token is created and token data is returned as XML in a response with status 201 (CREATED).
     * If authentication information is missing 403 (FORBIDDEN) is returned. If any of the required information is
     * missing 400 (BAD REQUEST) is returned, or if login could not be completed due to directory service error 500
     * (INTERNAL SERVER ERROR) is returned. All exceptional cases provide additional information about the error.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     * @see RequestUtilities#getLoginData(HttpServletRequest)
     * @see RequestUtilities#getRoleNames(HttpServletRequest)
     *
     * @param request POST request containing login information in appropriate HTTP headers
     *
     * @return token information as XML if authentication was successful, a BAD_REQUEST explaining the reason for
     *         failure if unsuccessful
     */
    @POST
    @Path("/token")
    @Produces(MediaType.APPLICATION_XML)
    public Response login(@Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        char[][] loginData = new char[2][0];
        try {
            loginData = RequestUtilities.getLoginData(request);
            ip = RequestUtilities.getIP(request);
            String[] roleNames = RequestUtilities.getRoleNames(request);

            TokenInfo tokenInfo = authenticationEJB.login(loginData[0], loginData[1], ip, roleNames);
            rbacLogger.info(tokenInfo.getUserID(), ip, Action.LOGIN,
                    Messages.getString(Messages.LOGGED_IN, tokenInfo.getUserID()));
            return Response.status(Status.CREATED).entity(tokenInfo).build();
        } catch (AuthAndAuthException e) {
            return handleException(e, Action.LOGIN, null, ip);
        } catch (Exception e) {
            return handleException(e, Action.LOGIN, new String(loginData[0]), ip);
        }
    }

    /**
     * Returns a previously generated token as XML. Caller's credentials are not verified, only the token ID is.
     * <p>
     * On successful retrieval token data is returned as XML in a response with status 200 (OK). If any of the required
     * information is missing 400 (BAD REQUEST) is returned, if a valid token with specified ID does not exist 410
     * (GONE) is returned. In any other case 500 (INTERNAL SERVER ERROR) is returned. In all exceptional cases more
     * information about the problem is provided.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the token to be retrieved
     * @param request GET request
     *
     * @return token information as XML if retrieval was successful, or BAD REQUEST, GONE, or INTERNAL SERVER ERROR
     *         explaining the reason for failure if unsuccessful or exception message in case of unexpected behaviour
     */
    @GET
    @Path("/token/{tokenID}")
    @Produces(MediaType.APPLICATION_XML)
    public Response getToken(@PathParam("tokenID") String tokenID, @Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        try {
            ip = RequestUtilities.getIP(request);
            TokenInfo tokenInfo = authenticationEJB.getToken(tokenID);
            rbacLogger.info(tokenInfo.getUserID(), ip, Action.TOKEN,
                    Messages.getString(Messages.TOKEN_RENEWED, tokenInfo.getUserID()));
            return Response.ok().entity(tokenInfo).build();
        } catch (Exception e) {
            return handleException(e, Action.TOKEN, null, ip);
        }
    }

    /**
     * Returns OK if the token is still valid or one of the error codes otherwise. This is a convenience method for
     * clients to quickly validate the token, without making any changes to the token or database (no renewal, no LDAP
     * queries etc.).
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the token to be validated
     * @param request the request that was used to make this call
     *
     * @return OK response if token is valid or GONE if not valid
     */
    @GET
    @Path("/token/{tokenID}/isvalid")
    @Produces(MediaType.TEXT_PLAIN)
    public Response isTokenValid(@PathParam("tokenID") String tokenID, @Context HttpServletRequest request) {
        String ip = EMPTY_STRING;
        try {
            ip = RequestUtilities.getIP(request);
            authenticationEJB.isTokenValid(tokenID);
            return Response.ok().build();
        } catch (Exception e) {
            // do not log validation, because there will be too much of it
            return handleException(e, null, null, ip);
        }
    }

    /**
     * Logs the caller out by deleting the token with the provided ID. Caller's credentials are not verified, only the
     * token ID is. On logout the token with the specified ID is removed from the server preventing any further actions
     * using that token.
     * <p>
     * On successful logout a response with status code 204 (NO CONTENT) is returned. If the token could not be found
     * anymore 200 (OK) is returned. If there is an error while logging out an error response with status code 500
     * (INTERNAL SERVER ERROR) is returned, containing more information about the problem.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the token to be removed
     * @param request DELETE request
     *
     * @return response with status code 200 was successful, or one of the appropriate error responses if the log out
     *         was no successful
     */
    @DELETE
    @Path("/token/{tokenID}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response logout(@PathParam("tokenID") String tokenID, @Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        try {
            ip = RequestUtilities.getIP(request);
            TokenInfo info = authenticationEJB.logout(tokenID);
            if (info == null) {
                rbacLogger.warn(null, ip, Action.LOGOUT, Messages.getString(Messages.NO_TOKEN, tokenID));
                return Response.ok().build();
            } else {
                rbacLogger.info(info.getUserID(), ip, Action.LOGOUT,
                        Messages.getString(Messages.LOGGED_OUT, info.getUserID()));
                return Response.noContent().build();
            }
        } catch (Exception e) {
            return handleException(e, Action.LOGOUT, null, ip);
        }
    }

    /**
     * Renews the token by extending its validity for the default expiration duration. Caller's credentials are not
     * verified, only the token ID is. Token expiration time is always extended for the default duration.
     * <p>
     * On successful renewal token data is returned as XML in a response with status 200 (OK). If the token does not
     * exist 410 (GONE) is returned, if there was an error in communication with the directory service 500 (INTERNAL
     * SERVER ERROR) is returned. All responses contain additional information explaining the reason for failure.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the token to be renewed
     * @param request GET request
     *
     * @return token information as XML if renewal was successful, or an appropriate error response explaining the
     *         reason for failure if unsuccessful or exception message in case of unexpected behaviour
     */
    @POST
    @Path("/token/{tokenID}/renew")
    @Produces(MediaType.APPLICATION_XML)
    public Response renewToken(@PathParam("tokenID") String tokenID, @Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        try {
            ip = RequestUtilities.getIP(request);
            TokenInfo tokenInfo = authenticationEJB.renewToken(tokenID);
            rbacLogger.info(tokenInfo.getUserID(), ip, Action.RENEW,
                    Messages.getString(Messages.TOKEN_RENEWED, tokenInfo.getUserID()));
            return Response.ok().entity(tokenInfo).build();
        } catch (Exception e) {
            return handleException(e, Action.RENEW, null, ip);
        }

    }

    /**
     * Checks whether the caller has the specified permissions. Caller's credentials are not verified, only the token ID
     * is. Permissions should be specified by their unique names and should use <code>','</code> as a delimiter. Because
     * they are bound to specific resource, resource name should also be provided.
     * <p>
     * On successful check an XML containing grant or deny response for each permission. Permission checks are returned
     * as a response with status 200 (OK). If any of the parameters is missing 400 (BAD REQUEST) is returned, if token
     * is invalid 410 (GONE) is returned, if any of the permissions or the resource is unknown 404 (NOT FOUND) is
     * returned.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the user's token
     * @param resource name of the resource for which permissions should be checked
     * @param permissions permission names delimited by <code>','</code>
     * @param request <code>GET</code> request
     *
     * @return permission check values as XML or appropriate error response
     */
    @GET
    @Path("/token/{tokenID}/{resource}/{permissions}")
    @Produces(MediaType.APPLICATION_XML)
    public Response hasPermissions(@PathParam("tokenID") String tokenID, @PathParam("resource") String resource,
            @PathParam("permissions") String permissions, @Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        try {
            ip = RequestUtilities.getIP(request);
            String[] permissionNames = URLDecoder.decode(permissions,CHAR_ENCODING).split("[,]");
            PermissionsInfoWrapper permissionsInfo = authorizationEJB
                    .hasPermissions(tokenID, resource, permissionNames);
            printAuthorisedLogMessage(permissionsInfo, permissionNames, resource, ip);
            return Response.ok().entity(permissionsInfo.getInfo()).build();
        } catch (Exception e) {
            return handleException(e, Action.AUTHORIZE, null, ip);
        }
    }

    /**
     * Requests exclusive access on specified permission for the user identified by the provided token. Caller's
     * credentials are not verified, only the token ID is. Permission and resource should be specified by their unique
     * names.
     * <p>
     * On success and XML containing exclusive access information is returned. It holds the permission name and access
     * expiration time in milliseconds. Response indicating success has status 201 (CREATED). If there is an error while
     * granting exclusive access an error response is returned. If token ID is missing 400 (BAD REQUEST) is returned, if
     * token is invalid 410 (GONE) is returned, if user does not have permission to request the exclusive access 403
     * (FORBIDDEN) is returned, if the permission or resource do not exist 404 (NOT FOUND) is returned. In any other
     * case 500 (INTERNAL SERVER ERROR) is returned.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the user's token
     * @param resource name of the resource for which exclusive access should be issued
     * @param permission name of the permission for which exclusive access should be issued
     * @param request POST request containing access expiration duration in seconds
     *
     * @return exclusive access information as XML or appropriate error response
     */
    @POST
    @Path("/token/{tokenID}/{resource}/{permission}/exclusive")
    @Produces(MediaType.APPLICATION_XML)
    public Response requestExclusiveAccess(@PathParam("tokenID") String tokenID,
            @PathParam("resource") String resource, @PathParam("permission") String permission,
            @Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        try {
            ip = RequestUtilities.getIP(request);
            long duration = RequestUtilities.getExclusiveDuration(request);
            ExclusiveInfoWrapper exclusiveInfo = exclusiveAccessEJB.requestExclusiveAccess(tokenID, resource,
                    permission, duration);
            rbacLogger.info(exclusiveInfo.getUser(), ip, Action.EXCLUSIVE,
                    Messages.getString(Messages.EXCLUSIVE_ACCESS_REQUEST, permission, resource));
            return Response.status(Status.CREATED).entity(exclusiveInfo.getInfo()).build();
        } catch (Exception e) {
            return handleException(e, Action.EXCLUSIVE, null, ip);
        }
    }

    /**
     * Releases exclusive access on the specified permission. Caller's credentials are not verified, only the token ID
     * is.
     * <p>
     * On successful removal a response with status code 204 (NO CONTENT) is returned, if the exclusive access did not
     * exist 200 (OK) is returned. If one of the parameters is missing, 400 (BAD REQUEST) is returned, if the token is
     * invalid 410 (GONE) is returned, if the token does not have the required permission to release the exclusive
     * access 403 (FORBIDDEN) is returned, if the permission or resource do not exist 404 (NOT FOUND) is returned. In
     * any other error case 500 (INTERNAL SERVER ERROR) is returned.
     * </p>
     *
     * @see RequestUtilities#getIP(HttpServletRequest)
     *
     * @param tokenID unique ID of the token
     * @param resource name of the resource for which exclusive access has been issued
     * @param permission name of the permission for which exclusive access has been issued
     * @param request DELETE request
     *
     * @return response with status code 204, if exclusive access for permission no longer exists after the execution of
     *         this method or appropriate error response
     */
    @DELETE
    @Path("/token/{tokenID}/{resource}/{permission}/exclusive")
    @Produces(MediaType.APPLICATION_XML)
    public Response releaseExclusiveAccess(@PathParam("tokenID") String tokenID,
            @PathParam("resource") String resource, @PathParam("permission") String permission,
            @Context HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        try {
            ip = RequestUtilities.getIP(request);
            ExclusiveInfoWrapper info = exclusiveAccessEJB.releaseExclusiveAccess(tokenID, resource, permission);
            if (info.isReleased()) {
                rbacLogger.info(info.getUser(), ip, Action.EXCLUSIVE,
                        Messages.getString(Messages.EXCLUSIVE_ACCESS_RELEASE, permission, resource));
                return Response.noContent().build();
            } else {
                rbacLogger.warn(info.getUser(), ip, Action.EXCLUSIVE,
                        Messages.getString(Messages.EXCLUSIVE_ACCESS_RELEASE_NOT_EXIST, permission, resource));
                return Response.ok().build();
            }
        } catch (Exception e) {
            return handleException(e, Action.EXCLUSIVE, null, ip);
        }
    }

    /**
     * Generates the content of an ACF file for the provided security groups. Authentication information is not checked,
     * so this method can be used without authentication.
     * <p>
     * If the ACF file content is generated successfully a response with status code 200 (OK) will be returned. If there
     * is an error while generating the ACF file content, for instance, if any of the specified groups could not be
     * found an error response with status code 400 (BAD REQUEST) is returned, containing more information about the
     * problem.
     * </p>
     *
     * @param groups access security group names delimited by <code>','</code>
     * @param request the HTTP GET request
     *
     * @return content of an ACF file for the provided security groups in a response with response code 200 or
     *         BAD_REQUEST in case of an error
     */
    @GET
    @Path("/pvaccess/group/{groups}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAccessSecurityGroups(@PathParam("groups") String groups, @Context HttpServletRequest request) {
        try {
            String[] gNames = groups.split("[|]");
            String[] groupNames = null;
            String defaultGroup = null;
            if (gNames.length == 2) {
                defaultGroup = gNames[0];
                groupNames = gNames[1].split("[,]");
            } else if (gNames.length == 1) {
                if (groups.trim().charAt(groups.length() - 1) == '|') {
                    defaultGroup = gNames[0];
                } else {
                    groupNames = gNames[0].split("[,]");
                }
            } else {
                throw new IllegalRBACArgumentException("Invalid access security groups requested: " + groups + ".");
            }
            return Response.ok().entity(pvAccessEJB.generateACFContent(defaultGroup, groupNames)).build();
        } catch (Exception e) {
            return handleException(e, null, null, request.getRemoteAddr());
        }
    }

    /**
     * Handle the exception, which was thrown during business logic execution. If Action is non null, the exception is
     * logged using the RBAC logger, after that a proper response is created and returned.
     *
     * @param e the exception to handle
     * @param action the action to log
     * @param userid the name of the user who requested the action
     * @param ip the IP from which the action was requested
     * @return the response
     *
     * @throws RBACRuntimeException if the exception could no be handled and is not an {@link EJBException}
     */
    private Response handleException(Throwable e, Action action, String userid, String ip)
            throws RBACRuntimeException {
        String username = userid;
        if (e instanceof RBACException) {
            if (username == null) {
                username = ((RBACException) e).getUsername();
            }
            if (action != null) {
                rbacLogger.error(username, ip, action, e.getMessage());
            }
        }
        if (e instanceof IllegalRBACArgumentException) {
            return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
        } else if (e instanceof TokenInvalidException) {
            return Response.status(Status.GONE).entity(e.getMessage()).build();
        } else if (e instanceof AuthAndAuthException) {
            if (action == Action.LOGIN) {
                return Response.status(Status.UNAUTHORIZED)
                        .entity(e.getMessage()).header("WWW-Authenticate",
                                HttpServletRequest.BASIC_AUTH + " realm=\"rbac\"").build();
            } else {
                return Response.status(Status.FORBIDDEN).entity(e.getMessage()).build();
            }
        } else if (e instanceof DataConsistencyException) {
            return Response.serverError().entity(e.getMessage()).build();
        } else if (e instanceof InvalidResourceException) {
            return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
        } else if (e instanceof RBACException) {
            return Response.serverError().entity(e.getMessage()).build();
        } else if (e instanceof EJBException) {
            Throwable t = e.getCause();
            if (t == null) {
                if (action != null) {
                    rbacLogger.error(username, ip, action, e.getMessage());
                }
                throw (EJBException) e;
            } else {
                return handleException(t, action, username, ip);
            }
        } else {
            if (action != null) {
                rbacLogger.error(username, ip, action, e.getMessage());
            }
            throw new RBACRuntimeException(e);
        }
    }

    /**
     * Creates log entry which depends on permissions info. Two separate entries are created, one for all granted
     * permissions and one for denied permissions.
     *
     * @param permissionsInfo permissions info
     * @param permissionNames permissions names
     * @param resource resource
     * @param ip ip
     */
    private void printAuthorisedLogMessage(PermissionsInfoWrapper permissionsInfo, String[] permissionNames,
            String resource, String ip) {
        Map<String, Boolean> info = permissionsInfo.getInfo().getPermissions();
        List<String> authorised = new ArrayList<>();
        List<String> notAuthorised = new ArrayList<>();
        for (Map.Entry<String,Boolean> permission : info.entrySet()) {
            if (permission.getValue()) {
                authorised.add(permission.getKey());
            } else {
                notAuthorised.add(permission.getKey());
            }
        }
        if (authorised.size() == permissionNames.length) {
            rbacLogger.info(permissionsInfo.getUser(), ip, Action.AUTHORIZE,
                    Messages.getString(Messages.AUTHORISED, Arrays.toString(permissionNames), resource));
        } else if (notAuthorised.size() == permissionNames.length) {
            rbacLogger.info(permissionsInfo.getUser(), ip, Action.AUTHORIZE,
                    Messages.getString(Messages.NOT_AUTHORISED, Arrays.toString(permissionNames), resource));
        } else {
            rbacLogger.info(permissionsInfo.getUser(), ip, Action.AUTHORIZE,
                    Messages.getString(Messages.AUTHORISED, authorised.toString(), resource));
            rbacLogger.info(permissionsInfo.getUser(), ip, Action.AUTHORIZE,
                    Messages.getString(Messages.NOT_AUTHORISED, notAuthorised.toString(), resource));
        }
    }
}
