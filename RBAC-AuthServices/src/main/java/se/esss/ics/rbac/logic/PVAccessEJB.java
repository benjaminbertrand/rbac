/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.DatatypeConverter;

import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.ConfigurationParameter;
import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.pvaccess.AccessSecurityGroup;
import se.esss.ics.rbac.pvaccess.AccessSecurityInput;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule.Permission;

/**
 * 
 * <code>PVAccessEJB</code> implements the logic related to PV access. The bean can be requested to generate .acf files
 * for a set of access security groups.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Stateless(description = "PVAccessEJB is responsible for generation of the AFC files content.")
public class PVAccessEJB implements Serializable {

    private static final long serialVersionUID = 8450982471635698966L;
    private static final Charset CHARSET = Charset.forName("UTF-8");
    private static final Map<Integer, String> INPUT_MAP = new HashMap<>();
    private static final AccessSecurityGroup DEFAULT_GROUP;

    /** The parameter that defines what rules the default group has: read or write. */
    public static final String PARAM_DEFAULT_ASG_RULE = "DEFAULT_ASG_RULE";

    static {
        INPUT_MAP.put(Integer.valueOf(1), "A");
        INPUT_MAP.put(Integer.valueOf(2), "B");
        INPUT_MAP.put(Integer.valueOf(3), "C");
        INPUT_MAP.put(Integer.valueOf(4), "D");
        INPUT_MAP.put(Integer.valueOf(5), "E");
        INPUT_MAP.put(Integer.valueOf(6), "F");
        INPUT_MAP.put(Integer.valueOf(7), "G");
        INPUT_MAP.put(Integer.valueOf(8), "H");
        INPUT_MAP.put(Integer.valueOf(9), "I");
        INPUT_MAP.put(Integer.valueOf(10), "J");
        INPUT_MAP.put(Integer.valueOf(11), "K");
        INPUT_MAP.put(Integer.valueOf(12), "L");

        DEFAULT_GROUP = new AccessSecurityGroup();
        AccessSecurityRule r = new AccessSecurityRule();
        r.setPermission(Permission.READ);
        r.setASGroup(DEFAULT_GROUP);
        r.setLevel(Boolean.TRUE);
        r.setName(Permission.READ.name());
        r.setIpGroups(new HashSet<IPGroup>(0));
        r.setRoles(new HashSet<Role>(0));
        Set<AccessSecurityRule> rules = new HashSet<>(1);
        rules.add(r);
        DEFAULT_GROUP.setName("DEFAULT");
        DEFAULT_GROUP.setDescription("Default group");
        DEFAULT_GROUP.setRules(rules);
        DEFAULT_GROUP.setInputs(new HashSet<AccessSecurityInput>(0));
    }

    /**
     * <code>ArrayMap</code> is an extension of the map that works
     * well with array type keys.
     */
    private static class ArrayMap extends LinkedHashMap<Role[], String> {

        private static final long serialVersionUID = 6668405169717253213L;

        /**
         * Retrieves the value for the given key. The keys match if the {@link Arrays#equals(Object[], Object[])}
         * matches.
         * 
         * @param key the key for which we want to fetch the value
         * @return the value stored under the given key
         */
        public String get(Role[] key) {
            for (Entry<Role[], String> e : entrySet()) {
                if (Arrays.equals(e.getKey(), key)) {
                    return e.getValue();
                }
            }
            return null;
        }
    }

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;
    @EJB
    private SignatureEJB sigUtil;

    /**
     * Gathers data for the security groups from the database and checks if all of the provided security groups exist.
     * If all the provided groups are accounted for it generates the content of the ACF file in accordance with the
     * specifications at <a href=
     * "http://www.aps.anl.gov/epics/base/R3-14/12-docs/AppDevGuide/node9.html#SECTION00934000000000000000" > 8.3.4
     * Access Security Configuration File</a>. RSA signature of the content is added to the top of the file. The content
     * to generate the signature is generated by removing all line breaks and tabs. The trimmed string is transformed
     * into bytes using the UTF-8 character set.
     * 
     * If both parameters are null an {@link IllegalRBACArgumentException} is thrown. If any of the security groups
     * could not be found, if there is a problem generating data signature or if there are any inconsistencies with data
     * in the database an {@link RBACException} is thrown.
     * 
     * @param defaultGroup the name of the group that will be used as the DEFAULT group
     * @param securityGroupNames array of security group names to be included into the ACF file.
     * 
     * @return content of the ACF file headed by a RSA signature.
     * 
     * @throws IllegalRBACArgumentException if securityGroupNames parameter is <code>null</code>.
     * @throws InvalidResourceException if any of the security groups could not be found,
     * @throws DataConsistencyException if the data in the database is inconsistent
     * @throws RBACException if there is a problem generating data signature
     */
    public String generateACFContent(String defaultGroup, String[] securityGroupNames)
            throws IllegalRBACArgumentException, DataConsistencyException, InvalidResourceException, RBACException {
        if (securityGroupNames == null && defaultGroup == null) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Access security group name"));
        }

        List<AccessSecurityGroup> asGroups = new ArrayList<>(0);
        if (securityGroupNames != null) {
            asGroups = entityManager.createNamedQuery("AccessSecurityGroup.findByNames", AccessSecurityGroup.class)
                    .setParameter("names", Arrays.asList(securityGroupNames)).getResultList();
            verifyGroups(securityGroupNames, asGroups);
        }

        AccessSecurityGroup defaultASG = null;
        if (defaultGroup != null) {
            List<AccessSecurityGroup> defaultGroups = entityManager
                    .createNamedQuery("AccessSecurityGroup.findByNames", AccessSecurityGroup.class)
                    .setParameter("names", Arrays.asList(defaultGroup)).getResultList();
            if (defaultGroups.isEmpty()) {
                throw new RBACException(null, Messages.getString(Messages.NO_ASG, defaultGroup));
            } else if (defaultGroups.size() > 1) {
                throw new DataConsistencyException(null, Messages.getString(Messages.MULTIPLE_ASG, defaultGroup));
            }
            defaultASG = defaultGroups.get(0);
        }
        setUpDefaultGroup();
        // Generate formatted ACF content.
        String acfContent = generateACFContent(defaultASG, asGroups);
        String signature = generateSignature(acfContent);

        return new StringBuilder(acfContent.length() + signature.length() + 2).append(signature).append('\r')
                .append('\r').append(acfContent).toString();
    }

    /**
     * Set up the rule of the default group according to the default setting in the database.
     */
    private void setUpDefaultGroup() {
        ConfigurationParameter pubParameter = entityManager.find(ConfigurationParameter.class, PARAM_DEFAULT_ASG_RULE);
        if (pubParameter == null) {
            pubParameter = new ConfigurationParameter();
            pubParameter.setName(PARAM_DEFAULT_ASG_RULE);
            pubParameter.setValue(Permission.READ.name());
            entityManager.persist(pubParameter);
        }
        Permission p = Permission.valueOf(pubParameter.getValue());
        AccessSecurityRule rule = DEFAULT_GROUP.getRules().iterator().next();
        rule.setPermission(p);
        rule.setName(p.name());
        DEFAULT_GROUP.setDescription("Default group allows reading" + (p == Permission.READ ? "." : " and writing."));
    }

    /**
     * Generates the signature. First the content is trimmed of all line breaks, then the signature is generates from
     * the concatenated content. The signature is properly formatted for print into 8-column lines as comments.
     * 
     * @param acfContent the content to trim and concatenate
     * @return the signature formatted signature
     * @throws RBACException if an error occurred during signature creation
     */
    private String generateSignature(String acfContent) throws RBACException {
        // Trim line breaks before generating signature.
        String trimmedContent = acfContent.replaceAll("[\r,\t]", "");
        // Convert to HEX.
        String acfSignature = DatatypeConverter.printHexBinary(sigUtil.generateSignature(trimmedContent
                .getBytes(CHARSET)));
        // Format 64 signature characters into an 8x16 block.
        StringBuilder sigBlockBuilder = new StringBuilder(acfSignature.length() + 40);
        sigBlockBuilder.append('#');
        for (int i = 0; i < acfSignature.length(); i++) {
            if (i % 16 == 0 && i > 0) {
                sigBlockBuilder.append('\r').append('#');
            } else if (i % 2 == 0 && i > 0) {
                sigBlockBuilder.append(' ');
            }
            sigBlockBuilder.append(acfSignature.charAt(i));
        }
        return sigBlockBuilder.toString();
    }

    /**
     * Verifies that all requested groups are known.
     * 
     * @param securityGroupNames the requested groups
     * @param asGroups known groups
     * @throws DataConsistencyException if more groups are found than requested
     * @throws InvalidResourceException if any of the requested groups could not be found
     */
    private static void verifyGroups(String[] securityGroupNames, List<AccessSecurityGroup> asGroups)
            throws DataConsistencyException, InvalidResourceException {
        if (securityGroupNames.length < asGroups.size()) {
            throw new DataConsistencyException(null, Messages.getString(Messages.TOO_MANY_ASGS));
        }
        if (securityGroupNames.length > asGroups.size()) {
            List<String> missing = new ArrayList<>();
            for (String groupName : securityGroupNames) {
                group: {
                    for (AccessSecurityGroup asGroup : asGroups) {
                        if (groupName.equals(asGroup.getName())) {
                            break group;
                        }
                    }
                    missing.add(groupName);
                }
            }
            throw new InvalidResourceException(null, "Access Security Groups", Messages.getString(
                    Messages.TOO_LITTLE_ASGS, missing.toString()));
        }
    }

    /**
     * Generates the content of an ACF file for the provided security groups. Content is generated with accordance to
     * ACF specifications, found here: <a
     * href="http://www.aps.anl.gov/epics/base/R3-14/12-docs/AppDevGuide/node9.html#SECTION00934000000000000000"> 8.3.4
     * Access Security Configuration File</a>
     * 
     * @param defaultGroup the group that is to be used as the default group for the ACF file
     * @param asGroups list of access security groups to be included in the generated ACF file content
     * 
     * @return content of the ACF file for the provided groups
     * 
     */
    private static String generateACFContent(AccessSecurityGroup defaultGroup, List<AccessSecurityGroup> asGroups) {
        StringBuilder acfBuilder = new StringBuilder(asGroups.size() * 1000 + (defaultGroup == null ? 0 : 1000));
        // first print all the UAGs and HAGs

        ArrayMap uags = printUserGroups(acfBuilder, defaultGroup, asGroups);
        printHostGroups(acfBuilder, defaultGroup, asGroups);

        acfBuilder.append('\r');

        if (defaultGroup == null) {
            printAccessSecurityGroup(acfBuilder, DEFAULT_GROUP, uags, true, DEFAULT_GROUP.getRules().iterator().next()
                    .getPermission() == Permission.WRITE);
        } else {
            printAccessSecurityGroup(acfBuilder, defaultGroup, uags, true, true);
        }
        for (AccessSecurityGroup asGroup : asGroups) {
            printAccessSecurityGroup(acfBuilder, asGroup, uags, false, true);
        }

        return acfBuilder.toString().trim();
    }

    /**
     * Prints the definitions of the host groups as they are defined in the default and other groups.
     * 
     * @param acfBuilder the builder to print to
     * @param defaultGroup the default group
     * @param asGroups the groups
     */
    private static void printHostGroups(StringBuilder acfBuilder, AccessSecurityGroup defaultGroup,
            List<AccessSecurityGroup> asGroups) {
        Set<IPGroup> ipGroups = new HashSet<>();
        if (defaultGroup != null) {
            for (AccessSecurityRule rule : defaultGroup.getRules()) {
                ipGroups.addAll(rule.getIpGroups());
            }
        }
        for (AccessSecurityGroup g : asGroups) {
            for (AccessSecurityRule rule : g.getRules()) {
                ipGroups.addAll(rule.getIpGroups());
            }
        }

        IPGroup[] groups = Util.sort(ipGroups.toArray(new IPGroup[ipGroups.size()]));
        for (IPGroup ipGroup : groups) {
            acfBuilder.append("HAG(").append(ipGroup.getName()).append(')').append(' ').append('{');
            StringBuilder sb = new StringBuilder(ipGroup.getIp().size() * 20);
            for (String host : ipGroup.getIp()) {
                sb.append(',').append(host);
            }
            if (sb.length() > 1) {
                acfBuilder.append(sb.substring(1));
            }
            acfBuilder.append('}').append('\r');
        }
    }

    /**
     * Prints the definition of the user groups as they are defined in the default and other groups.
     * 
     * @param acfBuilder the builder to print to
     * @param defaultGroup the default group
     * @param asGroups the groups
     * @return a map containing the arrays of roles as keys and the name of the user group as a value
     */
    private static ArrayMap printUserGroups(StringBuilder acfBuilder, AccessSecurityGroup defaultGroup,
            List<AccessSecurityGroup> asGroups) {
        ArrayMap roles = new ArrayMap();
        if (defaultGroup != null) {
            findAndAddRoles(defaultGroup, roles);
        }
        for (AccessSecurityGroup g : asGroups) {
            findAndAddRoles(g, roles);
        }

        int i = 1;
        String newName;
        for (Entry<Role[], String> e : roles.entrySet()) {
            newName = "RoleGroup_" + i++;
            acfBuilder.append("UAG(").append(newName).append("_uag) {");
            StringBuilder sb = new StringBuilder(roles.size() * 30);
            for (Role role : e.getKey()) {
                sb.append(',');
                sb.append(role.getName());
            }
            acfBuilder.append(sb.substring(1));
            acfBuilder.append('}').append('\r');
            roles.put(e.getKey(), newName);
        }
        return roles;
    }

    /**
     * Collect all role arrays from the group and add them to the map <code>roles</code>. The roles are sorted and two
     * equal arrays cannot coexist in the map.
     * 
     * @param group the group to load the roles from
     * @param roles the map to add the roles to
     */
    private static void findAndAddRoles(AccessSecurityGroup group, ArrayMap roles) {
        Role[] r;
        for (AccessSecurityRule rule : group.getRules()) {
            if (rule.getRoles() == null || rule.getRoles().isEmpty()) {
                continue;
            }
            r = Util.sort(rule.getRoles().toArray(new Role[rule.getRoles().size()]));
            if (roles.get(r) == null) {
                roles.put(r, rule.getName());
            }
        }
    }

    /**
     * Print the main body of the access security group definition.
     * 
     * @param acfBuilder the builder to print to
     * @param asGroup the group to print
     * @param uags the user groups map which maps role collections to defined uag names
     * @param printAsDefault true if print this group as a default group or false otherwise
     * @param includeReadRule true if the read rule should be added to every group or false if not
     */
    private static void printAccessSecurityGroup(StringBuilder acfBuilder, AccessSecurityGroup asGroup, ArrayMap uags,
            boolean printAsDefault, boolean includeReadRule) {
        // Print group info as a comment.
        acfBuilder.append("# Access Security Group ").append(asGroup.getName()).append(": ")
                .append(asGroup.getDescription());
        if (printAsDefault && !"DEFAULT".equalsIgnoreCase(asGroup.getName())) {
            acfBuilder.append(" (as default group)");
        }
        acfBuilder.append('\r').append("ASG(").append(printAsDefault ? "DEFAULT" : asGroup.getName())
                .append(')').append(' ').append('\r');
        // Print inputs.
        printInputs(acfBuilder, asGroup.getInputs());
        // Print rules.
        AccessSecurityRule[] rules = Util.sort(asGroup.getRules().toArray(
                new AccessSecurityRule[asGroup.getRules().size()]));
        printRules(acfBuilder, rules, uags, includeReadRule);
        acfBuilder.append('}').append('\r').append('\r');
    }

    /**
     * Print the input PV definitions.
     * 
     * @param acfBuilder the builder to append text to
     * @param inputs the inputs to append
     */
    private static void printInputs(StringBuilder acfBuilder, Set<AccessSecurityInput> inputs) {
        AccessSecurityInput[] inps = inputs.toArray(new AccessSecurityInput[inputs.size()]);
        Arrays.sort(inps, new Comparator<AccessSecurityInput>() {
            @Override
            public int compare(AccessSecurityInput o1, AccessSecurityInput o2) {
                return o1.getIndex().compareTo(o2.getIndex());
            }
        });
        for (AccessSecurityInput asInput : inps) {
            acfBuilder.append("\tINP").append(INPUT_MAP.get(asInput.getIndex())).append('(')
                    .append(asInput.getPvName()).append(')').append('\r');
        }
    }

    /**
     * Compose the main content of the acf files.
     * 
     * @param acfBuilder the builder to append the text to
     * @param rules the source of data
     * @param uags the user groups map which maps role collections to defined uag names
     * @param includeReadRule true if the default read rule should be added or false otherwise
     */
    private static void printRules(StringBuilder acfBuilder, AccessSecurityRule[] rules, ArrayMap uags,
            boolean includeReadRule) {
        Set<IPGroup> groups;
        Set<Role> roles;
        String calc;
        Role[] r;
        if (includeReadRule) {
            acfBuilder.append("\tRULE(1,READ)\r");
        }
        for (AccessSecurityRule asRule : rules) {
            groups = asRule.getIpGroups();
            calc = asRule.getCalc();
            roles = asRule.getRoles();
            acfBuilder.append("\tRULE(").append(asRule.getLevel().booleanValue() ? 1 : 0).append(',')
                    .append(asRule.getPermission()).append(')');
            if (!roles.isEmpty() || !groups.isEmpty() || calc != null) {
                acfBuilder.append(' ').append('{').append('\r');
                if (!roles.isEmpty()) {
                    r = Util.sort(roles.toArray(new Role[roles.size()]));
                    acfBuilder.append("\t\tUAG(").append(uags.get(r)).append("_uag)\r");
                }
                if (!groups.isEmpty()) {
                    acfBuilder.append("\t\tHAG(");
                    StringBuilder sb = new StringBuilder(groups.size() * 20);
                    for (IPGroup ipGroup : groups) {
                        sb.append(',');
                        sb.append(ipGroup.getName());
                    }
                    acfBuilder.append(sb.substring(1));
                    acfBuilder.append(')').append('\r');
                }
                if (calc != null && !calc.trim().isEmpty()) {
                    if (calc.charAt(0) != '"') {
                        calc = '"' + calc;
                    }
                    if (calc.charAt(calc.length() - 1) != '"') {
                        calc = calc + '"';
                    }
                    acfBuilder.append("\t\tCALC(").append(calc).append(')').append('\r');
                }
                acfBuilder.append('\t').append('}');
            }
            acfBuilder.append('\r');
        }
    }
}
