/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.datatypes;

import se.esss.ics.rbac.jaxb.PermissionsInfo;

/**
 * 
 * <code>PermissionsInfoWrapper</code> is a wrapper for {@link PermissionsInfo} which also provides the username of the
 * user whom the info belongs to.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class PermissionsInfoWrapper {

    /** The Permission info that this wrapper wraps */
    private final PermissionsInfo info;
    /** The owner of the permission info */
    private final String user;

    /**
     * Construct a new PermissionsInfoWrapper.
     * 
     * @param info the info
     * @param user the owner of the info
     */
    public PermissionsInfoWrapper(PermissionsInfo info, String user) {
        this.info = info;
        this.user = user;
    }

    /**
     * Returns the permission info wrapped by this wrapper.
     * 
     * @return the permission info
     */
    public PermissionsInfo getInfo() {
        return info;
    }

    /**
     * Returns the user for whom the permissions were requested.
     * 
     * @return the user that owns the permissions info
     */
    public String getUser() {
        return user;
    }

}
