/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.ResourcesInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.UsersInfo;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.ConfigurationParameter;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>GeneralEJB</code> implements the logic behind the general service requests. It provides methods to access those
 * entities which are not directly related to authentication, authorisation or exclusive access.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Stateless(description = "The bean provides general logic used by the services.")
public class GeneralEJB implements Serializable {

    private static final long serialVersionUID = 5736962151171014986L;

    /** The name of the configuration parameter that defines the token expiration time. */
    public static final String PARAM_EXPIRATION_TIME = "EXPIRATION_TIME";

    // default expiration time is 8 hours
    private static final long DEFAULT_EXPIRATION_TIME = 3600000 * 8;

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;
    @EJB
    private SignatureEJB signatureEJB;

    /**
     * Returns all {@link Role}s available to the user with the specified user name. The method does not check whether
     * the user exists. If the user does not exist a {@link RolesInfo} containing no roles is returned.
     * 
     * @param username for which the roles should be returned.
     * 
     * @return roles information containing a list of all of the user's roles.
     * 
     * @throws IllegalRBACArgumentException if <code>username</code> is null.
     */
    public RolesInfo getUserRoles(String username) throws IllegalRBACArgumentException {
        if (username == null || username.isEmpty()) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Username"));
        }

        List<Role> roles = entityManager.createNamedQuery("Role.findValidByUserId", Role.class)
                .setParameter("userId", username).getResultList();
        List<String> roleNames = new ArrayList<>();
        for (Role r : roles) {
            roleNames.add(r.getName());
        }
        return new RolesInfo(username, roleNames);
    }
    
    /**
     * Returns the list of all roles in the system.
     * 
     * @return the roles info containing the list of all role names, the user is null
     */
    public RolesInfo getRoles() {
        List<Role> roles = entityManager.createNamedQuery("Role.selectAll", Role.class).getResultList();
        List<String> roleNames = new ArrayList<>();
        for (Role r : roles) {
            roleNames.add(r.getName());
        }
        return new RolesInfo(null, roleNames);
    }
    
    /**
     * Returns the list of all users that are assigned the given role.
     * 
     * @param role the role for which we want the users
     * 
     * @return the users info containing the list of all usernames that have the given role
     * @throws IllegalRBACArgumentException if the role is null or empty
     * @throws InvalidResourceException if the role does not exist or more than one role with the same name was found
     */
    public UsersInfo getUsersWithRole(String role) throws IllegalRBACArgumentException, InvalidResourceException {
        if (role == null || role.isEmpty()) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Role"));
        }
        List<Role> roles = entityManager.createNamedQuery("Role.findByName", Role.class)
                .setParameter("name", role).getResultList();
        if (roles.isEmpty() || roles.size() > 1) {
            throw new InvalidResourceException(null, role, Messages.getString(Messages.NO_ROLE, role));
        }
        Role r = roles.get(0);
        List<String> users = new ArrayList<String>(r.getUsers().size());
        for (UserRole ur : r.getUsers()) {
            users.add(ur.getUserId());
        }
        return new UsersInfo(role,users);
    }
    
    /**
     * Returns the list of all users that are assigned a role with the given permission.
     * 
     * @param resource the name of the resource that owns the permission
     * @param permission the permission name
     * 
     * @return the users info containing the list of all usernames with the given permission
     * 
     * @throws IllegalRBACArgumentException if the role is null or empty
     * @throws InvalidResourceException if the resource or permission does not exist or more than one 
     *          resource or permission was found
     */
    public UsersInfo getUsersWithPermission(String resource, String permission) 
            throws IllegalRBACArgumentException, InvalidResourceException {
        verifyInputParameters("id", resource, permission);
        
        List<Resource> resources = entityManager.createNamedQuery("Resource.findByName",Resource.class)
                .setParameter("name", resource).getResultList();
        if (resources.isEmpty() || resources.size() > 1) {
            throw new InvalidResourceException(null, resource, Messages.getString(Messages.NO_RESOURCE, resource));
        }
        List<Permission> permissions = entityManager.createNamedQuery("Permission.findByName", Permission.class)
                .setParameter("name", permission).setParameter("resource", resources.get(0)).getResultList();
        if (permissions.isEmpty() || permissions.size() > 1) {
            throw new InvalidResourceException(null, permission,
                    Messages.getString(Messages.NO_PERMISSION, permission, resource));
        }
        Permission r = permissions.get(0);
        Set<String> set = new HashSet<>();
        for (Role role : r.getRole()) {
            for (UserRole ur : role.getUsers()) {
                set.add(ur.getUserId());
            }
        }
        List<String> users = new ArrayList<String>(set.size());
        for (String s : set) {
            users.add(s);
        }
        return new UsersInfo(resource,permission,users);
    }
    
    /**
     * Returns the list of all resources in the system.
     * 
     * @return the resources info containing the list of all resource names
     */
    public ResourcesInfo getResources() {
        List<Resource> resources = entityManager.createNamedQuery("Resource.selectAll", Resource.class).getResultList();
        List<String> resourceNames = new ArrayList<>();
        for (Resource r : resources) {
            resourceNames.add(r.getName());
        }
        return new ResourcesInfo(resourceNames);
    }
    
    /**
     * Returns the list of all permissions that belong to the given resource. 
     * 
     * @param resourceName the name of the resource
     * @return the permission info containing all permissions for the selected resource
     * 
     * @throws IllegalRBACArgumentException if the resource name is null or empty
     * @throws InvalidResourceException if the resource with the given name could not be found
     */
    public PermissionsInfo getPermissions(String resourceName) throws IllegalRBACArgumentException,
            InvalidResourceException {
        if (resourceName == null || resourceName.isEmpty()) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Resource name"));
        }
        
        List<Resource> resources = entityManager.createNamedQuery("Resource.findByName", Resource.class)
                .setParameter("name", resourceName).getResultList();
        if (resources.isEmpty()) {
            throw new InvalidResourceException(null, resourceName, Messages.getString(
                    Messages.NO_RESOURCE, resourceName));
        } 
        
        List<Permission> permissions = entityManager.createNamedQuery("Permission.selectNamesByResource", 
                Permission.class).setParameter("resource", resources.get(0)).getResultList();
        Map<String, Boolean> perms = new LinkedHashMap<>(permissions.size());
        for (Permission p : permissions) {
            perms.put(p.getName(),Boolean.TRUE);
        }
        return new PermissionsInfo(resourceName, perms);
    }

    /**
     * Returns the default expiration period. The value is loaded from the configuration parameter. If the parameter
     * does not exist it is created.
     * 
     * @return the expiration period in milliseconds
     */
    public long getExpirationPeriod() {
        ConfigurationParameter expirationParameter = entityManager.find(ConfigurationParameter.class,
                PARAM_EXPIRATION_TIME);
        if (expirationParameter == null) {
            expirationParameter = new ConfigurationParameter();
            expirationParameter.setName(PARAM_EXPIRATION_TIME);
            expirationParameter.setValue(String.valueOf(DEFAULT_EXPIRATION_TIME));
            entityManager.persist(expirationParameter);
        }
        return expirationParameter.getValueAsLong();
    }

    /**
     * Returns the public key used by RBAC. This key can be used to verify the RBAC signature.
     * 
     * @return the byte array representing the encoded public key
     * 
     * @throws RBACException if the public key could not be loaded
     * 
     * @see SignatureEJB#getPublicKey()
     */
    public byte[] getPublicKeyData() throws RBACException {
        return signatureEJB.getPublicKey().getEncoded();
    }

    /**
     * Checks if the array is null or if any of the elements in the array are null or empty. If yes, the method returns
     * false, otherwise it returns true.
     * 
     * @param array the array to check
     * 
     * @return true if the array is OK or false otherwise
     */
    static boolean isArrayValid(String[] array) {
        if (array == null || array.length == 0) {
            return false;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null || array[i].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if the token is valid. Token is valid if it has not yet expired. The token expires if its expiration time
     * is in the past or if one of its user roles has already expired. In the latter case the token might still exist in
     * the database, but will be purged at the next scheduled cleanup.
     * 
     * @param token the token to validate
     *  
     * @throws TokenInvalidException if the token expired
     */
    static void validateToken(Token token) throws TokenInvalidException {
        Date now = new Date();
        if (token.getExpirationDate().before(now)) {
            throw new TokenInvalidException(token.getTokenId(), token.getUserId(), Messages.getString(
                    Messages.TOKEN_EXPIRED, token.getUserId()));
        }
        for (TokenRole tr : token.getRole()) {
            Timestamp date = tr.getRole().getEndTime();
            if (date != null && date.before(now)) {
                throw new TokenInvalidException(token.getTokenId(), token.getUserId(), Messages.getString(
                        Messages.TOKEN_EXPIRED, token.getUserId()));
            }
        }
    }

    /**
     * Verifies the input parameters. If any of the parameters is empty or null the method throws an exception that
     * contains an appropriate message. The permission names are checked in details, which means that if any of the
     * elements in the array is null or empty an exception will be thrown.
     * 
     * @param tokenID the token ID
     * @param resourceName the name of the resource
     * @param permissionNames the names of the permissions
     * 
     * @throws IllegalRBACArgumentException if any of the parameters is null or empty
     */
    static void verifyInputParameters(String tokenID, String resourceName, String... permissionNames)
            throws IllegalRBACArgumentException {
        if (tokenID == null || tokenID.isEmpty()) {
            throw new IllegalRBACArgumentException(
                    Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Token ID"));
        } else if (resourceName == null || resourceName.isEmpty()) {
            throw new IllegalRBACArgumentException(
                    Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Resource name"));
        } else if (!GeneralEJB.isArrayValid(permissionNames)) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Permission name"));
        }
    }

    /**
     * Verifies the input parameter. If the tokenID is null or empty an exception is thrown.
     * 
     * @param tokenID the id to check
     * 
     * @throws IllegalRBACArgumentException if the parameter is null or empty
     */
    static void verifyInputParameters(String tokenID) throws IllegalRBACArgumentException {
        if (tokenID == null || tokenID.isEmpty()) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Token ID"));
        }
    }

    /**
     * Retrieves the token with the given ID. If the token exists it is returned, otherwise an exception is thrown.
     * 
     * @param tokenID the it of the token to fetch
     * 
     * @return the token
     * 
     * @throws TokenInvalidException if the token does not exist
     * @throws DataConsistencyException if more than one token exists
     */
    public Token fetchToken(String tokenID) throws TokenInvalidException, DataConsistencyException {
        List<Token> tokens = entityManager.createNamedQuery("Token.findByTokenId", Token.class)
                .setParameter("tokenId", tokenID).getResultList();
        if (tokens.size() == 1) {
            return tokens.get(0);
        } else if (tokens.isEmpty()) {
            throw new TokenInvalidException(tokenID, null, Messages.getString(Messages.NO_TOKEN, tokenID));
        } else {
            throw new DataConsistencyException(null, Messages.getString(Messages.MULTIPLE_TOKENS, tokenID));
        }
    }
}
