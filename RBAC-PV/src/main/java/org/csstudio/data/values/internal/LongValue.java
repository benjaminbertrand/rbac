/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

import org.csstudio.data.values.ILongValue;
import org.csstudio.data.values.INumericMetaData;
import org.csstudio.data.values.ISeverity;
import org.csstudio.data.values.ITimestamp;

/**
 * Implementation of {@link ILongValue}.
 * 
 * @see ILongValue
 * @author Kay Kasemir, Xihui Chen
 */
public class LongValue extends Value implements ILongValue {
    private final long[] values;

    /**
     * Constructs a new value from pieces.
     * 
     * @param time the timestamp of the value
     * @param severity the severity of the value
     * @param status the status of the value
     * @param metaData the metadata describing the PV
     * @param values the values
     */
    public LongValue(ITimestamp time, ISeverity severity, String status, INumericMetaData metaData, long[] values) {
        super(time, severity, status, metaData);
        this.values = values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ILongValue#getValues()
     */
    @Override
    public final long[] getValues() {
        return values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ILongValue#getValue()
     */
    @Override
    public final long getValue() {
        return values[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#format(org.csstudio.platform .data.IValue.Format, int)
     */
    @Override
    public final String format(final Format how, int prec) {
        // Any value at all?
        if (!getSeverity().hasValue()) {
            return Messages.NO_VALUE;
        }
        int precision = prec;
        if (precision < 0) {
            precision = 1;
        }
        StringBuilder buf = new StringBuilder(Math.min(MAX_FORMAT_VALUE_COUNT, values.length) * precision);
        if (how == Format.EXPONENTIAL) {
            // Is there a better way to get this silly format?
            NumberFormat fmt;
            StringBuilder pattern = new StringBuilder(10);
            pattern.append("0.");
            for (int i = 0; i < precision; ++i) {
                pattern.append('0');
            }
            pattern.append("E0");
            fmt = new DecimalFormat(pattern.toString());
            buf.append(fmt.format(values[0]));
            for (int i = 1; i < values.length; i++) {
                buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                buf.append(fmt.format(values[i]));
            }
        } else if (how == Format.STRING) {
            // Format array elements as characters
            for (int i = 0; i < values.length; i++) {
                final char c = getDisplayChar((char) values[i]);
                if (c == 0) {
                    break;
                }
                buf.append(c);
            }
        } else {
            buf.append(values[0]);
            for (int i = 1; i < values.length; i++) {
                buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                buf.append(values[i]);
                if (i >= MAX_FORMAT_VALUE_COUNT) {
                    buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                    buf.append("...");
                    break;
                }
            }
        }
        return buf.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj) {
        if (!(obj instanceof LongValue)) {
            return false;
        }
        final LongValue rhs = (LongValue) obj;
        if (rhs.values.length != values.length) {
            return false;
        }
        for (int i = 0; i < values.length; ++i) {
            if (rhs.values[i] != values[i]) {
                return false;
            }
        }
        return super.equals(obj);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#hashCode()
     */
    @Override
    public final int hashCode() {
        return 31 * super.hashCode() + Arrays.hashCode(values);
    }
}
