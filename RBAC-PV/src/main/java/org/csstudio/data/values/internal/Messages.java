/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

/**
 * Localisation.
 * <p>
 * Not really using messages.properties etc., just hard coded so that this works as a plain library, but at least a
 * start towards localisation.
 * 
 * @author Kay Kasemir
 */
public final class Messages {
    /** Empty string */
    public static final String EMPTY_STRING = "";
    /** Column separator for printing different parts of the value */
    public static final String COLUMN_SEPARATOR = "\t";
    /** Separator of the severity and status from the value */
    public static final String VALUE_SEVERITY_STATUS_SEPARATOR = " [";
    /** Separator of severity and status */
    public static final String SEVERITY_STATUS_SEPARATOR = ", ";
    /** Ending part of the severity and status */
    public static final String SEVERITY_STATUS_END = "]";
    /** Separator of array elements */
    public static final String ARRAY_ELEMENTS_SEPARATOR = ", ";
    /** Dots */
    public static final String DOTS = "...";
    /** No Value */
    public static final String NO_VALUE = "#N/A";
    /** Infinity */
    public static final String INFINITY = "Inf";
    /** Not a number */
    public static final String NAN = "NaN";
    /** Enumeration start tag */
    public static final String ENUM_START = "<enum ";
    /** Enumeration end tag */
    public static final String ENUM_END = ">";

    /** OK severity name */
    public static final String SEVERITY_OK = "OK";
    /** MINOR severity name */
    public static final String SEVERITY_MINOR = "MINOR";
    /** MAJOR severity name */
    public static final String SEVERITY_MAJOR = "MAJOR";
    /** INVALID severity name */
    public static final String SEVERITY_INVALID = "INVALID";

    private Messages() {
    }
}
