/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.util.Arrays;

import org.csstudio.data.values.ISeverity;
import org.csstudio.data.values.IStringValue;
import org.csstudio.data.values.ITimestamp;

/**
 * Implementation of {@link IStringValue}.
 * 
 * @see IStringValue
 * @author Kay Kasemir
 */
public class StringValue extends Value implements IStringValue {
    private final String[] values;

    /**
     * Constructs a new StringValue.
     * 
     * @param time the timestamp of the value
     * @param severity the severity of the value
     * @param status the status
     * @param values the values array
     */
    public StringValue(ITimestamp time, ISeverity severity, String status, String[] values) {
        super(time, severity, status, null);
        if (values == null || values.length < 1) {
            throw new java.lang.IllegalArgumentException("There are no values");
        }
        this.values = values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IStringValue#getValues()
     */
    @Override
    public final String[] getValues() {
        return values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IStringValue#getValue()
     */
    @Override
    public final String getValue() {
        return values[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#format(org.csstudio.platform .data.IValue.Format, int)
     */
    @Override
    public final String format(Format how, int precision) {
        if (getSeverity() != null && !getSeverity().hasValue()) {
            return Messages.NO_VALUE;
        } else if (values.length == 1) {
            return values[0];
        }
        StringBuilder result = new StringBuilder(values.length * 20);
        result.append(values[0]);
        for (int i = 1; i < values.length; i++) {
            result.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
            result.append(values[i]);
        }
        return result.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj) {
        if (!(obj instanceof StringValue)) {
            return false;
        }
        return Arrays.equals(values, ((StringValue) obj).getValues()) && super.equals(obj);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#hashCode()
     */
    @Override
    public int hashCode() {
        return 31 * super.hashCode() + Arrays.hashCode(values);
    }
}
