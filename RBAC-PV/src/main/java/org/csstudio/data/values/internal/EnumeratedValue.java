/*
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron,
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS.
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE.
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS,
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION,
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

import org.csstudio.data.values.IEnumeratedMetaData;
import org.csstudio.data.values.IEnumeratedValue;
import org.csstudio.data.values.ISeverity;
import org.csstudio.data.values.ITimestamp;

/**
 * Implementation of {@link IEnumeratedValue}.
 * 
 * @see IEnumeratedValue
 * @author Kay Kasemir, Xihui Chen
 */
public class EnumeratedValue extends Value implements IEnumeratedValue {
    private final int[] values;

    /**
     * Constructs a new value from pieces.
     * 
     * @param time the timestamp of the value
     * @param severity the severity of the value
     * @param status the status of the value
     * @param metaData the meta info describing the PV
     * @param values the values
     */
    public EnumeratedValue(final ITimestamp time, final ISeverity severity, final String status,
            final IEnumeratedMetaData metaData, final int[] values) {
        super(time, severity, status, metaData);
        this.values = values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IEnumeratedValue#getValues()
     */
    @Override
    public final int[] getValues() {
        return values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IEnumeratedValue#getValue()
     */
    @Override
    public final int getValue() {
        return values[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#format(org.csstudio.platform .data.IValue.Format, int)
     */
    @Override
    public final String format(final Format how, final int precision) {
        final IEnumeratedMetaData enumMeta = getMetaData();
        final StringBuilder buf = new StringBuilder();
        if (getSeverity().hasValue()) {
            if (how == Format.DEFAULT || how == Format.STRING) {
                buf.append(enumMeta.getState(values[0]));
                for (int i = 1; i < values.length; i++) {
                    buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                    buf.append(enumMeta.getState(values[i]));
                }
            } else if (how == Format.DECIMAL || how == Format.ADAPTIVE_DECIMAL) {
                buf.append(values[0]);
                for (int i = 1; i < values.length; i++) {
                    buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                    buf.append(values[i]);
                }
            } else if (how == Format.EXPONENTIAL) {
                // Is there a better way to get this silly format?
                NumberFormat fmt;
                final StringBuilder pattern = new StringBuilder(10);
                pattern.append("0."); //$NON-NLS-1$
                for (int i = 0; i < precision; ++i) {
                    pattern.append('0');
                }
                pattern.append("E0"); //$NON-NLS-1$
                fmt = new DecimalFormat(pattern.toString());
                buf.append(fmt.format(values[0]));
                for (int i = 1; i < values.length; i++) {
                    buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                    buf.append(buf.append(fmt.format(values[i])));
                }
            }

        } else {
            buf.append(Messages.NO_VALUE);
        }
        return buf.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj) {
        if (!(obj instanceof EnumeratedValue)) {
            return false;
        }
        final EnumeratedValue rhs = (EnumeratedValue) obj;
        if (rhs.values.length != values.length) {
            return false;
        }
        for (int i = 0; i < values.length; ++i) {
            if (rhs.values[i] != values[i]) {
                return false;
            }
        }
        return super.equals(obj);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#hashCode()
     */
    @Override
    public final int hashCode() {
        return 31 * super.hashCode() + Arrays.hashCode(values);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#getMetaData()
     */
    @Override
    public IEnumeratedMetaData getMetaData() {
        return (IEnumeratedMetaData) super.getMetaData();
    }
}
