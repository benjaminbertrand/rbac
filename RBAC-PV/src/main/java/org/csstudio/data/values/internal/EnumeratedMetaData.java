/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.util.Arrays;

import org.csstudio.data.values.IEnumeratedMetaData;

/**
 * Implementation of {@link IEnumeratedMetaData}.
 * 
 * @author Kay Kasemir
 */
public class EnumeratedMetaData implements IEnumeratedMetaData {
    /** The enumeration strings for the possible values of an EnumSample. */
    private final String[] states;

    /**
     * Constructor for meta data from pieces.
     * 
     * @param states array of states. Must not be <code>null</code>
     */
    public EnumeratedMetaData(String[] states) {
        if (states == null) {
            throw new IllegalArgumentException("The states cannot be null.");
        }
        this.states = states;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IEnumeratedMetaData#getStates()
     */
    @Override
    public final String[] getStates() {
        return states;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IEnumeratedMetaData#getState(int)
     */
    @Override
    public String getState(int state) {
        if (state < 0 || state >= states.length) {
            return Messages.ENUM_START + state + Messages.ENUM_END;
        } else {
            return states[state];
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Arrays.hashCode(states);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(states, ((EnumeratedMetaData) obj).states);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @SuppressWarnings("nls")
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(35 + states.length * 20).append("EnumeratedMetaData: " + states.length
                + " states:");
        for (String state : states) {
            buf.append("\n    '" + state + "'");
        }
        return buf.toString();
    }
}
