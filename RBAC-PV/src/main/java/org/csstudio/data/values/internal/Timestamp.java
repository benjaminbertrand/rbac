/* 
 * Copyright (c) 2006 Stiftung Deutsches Elektronen-Synchroton, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.util.Calendar;

import org.csstudio.data.values.ITimestamp;

/**
 * Implementation of the {@link ITimestamp} interface.
 * 
 * @author Sven Wende
 * @author Kay Kasemir
 */
public final class Timestamp implements ITimestamp {
    /** Milliseconds per second. */
    public static final long MILLIS_PER_SECOND = 1000L;

    /** Nanoseconds per millisecond. */
    public static final long NANOS_PER_MILLI = 1000000L;

    /** Nanoseconds per second. */
    public static final long NANOS_PER_SECOND = 1000000000L;

    /** Seconds since epoch. */
    private final long seconds;

    /**
     * Nanoseconds within the seconds.
     * <p>
     * Normalised, i.e. within 0..nanos_per_milli.
     */
    private final long nanoseconds;

    /**
     * Constructor with seconds and nanoseconds since epoch.
     * 
     * @param seconds Seconds since epoch
     * @param nanoseconds Nanoseconds within seconds
     */
    public Timestamp(long seconds, long nanoseconds) {
        if (nanoseconds < 0 || nanoseconds >= NANOS_PER_SECOND) {
            long fullsecs = nanoseconds / NANOS_PER_SECOND;
            this.seconds = seconds + fullsecs;
            this.nanoseconds = nanoseconds - fullsecs * NANOS_PER_SECOND;
        } else {
            this.seconds = seconds;
            this.nanoseconds = nanoseconds;
        }
    }

    /**
     * Constructor with fractional seconds since epoch.
     * 
     * @param seconds Seconds since epoch
     */
    public Timestamp(final double seconds) {
        this.seconds = (long) seconds;
        this.nanoseconds = (long) ((seconds - this.seconds) * 1e9);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#seconds()
     */
    @Override
    public long seconds() {
        return seconds;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#nanoseconds()
     */
    @Override
    public long nanoseconds() {
        return nanoseconds;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#isValid()
     */
    @Override
    public boolean isValid() {
        return seconds > 0 || nanoseconds > 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#toDouble()
     */
    @Override
    public double toDouble() {
        return seconds + nanoseconds() / 1e9;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#toCalendar()
     */
    @Override
    public Calendar toCalendar() {
        final Calendar result = Calendar.getInstance();
        result.setTimeInMillis(seconds * MILLIS_PER_SECOND + nanoseconds / NANOS_PER_MILLI);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#format(org.csstudio.platform.data .ITimestamp.Format)
     */
    @Override
    @SuppressWarnings("nls")
    public String format(final Format format) {
        final Calendar cal = toCalendar();

        // Formatting the time as a string is expensive.
        // JProfiler comparison of this String.format() code with
        // with custom StringBuilder.append(...) code seemed to save
        // very little CPU time, but String.format() is a lot easier to read.
        switch (format) {
            case DATE:
                return String.format("%04d/%02d/%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
                        cal.get(Calendar.DAY_OF_MONTH));
            case DATE_TIME:
                return String.format("%04d/%02d/%02d %02d:%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
                        cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
            case DATE_TIME_SECOND:
                return String.format("%04d/%02d/%02d %02d:%02d:%02d", cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
            default:
                // case Full:
                return String.format("%04d/%02d/%02d %02d:%02d:%02d.%09d", cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND), nanoseconds);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return format(Format.FULL);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#isGreaterThan(org.csstudio.platform .data.ITimestamp)
     */
    @Override
    public boolean isGreaterThan(final ITimestamp other) {
        if (seconds > other.seconds()) {
            return true;
        }
        if (seconds < other.seconds()) {
            return false;
        }
        // Seconds tie, let nanoseconds decide.
        return nanoseconds > other.nanoseconds();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#isGreaterOrEqual(org.csstudio.platform .data.ITimestamp)
     */
    @Override
    public boolean isGreaterOrEqual(final ITimestamp other) {
        if (seconds > other.seconds()) {
            return true;
        }
        if (seconds < other.seconds()) {
            return false;
        }
        // Seconds tie, let nanoseconds decide.
        return nanoseconds >= other.nanoseconds();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#isLessThan(org.csstudio.platform .data.ITimestamp)
     */
    @Override
    public boolean isLessThan(final ITimestamp other) {
        return !isGreaterOrEqual(other);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.ITimestamp#isLessOrEqual(org.csstudio.platform .data.ITimestamp)
     */
    @Override
    public boolean isLessOrEqual(final ITimestamp other) {
        return !isGreaterThan(other);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof ITimestamp)) {
            return false;
        }
        ITimestamp rhs = (ITimestamp) obj;
        return rhs.seconds() == seconds && rhs.nanoseconds() == nanoseconds;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int prime = 31;
        int result = prime + (int) (nanoseconds ^ (nanoseconds >>> 32));
        result = prime * result + (int) (seconds ^ (seconds >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final ITimestamp rhs) {
        if (isGreaterThan(rhs)) {
            return 1;
        }
        if (equals(rhs)) {
            return 0;
        }
        return -1;
    }
}
