/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * 
 * <code>PVFactoryProvider</code> is the provider of the {@link PVFactory}. This provider can be used to load the
 * default service that is used for creating the {@link PV}s or to load any factory that exists on the classpath. Use
 * this provider to load the factory when you need it rather than creating the factory directly.
 * 
 */
public final class PVFactoryProvider {

    private static final PVFactoryProvider INSTANCE = new PVFactoryProvider();

    /**
     * Returns the singleton instance of the factory provider.
     * 
     * @return the singleton instance of this provider
     */
    public static PVFactoryProvider getInstance() {
        return INSTANCE;
    }

    private PVFactory defaultFactory;
    private final Map<String, PVFactory> factories;

    private PVFactoryProvider() {
        factories = new HashMap<>();
        // singleton
    }

    /**
     * Search the registry for all services that implement {@link PVFactory}. It returns the first factory that it
     * encounters. In general there should never be more than one.
     * 
     * @return the default {@link PVFactory} instance
     */
    public PVFactory getDefaultFactory() {
        if (defaultFactory == null) {
            ServiceLoader<PVFactory> loader = ServiceLoader.load(PVFactory.class);
            for (PVFactory l : loader) {
                if (l != null) {
                    defaultFactory = l;
                    break;
                }
            }
        }
        return defaultFactory;
    }

    /**
     * Returns an instance of the factory with the given name. The name is the name of the class, which can be either a
     * full name or a simple name.
     * 
     * @param clazz the class name to load
     * @return the factory
     * @throws ClassNotFoundException in case the class cannot be found
     * @throws InstantiationException in case the factory cannot be instantiated
     * @throws IllegalAccessException in case the factory cannot be instantiated
     */
    public PVFactory getFactory(String clazz) throws ClassNotFoundException, InstantiationException,
            IllegalAccessException {
        PVFactory factory = findFactory(clazz);
        if (factory != null) {
            factories.put(clazz, factory);
        }

        return factory;
    }

    private static PVFactory findFactory(String clazz) throws InstantiationException, ClassNotFoundException,
            IllegalAccessException {
        ServiceLoader<PVFactory> loader = ServiceLoader.load(PVFactory.class);
        for (PVFactory l : loader) {
            if (l.getClass().getName().equals(clazz)) {
                return l;
            }
        }
        for (PVFactory l : loader) {
            if (l.getClass().getSimpleName().equals(clazz)) {
                return l;
            }
        }

        Class<?> cl = Class.forName(clazz);
        if (cl.isInstance(PVFactory.class)) {
            return (PVFactory) cl.newInstance();
        }
        return null;
    }
}
