/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.httputil;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

/**
 * 
 * <code>CookieUtilities</code> provides a set of utilities methods used when handling cookies and single sign on.
 * The utilities include a method that defines if single sign on is used or not, and methods to encode and
 * decode the cookie value.
 * 
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public final class CookieUtilities {

    public static final String SINGLE_SIGN_ON = "rbac.usesinglesignon";
    public static final String DOMAIN = "rbac.cookie.domain";
    private static final boolean USE_SINGLE_SIGN_ON = Boolean.parseBoolean(System.getProperty(SINGLE_SIGN_ON,"true"));
    private static final String DOMAIN_NAME = System.getProperty(DOMAIN,"localhost");
    
    private static final String CHAR_ENCODING = "UTF-8";
    private static final Charset CHARSET = Charset.forName(CHAR_ENCODING);
    
    private CookieUtilities() {
    }
    
    /**
     * Decode the cookie value and create the token ID.
     * 
     * @param cookieVal the cookie value to decode
     * @return the token id
     */
    public static char[] decode(String cookieVal) {
        try {
            byte[] cookie = DatatypeConverter.parseBase64Binary(cookieVal);
            CharBuffer buffer = CHARSET.newDecoder().decode(ByteBuffer.wrap(cookie));
            return Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
        } catch (CharacterCodingException e) {
            return cookieVal.toCharArray();
        }
    }
    
    /**
     * Encode the token id. The encoded value should be given to the cookie.
     * 
     * @param tokenId the value to encode
     * @return the encoded value
     */
    public static String encode(char[] tokenId) {
        try {
            ByteBuffer buffer = CHARSET.newEncoder().encode(CharBuffer.wrap(tokenId));
            byte[] data = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
            return DatatypeConverter.printBase64Binary(data);
        } catch (CharacterCodingException e) {
            return new String(tokenId);
        }
    }
    
    /**
     * Returns true is single sign on is turned on or false if it is off. Single sign on can be turned
     * on by setting the system property {@link #SINGLE_SIGN_ON} to <code>true</code>.
     * 
     * @return true if single sign on should be used or false otherwise
     */
    public static boolean isUseSingleSignOn() {
        return USE_SINGLE_SIGN_ON;
    }
    
    /**
     * Returns the domain name to use on the cookies.
     * 
     * @return the domain name
     */
    public static String getDomainName() {
        return DOMAIN_NAME;
    }
    
}
