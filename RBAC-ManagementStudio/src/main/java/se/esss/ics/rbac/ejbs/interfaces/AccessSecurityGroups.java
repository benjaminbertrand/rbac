/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.pvaccess.AccessSecurityGroup;
import se.esss.ics.rbac.pvaccess.AccessSecurityInput;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule;

/**
 * <code>AccessSecurityGroups</code> interface defines methods for dealing with access security groups.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface AccessSecurityGroups extends Serializable {

    /**
     * Retrieves access security group identified by <code>groupId</code> from database and returns it.
     * 
     * @param groupId unique access security group identifier
     * 
     * @return specific access security group identified by id.
     */
    AccessSecurityGroup getAccessSecurityGroup(int groupId);

    /**
     * @return list of the access security groups retrieved from database.
     */
    List<AccessSecurityGroup> getAccessSecurityGroups();

    /**
     * Retrieves all access security groups whose name matches the <code>wildcard</code> pattern from database and
     * returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of the access security groups whose name matches the wildcard pattern.
     */
    List<AccessSecurityGroup> getAccessSecurityGroupsByWildcard(String wildcard);

    /**
     * Inserts created access security group into database.
     * 
     * @param group created access security group
     */
    void createAccessSecurityGroup(AccessSecurityGroup group);

    /**
     * Updates access security group.
     * 
     * @param group updated access security
     */
    void updateAccessSecurityGroup(AccessSecurityGroup group);

    /**
     * Removes access security group from database.
     * 
     * @param group access security group which will be removed
     */
    void removeAccessSecurityGroup(AccessSecurityGroup group);

    /**
     * Retrieves access security rule identified by <code>ruleId</code> from database and returns it.
     * 
     * @param ruleId unique access security rule identifier
     * 
     * @return specific access security rule identified by id.
     */
    AccessSecurityRule getAccessSecurityRule(int ruleId);

    /**
     * Inserts created access security rule into database.
     * 
     * @param rule created access security rule
     */
    void createAccessSecurityRule(AccessSecurityRule rule);

    /**
     * Updates access security rule.
     * 
     * @param rule updated access security rule
     */
    void updateAccessSecurityRule(AccessSecurityRule rule);

    /**
     * Removes access security rule from database.
     * 
     * @param rule access security rule which will be removed
     */
    void removeAccessSecurityRule(AccessSecurityRule rule);

    /**
     * Retrieves access security input identified by <code>inputId</code> from database and returns it.
     * 
     * @param inputId unique access security input identifier
     * 
     * @return specific access security input identified by id.
     */
    AccessSecurityInput getAccessSecurityInput(int inputId);

    /**
     * Inserts created access security input into database.
     * 
     * @param input created access security input
     */
    void createAccessSecurityInput(AccessSecurityInput input);

    /**
     * Updates all access security inputs.
     * 
     * @param inputsList list with updated security inputs
     */
    void updateAccessSecurityInputs(List<AccessSecurityInput> inputsList);

    /**
     * Removes access security input from database.
     * 
     * @param inputId id of the access security input which will be removed
     */
    void removeAccessSecurityInput(Integer inputId);
}
