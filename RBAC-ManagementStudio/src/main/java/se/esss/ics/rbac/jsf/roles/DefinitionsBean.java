/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.roles;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.ejbs.interfaces.UserRoles;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioProperties;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.users.UsersBean;
import se.esss.ics.rbac.jsf.utils.MailService;
import se.esss.ics.rbac.jsf.utils.SetWithConstructorInput;

/**
 * <code>MatrixBean</code> is a managed bean (<code>rolesDefinitionsBean</code>), which contains roles data and methods
 * for executing actions triggered on <code>/Roles/Definitions</code> page. Definitions bean is view scoped so it lives
 * as long as user interacting with definitions page view.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "rolesDefinitionsBean")
@ViewScoped
public class DefinitionsBean implements Serializable {

    private static final long serialVersionUID = 552008038439757430L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(DefinitionsBean.class);

    @ManagedProperty(value = "#{usersBean}")
    private UsersBean usersBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Roles rolesEJB;
    @EJB
    private UserRoles userRolesEJB;
    @EJB
    private MailService mailServiceEJB;
    @Inject
    private DefinitionsRequestCacheBean requestCacheBean;

    private String wildcard = Constants.ALL;
    private String roleManager;
    private String newRoleName;
    private String newRoleDescription;
    private String editedRoleName;
    private String editedRoleDescription;
    private int roleAssignmentType = 1;
    private Date assignmentStartTime;
    private Date assignmentEndTime;
    private boolean notifyByEmail;
    private int selectedResourceId;
    private Role selectedRole;
    private UserInfo selectedManager;
    private UserRoleInfo selectedAssignee;
    private Permission selectedPermission;
    private List<Resource> resourceList;

    // permissions
    private List<Permission> permissionsSourceList;
    private List<Permission> selectedPermissionsSourceList;
    private List<Permission> permissionsTargetList;
    private List<Permission> selectedPermissionsTargetList;

    // assigness
    private List<UserInfo> assigneesSourceList;
    private List<UserInfo> selectedAssigneesSourceList;
    private List<UserInfo> assigneesTargetList;
    private List<UserInfo> selectedAssigneesTargetList;

    // managers
    private List<UserInfo> managersSourceList;
    private List<UserInfo> selectedManagersSourceList;
    private List<UserInfo> managersTargetList;
    private List<UserInfo> selectedManagersTargetList;

    /**
     * Initialises users dual list model which data are used in add new assignee dialog and in add new manager dialog on
     * <code>/Roles/Definitions</code> page. Also initialise permissions dual list model which data are used in add
     * permissions dialog on the same page. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        initializeUserLists();
        permissionsSourceList = new ArrayList<Permission>();
        selectedPermissionsSourceList = new ArrayList<Permission>();
        permissionsTargetList = new ArrayList<Permission>();
        selectedPermissionsTargetList = new ArrayList<Permission>();
    }

    /**
     * Initialises users dual list model which data are used in add new assignee dialog and in add new manager dialog on
     * <code>/Roles/Definitions</code> page.
     */
    private void initializeUserLists() {
        List<UserInfo> sourceList = null;
        try {
            sourceList = usersBean.getUsersList(Constants.ALL);
        } catch (DirectoryServiceAccessException e) {
            LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
        }
        sourceList = (sourceList == null) ? new ArrayList<UserInfo>() : sourceList;

        assigneesSourceList = new ArrayList<UserInfo>(sourceList);
        selectedAssigneesSourceList = new ArrayList<UserInfo>();
        assigneesTargetList = new ArrayList<UserInfo>();
        selectedAssigneesTargetList = new ArrayList<UserInfo>();

        managersSourceList = new ArrayList<UserInfo>(sourceList);
        selectedManagersSourceList = new ArrayList<UserInfo>();
        managersTargetList = new ArrayList<UserInfo>();
        selectedManagersTargetList = new ArrayList<UserInfo>();
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the bean through which logged in user info is retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets users bean, which holds informations about users from LDAP.
     *
     * @param usersBean managed bean with informations about users
     */
    public void setUsersBean(UsersBean usersBean) {
        this.usersBean = usersBean;
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     *
     * @param permissionsBean the bean through which the access permissions are checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * @return wildcard pattern which is used for searching by roles. Pattern is entered into search field on
     *         <code>/Roles/Definitions</code> page.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * Sets wildcard pattern which is used for searching by roles. Pattern is entered into search field on
     * <code>/Roles/Definitions</code> page.
     *
     * @param wildcard wildcard pattern
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * @return selected resource id retrieved from list box component, from add new permission dialog, on
     *         <code>/Roles/Definitions</code> page.
     */
    public int getSelectedResourceId() {
        return selectedResourceId;
    }

    /**
     * Sets selected resource id retrieved from list box component, from add new permission dialog, on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedResourceId selected resource identifier
     */
    public void setSelectedResourceId(int selectedResourceId) {
        this.selectedResourceId = selectedResourceId;
    }

    /**
     * Returns role which is selected in roles list on <code>/Roles/Definitions</code> page.
     *
     * @return role selected in roles list.
     */
    public Role getSelectedRole() {
        return selectedRole;
    }

    /**
     * Sets role which is selected in roles list on <code>/Roles/Definitions</code> page.
     *
     * @param selectedRole role selected in roles list
     */
    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * @return role assignee who is selected in assignees list on <code>/Roles/Definitions</code> page.
     */
    public UserRoleInfo getSelectedAssignee() {
        return selectedAssignee;
    }

    /**
     * Sets role assignee who is selected in assignees list on <code>/Roles/Definitions</code> page.
     *
     * @param selectedAssignee selected role assignee
     */
    public void setSelectedAssignee(UserRoleInfo selectedAssignee) {
        this.selectedAssignee = selectedAssignee;
    }

    /**
     * @return role manager who is selected in managers list on <code>/Roles/Definitions</code> page.
     */
    public UserInfo getSelectedManager() {
        return selectedManager;
    }

    /**
     * Sets role manager who is selected in managers list on <code>/Roles/Definitions</code> page.
     *
     * @param selectedManager selected role manager
     */
    public void setSelectedManager(UserInfo selectedManager) {
        this.selectedManager = selectedManager;
    }

    /**
     * @return selected permission which is selected in assigned permissions table on <code>/Roles/Definitions</code>
     *         page.
     */
    public Permission getSelectedPermission() {
        return selectedPermission;
    }

    /**
     * Sets permission which is selected in assigned permissions table on <code>/Roles/Definitions</code> page.
     *
     * @param selectedPermission selected permission
     */
    public void setSelectedPermission(Permission selectedPermission) {
        this.selectedPermission = selectedPermission;
    }

    /**
     * @return selected role name which is shown on <code>/Roles/Definitions</code> page in role name field.
     */
    public String getRoleName() {
        if (selectedRole != null) {
            return selectedRole.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return new role name which is entered into role name field in add new role dialog on
     *         <code>/Roles/Definitions</code> page.
     */
    public String getNewRoleName() {
        return newRoleName;
    }

    /**
     * Sets new role name which is entered into role name field in add new role dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param newRoleName new role name
     */
    public void setNewRoleName(String newRoleName) {
        this.newRoleName = newRoleName;
    }

    /**
     * @return edited role name which is entered into role name field in edit role info dialog on
     *         <code>/Roles/Definitions</code> page.
     */
    public String getEditedRoleName() {
        return getRoleName();
    }

    /**
     * Sets edited role name which is entered into role name field in edit role info dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param editedRoleName edited role name
     */
    public void setEditedRoleName(String editedRoleName) {
        this.editedRoleName = editedRoleName;
    }

    /**
     * @return selected role description which is shown on <code>/Roles/Definitions</code> page in role description
     *         field.
     */
    public String getRoleDescription() {
        if (selectedRole != null) {
            return selectedRole.getDescription() == null ? Constants.EMPTY_STRING : selectedRole.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return new role description which is entered into new role description field in add new role dialog on
     *         <code>/Roles/Definitions</code> page.
     */
    public String getNewRoleDescription() {
        return newRoleDescription;
    }

    /**
     * Sets new role description which is entered into role description field in add new role dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param newRoleDescription new role description
     */
    public void setNewRoleDescription(String newRoleDescription) {
        this.newRoleDescription = newRoleDescription;
    }

    /**
     * @return edited role description which is entered into role description field in edit role info dialog on
     *         <code>/Roles/Definitions</code> page.
     */
    public String getEditedRoleDescription() {
        return getRoleDescription();
    }

    /**
     * Sets edited role description which is entered into role description field in edit role info dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param editedRoleDescription edited role description
     */
    public void setEditedRoleDescription(String editedRoleDescription) {
        this.editedRoleDescription = editedRoleDescription;
    }

    /**
     * Returns selected role assignment type as <code>Integer</code>.
     *
     * <ul>
     * <li>1 - <code>AssignmentType.NORMAL</code></li>
     * <li>2 - <code>AssignmentType.LIMITED</code></li>
     * <li>else - <code>AssignmentType.DELEGATED</code></li>
     * </ul>
     *
     * @return selected role assignment type represented as integer.
     */
    public int getRoleAssignmentType() {
        return roleAssignmentType;
    }

    /**
     * Sets role assignment type as <code>Integer</code>.
     *
     * <ul>
     * <li>1 - <code>AssignmentType.NORMAL</code></li>
     * <li>2 - <code>AssignmentType.LIMITED</code></li>
     * <li>else - <code>AssignmentType.DELEGATED</code></li>
     * </ul>
     *
     * @param assignmentType role assignment type
     */
    public void setRoleAssignmentType(int assignmentType) {
        this.roleAssignmentType = assignmentType;
    }

    /**
     * @return assigned role start time. After this time role became active. Assignment start time is entered in add new
     *         assignee dialog on <code>/Roles/Definitions</code> page.
     */
    public Date getAssignmentStartTime() {
        return assignmentStartTime;
    }

    /**
     * Sets assigned role start time. After this time role became active. Assignment start time is entered in add new
     * assignee dialog on <code>/Roles/Definitions</code> page.
     *
     * @param startTime assigned role start time.
     */
    public void setAssignmentStartTime(Date startTime) {
        this.assignmentStartTime = startTime;
    }

    /**
     * @return assigned role end time. After this time role will not be active anymore. Assignment end time is entered
     *         add new assignee dialog on <code>/Roles/Definitions</code> page.
     */
    public Date getAssignmentEndTime() {
        return assignmentEndTime;
    }

    /**
     * Sets assigned role end time. After this time role will not be active anymore. Assignment end time is entered in
     * add new assignee dialog on <code>/Roles/Definitions</code> page.
     *
     * @param endTime assigned role end time
     */
    public void setAssignmentEndTime(Date endTime) {
        this.assignmentEndTime = endTime;
    }

    /**
     * @return value of the flag which tells if user should be notified by email about his new role.
     */
    public boolean isNotifyByEmail() {
        return notifyByEmail;
    }

    /**
     * Sets flag which tells if user should be notified by email about his new role. Value is retrieved from notify by
     * email checkbox which is placed in add new assignee on <code>/Roles/Definitions</code> page.
     *
     * @param notifyByEmail if true user will be notified by email otherwise not
     */
    public void setNotifyByEmail(boolean notifyByEmail) {
        this.notifyByEmail = notifyByEmail;
    }

    /**
     * @return list of roles retrieved from database, which corresponds to the wildcard pattern. Roles are shown in list
     *         box on <code>/Roles/Definitions</code> page.
     */
    public List<Role> getRolesList() {
        List<Role> roles = fetchRoles(false);
        if (!Util.contains(roles, selectedRole)) {
            selectedRole = null;
        }
        return roles;
    }

    /**
     * Retrieves and returns list of selected role assignees. Assignees are shown in assignees list on
     * <code>/Roles/Definitions</code> table.
     *
     * @return selected role assignees list.
     */
    public List<UserRoleInfo> getAssigneesList() {
        if (selectedRole != null && selectedRole.getUsers() != null) {
            try {
                List<UserRoleInfo> usersInfo = new ArrayList<>();
                Date now = new Date();
                for (UserRole userRole : selectedRole.getUsers()) {
                    if (userRole.getAssignment() == AssignmentType.NORMAL || userRole.getEndTime().after(now)) {
                        UserInfo info = usersBean.getUser(userRole.getUserId());
                        if (info == null || usersInfo.contains(info)) {
                            continue;
                        }
                        usersInfo.add(new UserRoleInfo(info, userRole.getAssignment()));
                    }
                }
                Collections.sort(usersInfo);
                return usersInfo;
            } catch (DirectoryServiceAccessException e) {
                LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
            }
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves and returns list of selected role managers. Managers are shown in managers list on
     * <code>/Roles/Definitions</code> table.
     *
     * @return selected role managers list.
     */
    public List<UserInfo> getManagersList() {
        if (selectedRole != null && selectedRole.getManagers() != null) {
            try {
                List<UserInfo> usersInfo = new ArrayList<>();
                for (String username : selectedRole.getManagers()) {
                    UserInfo info = usersBean.getUser(username);
                    if (info == null || usersInfo.contains(info)) {
                        continue;
                    }
                    usersInfo.add(info);
                }
                Collections.sort(usersInfo);
                return usersInfo;
            } catch (DirectoryServiceAccessException e) {
                LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
            }
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves and returns list of selected role permissions. Permissions are shown in assigned permissions table on
     * <code>/Roles/Definitions</code> page.
     *
     * @return selected role permissions list.
     */
    public List<Permission> getAssignedPermissionsList() {
        if (selectedRole != null) {
            List<Permission> p = new ArrayList<>(selectedRole.getPermissions());
            Collections.sort(p);
            return p;
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves and returns list of resources from database. Resources are shown in drop down list component in select
     * permissions dialog on <code>/Roles/Definitions</code> page.
     *
     * @return list of resources retrieved from database.
     */
    public List<Resource> getResourcesList() {
        resourceList = requestCacheBean.getResources();
        return resourceList;
    }

    /**
     * @return manager of the new role. Manager of the new role is logged in user who creates new role.
     */
    public String getRoleManager() {
        if (roleManager == null) {
            Token t = loginBean.getRbacAccessEJB().getToken();
            if (t != null) {
                roleManager = t.getUsername();
            }
        }
        return roleManager;
    }

    /**
     * Method is called when row in the permissions data table is selected.
     *
     * @param event select event
     */
    public void onPermissionRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof Permission) {
            selectedPermission = (Permission) event.getObject();
        }
    }

    /**
     * Method is called when row in the permissions data table is unselected.
     *
     * @param event deselect event
     */
    public void onPermissionRowUnselect(UnselectEvent event) {
        selectedPermission = null;
    }

    /**
     * Creates new role if logged in user has permission for adding roles. Method is called when OK button, in add new
     * role dialog on <code>/Roles/Definitions</code> page, is clicked. Results of the action are logged and shown to
     * the user as growl message.
     */
    public void createRole() {
        if (!permissionsBean.canAddRole(false)) {
            return;
        }
        final Role role = new Role();
        role.setName(newRoleName);
        role.setDescription(newRoleDescription);
        role.setManagers(new SetWithConstructorInput<>(roleManager));
        role.setPermissions(new HashSet<Permission>());
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_CREATION, loginBean.getUsername(), role.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_CREATION, loginBean.getUsername(),
                role.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rolesEJB.createRole(role);
                selectedRole = rolesEJB.getRoleByName(newRoleName);
                fetchRoles(true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected role if logged in user has permission for removing roles. Method is called when remove role
     * button on <code>/Roles/Definitions</code> page is clicked and when action is confirmed. Results of the action are
     * logged and shown to the user as growl message.
     */
    public void removeRole() {
        if (!permissionsBean.canRemoveRole(false) || isRemoveRoleButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_REMOVAL, loginBean.getUsername(),
                selectedRole.getName());
        final String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_REMOVAL, loginBean.getUsername(),
                selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                try {
                    rolesEJB.removeRole(selectedRole.getId());
                    selectedRole = null;
                    fetchRoles(true);
                } catch (Exception e) {
                    Throwable cause = e.getCause();
                    Throwable up = e;
                    while (cause != null) {
                        up = cause;
                        cause = cause.getCause();
                    }
                    String message = up.getMessage();
                    if (message == null) {
                        throw e;
                    }
                    if (message.contains("foreign key")) {
                        Role role = rolesEJB.getRole(selectedRole.getId(), true);
                        Set<String> set = new HashSet<>(role.getRules().size());
                        for (AccessSecurityRule r : role.getRules()) {
                            set.add(r.getASGroup().getName());
                        }
                        StringBuilder sb = new StringBuilder(role.getRules().size() * 20 + 30);
                        sb.append("The following access security groups are linked to this role: ");
                        for (String s : set) {
                            sb.append(s).append(", ");
                        }
                        taskBean.showFacesMessage(FacesMessage.SEVERITY_WARN, failure, sb.substring(0, sb.length() - 2)
                                + ".", false);
                    }
                    if (up instanceof Exception) {
                        throw (Exception) up;
                    } else {
                        throw new Exception(up);
                    }
                }
            }
        });
    }

    /**
     * Updates selected role if logged in user has permission for managing roles. Method is called when OK button, in
     * edit role info dialog on <code>/Roles/Definitions</code> page, is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void updateRole() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isEditRoleInfoButtonDisabled()) {
            return;
        }
        selectedRole.setName(editedRoleName);
        selectedRole.setDescription(editedRoleDescription);
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_UPDATE, loginBean.getUsername(),
                selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_UPDATE, loginBean.getUsername(),
                selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rolesEJB.updateRole(selectedRole);
                fetchRoles(true);
            }
        });
    }

    /**
     * Adds role manager to the selected role if logged in user has permission for managing roles. Method is called when
     * OK button, in add new manager dialog on <code>/Roles/Definitions</code> page, is clicked. Results of the action
     * are logged and shown to the user as growl message.
     */
    public void addRoleManagers() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isAddManagerButtonDisabled()
                || managersTargetList.isEmpty()) {
            return;
        }
        Set<String> managers = new HashSet<>(selectedRole.getManagers());
        for (UserInfo manager : managersTargetList) {
            managers.add(manager.getUsername());
        }
        selectedRole.setManagers(managers);
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_MANAGERS_ASSIGNMENT, loginBean.getUsername(),
                selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_MANAGERS_ASSIGNMENT, loginBean.getUsername(),
                selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rolesEJB.updateRole(selectedRole);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes manager from the selected role if logged in user has permission for managing resources. Method is called
     * when remove manager button on <code>/Roles/Definitions</code> page is clicked and when action is confirmed.
     * Results of the action are logged and shown to the user as growl message.
     */
    public void removeRoleManager() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isRemoveManagerButtonDisabled()) {
            return;
        }
        selectedRole.getManagers().remove(selectedManager.getUsername());
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_MANAGER_REMOVAL, loginBean.getUsername(),
                selectedManager.getUsername(), selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_MANAGER_REMOVAL, loginBean.getUsername(),
                selectedManager.getUsername(), selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rolesEJB.updateRole(selectedRole);
                selectedManager = null;
            }
        });
    }

    /**
     * Removes assignee from the selected role if logged in user has permission for managing roles. Method is called
     * when remove assignee button on <code>/Roles/Definitions</code> page is clicked and when action is confirmed.
     * Results of the action are logged and shown to the user as growl message.
     */
    public void removeRoleAssignee() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isRemoveAssigneeButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_ASSIGNMENT_REMOVAL, loginBean.getUsername(),
                selectedAssignee.getUser().getUsername(), selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_ASSIGNMENT_REMOVAL, loginBean.getUsername(),
                selectedAssignee.getUser().getUsername(), selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                for (UserRole userRole : selectedRole.getUsers()) {
                    if (userRole.getUserId().equals(selectedAssignee.getUser().getUsername())) {
                        userRolesEJB.removeUserRole(userRole);
                    }
                }
                selectedRole = rolesEJB.getRole(selectedRole.getId(), false);
                selectedAssignee = null;
            }
        });
    }

    /**
     * Removes permission from the selected role if logged in user has permission for managing resources. Method is
     * called when remove permission button on <code>/Roles/Definitions</code> page is clicked and when action is
     * confirmed. Results of the action are logged and shown to the user as growl message.
     */
    public void removeRolePermission() {
        if (!permissionsBean.canManageResource(selectedPermission.getResource(), false)
                || isRemovePermissionButtonDisabled()) {
            return;
        }
        selectedRole.getPermissions().remove(selectedPermission);
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rolesEJB.updateRole(selectedRole);
                selectedPermission = null;
            }
        });
    }

    /**
     * Assigns permissions to the selected role if logged in user has permission for managing the resource. Method is
     * called when OK button, in select permissions dialog on <code>/Roles/Definitions</code> page, is clicked. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void addRolePermissions() {
        if (!isAddPermissionButtonDisabled() && !permissionsTargetList.isEmpty()) {
            List<Permission> permissions = permissionsTargetList;
            Set<Resource> resources = new HashSet<>();
            for (Permission p : permissions) {
                resources.add(p.getResource());
            }
            // check if you have permission to manage the resource
            for (Resource r : resources) {
                if (!permissionsBean.canManageResource(r, false)) {
                    FacesContext.getCurrentInstance().addMessage(
                            null,
                            new FacesMessage(FacesMessage.SEVERITY_WARN, "",
                                    "You do not have permission to manage resource " + r.getName() + "."));
                    return;
                }
            }
            Set<Permission> currentPermissions = new HashSet<>(selectedRole.getPermissions());
            currentPermissions.addAll(permissionsTargetList);
            selectedRole.setPermissions(currentPermissions);
            String success = Messages.getString(Messages.SUCCESSFUL_ROLE_PERMISSIONS_ASSIGNMENT,
                    loginBean.getUsername(), selectedRole.getName());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_PERMISSIONS_ASSIGNMENT,
                    loginBean.getUsername(), selectedRole.getName());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    rolesEJB.updateRole(selectedRole);
                }
            });
        }
        setDefaultValues();
    }

    /**
     * Assigns selected role to selected users if logged in user has permission for managing roles. Method is called
     * when OK button, in add new assignee dialog on <code>/Roles/Definitions</code> page, is clicked. Results of the
     * action are logged and shown to the user as growl message.
     */
    public void addAssignees() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isAddAssigneeButtonDisabled()) {
            return;
        }
        final List<UserRole> userRoles = createUserRoles();
        if (userRoles.isEmpty()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_ASSIGNMENTS, loginBean.getUsername(),
                selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_ASSIGNMENTS, loginBean.getUsername(),
                selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                userRolesEJB.createUserRoles(userRoles);
                selectedRole.getUsers().addAll(userRoles);
                sendEmailToNewAssignees(userRoles, notifyByEmail);
            }
        });
        setDefaultValues();
    }

    /**
     * Selects the unique resource identifier of the first resource in the list. List of the resources is in select
     * permissions dialog on <code>/Roles/Definitions</code> page. Resource identifier is used for permissions
     * retrieval.
     */
    public void selectResourceId() {
        if (resourceList != null && !resourceList.isEmpty()) {
            selectedResourceId = resourceList.get(0).getId();
        }
    }

    /**
     * Checks whether the logged in user can manage the resource selected from the list in select permissions dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return true if the logged in user can manage the resource selected in the list.
     */
    public boolean isUserManagerOfSelectedResource() {
        Resource selectedResource = null;
        for (Resource resource : getResourcesList()) {
            if (resource.getId() == selectedResourceId) {
                selectedResource = resource;
                break;
            }
        }
        if (selectedResource == null) {
            return false;
        }
        return permissionsBean.canManageResource(selectedResource, true);
    }

    /**
     * Remove role button on <code>/Roles/Definitions</code> page is disabled if role is not selected.
     *
     * @return true if remove role button is disabled, otherwise false.
     */
    public boolean isRemoveRoleButtonDisabled() {
        return selectedRole == null;
    }

    /**
     * Edit role info button on <code>/Roles/Definitions</code> page is disabled if role is not selected.
     *
     * @return true if edit role info button is disabled, otherwise false.
     */
    public boolean isEditRoleInfoButtonDisabled() {
        return selectedRole == null;
    }

    /**
     * Add manager button on <code>/Roles/Definitions</code> page is disabled if manager is not selected.
     *
     * @return true if add manager button is disabled, otherwise false.
     */
    public boolean isAddManagerButtonDisabled() {
        return selectedRole == null;
    }

    /**
     * Remove manager button on <code>/Roles/Definitions</code> page is disabled if role is not selected or if manager
     * is not selected.
     *
     * @return true if remove manager button is disabled, otherwise false.
     */
    public boolean isRemoveManagerButtonDisabled() {
        return selectedRole == null || selectedManager == null;
    }

    /**
     * Add assignee button on <code>/Roles/Definitions</code> page is disabled if role is not selected.
     *
     * @return true if add assignee button is disabled, otherwise false.
     */
    public boolean isAddAssigneeButtonDisabled() {
        return selectedRole == null;
    }

    /**
     * Remove assignee button on <code>/Roles/Definitions</code> page is disabled if role is not selected or if assignee
     * is not selected.
     *
     * @return true if remove assignee button is disabled, otherwise false.
     */
    public boolean isRemoveAssigneeButtonDisabled() {
        return selectedRole == null || selectedAssignee == null;
    }

    /**
     * Add permission button on <code>/Roles/Definitions</code> page is disabled if role is not selected or if user is
     * not logged in.
     *
     * @return true if add permission button is disabled, otherwise false.
     */
    public boolean isAddPermissionButtonDisabled() {
        return selectedRole == null || !loginBean.isLoggedIn();
    }

    /**
     * Remove permission button on <code>/Roles/Definitions</code> page is disabled if role is not selected or if
     * permission in assigned permissions table is not selected.
     *
     * @return true if remove permission button is disabled, otherwise false.
     */
    public boolean isRemovePermissionButtonDisabled() {
        return selectedRole == null || selectedPermission == null;
    }

    /**
     * Notify buttons in dialogs on <code>/Users/Definitions</code> page are disabled if <code>KEY_MAIL_NOTIFY</code>
     * property is false. Property is read from <code>mail.properties</code> file.
     *
     * @return true if notify buttons (checkboxes) are disabled, otherwise false.
     */
    public boolean isNotifyButtonDisabled() {
        return !ManagementStudioProperties.isMailNotificationEnabled();
    }

    /**
     * Method that shows dialog for adding permissions to the role. Dialog is shown when add permission button on
     * <code>/Roles/Definitions</code> page is clicked.
     */
    public void showAddPermissionsDialog() {
        if (isAddPermissionButtonDisabled() || !loginBean.isLoggedInValidated()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addPermissionsDialog').show()");
    }

    /**
     * Method that shows dialog for editing roles. Dialog is shown when edit role info button on
     * <code>/Roles/Definitions</code> page is clicked and if user has permissions for managing roles.
     */
    public void showEditRoleInfoDialog() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isEditRoleInfoButtonDisabled()) {
            return;
        }
        RequestContext.getCurrentInstance().update("editDescriptionForm");
        RequestContext.getCurrentInstance().execute("PF('editRoleInfoDialog').show()");
    }

    /**
     * Method that shows dialog for adding managers to the role. Dialog is shown when add manager button on
     * <code>/Roles/Definitions</code> page is clicked and if user has permissions for managing roles.
     */
    public void showAddManagersDialog() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isAddManagerButtonDisabled()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addManagersDialog').show()");
    }

    /**
     * Method that shows dialog for adding new assignees to the role. Dialog is shown when add assignee button on
     * <code>/Roles/Definitions</code> page is clicked and if user has permissions for managing roles.
     */
    public void showAddNewAssigneeDialog() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isAddAssigneeButtonDisabled()) {
            return;
        }
        RequestContext.getCurrentInstance().update("addNewAssigneeForm");
        RequestContext.getCurrentInstance().execute("PF('addNewAssigneeDialog').show()");
    }

    /**
     * Method that shows dialog for adding new role. Dialog is shown when add role button on
     * <code>/Roles/Definitions</code> page is clicked and if user has permissions for adding roles.
     */
    public void showAddNewRoleDialog() {
        if (!permissionsBean.canAddRole(false)) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addNewRoleDialog').show()");
    }

    /**
     * Method that shows warning message if user checks notify check box in add assignees dialog on
     * <code>/Roles/Definition</code> page.
     *
     * @param e event which is fired if user checks notify check box
     */
    public void showWarning(AjaxBehaviorEvent e) {
        if (notifyByEmail) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Sending emails may take a while.",
                            Constants.EMPTY_STRING));
        }
    }

    /**
     * Resets all values used in dialogs on <code>/Roles/Definitions</code> page to the default ones, cleans dialogs
     * fields.
     */
    public void setDefaultValues() {
        newRoleName = Constants.EMPTY_STRING;
        newRoleDescription = Constants.EMPTY_STRING;
        roleAssignmentType = 1;
        assignmentStartTime = null;
        assignmentEndTime = null;
        notifyByEmail = false;
        selectedResourceId = -1;
        initializeUserLists();
        permissionsSourceList = new ArrayList<Permission>();
        permissionsTargetList = new ArrayList<Permission>();
        selectedPermissionsSourceList = new ArrayList<Permission>();
        selectedPermissionsTargetList = new ArrayList<Permission>();
    }

    /**
     * Creates and returns list of user roles. Each user role in the list contains data entered add new assignee dialog
     * on <code>/Roles/Definitions</code> page.
     *
     * @return list of created user roles.
     */
    private List<UserRole> createUserRoles() {
        List<UserRole> userRoles = new ArrayList<>();
        AssignmentType assignment = (roleAssignmentType == 1) ? AssignmentType.NORMAL : AssignmentType.LIMITED;
        Timestamp startTime = null;
        Timestamp endTime = null;
        if (assignment == AssignmentType.LIMITED) {
            if (areDatesInvalid()) {
                taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.INVALID_DATES),
                        Constants.EMPTY_STRING, false);
                return userRoles;
            }
            startTime = new Timestamp(assignmentStartTime.getTime());
            endTime = new Timestamp(assignmentEndTime.getTime());
        }
        List<String> users = userRolesEJB.getUsernames(selectedRole.getId());
        for (UserInfo user : assigneesTargetList) {
            if (!users.contains(user.getUsername())) {
                UserRole userRole = new UserRole();
                userRole.setUserId(user.getUsername());
                userRole.setRole(selectedRole);
                userRole.setAssignment(assignment);
                userRole.setStartTime(startTime);
                userRole.setEndTime(endTime);
                userRoles.add(userRole);
            }
        }
        return userRoles;
    }

    /**
     * Sends email to the all new role assignees. Email is send if the flag <code>notify</code> is true.
     *
     * @param assignees new role assignees
     * @param notify if true email is send otherwise false
     * @throws DirectoryServiceAccessException
     */
    private void sendEmailToNewAssignees(List<UserRole> assignees, boolean notify)
            throws DirectoryServiceAccessException {
        for (UserRole ur : assignees) {
            sendEmail(usersBean.getUser(ur.getUserId()).getEMail(), ur, false, notify);
        }
    }

    /**
     * Generates email, about role assignment or about role changes, and send it through <code>SendEmailService</code>.
     * Mail is send successfully if <code>send</code> flag is true and if <code>to</code> field is valid.
     *
     * @param to to whom email is sent
     * @param userRole informations about role assignment
     * @param edit parameter is true if role was edited, otherwise false
     * @param send parameter is true if email should be send, otherwise false
     */
    private void sendEmail(String to, UserRole userRole, boolean edit, boolean send) {
        if (send) {
            String[] emailData = mailServiceEJB.generateDefaultContent(userRole, edit);
            mailServiceEJB.send(to, emailData[0], emailData[1]);
        }
    }

    /**
     * Checks if the end time is greater than start time and if end time is in the future. If both conditions are met,
     * dates are valid, otherwise they are invalid.
     *
     * @return true if dates are invalid or false if valid
     */
    private boolean areDatesInvalid() {
        return assignmentEndTime.getTime() < System.currentTimeMillis()
                || assignmentEndTime.getTime() < assignmentStartTime.getTime();
    }

    /**
     * Returns list which contains users from source list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains users from source list.
     */
    public List<UserInfo> getManagersSourceList() {
        return managersSourceList;
    }

    /**
     * Sets list which contains users from source list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param managersSourceList list which contains roles users source list
     */
    public void setManagersSourceList(List<UserInfo> managersSourceList) {
        this.managersSourceList = managersSourceList;
    }

    /**
     * Returns list which contains users from target list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains users from target list.
     */
    public List<UserInfo> getManagersTargetList() {
        return managersTargetList;
    }

    /**
     * Sets list which contains users from target list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param managersTargetList list which contains users from target list
     */
    public void setManagersTargetList(List<UserInfo> managersTargetList) {
        this.managersTargetList = managersTargetList;
    }

    /**
     * Returns list which contains selected users from source list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains selected users from source list.
     */
    public List<UserInfo> getSelectedManagersSourceList() {
        return selectedManagersSourceList;
    }

    /**
     * Sets list which contains selected users from source list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedManagersSourceList list which contains selected users from source list
     */
    public void setSelectedManagersSourceList(List<UserInfo> selectedManagersSourceList) {
        this.selectedManagersSourceList = selectedManagersSourceList;
    }

    /**
     * Returns list which contains selected users from target list and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains selected users from target list.
     */
    public List<UserInfo> getSelectedManagersTargetList() {
        return selectedManagersTargetList;
    }

    /**
     * Sets list which contains selected users from target list, and is shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedManagersTargetList list which contains selected users from target list
     */
    public void setSelectedManagersTargetList(List<UserInfo> selectedManagersTargetList) {
        this.selectedManagersTargetList = selectedManagersTargetList;
    }

    /**
     * Moves selected users from source list to the target list. Lists are shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addManagersFromSourceToTarget() {
        if (!selectedManagersSourceList.isEmpty()) {
            managersTargetList.addAll(selectedManagersSourceList);
        }
        managersSourceList.removeAll(selectedManagersSourceList);
        selectedManagersSourceList.clear();
    }

    /**
     * Moves selected users from target list to the source list. Lists are shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addManagersFromTargetToSource() {
        if (!selectedManagersTargetList.isEmpty()) {
            managersSourceList.addAll(selectedManagersTargetList);
        }
        managersTargetList.removeAll(selectedManagersTargetList);
        selectedManagersTargetList.clear();
    }

    /**
     * Moves all users from source list to the target list. Lists are shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAllManagersFromSourceToTarget() {
        if (!managersSourceList.isEmpty()) {
            managersTargetList.addAll(managersSourceList);
        }
        managersSourceList.clear();
        selectedManagersSourceList.clear();
    }

    /**
     * Moves all users from target list to the source list. Lists are shown on add managers dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAllManagersFromTargetToSource() {
        if (!managersTargetList.isEmpty()) {
            managersSourceList.addAll(managersTargetList);
        }
        managersTargetList.clear();
        selectedManagersTargetList.clear();
    }

    /**
     * Returns list which contains users from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains users from source list.
     */
    public List<UserInfo> getAssigneesSourceList() {
        return assigneesSourceList;
    }

    /**
     * Sets list which contains users from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param assigneesSourceList list which contains users from source list
     */
    public void setAssigneesSourceList(List<UserInfo> assigneesSourceList) {
        this.assigneesSourceList = assigneesSourceList;
    }

    /**
     * Returns list which contains users from target list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains users from target list.
     */
    public List<UserInfo> getAssigneesTargetList() {
        return assigneesTargetList;
    }

    /**
     * Sets list which contains permissions from target list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param assigneesTargetList list which contains permissions from target list
     */
    public void setAssigneesTargetList(List<UserInfo> assigneesTargetList) {
        this.assigneesTargetList = assigneesTargetList;
    }

    /**
     * Returns list which contains selected users from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains selected users from source list.
     */
    public List<UserInfo> getSelectedAssigneesSourceList() {
        return selectedAssigneesSourceList;
    }

    /**
     * Sets list which contains selected users from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedAssigneesSourceList list which contains selected users from source list
     */
    public void setSelectedAssigneesSourceList(List<UserInfo> selectedAssigneesSourceList) {
        this.selectedAssigneesSourceList = selectedAssigneesSourceList;
    }

    /**
     * Returns list which contains selected users from target list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains selected users from target list.
     */
    public List<UserInfo> getSelectedAssigneesTargetList() {
        return selectedAssigneesTargetList;
    }

    /**
     * Sets list which contains selected users from target list, and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedAssigneesTargetList list which contains selected users from target list
     */
    public void setSelectedAssigneesTargetList(List<UserInfo> selectedAssigneesTargetList) {
        this.selectedAssigneesTargetList = selectedAssigneesTargetList;
    }

    /**
     * Moves selected users from source list to the target list. Lists are shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAssigneesFromSourceToTarget() {
        if (!selectedAssigneesSourceList.isEmpty()) {
            assigneesTargetList.addAll(selectedAssigneesSourceList);
        }
        assigneesSourceList.removeAll(selectedAssigneesSourceList);
        selectedAssigneesSourceList.clear();
    }

    /**
     * Moves selected users from target list to the source list. Lists are shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAssigneesFromTargetToSource() {
        if (!selectedAssigneesTargetList.isEmpty()) {
            assigneesSourceList.addAll(selectedAssigneesTargetList);
        }
        assigneesTargetList.removeAll(selectedAssigneesTargetList);
        selectedAssigneesTargetList.clear();
    }

    /**
     * Moves all users from source list to the target list. Lists are shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAllAssigneesFromSourceToTarget() {
        if (!assigneesSourceList.isEmpty()) {
            assigneesTargetList.addAll(assigneesSourceList);
        }
        assigneesSourceList.clear();
        selectedAssigneesSourceList.clear();
    }

    /**
     * Moves all users from target list to the source list. Lists are shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAllAssigneesFromTargetToSource() {
        if (!assigneesTargetList.isEmpty()) {
            assigneesSourceList.addAll(assigneesTargetList);
        }
        assigneesTargetList.clear();
        selectedAssigneesTargetList.clear();
    }

    /**
     * Returns list which contains permissions from source list and is shown on add permissions dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains permissions from source list.
     */
    public List<Permission> getPermissionsSourceList() {
        if (selectedResourceId == -1) {
            permissionsSourceList = new ArrayList<Permission>();
        } else {
            permissionsSourceList = new ArrayList<Permission>(
                    requestCacheBean.getPermissionsByResource(selectedResourceId));
            permissionsSourceList.removeAll(permissionsTargetList);
        }
        return permissionsSourceList;
    }

    /**
     * Sets list which contains permissions from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param permissionsSourceList list which contains permissions from source list
     */
    public void setPermissionsSourceList(List<Permission> permissionsSourceList) {
        this.permissionsSourceList = permissionsSourceList;
    }

    /**
     * Returns list which contains permissions from target list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains permissions from target list.
     */
    public List<Permission> getPermissionsTargetList() {
        return permissionsTargetList;
    }

    /**
     * Sets list which contains permissions from target list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param permissionsTargetList list which contains permissions from target list
     */
    public void setPermissionsTargetList(List<Permission> permissionsTargetList) {
        this.permissionsTargetList = permissionsTargetList;
    }

    /**
     * Returns list which contains selected permissions from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains selected permissions from source list.
     */
    public List<Permission> getSelectedPermissionsSourceList() {
        return selectedPermissionsSourceList;
    }

    /**
     * Sets list which contains selected permissions from source list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedPermissionsSourceList list which contains selected permissions from source list
     */
    public void setSelectedPermissionsSourceList(List<Permission> selectedPermissionsSourceList) {
        this.selectedPermissionsSourceList = selectedPermissionsSourceList;
    }

    /**
     * Returns list which contains selected permissions from target list and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @return list which contains selected permissions from target list.
     */
    public List<Permission> getSelectedPermissionsTargetList() {
        return selectedPermissionsTargetList;
    }

    /**
     * Sets list which contains selected permissions from target list, and is shown on add assignees dialog on
     * <code>/Roles/Definitions</code> page.
     *
     * @param selectedPermissionsTargetList list which contains selected permissions from target list
     */
    public void setSelectedPermissionsTargetList(List<Permission> selectedPermissionsTargetList) {
        this.selectedPermissionsTargetList = selectedPermissionsTargetList;
    }

    /**
     * Moves selected permissions from source list to the target list. Lists are shown on add permissions dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addPermissionsFromSourceToTarget() {
        if (!selectedPermissionsSourceList.isEmpty()) {
            permissionsTargetList.addAll(selectedPermissionsSourceList);
        }
        permissionsSourceList.removeAll(selectedPermissionsSourceList);
        selectedPermissionsSourceList.clear();
    }

    /**
     * Moves selected permissions from target list to the source list. Lists are shown on add permissions dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addPermissionsFromTargetToSource() {
        if (!selectedPermissionsTargetList.isEmpty()) {
            permissionsSourceList.addAll(selectedPermissionsTargetList);
        }
        permissionsTargetList.removeAll(selectedPermissionsTargetList);
        selectedPermissionsTargetList.clear();
    }

    /**
     * Moves all permissions from source list to the target list. Lists are shown on add permissions dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAllPermissionsFromSourceToTarget() {
        if (!permissionsSourceList.isEmpty()) {
            permissionsTargetList.addAll(permissionsSourceList);
        }
        permissionsSourceList.clear();
        selectedPermissionsSourceList.clear();
    }

    /**
     * Moves all permissions from target list to the source list. Lists are shown on add permissions dialog on
     * <code>/Roles/Definitions</code> page.
     */
    public void addAllPermissionsFromTargetToSource() {
        if (!permissionsTargetList.isEmpty()) {
            permissionsSourceList.addAll(permissionsTargetList);
        }
        permissionsTargetList.clear();
        selectedPermissionsTargetList.clear();
    }

    private List<Role> fetchRoles(boolean fromDB) {
        List<Role> roles = null;
        if (Constants.ALL.equals(wildcard)) {
            roles = requestCacheBean.getRoles(fromDB);
        } else {
            roles = requestCacheBean.getRolesByWildcard(wildcard + Constants.ALL_DB, fromDB);
        }
        return roles;
    }
}
