/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>OverviewBean</code> is a managed bean (<code>permissionsOverviewBean</code>), which contains permissions data
 * and methods for executing actions triggered on <code>/Permissions/Overview</code> page. Overview bean is view scoped
 * so it lives as long as user interacting with overview page view.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "permissionsOverviewBean")
@ViewScoped
public class OverviewBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @Inject
    private OverviewRequestCacheBean requestCacheBean;

    private String rolesString;
    private String resourcesString;
    private String ips;
    private String ipGroupsString;
    private String expressionsString;
    private Permission selectedPermission;
    private List<Role> selectedRoles;
    private List<Resource> selectedResources;
    private List<IPGroup> selectedIPGroups;
    private List<Expression> selectedExpressions;
    private List<Permission> selectedPermissions;

    // expressions
    private List<Expression> expressionsSourceList;
    private List<Expression> selectedExpressionsSourceList;
    private List<Expression> expressionsTargetList;
    private List<Expression> selectedExpressionsTargetList;

    // roles
    private List<Role> rolesSourceList;
    private List<Role> selectedRolesSourceList;
    private List<Role> rolesTargetList;
    private List<Role> selectedRolesTargetList;

    // resources
    private List<Resource> resourcesSourceList;
    private List<Resource> selectedResourcesSourceList;
    private List<Resource> resourcesTargetList;
    private List<Resource> selectedResourcesTargetList;

    // ip groups
    private List<IPGroup> ipGroupsSourceList;
    private List<IPGroup> selectedIPGroupsSourceList;
    private List<IPGroup> ipGroupsTargetList;
    private List<IPGroup> selectedIPGroupsTargetList;

    /**
     * Initialises roles, resources, IP groups and expressions dual list models. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        initializeRolesDualListModel();
        initializeResourcesDualListModel();
        initializeIPGroupsDualListModel();
        initializeExpressionsDualListModel();
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns permission which is selected in available permissions list on <code>/Permissions/Overview</code> page.
     * 
     * @return selected permission.
     */
    public Permission getSelectedPermission() {
        return selectedPermission;
    }

    /**
     * Sets permission which is selected in available permissions list on <code>/Permissions/Overview</code> page.
     * 
     * @param selectedPermission selected permission
     */
    public void setSelectedPermission(Permission selectedPermission) {
        this.selectedPermission = selectedPermission;
    }

    /**
     * @return generated roles string from roles field on <code>/Permissions/Overview</code> page.
     */
    public String getRolesString() {
        if (rolesString != null && !rolesString.isEmpty()) {
            return rolesString;
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return generated resources string from resources field on <code>/Permissions/Overview</code> page.
     */
    public String getResourcesString() {
        if (resourcesString != null && !resourcesString.isEmpty()) {
            return resourcesString;
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return generated IP groups string from IP groups field on <code>/Permissions/Overview</code> page.
     */
    public String getIPGroupsString() {
        if (ipGroupsString != null && !ipGroupsString.isEmpty()) {
            return ipGroupsString;
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return generated expressions string from expressions field on <code>/Permissions/Overview</code> page.
     */
    public String getExpressionsString() {
        if (expressionsString != null && !expressionsString.isEmpty()) {
            return expressionsString;
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns IPs entered in IPs field on <code>/Permissions/Overview</code> page.
     * 
     * @return IPs entered in IPs field.
     */
    public String getIPString() {
        if (ips != null && !ips.isEmpty()) {
            return ips;
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Sets IPs entered in IPs field on <code>/Permissions/Overview</code> page.
     * 
     * @param ipString IPs entered in IPs field
     */
    public void setIPString(String ipString) {
        this.ips = ipString;
    }

    /**
     * @return selected permission name. Value is shown in permission name field on <code>/Permissions/Overview</code>
     *         page.
     */
    public String getPermissionName() {
        if (selectedPermission != null) {
            return selectedPermission.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return selected permission resource name. Value is shown in resource field on <code>/Permissions/Overview</code>
     *         page.
     */
    public String getPermissionResource() {
        if (selectedPermission != null) {
            return selectedPermission.getResource().getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return selected permission description. Value is shown in description field on
     *         <code>/Permissions/Overview</code> page.
     */
    public String getPermissionDescription() {
        if (selectedPermission != null) {
            return selectedPermission.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return selected permission rule. Value is shown in rule drop down on <code>/Permissions/Overview</code> page.
     */
    public String getPermissionRule() {
        if (selectedPermission != null && selectedPermission.getRule() != null) {
            return selectedPermission.getRule().getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return selected permission rule description. Value is shown in rule description field on
     *         <code>/Permissions/Overview</code> page.
     */
    public String getPermissionRuleDescription() {
        if (selectedPermission != null && selectedPermission.getRule() != null) {
            Rule rule = selectedPermission.getRule();
            return new StringBuilder(500).append(rule.getDescription()).append(":\n").append(rule.getDefinition())
                    .toString();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return true if exclusive access on selected permission is allowed, otherwise false. Value is shown in check box
     *         on <code>/Permissions/Overview</code> page.
     */
    public Boolean getPermissionExclusiveAccessAllowed() {
        if (selectedPermission != null) {
            return selectedPermission.isExclusiveAccessAllowed();
        }
        return Boolean.FALSE;
    }

    /**
     * @return list of roles which corresponds to the selected permission. Roles are shown in roles list on
     *         <code>/Permissions/Overview</code> page.
     */
    public List<Role> getPermissionRoles() {
        if (selectedPermission != null) {
            return new ArrayList<>(selectedPermission.getRole());
        }
        return new ArrayList<>();
    }

    /**
     * @return list of permissions which corresponds to the selected filters on. Permissions are shown in available
     *         permissions list on <code>/Permissions/Overview</code> page.
     */
    public List<Permission> getPermissions() {
        if (selectedPermissions != null) {
            return selectedPermissions;
        }
        return new ArrayList<>();
    }

    /**
     * Generates string from selected roles which is shown in roles field on <code>/Permissions/Overview</code> page.
     */
    public void generateRolesString() {
        if (rolesTargetList == null || rolesTargetList.isEmpty()) {
            selectedRoles = new ArrayList<>();
            rolesString = Constants.EMPTY_STRING;
        } else {
            selectedRoles = new ArrayList<>(rolesTargetList);
            int size = selectedRoles.size();
            StringBuilder sb = new StringBuilder(size * 30);
            sb.append(selectedRoles.get(0).getName());
            for (int i = 1; i < size; i++) {
                sb.append(", ").append(selectedRoles.get(i).getName());
            }
            rolesString = sb.toString();
        }
        // initializeRolesDualListModel(); do not reinitialise, we need to be able to remove roles as well
    }

    /**
     * Generates string from selected resources which is shown in resources field on <code>/Permissions/Overview</code>
     * page.
     */
    public void generateResourcesString() {
        if (resourcesTargetList == null || resourcesTargetList.isEmpty()) {
            selectedResources = new ArrayList<>();
            resourcesString = Constants.EMPTY_STRING;
        } else {
            selectedResources = new ArrayList<>(resourcesTargetList);
            int size = selectedResources.size();
            StringBuilder sb = new StringBuilder(size * 30);
            sb.append(selectedResources.get(0).getName());
            for (int i = 1; i < size; i++) {
                sb.append(", ").append(selectedResources.get(i).getName());
            }
            resourcesString = sb.toString();
        }
        // initializeResourcesDualListModel(); do not reinitialise, we need to be able to remove resources as well
    }

    /**
     * Generates string from selected IP groups which is shown in IP groups field on <code>/Permissions/Overview</code>
     * page.
     */
    public void generateIPGroupsString() {
        if (ipGroupsTargetList == null || ipGroupsTargetList.isEmpty()) {
            selectedIPGroups = new ArrayList<>();
            ipGroupsString = Constants.EMPTY_STRING;
        } else {
            selectedIPGroups = new ArrayList<>(ipGroupsTargetList);
            int size = selectedIPGroups.size();
            StringBuilder sb = new StringBuilder(size * 30);
            sb.append(selectedIPGroups.get(0).getName());
            for (int i = 1; i < size; i++) {
                sb.append(", ").append(selectedIPGroups.get(i).getName());
            }
            ipGroupsString = sb.toString();
        }
        // initializeIPGroupsDualListModel(); do not reinitialise, we need to be able to remove groups as well
    }

    /**
     * Generates string from selected expressions which is shown in expressions field on
     * <code>/Permissions/Overview</code> page.
     */
    public void generateExpressionsString() {
        if (expressionsTargetList == null || expressionsTargetList.isEmpty()) {
            selectedExpressions = new ArrayList<>();
            expressionsString = Constants.EMPTY_STRING;
        } else {
            selectedExpressions = new ArrayList<>(expressionsTargetList);
            int size = selectedExpressions.size();
            StringBuilder sb = new StringBuilder(size * 100);
            sb.append(selectedExpressions.get(0).getLongName());
            for (int i = 1; i < size; i++) {
                sb.append(", ").append(selectedExpressions.get(i).getLongName());
            }
            expressionsString = sb.toString();
        }
        // initializeExpressionsDualListModel(); do not reinitialise, we need to be able to remove expressions as well
    }

    /**
     * Checks all filters and retrieves permissions corresponds to the filters from database. Retrieved permissions are
     * added into available permissions list. Method is called when search button on <code>/Permissions/Overview</code>
     * page is clicked.
     */
    public void search() {
        int i = 0;
        final List<String> ipsList = new ArrayList<>();
        if (ips == null || ips.isEmpty()) {
            i++;
        } else {
            String[] splited = ips.replaceAll("\\s", "").split(",");
            ipsList.addAll(Arrays.asList(splited));
        }
        if (selectedRoles == null || selectedRoles.isEmpty()) {
            selectedRoles = new ArrayList<>();
            i++;
        }
        if (selectedResources == null || selectedResources.isEmpty()) {
            selectedResources = new ArrayList<>();
            i++;
        }
        if (selectedIPGroups == null || selectedIPGroups.isEmpty()) {
            selectedIPGroups = new ArrayList<>();
            i++;
        }
        if (selectedExpressions == null || selectedExpressions.isEmpty()) {
            selectedExpressions = new ArrayList<>();
            i++;
        }
        if (i == 5) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.FILTER_WARN),
                    Constants.EMPTY_STRING, false);
        } else {
            String user = loginBean.getUsername() == null ? "unknown" : loginBean.getUsername();
            String success = Messages.getString(Messages.SUCCESSFUL_FILTERED_PERMISSIONS_RETRIEVAL, user);
            String failure = Messages.getString(Messages.UNSUCCESSFUL_FILTERED_PERMISSIONS_RETRIEVAL, user);
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    selectedPermissions = requestCacheBean.getFilteredPermissions(selectedRoles, selectedResources,
                            ipsList, selectedIPGroups, selectedExpressions);
                    selectedPermission = null;
                }
            });
        }
    }

    /**
     * Method that shows dialog for selecting roles. Dialog is shown when select roles button on
     * <code>/Permissions/Overview</code> page is clicked.
     */
    public void showSelectRolesDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectRolesDialog').show()");
    }

    /**
     * Method that shows dialog for selecting resources. Dialog is shown when select resources button on
     * <code>/Permissions/Overview</code> page is clicked.
     */
    public void showSelectResourcesDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectResourcesDialog').show()");
    }

    /**
     * Method that shows dialog for selecting IP groups. Dialog is shown when select IP groups button on
     * <code>/Permissions/Overview</code> page is clicked.
     */
    public void showSelectIPGroupsDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectIPGroupsDialog').show()");
    }

    /**
     * Method that shows dialog for selecting expressions. Dialog is shown when select expressions button on
     * <code>/Permissions/Overview</code> page is clicked.
     */
    public void showSelectExpressionsDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectExpressionsDialog').show()");
    }

    /**
     * Returns list which contains expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains expressions from source list.
     */
    public List<Expression> getExpressionsSourceList() {
        return expressionsSourceList;
    }

    /**
     * Sets list which contains expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param expressionsSourceList list which contains expressions from source list
     */
    public void setExpressionsSourceList(List<Expression> expressionsSourceList) {
        this.expressionsSourceList = expressionsSourceList;
    }

    /**
     * Returns list which contains expressions from target list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains expressions from target list.
     */
    public List<Expression> getExpressionsTargetList() {
        return expressionsTargetList;
    }

    /**
     * Sets list which contains expressions from target list and is shown on and is shown on select expressions dialog
     * on <code>/Permissions/Overview</code> page.
     * 
     * @param expressionsTargetList list which contains expressions from target list
     */
    public void setExpressionsTargetList(List<Expression> expressionsTargetList) {
        this.expressionsTargetList = expressionsTargetList;
    }

    /**
     * Returns list which contains selected expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected expressions from source list.
     */
    public List<Expression> getSelectedExpressionsSourceList() {
        return selectedExpressionsSourceList;
    }

    /**
     * Sets list which contains selected expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedExpressionsSourceList list which contains selected expressions from source list
     */
    public void setSelectedExpressionsSourceList(List<Expression> selectedExpressionsSourceList) {
        this.selectedExpressionsSourceList = selectedExpressionsSourceList;
    }

    /**
     * Returns list which contains selected expressions from target list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected expressions from target list.
     */
    public List<Expression> getSelectedExpressionsTargetList() {
        return selectedExpressionsTargetList;
    }

    /**
     * Sets list which contains selected expressions from target list and is shown on select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedExpressionsTargetList list which contains selected expressions from target list
     */
    public void setSelectedExpressionsTargetList(List<Expression> selectedExpressionsTargetList) {
        this.selectedExpressionsTargetList = selectedExpressionsTargetList;
    }

    /**
     * Moves selected expressions from source list to the target list. Lists are shown on the select expressions dialog
     * on <code>/Permissions/Overview</code> page.
     */
    public void addExpressionsFromSourceToTarget() {
        if (!selectedExpressionsSourceList.isEmpty()) {
            expressionsTargetList.addAll(selectedExpressionsSourceList);
        }
        expressionsSourceList.removeAll(selectedExpressionsSourceList);
        selectedExpressionsSourceList.clear();
    }

    /**
     * Moves selected expressions from target list to the source list. Lists are shown on the select expressions dialog
     * on <code>/Permissions/Overview</code> page.
     */
    public void addExpressionsFromTargetToSource() {
        if (!selectedExpressionsTargetList.isEmpty()) {
            expressionsSourceList.addAll(selectedExpressionsTargetList);
        }
        expressionsTargetList.removeAll(selectedExpressionsTargetList);
        selectedExpressionsTargetList.clear();
    }

    /**
     * Moves all expressions from source list to the target list. Lists are shown on the select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllExpressionsFromSourceToTarget() {
        if (!expressionsSourceList.isEmpty()) {
            expressionsTargetList.addAll(expressionsSourceList);
        }
        expressionsSourceList.clear();
        selectedExpressionsSourceList.clear();
    }

    /**
     * Moves all expressions from target list to the source list. Lists are shown on the select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllExpressionsFromTargetToSource() {
        if (!expressionsTargetList.isEmpty()) {
            expressionsSourceList.addAll(expressionsTargetList);
        }
        expressionsTargetList.clear();
        selectedExpressionsTargetList.clear();
    }

    /**
     * Returns list which contains roles from source list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains roles from source list.
     */
    public List<Role> getRolesSourceList() {
        return rolesSourceList;
    }

    /**
     * Sets list which contains roles from source list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param rolesSourceList list which contains roles from source list
     */
    public void setRolesSourceList(List<Role> rolesSourceList) {
        this.rolesSourceList = rolesSourceList;
    }

    /**
     * Returns list which contains roles from target list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains roles from target list.
     */
    public List<Role> getRolesTargetList() {
        return rolesTargetList;
    }

    /**
     * Sets list which contains roles from target list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param rolesTargetList list which contains roles from target list
     */
    public void setRolesTargetList(List<Role> rolesTargetList) {
        this.rolesTargetList = rolesTargetList;
    }

    /**
     * Returns list which contains selected roles from source list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected roles from source list.
     */
    public List<Role> getSelectedRolesSourceList() {
        return selectedRolesSourceList;
    }

    /**
     * Sets list which contains selected roles from source list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedRolesSourceList list which contains selected roles from source list
     */
    public void setSelectedRolesSourceList(List<Role> selectedRolesSourceList) {
        this.selectedRolesSourceList = selectedRolesSourceList;
    }

    /**
     * Returns list which contains selected roles from target list and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected roles from target list.
     */
    public List<Role> getSelectedRolesTargetList() {
        return selectedRolesTargetList;
    }

    /**
     * Sets list which contains selected roles from target list, and is shown on select roles dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedRolesTargetList list which contains selected roles from target list
     */
    public void setSelectedRolesTargetList(List<Role> selectedRolesTargetList) {
        this.selectedRolesTargetList = selectedRolesTargetList;
    }

    /**
     * Moves selected roles from source list to the target list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addRolesFromSourceToTarget() {
        if (!selectedRolesSourceList.isEmpty()) {
            rolesTargetList.addAll(selectedRolesSourceList);
        }
        rolesSourceList.removeAll(selectedRolesSourceList);
        selectedRolesSourceList.clear();
    }

    /**
     * Moves selected roles from target list to the source list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addRolesFromTargetToSource() {
        if (!selectedRolesTargetList.isEmpty()) {
            rolesSourceList.addAll(selectedRolesTargetList);
        }
        rolesTargetList.removeAll(selectedRolesTargetList);
        selectedRolesTargetList.clear();
    }

    /**
     * Moves all roles from source list to the target list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllRolesFromSourceToTarget() {
        if (!rolesSourceList.isEmpty()) {
            rolesTargetList.addAll(rolesSourceList);
        }
        rolesSourceList.clear();
        selectedRolesSourceList.clear();
    }

    /**
     * Moves all roles from target list to the source list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllRolesFromTargetToSource() {
        if (!rolesTargetList.isEmpty()) {
            rolesSourceList.addAll(rolesTargetList);
        }
        rolesTargetList.clear();
        selectedRolesTargetList.clear();
    }

    /**
     * Returns list which contains resources from source list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains resources from source list.
     */
    public List<Resource> getResourcesSourceList() {
        return resourcesSourceList;
    }

    /**
     * Sets list which contains resources from source list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param resourcesSourceList list which contains resources from source list
     */
    public void setResourcesSourceList(List<Resource> resourcesSourceList) {
        this.resourcesSourceList = resourcesSourceList;
    }

    /**
     * Returns list which contains resources from target list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains resources from target list.
     */
    public List<Resource> getResourcesTargetList() {
        return resourcesTargetList;
    }

    /**
     * Sets list which contains resources from target list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param resourcesTargetList list which contains resources from target list
     */
    public void setResourcesTargetList(List<Resource> resourcesTargetList) {
        this.resourcesTargetList = resourcesTargetList;
    }

    /**
     * Returns list which contains selected resources from source list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected resources from source list.
     */
    public List<Resource> getSelectedResourcesSourceList() {
        return selectedResourcesSourceList;
    }

    /**
     * Sets list which contains selected resources from source list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedResourcesSourceList list which contains selected resources from source list
     */
    public void setSelectedResourcesSourceList(List<Resource> selectedResourcesSourceList) {
        this.selectedResourcesSourceList = selectedResourcesSourceList;
    }

    /**
     * Returns list which contains selected resources from target list and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected resources from target list.
     */
    public List<Resource> getSelectedResourcesTargetList() {
        return selectedResourcesTargetList;
    }

    /**
     * Sets list which contains selected resources from target list, and is shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedResourcesTargetList list which contains selected resources from target list
     */
    public void setSelectedResourcesTargetList(List<Resource> selectedResourcesTargetList) {
        this.selectedResourcesTargetList = selectedResourcesTargetList;
    }

    /**
     * Moves selected resources from source list to the target list. Lists are shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addResourcesFromSourceToTarget() {
        if (!selectedResourcesSourceList.isEmpty()) {
            resourcesTargetList.addAll(selectedResourcesSourceList);
        }
        resourcesSourceList.removeAll(selectedResourcesSourceList);
        selectedResourcesSourceList.clear();
    }

    /**
     * Moves selected resources from target list to the source list. Lists are shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addResourcesFromTargetToSource() {
        if (!selectedResourcesTargetList.isEmpty()) {
            resourcesSourceList.addAll(selectedResourcesTargetList);
        }
        resourcesTargetList.removeAll(selectedResourcesTargetList);
        selectedResourcesTargetList.clear();
    }

    /**
     * Moves all resources from source list to the target list. Lists are shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllResourcesFromSourceToTarget() {
        if (!resourcesSourceList.isEmpty()) {
            resourcesTargetList.addAll(resourcesSourceList);
        }
        resourcesSourceList.clear();
        selectedResourcesSourceList.clear();
    }

    /**
     * Moves all resources from target list to the source list. Lists are shown on select resources dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllResourcesFromTargetToSource() {
        if (!resourcesTargetList.isEmpty()) {
            resourcesSourceList.addAll(resourcesTargetList);
        }
        resourcesTargetList.clear();
        selectedResourcesTargetList.clear();
    }

    /**
     * Returns list which contains ip groups from source list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains ip groups from source list.
     */
    public List<IPGroup> getIPGroupsSourceList() {
        return ipGroupsSourceList;
    }

    /**
     * Sets list which contains ip groups from source list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param ipGroupsSourceList list which contains ip groups from source list
     */
    public void setIPGroupsSourceList(List<IPGroup> ipGroupsSourceList) {
        this.ipGroupsSourceList = ipGroupsSourceList;
    }

    /**
     * Returns list which contains ip groups from target list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains ip groups from target list.
     */
    public List<IPGroup> getIPGroupsTargetList() {
        return ipGroupsTargetList;
    }

    /**
     * Sets list which contains ip groups from target list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param ipGroupsTargetList list which contains ip groups from target list
     */
    public void setIPGroupsTargetList(List<IPGroup> ipGroupsTargetList) {
        this.ipGroupsTargetList = ipGroupsTargetList;
    }

    /**
     * Returns list which contains selected ip groups from source list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected ip groups from source list.
     */
    public List<IPGroup> getSelectedIPGroupsSourceList() {
        return selectedIPGroupsSourceList;
    }

    /**
     * Sets list which contains selected ip groups from source list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedIPGroupsSourceList list which contains selected ip groups from source list
     */
    public void setSelectedIPGroupsSourceList(List<IPGroup> selectedIPGroupsSourceList) {
        this.selectedIPGroupsSourceList = selectedIPGroupsSourceList;
    }

    /**
     * Returns list which contains selected ip groups from target list and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list which contains selected ipGroups from target list.
     */
    public List<IPGroup> getSelectedIPGroupsTargetList() {
        return selectedIPGroupsTargetList;
    }

    /**
     * Sets list which contains selected ip groups from target list, and is shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     * 
     * @param selectedIPGroupsTargetList list which contains selected ip groups from target list
     */
    public void setSelectedIPGroupsTargetList(List<IPGroup> selectedIPGroupsTargetList) {
        this.selectedIPGroupsTargetList = selectedIPGroupsTargetList;
    }

    /**
     * Moves selected ip groups from source list to the target list. Lists are shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addIPGroupsFromSourceToTarget() {
        if (!selectedIPGroupsSourceList.isEmpty()) {
            ipGroupsTargetList.addAll(selectedIPGroupsSourceList);
        }
        ipGroupsSourceList.removeAll(selectedIPGroupsSourceList);
        selectedIPGroupsSourceList.clear();
    }

    /**
     * Moves selected ip groups from target list to the source list. Lists are shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addIPGroupsFromTargetToSource() {
        if (!selectedIPGroupsTargetList.isEmpty()) {
            ipGroupsSourceList.addAll(selectedIPGroupsTargetList);
        }
        ipGroupsTargetList.removeAll(selectedIPGroupsTargetList);
        selectedIPGroupsTargetList.clear();
    }

    /**
     * Moves all ip groups from source list to the target list. Lists are shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllIPGroupsFromSourceToTarget() {
        if (!ipGroupsSourceList.isEmpty()) {
            ipGroupsTargetList.addAll(ipGroupsSourceList);
        }
        ipGroupsSourceList.clear();
        selectedIPGroupsSourceList.clear();
    }

    /**
     * Moves all ip groups from target list to the source list. Lists are shown on select ip groups dialog on
     * <code>/Permissions/Overview</code> page.
     */
    public void addAllIPGroupsFromTargetToSource() {
        if (!ipGroupsTargetList.isEmpty()) {
            ipGroupsSourceList.addAll(ipGroupsTargetList);
        }
        ipGroupsTargetList.clear();
        selectedIPGroupsTargetList.clear();
    }

    /**
     * Initialises roles dual list model which data are used in select roles dialog on
     * <code>/Permissions/Overview</code> page.
     */
    private void initializeRolesDualListModel() {
        rolesSourceList = requestCacheBean.getRoles();
        if (rolesSourceList == null) {
            rolesSourceList = new ArrayList<>();
        }
        rolesTargetList = new ArrayList<Role>();
    }

    /**
     * Initialises resources dual list model which data are used in select resources dialog on
     * <code>/Permissions/Overview</code> page.
     */
    private void initializeResourcesDualListModel() {
        resourcesSourceList = requestCacheBean.getResources();
        if (resourcesSourceList == null) {
            resourcesSourceList = new ArrayList<>();
        }
        resourcesTargetList = new ArrayList<Resource>();
    }

    /**
     * Initialises IP groups dual list model which data are used in select IP groups dialog on
     * <code>/Permissions/Overview</code> page.
     */
    private void initializeIPGroupsDualListModel() {
        ipGroupsSourceList = requestCacheBean.getIPGroups();
        if (ipGroupsSourceList == null) {
            ipGroupsSourceList = new ArrayList<>();
        }
        ipGroupsTargetList = new ArrayList<IPGroup>();
    }

    /**
     * Initialises expressions dual list model which data are used in select expressions dialog on
     * <code>/Permissions/Overview</code> page.
     */
    private void initializeExpressionsDualListModel() {
        expressionsSourceList = requestCacheBean.getExpressions();
        if (expressionsSourceList == null) {
            expressionsSourceList = new ArrayList<>();
        }
        expressionsTargetList = new ArrayList<Expression>();
    }
}
