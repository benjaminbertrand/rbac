/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.ProtectionDomain;
import java.util.jar.Manifest;

import javax.ejb.Singleton;

import se.esss.ics.rbac.ejbs.interfaces.MetaInfo;

/**
 * <code>MetaInfoEJB</code> is a singleton bean containing utility methods for dealing with application's meta
 * information.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@Singleton
public class MetaInfoEJB implements MetaInfo {

    private static final long serialVersionUID = -8444320945863168285L;

    private static final String MANIFEST_RELATIVE_PATH = "../META-INF/MANIFEST.MF";
    private static final String ATTRIBUTE_BUILD_VERSION = "Build-Version";

    private transient Manifest manifest;

    /**
     * Returns the application's {@link Manifest}. Manifest file resides at <code>META-INF/MANIFEST.MF</code> inside the
     * web application's deployment directory.
     * 
     * @return the application's manifest file.
     * 
     * @throws IOException if there is an error while reading the manifest file.
     */
    @Override
    public Manifest getManifest() throws IOException {
        if (manifest == null) {
            ProtectionDomain domain = MetaInfoEJB.class.getProtectionDomain();
            if (domain != null && domain.getCodeSource() != null) {
                try (InputStream stream = new URL(domain.getCodeSource().getLocation(), 
                        MANIFEST_RELATIVE_PATH).openStream()) {
                    manifest = new Manifest(stream);
                }
            }
            if (manifest == null) {
                throw new FileNotFoundException("Manifest file could not be found or accessed.");
            }
        }
        return manifest;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.MetaInfo#getRBACManagementVersion()
     */
    @Override
    public String getRBACManagementVersion() {
        try {
            return getManifest().getMainAttributes().getValue(ATTRIBUTE_BUILD_VERSION);
        } catch (IOException e) {
            return "UNKNOWN VERSION";
        }
    }
}
