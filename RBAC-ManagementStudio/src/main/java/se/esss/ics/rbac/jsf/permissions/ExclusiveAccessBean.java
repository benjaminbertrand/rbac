/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.ejbs.interfaces.Admin;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>ExclusiveAccessBean</code> is a managed bean (<code>exclusiveAccessBean</code>), which contains exclusive
 * accesses data and methods for executing actions triggered on <code>/Permissions/ExclusiveAccess</code> page.
 * Exclusive access bean is view scoped so it lives as long as user interacting with exclusive access page view.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "exclusiveAccessBean")
@ViewScoped
public class ExclusiveAccessBean implements Serializable {

    private static final long serialVersionUID = 6577623739557555498L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(ExclusiveAccessBean.class);

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Admin adminEJB;
    @Inject
    private ExclusiveAccessRequestCacheBean requestCacheBean;

    private Date duration;
    private ExclusiveAccess selectedExclusiveAccess;
    private Resource selectedResource;
    private List<ExclusiveAccess> exclusiveAccesses;

    // permissions
    private List<Permission> permissionsSourceList;
    private List<Permission> selectedPermissionsSourceList;
    private List<Permission> permissionsTargetList;
    private List<Permission> selectedPermissionsTargetList;

    /**
     * Sets default exclusive access duration value. Value is retrieved from database. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        permissionsSourceList = new ArrayList<Permission>();
        permissionsTargetList = new ArrayList<Permission>();
        resetExclusiveAccessDuration();
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     * 
     * @param permissionsBean the permission bean through which the permissions can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns exclusive access which is selected in active exclusive access requests table on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return selected exclusive access.
     */
    public ExclusiveAccess getSelectedExclusiveAccess() {
        return selectedExclusiveAccess;
    }

    /**
     * Sets exclusive access which is selected in active exclusive access requests table on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param selectedExclusiveAccess selected exclusive access
     */
    public void setSelectedExclusiveAccess(ExclusiveAccess selectedExclusiveAccess) {
        this.selectedExclusiveAccess = selectedExclusiveAccess;
    }

    /**
     * Returns resource which is selected in request exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return selected resource.
     */
    public Resource getSelectedResource() {
        return selectedResource;
    }

    /**
     * Sets resource which is selected in request exclusive access dialog on <code>/Permissions/ExclusiveAccess</code>
     * page.
     * 
     * @param selectedResource selected resource
     */
    public void setSelectedResource(Resource selectedResource) {
        this.selectedResource = selectedResource;
    }

    /**
     * Returns exclusive access duration which is entered into duration field on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return exclusive access duration.
     */
    public Date getDuration() {
        return duration;
    }

    /**
     * Sets exclusive access duration which is entered into duration field on <code>/Permissions/ExclusiveAccess</code>
     * page.
     * 
     * @param duration exclusive access duration
     */
    public void setDuration(Date duration) {
        this.duration = duration;
    }

    /**
     * @return list of all active exclusive access requests retrieved from database. Exclusive accesses are shown in
     *         active exclusive access request on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public List<ExclusiveAccess> getExclusiveAccesses() {
        exclusiveAccesses = requestCacheBean.getExclusiveAccesses(false);
        return exclusiveAccesses;
    }

    /**
     * @return list of all resources retrieved from database. Resources are shown in drop down menu in request exclusive
     *         access dialog on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public List<Resource> getResources() {
        return requestCacheBean.getResources();
    }

    /**
     * Method which is called when user selects row in active exclusive access requests table on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param event select event
     */
    public void onExclusiveAccessRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof ExclusiveAccess) {
            selectedExclusiveAccess = (ExclusiveAccess) event.getObject();
        }
    }

    /**
     * Method which is called when user deselects row in active exclusive access table on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param event deselect event
     */
    public void onExclusiveAccessRowUnselect(UnselectEvent event) {
        selectedExclusiveAccess = null;
    }

    /**
     * Creates exclusive access for selected permissions. Method is called when OK button in request exclusive access
     * dialog on <code>/Permissions/ExclusiveAccess</code> page is clicked. Results of the action are logged and shown
     * to the user as growl message.
     */
    public void createExclusiveAccess() {
        if (!permissionsTargetList.isEmpty() && loginBean.isLoggedInValidated()) {
            for (final Permission permission : permissionsTargetList) {
                long rawOffset = TimeZone.getDefault().getRawOffset();
                final long seconds = rawOffset < 0 ? duration.getTime() - rawOffset : duration.getTime() + rawOffset;
                String success = Messages.getString(Messages.SUCCESSFUL_EA_REQUEST, loginBean.getUsername(),
                        permission.getName(), permission.getResource().getName());
                String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_REQUEST, loginBean.getUsername(),
                        permission.getName(), permission.getResource().getName());
                taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                    @Override
                    public void execute() throws Exception {
                        loginBean.getRbacAccessEJB().requestExclusiveAccess(permission.getResource().getName(),
                                permission.getName(), (int) TimeUnit.MILLISECONDS.toMinutes(seconds));
                    }
                });
            }
            resetExclusiveAccessDuration();
            requestCacheBean.getExclusiveAccesses(true);
        }
    }

    /**
     * Removes exclusive access. Method is called when release button on <code>/Permissions/ExclusiveAccess</code> page
     * is clicked. Results of the action are logged and shown to the user as growl message.
     */
    public void removeExclusiveAccess() {
        if (!permissionsBean.canReleaseExclusiveAccessForPermission(selectedExclusiveAccess.getPermission(),
                selectedExclusiveAccess.getPermission().getResource(), false)
                || isReleaseExclusiveAccessButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                selectedExclusiveAccess.getPermission().getName(), selectedExclusiveAccess.getPermission()
                        .getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                selectedExclusiveAccess.getPermission().getName(), selectedExclusiveAccess.getPermission()
                        .getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                loginBean.getRbacAccessEJB().releaseExclusiveAccess(
                        selectedExclusiveAccess.getPermission().getResource().getName(),
                        selectedExclusiveAccess.getPermission().getName());
                selectedExclusiveAccess = null;
                requestCacheBean.getExclusiveAccesses(true);
            }
        });
    }

    /**
     * Removes all exclusive accesses. Method is called when release all button on
     * <code>/Permissions/ExclusiveAccess</code> page is clicked. Results of the action are logged and shown to the user
     * as growl message.
     */
    public void removeAll() {
        if (!permissionsBean.canReleaseAnyExclusiveAccess(false) || isReleaseAllButtonDisabled()) {
            return;
        }
        for (final ExclusiveAccess exclusiveAccess : exclusiveAccesses) {
            String success = Messages.getString(Messages.SUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                    exclusiveAccess.getPermission().getName(), exclusiveAccess.getPermission().getResource().getName());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                    exclusiveAccess.getPermission().getName(), exclusiveAccess.getPermission().getResource().getName());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    loginBean.getRbacAccessEJB().releaseExclusiveAccess(
                            exclusiveAccess.getPermission().getResource().getName(),
                            exclusiveAccess.getPermission().getName());
                    selectedExclusiveAccess = null;
                    requestCacheBean.getExclusiveAccesses(true);
                }
            });
        }
    }

    /**
     * Resets exclusive access duration to the default exclusive access duration. This method is called when new
     * exclusive access from request exclusive access dialog on <code>/Permissions/ExclusiveAccess</code> page is
     * created or if cancel button on the same dialog is clicked.
     */
    public void resetExclusiveAccessDuration() {
        try {
            long defaultDuration = adminEJB.getExclusiveAccessExpirationPeriod();
            long rawOffset = TimeZone.getDefault().getRawOffset();
            defaultDuration = rawOffset < 0 ? defaultDuration + rawOffset : defaultDuration - rawOffset;
            duration = new Date(defaultDuration);
            addAllPermissionsFromTargetToSource();
        } catch (Exception e) {
            LOGGER.error(Messages.EA_RESET_EXCEPTION, e);
        }
        selectedResource = null;
    }

    /**
     * Checks permissions and if granted it opens the request exclusive access dialog.
     */
    public void showRequestExclusiveAccessDialog() {
        if (loginBean.isLoggedInValidated()) {
            RequestContext.getCurrentInstance().execute("PF('requestExclusiveAccessDialog').show()");
        }
    }

    /**
     * Request exclusive access button on <code>/Permissions/ExclusiveAccess</code> page is disabled if user is not
     * logged in.
     * 
     * @return true if is request exclusive access button disabled, otherwise false.
     */
    public boolean isRequestExclusiveAccessButtonDisabled() {
        return !loginBean.isLoggedIn();
    }

    /**
     * Release exclusive access button on <code>/Permissions/ExclusiveAccess</code> page is disabled if no exclusive
     * access is selected.
     * 
     * @return true if is release exclusive access button disabled, otherwise false.
     */
    public boolean isReleaseExclusiveAccessButtonDisabled() {
        return selectedExclusiveAccess == null;
    }

    /**
     * Release all exclusive accesses button on <code>/Permissions/ExclusiveAccess</code> page is disabled if exclusive
     * accesses list is empty.
     * 
     * @return true if is release all exclusive access button disabled, otherwise false.
     */
    public boolean isReleaseAllButtonDisabled() {
        return exclusiveAccesses == null || exclusiveAccesses.isEmpty();
    }

    /**
     * Returns list which contains permissions from source list and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return list which contains permissions from source list.
     */
    public List<Permission> getPermissionsSourceList() {
        if (selectedResource != null && loginBean.isLoggedInValidated()) {
            permissionsSourceList = requestCacheBean.getPermissionsByResourceAndUser(selectedResource,
                    loginBean.getUsername());
            if (permissionsSourceList == null) {
                permissionsSourceList = new ArrayList<>();
            } else {
                permissionsSourceList.removeAll(permissionsTargetList);
            }
        } else {
            permissionsSourceList = new ArrayList<>();
        }
        return permissionsSourceList;
    }

    /**
     * Sets list which contains permissions from source list and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param permissionsSourceList list which contains permissions from source list
     */
    public void setPermissionsSourceList(List<Permission> permissionsSourceList) {
        this.permissionsSourceList = permissionsSourceList;
    }

    /**
     * Returns list which contains roles from target list and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return list which contains permissions from target list.
     */
    public List<Permission> getPermissionsTargetList() {
        return permissionsTargetList;
    }

    /**
     * Sets list which contains roles from target list and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param permissionsTargetList list which contains permissions from target list
     */
    public void setPermissionsTargetList(List<Permission> permissionsTargetList) {
        this.permissionsTargetList = permissionsTargetList;
    }

    /**
     * Returns list which contains selected roles from source list aand is shown on request new exclusive access dialog
     * on <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return list which contains selected permissions from source list.
     */
    public List<Permission> getSelectedPermissionsSourceList() {
        return selectedPermissionsSourceList;
    }

    /**
     * Sets list which contains selected roles from source list and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     *
     * @param selectedPermissionsSourceList list which contains selected permissions from source list
     */
    public void setSelectedPermissionsSourceList(List<Permission> selectedPermissionsSourceList) {
        this.selectedPermissionsSourceList = selectedPermissionsSourceList;
    }

    /**
     * Returns list which contains selected roles from target list and is shown on request new exclusive access dialog
     * on <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @return list which contains selected permissions from target list.
     */
    public List<Permission> getSelectedPermissionsTargetList() {
        return selectedPermissionsTargetList;
    }

    /**
     * Sets list which contains selected roles from target list, and is shown on request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     * 
     * @param selectedPermissionsTargetList list which contains selected permissions from target list
     */
    public void setSelectedPermissionsTargetList(List<Permission> selectedPermissionsTargetList) {
        this.selectedPermissionsTargetList = selectedPermissionsTargetList;
    }

    /**
     * Moves selected roles from source list to the target list. Lists are shown on the request new exclusive access
     * dialog on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public void addPermissionsFromSourceToTarget() {
        if (!selectedPermissionsSourceList.isEmpty()) {
            permissionsTargetList.addAll(selectedPermissionsSourceList);
        }
        permissionsSourceList.removeAll(selectedPermissionsSourceList);
        selectedPermissionsSourceList.clear();
    }

    /**
     * Moves selected roles from target list to the source list. Lists are shown on the request new exclusive access
     * dialog on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public void addPermissionsFromTargetToSource() {
        if (!selectedPermissionsTargetList.isEmpty()) {
            permissionsSourceList.addAll(selectedPermissionsTargetList);
        }
        permissionsTargetList.removeAll(selectedPermissionsTargetList);
        selectedPermissionsTargetList.clear();
    }

    /**
     * Moves all roles from source list to the target list.Lists are shown on the request new exclusive access dialog on
     * <code>/Permissions/ExclusiveAccess</code> page.
     */
    public void addAllPermissionsFromSourceToTarget() {
        if (permissionsSourceList == null || selectedPermissionsSourceList == null) {
            return;
        }
        if (!permissionsSourceList.isEmpty()) {
            permissionsTargetList.addAll(permissionsSourceList);
        }
        permissionsSourceList.clear();
        selectedPermissionsSourceList.clear();
    }

    /**
     * Moves all roles from target list to the source list. Lists are shown on the request new exclusive access dialog
     * on <code>/Permissions/ExclusiveAccess</code> page.
     */
    public void addAllPermissionsFromTargetToSource() {
        if (permissionsTargetList == null || selectedPermissionsTargetList == null) {
            return;
        }
        if (!permissionsTargetList.isEmpty()) {
            permissionsSourceList.addAll(permissionsTargetList);
        }
        permissionsTargetList.clear();
        selectedPermissionsTargetList.clear();
    }
}
