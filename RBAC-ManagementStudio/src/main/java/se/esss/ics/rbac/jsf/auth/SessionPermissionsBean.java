/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.auth;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.ejbs.interfaces.RBACAccess;
import se.esss.ics.rbac.ejbs.interfaces.Resources;

/**
 * 
 * <code>SessionPermissionBean</code> is a bean that provides permission check in the session scope. It should be used
 * by those managed beans that cannot handle view scoped beans.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@ManagedBean(name = "sessionPermissionsBean")
@SessionScoped
public class SessionPermissionsBean implements Serializable {

    private static final long serialVersionUID = 5443634965085998024L;

    @ManagedProperty(value = "#{loginBean.rbacAccessEJB}")
    private RBACAccess rbacAccessEJB;
    @EJB
    private Resources resourcesEJB;

    /**
     * Sets the {@link RBACAccess} bean, which holds methods for communicating with RBAC authentication and
     * authorisation web services. Injected instance is the one held by the login bean, so that the token of the
     * currently logged in user is used for authorisation.
     * 
     * @param rbacAccessEJB the RBAC access used by this bean to access RBAC services
     */
    public void setRbacAccessEJB(RBACAccess rbacAccessEJB) {
        this.rbacAccessEJB = rbacAccessEJB;
    }

    /**
     * Checks whether the currently logged in user may manage the specified resource. User may manage a resource if he
     * is the resource manager, or if he has the {@link PermissionsBean#PERM_MANAGE_RESOURCE} permission.
     * 
     * @param resource to be managed
     * @return <code>true</code> if the logged in user may manage the resource
     */
    public boolean canManageResource(Resource resource) {
        if (resource == null) {
            return false;
        } else if (rbacAccessEJB.userHasPermission(PermissionsBean.PERM_MANAGE_RESOURCE,
                PermissionsBean.RESOURCE_NAME)) {
            return true;
        } else if (rbacAccessEJB.getToken() == null) {
            // user has been logged out during permission check
            return false;
        } else {
            // Re-attach the resource to access lazy fields.
            Resource loadedResource = resourcesEJB.getResource(resource.getId());
            return loadedResource.getManagers().contains(rbacAccessEJB.getToken().getUsername());
        }
    }
}
