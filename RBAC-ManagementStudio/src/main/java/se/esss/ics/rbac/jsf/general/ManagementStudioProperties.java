/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.general;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * <code>ManagementStudioProperties</code> provides access to the externally set properties used by the management
 * studio.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public final class ManagementStudioProperties {

    /** A flag whether mail notifications are enabled or not */
    public static final String KEY_MAIL_NOTIFICATION = "rbac.mailNotification";

    private static final Properties PROPERTIES = new Properties();
    static {
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mail.properties")) {
            PROPERTIES.load(stream);
        } catch (IOException e) {
            // ignore
        }
    }

    /**
     * Private constructor.
     */
    private ManagementStudioProperties() {
    }

    /**
     * Returns the value of the {@link ManagementStudioProperties#KEY_MAIL_NOTIFICATION} property as an boolean value.
     * If true mail notifications are enabled otherwise not.
     * 
     * @return true if mail notifications are enabled otherwise false.
     */
    public static boolean isMailNotificationEnabled() {
        return Boolean.parseBoolean(PROPERTIES.getProperty(KEY_MAIL_NOTIFICATION, "true"));
    }

    /**
     * Searches for the property with the specified key amongst the properties. The method returns null if the property
     * is not found.
     * 
     * @param key the property key.
     * 
     * @return the value of the property with the specified key value.
     */
    public static String getProperty(String key) {
        return PROPERTIES.getProperty(key);
    }
}
