/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.Token;

/**
 * <code>Tokens</code> interface defines methods for dealing with tokens.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface Tokens extends Serializable {

    /**
     * Retrieves token identified by <code>tokenId</code> from database and returns it.
     * 
     * @param tokenId unique token identifier
     * 
     * @return specific token identified by id.
     */
    Token getToken(int tokenId);

    /**
     * @return list of tokens retrieved from database.
     */
    List<Token> getTokens();

    /**
     * Removes expired tokens from database.
     * 
     * @return number of removed expired tokens.
     */
    int removeExpiredTokens();

    /**
     * Invalidates and removes token from database.
     * 
     * @param token token which will be invalidated
     */
    void invalidateToken(Token token);

    /**
     * Invalidate all tokens from list and removes it from database.
     * 
     * @param tokens tokens which will be invalidated
     */
    void invalidateAllTokens(List<Token> tokens);
}
