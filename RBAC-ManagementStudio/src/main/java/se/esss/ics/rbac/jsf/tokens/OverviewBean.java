/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.tokens;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.ejbs.interfaces.Tokens;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>OverviewBean</code> is a managed bean (<code>tokensBean</code>), which contains tokens data and methods for
 * executing actions triggered on <code>/Tokens/Overview</code> page. Overview bean is view scoped so it lives as long
 * as user interacting with tokens overview page view.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "tokensBean")
@ViewScoped
public class OverviewBean implements Serializable {

    private static final long serialVersionUID = 7961994817642674535L;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Tokens tokensEJB;
    @Inject
    private RequestCacheBean requestCahceBean;

    private List<Token> tokens;
    private Token selectedToken;

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     * 
     * @param permissionsBean the permissions bean through which permission can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Method which is called when user selects row (token) in tokens table.
     * 
     * @param event select event
     */
    public void onTokenRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof Token) {
            selectedToken = (Token) event.getObject();
        }
    }

    /**
     * Method which is called when user deselects row (token) in tokens table.
     * 
     * @param event deselect event
     */
    public void onTokenRowUnselect(UnselectEvent event) {
        selectedToken = null;
    }

    public List<Token> getTokens() {
        if (tokens == null) {
            tokens = requestCahceBean.getTokens(false);
        }
        return tokens;
    }

    /**
     * Checks if some token from table on <code>/Tokens/Overview</code> page is selected.
     * 
     * @return true if token is selected, otherwise false.
     */
    public boolean isTokenSelected() {
        return selectedToken != null;
    }

    /**
     * Checks if list of tokens is empty. Tokens are shown in table on <code>/Tokens/Overview</code> page.
     * 
     * @return true if list of tokens is empty, otherwise false.
     */
    public boolean isTokensListEmpty() {
        return tokens.isEmpty();
    }

    /**
     * Generates string, which contains names of all roles belonging to given token, separated by comma. String is shown
     * in table in column roles on <code>/Tokens/Overview</code> page.
     * 
     * @param token token for which string is generated
     * @return generated string which contains names of all roles belonging to given token.
     */
    public String getRolesString(Token token) {
        if (tokens != null && !isTokensListEmpty()) {
            Set<TokenRole> tokenRoles = tokens.get(tokens.indexOf(token)).getRole();
            if (tokenRoles != null && !tokenRoles.isEmpty()) {
                if (tokenRoles.size() == 1) {
                    return tokenRoles.iterator().next().getRole().getRole().getName();
                } else {
                    return "All assigned roles";
                }
            }
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Invalidates all tokens and removes it from database. This method is called when invalidate all button on
     * <code>/Tokens/Overview</code> page is clicked. Results of the action are logged and shown to the user as growl
     * message.
     */
    public void invalidateAll() {
        if (!permissionsBean.canInvalidateTokens(false) || isTokensListEmpty()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_TOKENS_INVALIDATION, loginBean.getUsername());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_TOKENS_INVALIDATION, loginBean.getUsername());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                tokensEJB.invalidateAllTokens(tokens);
                selectedToken = null;
                tokens = requestCahceBean.getTokens(true);
            }
        });
    }

    /**
     * Invalidates selected token and remove it from database. This method is called when invalidate button on
     * <code>/Tokens/Overview</code> page is clicked. Results of the action are logged and shown to the user as growl
     * message.
     */
    public void invalidateToken() {
        if (!permissionsBean.canInvalidateTokens(false) || selectedToken == null) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_TOKEN_INVALIDATION, loginBean.getUsername());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_TOKEN_INVALIDATION, loginBean.getUsername());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                tokensEJB.invalidateToken(selectedToken);
                selectedToken = null;
                tokens = requestCahceBean.getTokens(true);
            }
        });
    }

    /**
     * Removes all expired tokens from database. This method is called when remove expired button on
     * <code>/Tokens/Overview</code> page is clicked. Results of the action are logged and shown to the user as growl
     * message.
     */
    public void removeExpiredTokens() {
        String success = Messages.getString(Messages.SUCCESSFUL_TOKENS_REMOVAL, loginBean.getUsername());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_TOKENS_REMOVAL, loginBean.getUsername());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                tokensEJB.removeExpiredTokens();
                tokens = requestCahceBean.getTokens(true);
            }
        });
    }
}
