/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;

/**
 * <code>ExclusiveAccesses</code> interface defines methods for dealing with exclusive accesses.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface ExclusiveAccesses extends Serializable {

    /**
     * Retrieves exclusive access identified by <code>exclusiveAccessId</code> from database and returns it.
     * 
     * @param exclusiveAccessId unique exclusive access identifier
     * 
     * @return specific exclusive access identified by id.
     */
    ExclusiveAccess getExclusiveAccess(int exclusiveAccessId);

    /**
     * @return list of the exclusive accesses retrieved from database.
     */
    List<ExclusiveAccess> getExclusiveAccesses();

    /**
     * Retrieves all exclusive accesses which belongs to the specific user identified by <code>userId</code> from
     * database and returns it.
     * 
     * @param userId unique user identifier
     * 
     * @return exclusive accesses which belongs to the specific user.
     */
    List<ExclusiveAccess> getActiveExclusiveAccesses(String userId);

    /**
     * @return list of the permissions which have already active exclusive access.
     */
    List<Permission> getExclusiveAccessPermissions();

    /**
     * Removes given exclusive access from database.
     * 
     * @param exclusiveAccess exclusive access which will be removed
     */
    void releaseExclusiveAccess(ExclusiveAccess exclusiveAccess);

    /**
     * Removes given exclusive accesses from database.
     * 
     * @param exclusiveAccesses list of exclusive accesses which will be removed
     */
    void releaseExclusiveAccesses(List<ExclusiveAccess> exclusiveAccesses);

    /**
     * Inserts created exclusive accesses from given list into database.
     * 
     * @param exclusiveAccessList created exclusive accesses
     */
    void createExclusiveAccesses(List<ExclusiveAccess> exclusiveAccessList);
}
