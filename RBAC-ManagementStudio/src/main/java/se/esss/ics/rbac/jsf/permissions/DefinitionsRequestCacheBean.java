/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.ejbs.interfaces.Rules;

/**
 * <code>DefinitionsRequestCacheBean</code> is a request bean used on the definitions page. It provides access to DB
 * content by caching the results for the duration of the request. Using this bean eliminates multiple calls to the
 * database, which can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "definitionsRequestCacheBean")
@RequestScoped
public class DefinitionsRequestCacheBean implements Serializable {

    private static final long serialVersionUID = 4421265100906853490L;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private Rules rulesEJB;
    @EJB
    private Roles rolesEJB;

    private int currentResourceId;

    private List<Permission> permissionsByResource;
    private List<Resource> resources;
    private List<Rule> rules;
    private List<Role> roles;

    /**
     * Retrieves list of permissions which corresponds to the selected resource from database and returns it.
     * Permissions are shown in permissions list on <code>/Permissions/Definitions</code> page.
     * 
     * @param resourceId resource id
     * @param refresh if true retrieve permissions from database, otherwise return cached permissions
     * 
     * @return permissions which corresponds to the selected resource.
     */
    public List<Permission> getPermissionsByResource(int resourceId, boolean refresh) {
        if (refresh || permissionsByResource == null || currentResourceId != resourceId) {
            currentResourceId = resourceId;
            permissionsByResource = permissionsEJB.getPermissionsByResource(resourceId);
        }
        return permissionsByResource;
    }

    /**
     * Retrieves list of resources from database and returns it. Resources are shown in resources drop down list on
     * <code>/Permissions/Definitions</code> page.
     * 
     * @return resources retrieved from database.
     */
    public List<Resource> getResources() {
        if (resources == null) {
            resources = resourcesEJB.getResources();
        }
        return resources;
    }

    /**
     * Retrieves list of rules from database and returns it. Rules are shown in drop down list on
     * <code>/Permissions/Definitions</code> page.
     * 
     * @return rules retrieved from database.
     */
    public List<Rule> getRules() {
        if (rules == null) {
            rules = rulesEJB.getRules();
        }
        return rules;
    }

    /**
     * @return roles which corresponds to the selected permission. Roles are shown in roles list box on
     *         <code>/Permissions/Definitions</code> page.
     */
    public List<Role> getRoles() {
        if (roles == null) {
            roles = rolesEJB.getRoles();
        }
        return roles;
    }
}
