/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.Local;

import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;

/**
 * {@link RBACAccess} interface defines methods which are called from all pages when authentication and authorisation
 * checks are required.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@Local
public interface RBACAccess extends Serializable {

    /**
     * Authenticates the user using the provided username and password. If authentication is successful a token
     * containing user's information is returned. If unsuccessful, <code>null</code> is returned instead.
     * 
     * @param username the username used for authentication
     * @param password the password used for authentication
     * 
     * @return token for the authenticated user or null if authentication was unsuccessful
     * @throws SecurityFacadeException if authentication failed for any reason
     */
    Token authenticate(final String username, final char[] password) throws SecurityFacadeException;

    /**
     * Logs the current user out and deletes the token.
     * 
     * @return <code>true</code> if the logout was successful or false otherwise
     * @throws SecurityFacadeException if logout could not be performed
     */
    boolean logout() throws SecurityFacadeException;

    /**
     * Returns the token of the last authenticated user.
     * 
     * @return the token of the last authenticated user
     */
    Token getToken();

    /**
     * Sets the tokenID on the security facade.
     * 
     * @param tokenID the token id
     */
    void setTokenID(char[] tokenID);
    
    /**
     * Returns validated token of the last authenticated user. Methods check whether the token is still valid before
     * returning it.
     * 
     * @return validated token of the last authenticated user
     */
    Token getValidatedToken();

    /**
     * Returns <code>true</code>, if the currently logged in user has the specified permission for the specified
     * resource.
     * 
     * @param permissionName name of the permission to be checked
     * @param resourceName name of the resource
     * 
     * @return true if the currently logged in user has the specified permission for the RBAC Management Studio or false
     *         otherwise
     */
    boolean userHasPermission(String permissionName, String resourceName);

    /**
     * Returns a map containing the the permission grant state for all permissions provided as a parameter. The keys are
     * permission name, the values are true for granted permissions and false for denied permissions.
     * 
     * @param permissions the name of permissions to check
     * @param resourceName the name of the resource that permissions belong to
     * @return a map containing the permission grant state
     */
    Map<String, Boolean> userHasPermissions(String[] permissions, String resourceName);

    /**
     * Return <code>true</code>, if the currently logged in user successfully requested for exclusive access.
     * 
     * @param resourceName name of the resource where permission is
     * @param permissionName name of the permission for which exclusive access is requested
     * @param durationInMinutes duration (in minutes) of the exclusive access
     * 
     * @return true if the currently logged in user successfully requested for exclusive access, otherwise false
     * 
     * @throws SecurityFacadeException if there was an error during the request for exclusive access
     * @see ISecurityFacade#requestExclusiveAccess(String, String, int)
     */
    boolean requestExclusiveAccess(String resourceName, String permissionName, int durationInMinutes)
            throws SecurityFacadeException;

    /**
     * Return <code>true</code>, if the currently logged in user successfully released exclusive access.
     * 
     * @param resourceName name of the resource where permission is
     * @param permissionName name of the permission for which exclusive access will be released
     * 
     * @return true if exclusive access was successfully released, otherwise false
     * 
     * @throws SecurityFacadeException if there was an error during the release of exclusive access
     * @see ISecurityFacade#releaseExclusiveAccess(String, String)
     */
    boolean releaseExclusiveAccess(String resourceName, String permissionName) throws SecurityFacadeException;

    /**
     * Returns the version of the RBAC services.
     * 
     * @return the version number
     */
    String getRBACVersion();
}
