/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Rule;

/**
 * <code>Rules</code> interface defines methods for dealing with rules.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface Rules extends Serializable {

    /**
     * Retrieves rule identified by <code>ruleId</code> from database and returns it.
     * 
     * @param ruleId unique rule identifier
     * 
     * @return specific rule identified by id.
     */
    Rule getRule(int ruleId);

    /**
     * @return list of the rules retrieved from database.
     */
    List<Rule> getRules();

    /**
     * Retrieves rules which belongs to the specific <code>expression</code> and returns it.
     * 
     * @param expression expression
     * 
     * @return list of rules which belongs to the specific expression.
     */
    List<Rule> getRulesByExpression(Expression expression);

    /**
     * Retrieves all rules whose name matches the <code>wildcard</code> pattern from database and returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of the rules whose name matches the wildcard pattern.
     */
    List<Rule> getRulesByWildcard(String wildcard);

    /**
     * Inserts created rule into database.
     * 
     * @param rule created rule
     */
    void createRule(Rule rule);

    /**
     * Removes rule from database.
     * 
     * @param rule rule which will be removed
     */
    void removeRule(Rule rule);

    /**
     * Updates rule.
     * 
     * @param rule updated rule
     */
    void updateRule(Rule rule);

    /**
     * Updates rules.
     * 
     * @param rules list of the updated rules
     */
    void updateRules(List<Rule> rules);
}
