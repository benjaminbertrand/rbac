/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;

import javax.ejb.Local;

/**
 * <code>Admin</code> interface defines methods for dealing with administrator logs and system settings.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface Admin extends Serializable {

    /**
     * Authenticates logged in user as administrator. Authentication is successful if given <code>password</code> is
     * correct, otherwise authentication fails.
     * 
     * @param password entered password
     * 
     * @return true if administrator was successfully authenticated, otherwise false.
     */
    boolean authenticate(char[] password);

    /**
     * Changes administrator password, if administrator is successfully authenticated.
     * 
     * @param password new administrator password
     * 
     * @return true if password was successfully changed, otherwise false.
     */
    boolean changePassword(char[] password);

    /**
     * Inserts new exclusive access expiration period into database.
     * 
     * @param exclusiveAccessExpirationPeriod exclusive access expiration period
     */
    void setExclusiveAccessExpirationPeriod(long exclusiveAccessExpirationPeriod);

    /**
     * Retrieves exclusive access expiration period from database and returns it. If exclusive access expiration period
     * does not exist, inserts default exclusive access expiration period into database and returns it.
     * 
     * @return default exclusive access expiration period.
     */
    long getExclusiveAccessExpirationPeriod();

    /**
     * Inserts new token expiration period into database.
     * 
     * @param tokenExpirationPeriod new token expiration period
     */
    void setTokenExpirationPeriod(long tokenExpirationPeriod);

    /**
     * Retrieves token expiration period from database and returns it. If token expiration period does not exist,
     * inserts default token expiration period into database and returns it.
     * 
     * @return default token expiration period.
     */
    long getTokenExpirationPeriod();

    /**
     * Retrieves default access security rule permission from database and returns it. If access security rule
     * permission does not exist, inserts default ASG permission into database and return it.
     * 
     * @return default access security rule permission
     */
    String getAccessSecurityRulePermission();

    /**
     * Inserts new current access security rule permission into database.
     * 
     * @param permission default access security rule permission
     */
    void setAccessSecurityRulePermission(String permission);

    /**
     * Regenerates private and public key, if administrator is successfully authenticated.
     */
    void regenerateKeyPair();
}
