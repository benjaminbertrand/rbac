/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.logs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import se.esss.ics.rbac.RBACLog;

/**
 * <code>LazyLogsDataModel</code> is a class which extends <code>LazyDataModel</code>. This model is used on
 * <code>/Logs/Overview</code> page where log entries are shown.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class LazyLogsDataModel extends LazyDataModel<RBACLog> {

    private static final long serialVersionUID = -6569297023746713683L;

    private final List<RBACLog> logs;

    /**
     * Constructs a new <code>LazyLogsDataModel</code> model.
     * 
     * @param logs list of log entries which will be lazy loaded
     */
    public LazyLogsDataModel(List<RBACLog> logs) {
        super();
        this.logs = logs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.primefaces.model.LazyDataModel#getRowData(java.lang.String)
     */
    @Override
    public RBACLog getRowData(String rowKey) {
        int key = Integer.parseInt(rowKey);
        for (RBACLog log : logs) {
            if (log.getId() == key) {
                return log;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.primefaces.model.LazyDataModel#getRowKey(java.lang.Object)
     */
    @Override
    public Object getRowKey(RBACLog log) {
        return log.getId();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.primefaces.model.LazyDataModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return logs.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.primefaces.model.LazyDataModel#load(int, int, java.lang.String, org.primefaces.model.SortOrder,
     * java.util.Map)
     */
    @Override
    public List<RBACLog> load(int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters) {
        List<RBACLog> data = new ArrayList<>(logs);
        if (logs.size() > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (logs.size() % pageSize));
            }
        }
        return data;
    }
}
