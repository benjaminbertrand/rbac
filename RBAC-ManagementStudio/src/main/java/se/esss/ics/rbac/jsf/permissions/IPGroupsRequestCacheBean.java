/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.ejbs.interfaces.IPGroups;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>IPGroupsRequestCacheBean</code> is a request bean used on the ip groups page. It provides access to DB content
 * by caching the results for the duration of the request. Using this bean eliminates multiple calls to the database,
 * which can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "ipGroupsRequestCacheBean")
@RequestScoped
public class IPGroupsRequestCacheBean implements Serializable {

    private static final long serialVersionUID = -8024739239221453582L;

    @EJB
    private IPGroups ipGroupsEJB;

    private String currentWildcard = Constants.ALL_DB;

    private List<IPGroup> ipGroups;
    private List<IPGroup> ipGroupsWildcard;

    /**
     * Retrieves list of IP groups from database and returns it. IP groups are shown in IP groups list on
     * <code>/Permissions/IPGroups</code> page.
     * 
     * @param refresh if true retrieves ip groups from database otherwise return cached ip groups
     * 
     * @return list of IP groups retrieved from database.
     */
    public List<IPGroup> getIPGroups(boolean refresh) {
        if (refresh || ipGroups == null) {
            ipGroups = ipGroupsEJB.getIPGroups();
        }
        return ipGroups;
    }

    /**
     * Retrieves list of IP groups (which matches wildcard) from database and returns it. IP groups are shown in IP
     * groups list on <code>/Permissions/IPGroups</code> page.
     * 
     * @param wildcard search ip groups by wildcard
     * @param refresh if true retrieves ip groups from database otherwise return cached ip groups
     * 
     * @return list of IP groups retrieved from database.
     */
    public List<IPGroup> getIPGroupsByWildcard(String wildcard, boolean refresh) {
        if (refresh || ipGroupsWildcard == null || !currentWildcard.equals(wildcard)) {
            currentWildcard = wildcard;
            ipGroupsWildcard = ipGroupsEJB.getIPGroupsByWildcard(wildcard);
        }
        return ipGroupsWildcard;
    }
}
