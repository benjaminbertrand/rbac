/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.general;

/**
 * <code>ManagementStudioExecutionTask</code> is a task that executes the given method. The purpose of this class is to
 * logs executed action results and shows it to the user as growl message.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 * 
 * @param <E> the type of exception thrown by the method of this interface
 */
public interface ManagementStudioExecutionTask<E extends Throwable> {

    /**
     * Executes the task.
     * 
     * @throws E if there was an error during method execution
     */
    void execute() throws E;
}
