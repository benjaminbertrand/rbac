/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.IPGroup;

/**
 * <code>IPGroups</code> interface defines methods for dealing with IP groups.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface IPGroups extends Serializable {

    /**
     * Retrieves IP group identified by <code>ipGroupId</code> from database and returns it.
     * 
     * @param ipGroupId unique IP group identifier
     * 
     * @return specific IP group identified by id.
     */
    IPGroup getIPGroup(int ipGroupId);

    /**
     * @return list of the IP groups retrieved from database.
     */
    List<IPGroup> getIPGroups();

    /**
     * Retrieves all IP groups whose name matches the <code>wildcard</code> pattern from database and returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of the IP groups whose name matches the wildcard pattern.
     */
    List<IPGroup> getIPGroupsByWildcard(String wildcard);

    /**
     * Inserts created IP group into database.
     * 
     * @param ipGroup created IP group
     */
    void createIPGroup(IPGroup ipGroup);

    /**
     * Updates IP group.
     * 
     * @param ipGroup updated IP group
     */
    void updateIPGroup(IPGroup ipGroup);

    /**
     * Removes IP group from database.
     * 
     * @param ipGroup IP group which will be removed
     */
    void removeIPGroup(IPGroup ipGroup);
}
