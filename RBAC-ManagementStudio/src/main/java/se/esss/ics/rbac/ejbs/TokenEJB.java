/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.ejbs.interfaces.Tokens;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>TokenEJB</code> is a stateless bean containing utility methods for dealing with tokens. Contains methods for
 * retrieving, inserting, deleting and updating token informations. All methods are defined in <code>Tokens</code>
 * interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class TokenEJB implements Tokens {

    private static final long serialVersionUID = -2114916050591823705L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Tokens#getToken(int)
     */
    @Override
    public Token getToken(int tokenId) {
        Token token = em.find(Token.class, tokenId);
        // collections are lazy loaded
        if (token != null) {
            token.getRole().size();
        }
        return token;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Tokens#getTokens()
     */
    @Override
    public List<Token> getTokens() {
        List<Token> resultList = em.createNamedQuery("Token.selectAll", Token.class).getResultList();
        // collections are lazy loaded
        if (resultList != null) {
            for (Token token : resultList) {
                token.getRole().size();
            }
        }
        return resultList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Tokens#removeExpiredTokens()
     */
    @Override
    public int removeExpiredTokens() {
        return em.createNamedQuery("Token.deleteExpired").executeUpdate();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Tokens#invalidateToken(se.esss.ics.rbac.datamodel.Token)
     */
    @Override
    public void invalidateToken(Token token) {
        em.remove(em.find(Token.class, token.getId()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Tokens#invalidateAllTokens(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void invalidateAllTokens(List<Token> tokens) {
        int i = 0;
        for (Token token : tokens) {
            em.remove(em.find(Token.class, token.getId()));
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }
    }
}
