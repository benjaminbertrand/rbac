/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.tokens;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.ejbs.interfaces.Tokens;

/**
 * <code>RequestCacheBean</code> is a request bean used on the tokens page. It provides access to DB content by caching
 * the results for the duration of the request. Using this bean eliminates multiple calls to the database, which can be
 * a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "tokensRequestCacheBean")
@RequestScoped
public class RequestCacheBean implements Serializable {

    private static final long serialVersionUID = -618041088255343031L;

    @EJB
    private Tokens tokensEJB;

    private List<Token> tokens;

    /**
     * @param refresh if true returns tokens from database otherwise returns cached tokens
     * 
     * @return list of all tokens, tokens are displayed in table on <code>/Tokens/Overview</code> page.
     */
    public List<Token> getTokens(boolean refresh) {
        if (refresh || tokens == null) {
            tokens = tokensEJB.getTokens();
        }
        return tokens;
    }
}
