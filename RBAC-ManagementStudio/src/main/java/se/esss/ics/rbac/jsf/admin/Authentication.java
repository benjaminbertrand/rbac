package se.esss.ics.rbac.jsf.admin;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import se.esss.ics.rbac.ejbs.interfaces.Admin;

/**
 *
 * <code>Authentication</code> is a session scoped bean that handles authentication of the admin user.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "adminAuthBean")
@SessionScoped
public class Authentication {


    @EJB
    private Admin adminAuthEJB;

    private boolean isAuthenticated;
    private char[] enteredPassword;

    /**
     * Authenticates logged in user as administrator. User is successfully authenticated as administrator if entered
     * administrator password is correct.
     */
    public void authenticate() {
        if (isCharArrayEmpty(enteredPassword)) {
            isAuthenticated = false;
        } else {
            isAuthenticated = adminAuthEJB.authenticate(enteredPassword);
        }
        enteredPassword = null;
    }

    /**
     * @return password which is entered into password field in enter password dialog on <code>/Admin</code> pages.
     */
    public char[] getEnteredPassword() {
        return enteredPassword;
    }

    /**
     * Sets password which is entered into password field in enter password dialog on <code>/Admin</code> pages.
     *
     * @param enteredPassword entered password
     */
    public void setEnteredPassword(char[] enteredPassword) {
        this.enteredPassword = enteredPassword;
    }

    /**
     * @return true if is logged in user authenticated as administrator, otherwise false.
     */
    public boolean getIsAuthenticated() {
        return isAuthenticated;
    }

    /**
     * Invalidate authentication.
     */
    void invalidate() {
        this.isAuthenticated = false;
    }

    /**
     * Checks if given char array is empty and return true if is empty, otherwise false.
     *
     * @param array char array
     * @return true if char array is empty, otherwise false.
     */
    private static boolean isCharArrayEmpty(char[] array) {
        return array == null || array.length == 0;
    }
}
