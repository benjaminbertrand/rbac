/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.admin.logs;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.ManagementStudioLog;
import se.esss.ics.rbac.ManagementStudioLog.Severity;
import se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs;

/**
 * <code>RequestCacheBean</code> is a request bean used on the admin logs page. It provides access to DB content by
 * caching the results for the duration of the request. Using this bean eliminates multiple calls to the database, which
 * can be a result of primefaces requesting specific data multiple times.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "adminLogsRequestCacheBean")
@RequestScoped
public class RequestCacheBean implements Serializable {

    private static final long serialVersionUID = 1787621331255127282L;

    @EJB
    private ManagementStudioLogs studioLogsEJB;

    private Date currentStartTime;
    private Date currentEndTime;
    private Severity[] currentSeverityEnum;
    private List<String> currentUsers;
    private List<ManagementStudioLog> logs;

    /**
     * Retrieves logs and returns it.
     *
     * @param startTime start time
     * @param endTime end time
     * @param severityEnum severities
     * @param users users
     *
     * @return retrieved list of logs.
     */
    public List<ManagementStudioLog> getLogs(Date startTime, Date endTime, Severity[] severityEnum,
                List<String> users) {
        if (logs == null || newRequest(startTime, endTime, severityEnum, users)) {
            initialise(startTime, endTime, severityEnum, users);
            logs = studioLogsEJB.getLogs(startTime, endTime, severityEnum, users);
        }
        return logs;
    }

    /**
     * Initialises current log request parameters.
     *
     * @param startTime start time
     * @param endTime end time
     * @param severityEnum severities
     * @param users users
     */
    private void initialise(Date startTime, Date endTime, Severity[] severityEnum, List<String> users) {
        currentStartTime = startTime;
        currentEndTime = endTime;
        currentSeverityEnum = severityEnum;
        currentUsers = users;
    }

    /**
     * Checks if log requests parameters differs from previous request.
     *
     * @param startTime start time
     * @param endTime end time
     * @param severityEnum severities
     * @param users users
     *
     * @return true if differs, otherwise false.
     */
    private boolean newRequest(Date startTime, Date endTime, Severity[] severityEnum, List<String> users) {
        if (currentStartTime.compareTo(startTime) != 0 || currentEndTime.compareTo(endTime) != 0
                || currentUsers.size() != users.size() || !currentUsers.containsAll(users)
                || currentSeverityEnum.length != severityEnum.length) {
            return true;
        }
        for (int i = 0; i < severityEnum.length; i++) {
            if (currentSeverityEnum[i] != severityEnum[i]) {
                return true;
            }
        }
        return false;
    }
}
