/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.auth;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.ejbs.interfaces.RBACAccess;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * Permissions bean is a managed bean (<code>permissionsBean</code>), which can verify various permissions for actions
 * performed by the logged in user. Permissions bean is view scoped.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@ManagedBean(name = "permissionsBean")
@ViewScoped
public class PermissionsBean implements Serializable {

    private static final long serialVersionUID = 4308784343095593341L;

    /** Permission name for managing roles (add, remove, edit etc.) */
    public static final String PERM_MANAGE_ROLE = "ManageRole";
    /** Permission name for managing resources (add, edit, remove etc.) */
    public static final String PERM_MANAGE_RESOURCE = "ManageResource";
    /** Permission name for managing rules */
    public static final String PERM_MANAGE_RULE = "ManageRule";
    /** Permission name that allows releasing exclusive access owned by other users */
    public static final String PERM_RELEASE_EXCLUSIVE_ACCESS = "ReleaseExclusiveAccess";
    /** Permission name that allows invalidating tokens */
    public static final String PERM_INVALIDATE_TOKENS = "InvalidateTokens";
    /** Permission name that allows managing access security groups */
    public static final String PERM_MANAGE_ACCESS_SECURITY_GROUPS = "ManageASG";
    /** Resource name of the permissions defined in this class */
    public static final String RESOURCE_NAME = "RBACManagementStudio";

    private static final String[] PERMISSIONS = new String[] { PermissionsBean.PERM_MANAGE_ROLE,
            PermissionsBean.PERM_MANAGE_RESOURCE, PermissionsBean.PERM_MANAGE_RULE,
            PermissionsBean.PERM_RELEASE_EXCLUSIVE_ACCESS, PermissionsBean.PERM_INVALIDATE_TOKENS,
            PermissionsBean.PERM_MANAGE_ACCESS_SECURITY_GROUPS };

    private Map<String, Boolean> permissionsCache;

    @ManagedProperty(value = "#{loginBean.rbacAccessEJB}")
    private RBACAccess rbacAccessEJB;
    @EJB
    private Roles rolesEJB;
    @EJB
    private Resources resourcesEJB;

    private Token localToken;

    /**
     * Sets the {@link RBACAccess} bean, which holds methods for communicating with RBAC authentication and
     * authorisation web services. Injected instance is the one held by the login bean, so that the token of the
     * currently logged in user is used for authorisation.
     * 
     * @param rbacAccessEJB the RBAC access used by this bean to access RBAC services
     */
    public void setRbacAccessEJB(RBACAccess rbacAccessEJB) {
        this.rbacAccessEJB = rbacAccessEJB;
    }

    private boolean getPermission(String permissionName, boolean fetchFromCache) {
        checkIfNotLoggedOutElsewhere();
        if (fetchFromCache) {
            if (rbacAccessEJB.getToken() == null) {
                return false;
            }
            if (permissionsCache == null || !permissionsCache.containsKey(permissionName)) {
                permissionsCache = rbacAccessEJB.userHasPermissions(PERMISSIONS, PermissionsBean.RESOURCE_NAME);
            }
            if (rbacAccessEJB.getToken() == null) {
                permissionsCache.clear();
                return false;
            } else {
                return permissionsCache.get(permissionName);
            }
        } else {
            return rbacAccessEJB.userHasPermission(permissionName, PermissionsBean.RESOURCE_NAME);
        }
    }

    /**
     * Checks whether the currently logged in user may delegate the specified role. User may delegate a role, if he's
     * logged in and if he has the role in question.
     * 
     * @param role to be delegated
     * @return <code>true</code> if the logged in user may delegate the role
     */
    public boolean canDelegateUserRole(UserRole role) {
        checkIfNotLoggedOutElsewhere();
        if (role == null || rbacAccessEJB.getToken() == null) {
            return false;
        } else {
            String name = role.getRole().getName();
            for (String userRole : rbacAccessEJB.getToken().getRoles()) {
                if (userRole.equals(name)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Checks whether the currently logged in user may request exclusive access for the specified user. Users may
     * request exclusive access only for themselves.
     * 
     * @param userInfo of the user for whom exclusive access should be requested
     * @return <code>true</code> if the logged in user may request exclusive access
     */
    public boolean canRequestExclusiveAccessForUser(UserInfo userInfo) {
        checkIfNotLoggedOutElsewhere();
        if (userInfo == null || rbacAccessEJB.getToken() == null) {
            return false;
        } else {
            return userInfo.getUsername().equals(rbacAccessEJB.getToken().getUsername());
        }
    }

    private void checkIfNotLoggedOutElsewhere() {
        if (localToken != null && rbacAccessEJB.getToken() == null) {
            RequestContext.getCurrentInstance().update(Constants.SITE_CONTENT);
            RequestContext.getCurrentInstance().update(Constants.SITE_BANNER);
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Not Signed In",
                            "You are no longer signed in. Probably you signed out from another view."));
            RequestContext.getCurrentInstance().update(Constants.SITE_CONTAINER_GROWL);
        }
        localToken = rbacAccessEJB.getToken();
    }

    /**
     * Checks if exclusive access can be requested. It can be requested if the use has the given permission, the user is
     * logged in, exclusive access is not active or is owned by him or he has permission to release any exclusive
     * access.
     * 
     * @param permission the permission to request exclusive access for
     * @param resource the resource that owns the permission
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return true if request is granted or false otherwise
     */
    public boolean canRequestExclusiveAccessForPermission(Permission permission, Resource resource, boolean fromCache) {
        if (permission == null || resource == null || !permission.isExclusiveAccessAllowed()) {
            return false;
        } else if (rbacAccessEJB.getToken() == null) {
            return false;
        } else if (permission.getExclusiveAccess() == null) {
            return true;
        } else if (permission.getExclusiveAccess().getUserId().equals(rbacAccessEJB.getToken().getUsername())) {
            return true;
        } else {
            return getPermission(PERM_RELEASE_EXCLUSIVE_ACCESS, fromCache);
        }
    }

    /**
     * Checks if the currently logged in user may release any exclusive access, regardless of who owns it.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * 
     * @return true if exclusive access can be released or false otherwise
     */
    public boolean canReleaseAnyExclusiveAccess(boolean fromCache) {
        return getPermission(PERM_RELEASE_EXCLUSIVE_ACCESS, fromCache);
    }

    /**
     * Checks whether the currently logged in user may release exclusive access for the specified user. Users may
     * release exclusive access only for themselves, or if they have {@value #PERM_RELEASE_EXCLUSIVE_ACCESS} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @param userInfo of the user for whom exclusive access should be released
     * @return <code>true</code> if the logged in user may release exclusive access
     */
    public boolean canReleaseExclusiveAccessForUser(UserInfo userInfo, boolean fromCache) {
        if (userInfo == null) {
            return false;
        } else if (getPermission(PERM_RELEASE_EXCLUSIVE_ACCESS, fromCache)) {
            return true;
        } else if (rbacAccessEJB.getToken() != null) {
            return userInfo.getUsername().equals(rbacAccessEJB.getToken().getUsername());
        }
        return false;
    }

    /**
     * Check whether the currently logged in user can release exclusive access for the specified permission. Action is
     * granted if exclusive access is allowed, if it is active, if it is owned by the currently logged in user and if
     * the user has this permission.
     * 
     * @param permission the permission to check
     * @param resource the resource that owns the permission
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return true if release is granted or false otherwise
     */
    public boolean canReleaseExclusiveAccessForPermission(Permission permission, Resource resource, boolean fromCache) {
        if (permission == null || resource == null || !permission.isExclusiveAccessAllowed()
                || permission.getExclusiveAccess() == null) {
            return false;
        } else if (getPermission(PERM_RELEASE_EXCLUSIVE_ACCESS, fromCache)) {
            return true;
        } else if (rbacAccessEJB.getToken() == null) {
            // logged out during permission check
            return false;
        } else {
            return permission.getExclusiveAccess().getUserId().equals(rbacAccessEJB.getToken().getUsername());
        }
    }

    /**
     * Checks whether the currently logged in user may add roles. User may add roles if he has
     * {@value #PERM_MANAGE_ROLE} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user may add roles
     */
    public boolean canAddRole(boolean fromCache) {
        return getPermission(PERM_MANAGE_ROLE, fromCache);
    }

    /**
     * Checks whether the currently logged in user may remove roles. User may remove roles if he has
     * {@value #PERM_MANAGE_ROLE} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user may remove roles
     */
    public boolean canRemoveRole(boolean fromCache) {
        return getPermission(PERM_MANAGE_ROLE, fromCache);
    }

    /**
     * Checks whether the currently logged in user may manage the specified role. User may manage a role if he is the
     * role's manager, or if he has the {@value #PERM_MANAGE_ROLE} permission.
     * 
     * @param role to be managed
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user may manage the role
     */
    public boolean canManageRole(Role role, boolean fromCache) {
        if (role == null) {
            return false;
        } else if (getPermission(PERM_MANAGE_ROLE, fromCache)) {
            return true;
        } else if (rbacAccessEJB.getToken() == null) {
            // maybe user has been logged out during permission check
            return false;
        } else {
            // Re-attach the role to access lazy fields.
            return rolesEJB.getRole(role.getId(),false).getManagers().contains(rbacAccessEJB.getToken().getUsername());
        }

    }

    /**
     * Checks whether the currently logged in user may add resources. User may add resources if he has
     * {@value #PERM_MANAGE_RESOURCE} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user may add resources
     */
    public boolean canAddResource(boolean fromCache) {
        return getPermission(PERM_MANAGE_RESOURCE, fromCache);
    }

    /**
     * Checks whether the currently logged in user may manage the specified resource. User may manage a resource if he
     * is the resource's manager, or if he has the {@value #PERM_MANAGE_RESOURCE} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @param resource to be managed
     * @return <code>true</code> if the logged in user may manage the resource
     */
    public boolean canManageResource(Resource resource, boolean fromCache) {
        if (resource == null) {
            return false;
        } else if (getPermission(PERM_MANAGE_RESOURCE, fromCache)) {
            return true;
        } else if (rbacAccessEJB.getToken() == null) {
            // user has been logged out during permission check
            return false;
        } else {
            // Re-attach the resource to access lazy fields.
            return resourcesEJB.getResource(resource.getId()).getManagers()
                    .contains(rbacAccessEJB.getToken().getUsername());
        }
    }

    /**
     * Checks whether the currently logged in user can manage access security groups. User can manage the groups if he
     * has the {@link #PERM_MANAGE_ACCESS_SECURITY_GROUPS} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user can manage access security groups
     */
    public boolean canManageAccessSecurityGroups(boolean fromCache) {
        return getPermission(PERM_MANAGE_ACCESS_SECURITY_GROUPS, fromCache);
    }

    /**
     * Checks whether the currently logged in user may manage rules. User may manage rules if he has
     * {@value #PERM_MANAGE_RULE} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user may manage rules
     */
    public boolean canManageRule(boolean fromCache) {
        return getPermission(PERM_MANAGE_RULE, fromCache);
    }

    /**
     * Checks whether the currently logged in user may invalidate tokens. User may invalidate tokens if he has
     * {@value #PERM_INVALIDATE_TOKENS} permission.
     * 
     * @param fromCache true if the permission should be loaded from cache or false is RBAC should be consulted
     * @return <code>true</code> if the logged in user may invalidate tokens
     */
    public boolean canInvalidateTokens(boolean fromCache) {
        return getPermission(PERM_INVALIDATE_TOKENS, fromCache);
    }

    /**
     * Returns the locally stored token as it is.
     * @return the locally stored token
     */
    public Token getLocalToken() {
        return localToken;
    }
}
