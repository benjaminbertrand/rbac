/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Expressions;
import se.esss.ics.rbac.ejbs.interfaces.Rules;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>ExpressionsBean</code> is a managed bean (<code>expressionsBean</code>), which contains expressions data and
 * methods for executing actions triggered on <code>/Permissions/Expressions</code> page. Expressions bean is view
 * scoped so it lives as long as user interacting with expressions page view.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "expressionsBean")
@ViewScoped
public class ExpressionsBean implements Serializable {

    private static final long serialVersionUID = -3357182350048618525L;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Expressions expressionsEJB;
    @EJB
    private Rules rulesEJB;
    @Inject
    private ExpressionsRequestCacheBean requestCacheBean;
    
    private String wildcard = Constants.ALL;
    private String newExpressionName;
    private String newExpressionLongName;
    private String newExpressionDescription;
    private String newExpressionDefinition;
    private String updatedExpressionName;
    private String updatedExpressionLongName;
    private String updatedExpressionDescription;
    private String updatedExpressionDefinition;
    private Expression selectedExpression;
    private List<Rule> expressionRules;

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     * 
     * @param permissionsBean the permission bean through which the permissions can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns wildcard pattern which is used for searching by expressions. Pattern is entered into search field on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return wildcard pattern used for searching by expressions.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * Sets wildcard pattern which is used for searching by expressions. Pattern is entered into search field on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param wildcard wildcard pattern used for searching by expressions
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * Returns expression which is selected in expressions list on <code>/Permissions/Expressions</code> page.
     * 
     * @return selected expression.
     */
    public Expression getSelectedExpression() {
        return selectedExpression;
    }

    /**
     * Sets expression which is selected in expressions list on <code>/Permissions/Expressions</code> page.
     * 
     * @param selectedExpression selected expression
     */
    public void setSelectedExpression(Expression selectedExpression) {
        this.selectedExpression = selectedExpression;
    }

    /**
     * @return selected expression name which is shown in expression name field on <code>/Permissions/Expressions</code>
     *         page.
     */
    public String getExpressionName() {
        if (selectedExpression != null) {
            return selectedExpression.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new expression name which is entered into expression name field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return new expression name which is entered into expression name field.
     */
    public String getNewExpressionName() {
        return newExpressionName;
    }

    /**
     * Sets new expression name which is entered into expression name field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param newExpressionName new expression name
     */
    public void setNewExpressionName(String newExpressionName) {
        this.newExpressionName = newExpressionName;
    }

    /**
     * Returns updated expression name which is entered into expression name field in edit expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return updated expression name which is entered into expression name field.
     */
    public String getUpdatedExpressionName() {
        return getExpressionName();
    }

    /**
     * Sets updated expression name which is entered into expression name field in edit expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param updatedExpressionName updated expression name
     */
    public void setUpdatedExpressionName(String updatedExpressionName) {
        this.updatedExpressionName = updatedExpressionName;
    }

    /**
     * @return selected expression long name which is shown in expression long name field on
     *         <code>/Permissions/Expressions</code> page.
     */
    public String getExpressionLongName() {
        if (selectedExpression != null) {
            return selectedExpression.getLongName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new expression long name which is entered into expression long name field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return new expression long name which is entered into expression long name field.
     */
    public String getNewExpressionLongName() {
        return newExpressionLongName;
    }

    /**
     * Sets new expression long name which is entered into expression long name field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param newExpressionLongName new expression long name
     */
    public void setNewExpressionLongName(String newExpressionLongName) {
        this.newExpressionLongName = newExpressionLongName;
    }

    /**
     * Returns updated expression long name which is entered into expression long name field in edit expression dialog
     * on <code>/Permissions/Expressions</code> page.
     * 
     * @return updated expression long name which is entered into expression long name field.
     */
    public String getUpdatedExpressionLongName() {
        return getExpressionLongName();
    }

    /**
     * Sets updated expression long name which is entered into expression long name field in edit expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param updatedExpressionLongName updated expression long name
     */
    public void setUpdatedExpressionLongName(String updatedExpressionLongName) {
        this.updatedExpressionLongName = updatedExpressionLongName;
    }

    /**
     * @return selected expression description which is shown in expression description field on
     *         <code>/Permissions/Expressions</code> page.
     */
    public String getExpressionDescription() {
        if (selectedExpression != null) {
            return selectedExpression.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new expression description which is entered into expression description field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return new expression description which is entered into expression description field.
     */
    public String getNewExpressionDescription() {
        return newExpressionDescription;
    }

    /**
     * Sets new expression description which is entered into expression description field in add expression dialog on
     * <code>/Permissions/Expressions</code> p age.
     * 
     * @param newExpressionDescription new expression description
     */
    public void setNewExpressionDescription(String newExpressionDescription) {
        this.newExpressionDescription = newExpressionDescription;
    }

    /**
     * Returns updated expression description which is entered into expression description field in edit expression
     * dialog on <code>/Permissions/Expressions</code> page.
     * 
     * @return updated expression description which is entered into expression description field.
     */
    public String getUpdatedExpressionDescription() {
        return getExpressionDescription();
    }

    /**
     * Sets updated expression description which is entered into expression description field in edit expression dialog
     * on <code>/Permissions/Expressions</code> page.
     * 
     * @param updatedExpressionDescription updated expression description
     */
    public void setUpdatedExpressionDescription(String updatedExpressionDescription) {
        this.updatedExpressionDescription = updatedExpressionDescription;
    }

    /**
     * @return selected expression definition which is shown in expression definition field on
     *         <code>/Permissions/Expressions</code> page.
     */
    public String getExpressionDefinition() {
        if (selectedExpression != null) {
            return selectedExpression.getDefinition();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new expression definition which is entered into expression definition field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return new expression definition which is entered into expression definition field.
     */
    public String getNewExpressionDefinition() {
        return newExpressionDefinition;
    }

    /**
     * Sets new expression definition which is entered into expression definition field in add expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param newExpressionDefinition new expression definition
     */
    public void setNewExpressionDefinition(String newExpressionDefinition) {
        this.newExpressionDefinition = newExpressionDefinition;
    }

    /**
     * Returns updated expression definition which is entered into expression definition field in edit expression dialog
     * on <code>/Permissions/Expressions</code> page.
     * 
     * @return updated expression definition which is entered into expression definition field.
     */
    public String getUpdatedExpressionDefinition() {
        return getExpressionDefinition();
    }

    /**
     * Sets updated expression definition which is entered into expression definition field in edit expression dialog on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param updatedExpressionDefinition updated expression definition
     */
    public void setUpdatedExpressionDefinition(String updatedExpressionDefinition) {
        this.updatedExpressionDefinition = updatedExpressionDefinition;
    }

    /**
     * Retrieves list of expressions from database and returns it. Expressions are shown in expressions list on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @return list of expressions retrieved from database.
     */
    public List<Expression> getExpressions() {
        List<Expression> expressions = fetchExpressions(false);
        if (!Util.contains(expressions, selectedExpression)) {
            selectedExpression = null;
        }
        return expressions;
    }

    /**
     * @return list of rules retrieved from database which corresponds to the selected expression.
     */
    public List<Rule> getRules() {
        if (selectedExpression != null) {
            expressionRules = requestCacheBean.getRulesByExpression(selectedExpression);
            return expressionRules;
        }
        return new ArrayList<>();
    }

    /**
     * Creates new permission if user has permission for managing rules. Method is called when OK button, in add
     * expression dialog on <code>/Permissions/Expressions</code> page is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void createExpression() {
        if (!permissionsBean.canManageRule(false)) {
            return;
        }
        final Expression expression = new Expression();
        expression.setDefinition(newExpressionDefinition);
        expression.setDescription(newExpressionDescription);
        expression.setLongName(newExpressionLongName);
        expression.setName(newExpressionName);
        String success = Messages.getString(Messages.SUCCESSFUL_EXPRESSION_CREATION, loginBean.getUsername(),
                expression.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EXPRESSION_CREATION, loginBean.getUsername(),
                expression.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                expressionsEJB.createExpression(expression);
                selectedExpression = expression;
                fetchExpressions(true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected expression if user has permission for managing rules. Method is called when remove button on
     * <code>/Permissions/Expressions</code> page is clicked. Results of the action are logged and shown to the user as
     * growl message.
     */
    public void removeExpression() {
        if (!permissionsBean.canManageRule(false) || isRemoveExpressionButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_EXPRESSION_REMOVAL, loginBean.getUsername(),
                selectedExpression.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EXPRESSION_REMOVAL, loginBean.getUsername(),
                selectedExpression.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                expressionsEJB.removeExpression(selectedExpression);
                if (expressionRules != null && !expressionRules.isEmpty()) {
                    List<Rule> updatedRules = new ArrayList<>();
                    for (Rule rule : expressionRules) {
                        rule.getExpression().remove(selectedExpression);
                        updatedRules.add(rule);
                    }
                    rulesEJB.updateRules(updatedRules);
                }
                selectedExpression = null;
                fetchExpressions(true);
            }
        });
    }

    /**
     * Updates selected expression if user has permission for managing rules. Method is called when OK button, in edit
     * expression dialog on <code>/Permissions/Expressions</code> page is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void updateExpression() {
        if (!permissionsBean.canManageRule(false) || isEditExpressionInfoButtonDisabled()) {
            return;
        }
        selectedExpression.setDefinition(updatedExpressionDefinition);
        selectedExpression.setDescription(updatedExpressionDescription);
        selectedExpression.setLongName(updatedExpressionLongName);
        selectedExpression.setName(updatedExpressionName);
        String success = Messages.getString(Messages.SUCCESSFUL_EXPRESSION_UPDATE, loginBean.getUsername(),
                selectedExpression.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EXPRESSION_UPDATE, loginBean.getUsername(),
                selectedExpression.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                expressionsEJB.updateExpression(selectedExpression);
                fetchExpressions(true);
            }
        });
    }

    /**
     * Remove expression button is disabled if no expression is selected.
     * 
     * @return true if remove expression button is disabled, otherwise false.
     */
    public boolean isRemoveExpressionButtonDisabled() {
        return selectedExpression == null;
    }

    /**
     * Edit expression button is disabled if no expression is selected.
     * 
     * @return true if edit expression button is disabled, otherwise false.
     */
    public boolean isEditExpressionInfoButtonDisabled() {
        return selectedExpression == null;
    }

    /**
     * Method that shows dialog for editing expressions. Dialog is shown when edit expression info button on
     * <code>/Permissions/Expressions</code> page is clicked and if user has permissions for managing rules.
     */
    public void showEditExpressionDialog() {
        if (!permissionsBean.canManageRule(false) || isEditExpressionInfoButtonDisabled()) {
            return;
        }
        RequestContext.getCurrentInstance().update("editExpressionForm");
        RequestContext.getCurrentInstance().execute("PF('editExpressionDialog').show()");
    }

    /**
     * Method that shows dialog for adding expressions. Dialog is shown when add expression button on
     * <code>/Permissions/Expressions</code> page is clicked and if user has permissions for managing rules.
     */
    public void showAddExpressionDialog() {
        if (!permissionsBean.canManageRule(false)) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addExpressionDialog').show()");
    }

    /**
     * Resets variables used in add expression dialog on <code>/Permissions/Expressions</code> page to the default
     * values.
     */
    public void setDefaultValues() {
        newExpressionName = Constants.EMPTY_STRING;
        newExpressionLongName = Constants.EMPTY_STRING;
        newExpressionDescription = Constants.EMPTY_STRING;
        newExpressionDefinition = Constants.EMPTY_STRING;
    }
    
    /**
     * @param fromDB fetch expressions from database, or return cached expressions.
     * 
     * @return list of expressions
     */
    private List<Expression> fetchExpressions(boolean fromDB) {
        List<Expression> expressions = null;
        if (Constants.ALL.equals(wildcard)) {
            expressions = requestCacheBean.getExpressions(fromDB);
        } else {
            expressions = requestCacheBean.getExpressionsByWildcard(wildcard + Constants.ALL_DB, fromDB);
        }
        return expressions;
    }
}
