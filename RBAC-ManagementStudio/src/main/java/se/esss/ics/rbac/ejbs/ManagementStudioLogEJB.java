/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.ManagementStudioLog;
import se.esss.ics.rbac.ManagementStudioLog.Severity;
import se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>ManagementStudioLogEJB</code> is a singleton bean containing utility methods for logging user actions. Contains
 * methods for retrieving log entries, and for inserting log entries with different severities (INFO, WARN, ERROR). All
 * methods are defined in <code>ManagementStudioLogs</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Singleton
public class ManagementStudioLogEJB implements ManagementStudioLogs {

    private static final long serialVersionUID = -3904955347571512591L;

    private static final String NULL_USERID = "unknown";

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs#getLogs(java.util.Date, java.util.Date,
     * se.esss.ics.rbac.ManagementStudioLog.Severity[], java.util.List)
     */
    @Override
    public List<ManagementStudioLog> getLogs(Date startTime, Date endTime, Severity[] severities, List<String> users) {
        return em.createNamedQuery("ManagementStudioLog.filterLogs", ManagementStudioLog.class)
                .setParameter("startTime", startTime).setParameter("endTime", endTime)
                .setParameter("severities", Arrays.asList(severities)).setParameter("users", users).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs#queueLogEntry(java.lang.String,
     * se.esss.ics.rbac.ManagementStudioLog.Severity, java.lang.String)
     */
    @Override
    public void queueLogEntry(String userID, Severity severity, String content) {
        ManagementStudioLog entry = new ManagementStudioLog();
        entry.setTimestamp(new Timestamp(System.currentTimeMillis()));
        entry.setUserID(userID == null ? NULL_USERID : userID);
        entry.setSeverity(severity);
        entry.setContent(content);
        em.persist(entry);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs#info(java.lang.String, java.lang.String)
     */
    @Override
    public void info(String userID, String content) {
        queueLogEntry(userID, Severity.INFO, content);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs#warn(java.lang.String, java.lang.String)
     */
    @Override
    public void warn(String userID, String content) {
        queueLogEntry(userID, Severity.WARNING, content);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs#error(java.lang.String, java.lang.String)
     */
    @Override
    public void error(String userID, String content) {
        queueLogEntry(userID, Severity.ERROR, content);
    }
}
