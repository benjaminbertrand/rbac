/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.UserRole;

/**
 * <code>UserRoles</code> interface defines methods for dealing with tokens.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface UserRoles extends Serializable {

    /**
     * Retrieves user roles which belongs to the user identified by <code>userId</code> from database and returns it.
     * 
     * @param userId unique user identifier
     * 
     * @return list of the user roles which belongs to the specific user.
     */
    List<UserRole> getUserRoles(String userId);

    /**
     * Retrieves usernames of the users which are assigned to the role identified by <code>roleId</code> from database
     * and returns it.
     * 
     * @param roleId unique role identifier
     * 
     * @return list of the usernames of the users which are assigned to the specific role.
     */
    List<String> getUsernames(int roleId);

    /**
     * Inserts created user role into database.
     * 
     * @param userRole created user role
     */
    void createUserRole(UserRole userRole);

    /**
     * Inserts created userRoles from the given list <code>userRoles</code> into database.
     * 
     * @param userRoles list of the created user roles
     */
    void createUserRoles(List<UserRole> userRoles);

    /**
     * Updates user role.
     * 
     * @param userRole updated user role
     */
    void updateUserRole(UserRole userRole);

    /**
     * Removes user role from database.
     * 
     * @param userRole user role which will be removed from database.
     */
    void removeUserRole(UserRole userRole);
}
