/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.ldap;

import java.io.IOException;
import java.io.Serializable;

import org.apache.directory.api.ldap.codec.api.DefaultConfigurableBinaryAttributeDetector;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.util.Strings;
import org.apache.directory.ldap.client.api.LdapAsyncConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;

/**
 *
 * <code>LDAPConnectionFactory</code> is a factory that takes care of creating and closing LDAP connections.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class LDAPConnectionFactory implements Serializable {

    private static final long serialVersionUID = -698813921240409233L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(LDAPConnectionFactory.class);

    /** Timeout in milliseconds for all connections */
    static final long TIMEOUT;
    static {
        long timeout = 15000;
        try {
            String prop = LDAPProperties.getInstance().getProperty(LDAPProperties.KEY_LDAP_TIMEOUT);
            if (prop != null) {
                timeout = Long.parseLong(prop);
            }
        } catch (Exception e) {
            LOGGER.error("Cannot initialize timeout. Using default value.",e);
        }
        TIMEOUT = timeout;
    }

    /**
     * <code>LogSuppressConnection</code> is an LDAP connection which suppresses certain log entries.
     */
    private static class LogSuppressConnection extends LdapNetworkConnection {
        private static final String CONNECTION_RESET_MESSAGE = "Connection reset by peer";

        /**
         * Constructs a new connection to the host through port.
         *
         * @param host the host address
         * @param port the connection port
         * @see LdapNetworkConnection#LdapNetworkConnection(String, int)
         */
        public LogSuppressConnection(String host, int port) {
            super(host, port);
            setTimeOut(TIMEOUT);
        }

        /**
         * Construct new connection.
         *
         * @param config the configuration
         * @see LdapNetworkConnection#LdapNetworkConnection(LdapConnectionConfig)
         */
        public LogSuppressConnection(LdapConnectionConfig config) {
            super(config);
            setTimeOut(TIMEOUT);
        }

        /*
         * @see org.apache.directory.ldap.client.api.LdapNetworkConnection#exceptionCaught(
         * org.apache.mina.core.session.IoSession, java.lang.Throwable)
         */
        @Override
        public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
            if (cause instanceof IOException && cause.getMessage().contains(CONNECTION_RESET_MESSAGE)) {
                LOGGER.warn(cause.getMessage());
                session.setAttribute("sessionException", cause);
                session.close(true);
            } else {
                super.exceptionCaught(session, cause);
            }
        }
    }

    private enum SecurityMethod {NONE, SSL, TLS }

    private final String valuePrimaryHost;
    private final int valuePrimaryPort;
    private final String valueSecondaryHost;
    private final int valueSecondaryPort;
    private final String valueSecurityPrincipal;
    private final String valueSecurityCredentials;
    private final SecurityMethod valueSecurityMethod;

    private transient LdapAsyncConnection defaultConnection;
    private boolean usingPrimaryConnection = false;

    /**
     * Construct a new factory.
     *
     * @throws DirectoryServiceAccessException if there was an error reading the default connection properties
     */
    public LDAPConnectionFactory() throws DirectoryServiceAccessException {
        LDAPProperties properties = LDAPProperties.getInstance();
        valuePrimaryHost = properties.getProperty(LDAPProperties.KEY_PRIMARY_HOST);
        valuePrimaryPort = Integer.parseInt(properties.getProperty(LDAPProperties.KEY_PRIMARY_PORT, "389"));
        valueSecondaryHost = properties.getProperty(LDAPProperties.KEY_SECONDARY_HOST);
        valueSecondaryPort = Integer.parseInt(properties.getProperty(LDAPProperties.KEY_SECONDARY_PORT, "389"));
        valueSecurityPrincipal = properties.getProperty(LDAPProperties.KEY_SECURITY_PRINCIPAL);
        valueSecurityCredentials = properties.getProperty(LDAPProperties.KEY_SECURITY_CREDENTIALS);
        valueSecurityMethod = getSecurityMethod(properties.getProperty(LDAPProperties.KEY_SECURITY_METHOD, "SSL"));
    }

    /**
     * Returns an LDAP connection, bound with default credentials. LDAP connection is created with default environment
     * settings. Connection credentials are retrieved as LDAP property values
     * {@link LDAPProperties#KEY_SECURITY_PRINCIPAL} and {@link LDAPProperties#KEY_SECURITY_CREDENTIALS}. Connection is
     * initially attempted on primary LDAP provider, but if that fails, it attempts to connect to the secondary
     * provider.
     *
     * @return LDAP connection, bound with default credentials.
     *
     * @throws LdapException if an error is encountered while creating connection on both provider URLs
     */
    public synchronized LdapAsyncConnection getDefaultConnection() throws LdapException {
        if (defaultConnection == null || !defaultConnection.isConnected()) {
            if (defaultConnection != null) {
                try {
                    defaultConnection.connect();
                    if (valueSecurityPrincipal != null && valueSecurityCredentials != null) {
                        defaultConnection.bind(valueSecurityPrincipal, valueSecurityCredentials);
                    }
                } catch (LdapException | IllegalStateException e) {
                    LOGGER.warn("Could not establish connection to " + defaultConnection.getConfig().getLdapHost()
                            + ".", e);
                }
                if (defaultConnection.isConnected()) {
                    return defaultConnection;
                }
                closeConnection(defaultConnection);
                defaultConnection = null;
            }

            defaultConnection = getConnection();
        }
        if (defaultConnection == null) {
            usingPrimaryConnection = false;
            throw new LdapException("Could not establish LDAP connection to '" + valuePrimaryHost + "' or '"
                    + valueSecondaryHost + "'.");
        }
        return defaultConnection;
    }

    /**
     * Constructs a new LDAP connection based on the default connection configuration.
     *
     * @return the connection
     * @throws LdapException if there was an error in creating the connection
     */
    public LdapAsyncConnection createNewConnection() throws LdapException {
        return new LogSuppressConnection(getDefaultConnection().getConfig());
    }

    /**
     * Creates a connection to the primary or secondary host. Depending on which connection was tried to establish last,
     * this method will try to establish connection to the other one. If one is successful, it will be returned
     *
     * @return the connection if established successfully or null if connection was unable to establish
     */
    private LdapAsyncConnection getConnection() {
        for (int i = 0; i < 2; i++) {
            LdapAsyncConnection connection = null;
            String host = usingPrimaryConnection ? valueSecondaryHost : valuePrimaryHost;
            int port = usingPrimaryConnection ? valueSecondaryPort : valuePrimaryPort;
            try {
                usingPrimaryConnection ^= true;
                connection = new LogSuppressConnection(buildConfig(host, port, valueSecurityMethod));
                if (valueSecurityPrincipal != null && valueSecurityCredentials != null) {
                    connection.bind(valueSecurityPrincipal, valueSecurityCredentials);
                }
                return connection;
            } catch (LdapException e) {
                LOGGER.warn("Could not establish connection to '" + host + "'.", e);
                closeConnection(connection);
            }
        }

        return null;
    }

    /**
     * Switch the connection.
     */
    synchronized void switchConnection() {
        closeConnection(defaultConnection);
        defaultConnection = null;
        usingPrimaryConnection ^= true;
    }

    /**
     * Close the default connection.
     */
    synchronized void close() {
        closeConnection(defaultConnection);
    }

    /**
     * Close the given connection. On exception log the exception as an error.
     *
     * @param connection the connection to close.
     */
    public static void closeConnection(LdapAsyncConnection connection) {
        if (connection != null) {
            try {
                connection.unBind();
            } catch (LdapException e) {
                LOGGER.warn("Could not unbind connection " + connection.getConfig().getLdapHost() + ".");
            }
            try {
                connection.close();
            } catch (IOException ex) {
                LOGGER.warn("Could not close connection to " + connection.getConfig().getLdapHost() + ".");
            }
        }
    }

    /**
     * Utility method to get <tt>SecurityMethod</tt> from given value.
     *
     * <p>
     * Note that <tt>SecurityMethod</tt> values are mutually exclusive.
     *
     * @param method for comparison
     * @return
     *
     * @see SecurityMethod
     * @see SecurityMethod#NONE
     * @see SecurityMethod#SSL
     * @see SecurityMethod#TLS
     */
    private SecurityMethod getSecurityMethod(String method) {
        if (SecurityMethod.NONE.toString().equalsIgnoreCase(method)) {
            return SecurityMethod.NONE;
        } else if (SecurityMethod.SSL.toString().equalsIgnoreCase(method)) {
            return SecurityMethod.SSL;
        } else if (SecurityMethod.TLS.toString().equalsIgnoreCase(method)) {
            return SecurityMethod.TLS;
        } else  {
            return SecurityMethod.SSL;
        }
    }

    /**
     * Utility method to help build <tt>LdapConnectionConfig</tt> object for <tt>LdapConnection</tt>.
     *
     * <p>
     * Important to consider combination of port and securityMethod. E.g. <tt>SecurityMethod.SSL</tt>
     * and non-LDAPS port will not work,
     *
     * @param server ldap host.
     *               If null or empty value then replaced by <tt>LdapConnectionConfig#DEFAULT_LDAP_HOST</tt>.
     * @param port ldap port.
     *             If negative value then replaced by
     *             <tt>LdapConnectionConfig#DEFAULT_LDAPS_PORT</tt> or <tt>LdapConnectionConfig#DEFAULT_LDAP_PORT</tt>,
     *             depending on <code>securityMethod</tt> parameter.
     * @param securityMethod ldap security method
     * @return
     *
     * @see SecurityMethod
     * @see SecurityMethod#NONE
     * @see SecurityMethod#SSL
     * @see SecurityMethod#TLS
     * @see LdapConnectionConfig
     * @see LdapConnectionConfig#DEFAULT_LDAP_HOST
     * @see LdapConnectionConfig#DEFAULT_LDAP_PORT
     * @see LdapConnectionConfig#DEFAULT_LDAPS_PORT
     * @see LdapNetworkConnection#buildConfig(String, int, boolean)
     */
    private static LdapConnectionConfig buildConfig(String server, int port, SecurityMethod securityMethod) {
        // note
        //     SecurityMethod values mutually exclusive
        //     no test for combinations of port and securityMethod
        //         port:           -/ldap/ldaps
        //         securityMethod: none, ssl, tls

        boolean useSsl = SecurityMethod.SSL == securityMethod;
        boolean useTls = SecurityMethod.TLS == securityMethod;
        server = Strings.isEmpty(server) ? LdapConnectionConfig.DEFAULT_LDAP_HOST : server;
        port = port < 0 ? port : useSsl ? LdapConnectionConfig.DEFAULT_LDAPS_PORT : LdapConnectionConfig.DEFAULT_LDAP_PORT;

        LdapConnectionConfig config = new LdapConnectionConfig();
        config.setUseSsl(useSsl);
        config.setUseTls(useTls);
        config.setLdapHost(server);
        config.setLdapPort(port);
        config.setBinaryAttributeDetector(new DefaultConfigurableBinaryAttributeDetector());

        return config;
    }

}
