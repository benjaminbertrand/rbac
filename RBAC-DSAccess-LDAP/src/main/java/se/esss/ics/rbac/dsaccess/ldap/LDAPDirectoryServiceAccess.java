/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.ldap;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.annotation.PreDestroy;
import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import org.apache.directory.api.ldap.codec.controls.search.pagedSearch.PagedResultsDecorator;
import org.apache.directory.api.ldap.codec.controls.search.pagedSearch.PagedResultsFactory;
import org.apache.directory.api.ldap.codec.standalone.StandaloneLdapApiService;
import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.CursorLdapReferralException;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.cursor.SearchCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.BindRequest;
import org.apache.directory.api.ldap.model.message.BindRequestImpl;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.message.SearchRequest;
import org.apache.directory.api.ldap.model.message.SearchRequestImpl;
import org.apache.directory.api.ldap.model.message.SearchResultDone;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.message.controls.PagedResults;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.api.util.Strings;
import org.apache.directory.ldap.client.api.EntryCursorImpl;
import org.apache.directory.ldap.client.api.LdapAsyncConnection;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;

/**
 *
 * <code>LDAPDirectoryServiceAccess</code> is an implementation of {@link DirectoryServiceAccess} which retrieves data
 * from the directory service using LDAP protocol. A configurable LDAP account is used to retrieve information about
 * different users. When logging in only the username needs to be provided instead of the complete distinguished name.
 * All information is returned as {@link LDAPUserInfo} and contains only information that can be retrieved from LDAP.
 * <p>
 * LDAP connection and search parameters are defined in <code>ldap.properties</code> file, which has to be in the
 * classpath root. Alternatively the parameters can be provided through system properties.
 * </p>
 * <p>
 * This implementation is annotated with {@link Singleton} to allow its usage in EJBs. The implementation is thread safe
 * and uses CONTAINER managed synchronisation, with a timeout of 15 seconds. Methods are also synchronised so they are
 * thread safe in JSE. 15 seconds is chosen, because the default LDAP timeout is set at 7 seconds. Up to two attempts
 * are made when talking to the active directory; therefore, the timeout needs to be more than 2*7 s.
 * </p>
 *
 * @see LDAPProperties
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@AccessTimeout(value = 15, unit = TimeUnit.SECONDS)
public class LDAPDirectoryServiceAccess implements DirectoryServiceAccess, AutoCloseable {

    private static final long serialVersionUID = 1632460356363112154L;

    private static final transient Logger LOG = LoggerFactory.getLogger(LDAPDirectoryServiceAccess.class);

    private static class NonNullSet<E> extends HashSet<E> {
        private static final long serialVersionUID = 2561064703971551931L;

        NonNullSet() {
            super();
        }

        @Override
        public boolean add(E e) {
            if (e == null) {
                return false;
            }
            return super.add(e);
        }
    }

    // pattern that matches all non-printable ascii characters: 0-31, 127, 255
    private static final Pattern INVALID_CHARACTER_PATTERN = Pattern.compile("[\\x00-\\x1F\\x7F\\xFF]");
    private static final String EMPTY_STRING = "";
    private static final String TIMEOUT = "TimeOut occurred";

    private final String valueSearchName;
    private final String valueSearchFilterUsername;
    private final String valueSearchFilterFirstName;
    private final String valueSearchFilterLastName;
    private final String valueSearchFilterObject;

    private final String valueAttributeUsername;
    private final String valueAttributeFirstName;
    private final String valueAttributeMiddleName;
    private final String valueAttributeLastName;
    private final String valueAttributeGroup;
    private final String valueAttributeGroupFilter;
    private final String valueAttributeEmail;
    private final String valueAttributePhone;
    private final String valueAttributeMobile;
    private final String valueAttributeLocation;
    private final int searchPageSize;

    private final String[] interestingAttributes;

    private final LDAPConnectionFactory connectionFactory;

    /**
     * Constructs a new LDAPDirectoryServices and loads the properties.
     *
     * @throws DirectoryServiceAccessException if loading of the properties failed
     */
    public LDAPDirectoryServiceAccess() throws DirectoryServiceAccessException {
        LDAPProperties properties = LDAPProperties.getInstance();

        valueSearchName = properties.getProperty(LDAPProperties.KEY_SEARCH_NAME);
        valueSearchFilterUsername = properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_USERNAME);
        valueSearchFilterFirstName = properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_FIRST_NAME);
        valueSearchFilterLastName = properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_LAST_NAME);
        valueSearchFilterObject = properties.getProperty(LDAPProperties.KEY_SEARCH_FILTER_OBJECT);

        valueAttributeUsername = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_USERNAME);
        valueAttributeFirstName = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_FIRST_NAME);
        valueAttributeMiddleName = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_MIDDLE_NAME);
        valueAttributeLastName = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_LAST_NAME);
        valueAttributeGroup = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_GROUP);
        valueAttributeGroupFilter = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_GROUP_FILTER);
        valueAttributeEmail = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_EMAIL);
        valueAttributePhone = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_PHONE);
        valueAttributeMobile = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_MOBILE);
        valueAttributeLocation = properties.getProperty(LDAPProperties.KEY_ATTRIBUTE_LOCATION);
        searchPageSize = Integer.parseInt(properties.getProperty(LDAPProperties.KEY_PAGE_SIZE, "500"));

        List<String> attrs = new ArrayList<>(9);
        if (valueAttributeUsername != null && !valueAttributeUsername.isEmpty()) {
            attrs.add(valueAttributeUsername);
        }
        if (valueAttributeFirstName != null && !valueAttributeFirstName.isEmpty()) {
            attrs.add(valueAttributeFirstName);
        }
        if (valueAttributeMiddleName != null && !valueAttributeMiddleName.isEmpty()) {
            attrs.add(valueAttributeMiddleName);
        }
        if (valueAttributeLastName != null && !valueAttributeLastName.isEmpty()) {
            attrs.add(valueAttributeLastName);
        }
        if (valueAttributeGroup != null && !valueAttributeGroup.isEmpty()) {
            attrs.add(valueAttributeGroup);
        }
        if (valueAttributeEmail != null && !valueAttributeEmail.isEmpty()) {
            attrs.add(valueAttributeEmail);
        }
        if (valueAttributePhone != null && !valueAttributePhone.isEmpty()) {
            attrs.add(valueAttributePhone);
        }
        if (valueAttributeMobile != null && !valueAttributeMobile.isEmpty()) {
            attrs.add(valueAttributeMobile);
        }
        if (valueAttributeLocation != null && !valueAttributeLocation.isEmpty()) {
            attrs.add(valueAttributeLocation);
        }
        interestingAttributes = attrs.toArray(new String[attrs.size()]);

        connectionFactory = new LDAPConnectionFactory();
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#login(char[], char[])
     */
    @Override
    public UserInfo login(char[] username, char[] password) throws DirectoryServiceAccessException {
        if (areParametersFaulty(username, password)) {
            return null;
        }

        LdapAsyncConnection userConnection = null;
        try {
            List<Entry> userEntries = searchForUsername(username, true);
            if (!userEntries.isEmpty()) {
                // Runs through user search results and returns the first one
                // that can be correctly authenticated with the given password.
                userConnection = connectionFactory.createNewConnection();
                for (Entry userEntry : userEntries) {
                    LDAPUserInfo info = bind(userEntry, userConnection, password);
                    if (info != null) {
                        return info;
                    }
                }
            }
            return null;
        } catch (LdapException | CharacterCodingException e) {
            throw new DirectoryServiceAccessException(e);
        } finally {
            LDAPConnectionFactory.closeConnection(userConnection);
        }
    }

    private LDAPUserInfo bind(Entry userEntry, LdapConnection userConnection, char[] password) throws LdapException,
            CharacterCodingException {
        try {
            BindRequest request = new BindRequestImpl();
            request.setDn(userEntry.getDn());
            ByteBuffer buffer = Charset.forName("UTF-8").newEncoder().encode(CharBuffer.wrap(password));
            request.setCredentials(Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit()));
            ResultCodeEnum.processResponse(userConnection.bind(request));
            return userInfoFromSearchEntry(userEntry);
        } catch (LdapException userException) {
            // AcceptSecurityContext error indicates incorrect credentials.
            if (!userException.getMessage().contains("AcceptSecurityContext error")) {
                throw userException;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#logout(char[])
     */
    @Override
    public boolean logout(char[] username) {
        // no logout required by LDAP
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#getUserInfo(char[])
     */
    @Override
    public UserInfo getUserInfo(char[] username) throws DirectoryServiceAccessException {
        if (username == null || username.length == 0) {
            throw new DirectoryServiceAccessException("Username must not be null or empty.");
        }
        try {
            List<Entry> userEntries = searchForUsername(username, true);
            if (userEntries.size() > 1) {
                throw new DirectoryServiceAccessException("Found more than one user with username '"
                        + new String(username) + "'.");
            } else if (userEntries.isEmpty()) {
                return null;
            }
            return userInfoFromSearchEntry(userEntries.get(0));
        } catch (LdapException e) {
            throw new DirectoryServiceAccessException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.DirectoryServiceAccess#getUserField(char[], java.lang.String)
     */
    @Override
    public String getUserField(char[] username, String fieldName) throws DirectoryServiceAccessException {
        if (username == null || username.length == 0 || fieldName == null || fieldName.isEmpty()) {
            throw new DirectoryServiceAccessException("Username and field name must not be null or empty.");
        }
        try {
            List<Entry> userEntries = searchForUsername(username, true);
            if (userEntries.isEmpty()) {
                return null;
            } else if (userEntries.size() > 1) {
                throw new DirectoryServiceAccessException("Found more than one user with username '"
                        + String.copyValueOf(username) + "'.");
            }

            Collection<Attribute> attributes = userEntries.get(0).getAttributes();
            for (Attribute attribute : attributes) {
                if (attribute.getId().equalsIgnoreCase(fieldName)) {
                    if (attribute.get() != null) {
                        return attribute.get().getString();
                    }
                    break;
                }
            }
            return null;
        } catch (LdapException e) {
            throw new DirectoryServiceAccessException("Could not read the value of '" + fieldName + "' for user '"
                    + String.copyValueOf(username) + "'.", e);
        }
    }

    /**
     * Returns an array of all users whose username, first name or last name matches the given wildcard. Method uses
     * search filters defined by property values {@link LDAPProperties#KEY_SEARCH_FILTER_USERNAME},
     * {@link LDAPProperties#KEY_SEARCH_FILTER_FIRST_NAME}, and {@link LDAPProperties#KEY_SEARCH_FILTER_LAST_NAME} to
     * gather the users' information. It also filters out users with the same username, which are being treated as
     * duplicated search results and users without any of the attributes it searches for (e.g. user without name).
     *
     * @param wildcard the wildcard to use for search
     * @param searchByUsername indicates if the wildcard pattern should be used to search all the usernames
     * @param searchByFirstName indicates if the wildcard pattern should be used to search all the first names
     * @param searchByLastName indicates if the wildcard pattern should be used to search all the last names
     *
     * @return all user info objects that match the wildcard
     *
     * @throws DirectoryServiceAccessException is thrown, if there is an error while opening LDAP connection, if there
     *         is an error while searching through LDAP or if there is an error while reading LDAP properties
     */
    @Override
    public UserInfo[] getUsers(String wildcard, boolean searchByUsername, boolean searchByFirstName,
            boolean searchByLastName) throws DirectoryServiceAccessException {
        if ((!searchByUsername && !searchByFirstName && !searchByLastName) || wildcard == null) {
            return new UserInfo[0];
        }
        try {
            Collection<UserInfo> userInfos = new NonNullSet<>();
            if ("*".equals(wildcard)) {
                // if searching for all entries, search only by username
                doSearch(valueSearchFilterUsername, wildcard, userInfos);
            } else {
                // if not searching any string, do separate searches
                if (searchByUsername) {
                    doSearch(valueSearchFilterUsername, wildcard, userInfos);
                }
                if (searchByFirstName) {
                    doSearch(valueSearchFilterFirstName, wildcard, userInfos);
                }
                if (searchByLastName) {
                    doSearch(valueSearchFilterLastName, wildcard, userInfos);
                }
            }
            LDAPUserInfo[] infoArray = userInfos.toArray(new LDAPUserInfo[userInfos.size()]);
            Arrays.sort(infoArray);
            return infoArray;
        } catch (LdapException e) {
            throw new DirectoryServiceAccessException(e);
        }
    }

    /**
     * Makes a search using the given filter and wildcard. The method decides whether to make a paged or direct search.
     *
     * @param filterName the filter used for searching
     * @param wildcard the searched parameter
     * @param userInfos the collection into which the results will be stored as well
     *
     * @return the collection of retrieved user info objects
     *
     * @throws LdapException is connection failed even after the connection switch
     */
    private Collection<UserInfo> doSearch(String filterName, String wildcard, Collection<UserInfo> userInfos)
            throws LdapException {
        if (searchPageSize > 0) {
            return doPagedSearch(filterName, wildcard, userInfos, true);
        } else {
            return doBruteForceSearch(filterName, wildcard, userInfos, true);
        }
    }

    /**
     * Makes a paged search using the given filter and wildcard. The page size is specified by one of the parameters.
     * This method is synchronised and locked in order for synchronisation to work in JEE and JSE.
     *
     * @param filterName the filter used for searching
     * @param wildcard the searched parameter
     * @param userInfos the collection into which the results will be stored as well
     * @param firstAttempt true if this is the first attempt to search or false if the second. On first attempt a
     *        connection switch is made.
     * @return the collection of retrieved user info objects
     * @throws LdapException is connection failed even after the connection switch
     */
    @Lock(LockType.WRITE)
    private synchronized Collection<UserInfo> doPagedSearch(String filterName, String wildcard,
            Collection<UserInfo> userInfos, boolean firstAttempt) throws LdapException {
        System.setProperty(StandaloneLdapApiService.CONTROLS_LIST, PagedResultsFactory.class.getName());
        SearchRequest request = new SearchRequestImpl().setBase(new Dn(valueSearchName)).setScope(SearchScope.SUBTREE)
                .setFilter("(&" + MessageFormat.format(filterName, wildcard) + valueSearchFilterObject + ")")
                .addAttributes(interestingAttributes).setTimeLimit((int) (LDAPConnectionFactory.TIMEOUT / 1000));

        try {
            LdapAsyncConnection connection = connectionFactory.getDefaultConnection();
            PagedResults pagedSearchControl = new PagedResultsDecorator(connection.getCodecService());
            pagedSearchControl.setSize(searchPageSize);
            EntryCursor usernameEntries = null;
            while (true) {
                try {
                    request.addControl(pagedSearchControl);
                    usernameEntries = new EntryCursorImpl(connection.search(request));
                    while (usernameEntries.next()) {
                        try {
                            userInfos.add(userInfoFromSearchEntry(usernameEntries.get()));
                        } catch (CursorLdapReferralException e) {
                            // ignore
                        }
                    }

                    SearchResultDone result = usernameEntries.getSearchResultDone();
                    pagedSearchControl = (PagedResults) result.getControl(PagedResults.OID);
                    if (result.getLdapResult().getResultCode() == ResultCodeEnum.UNWILLING_TO_PERFORM) {
                        throw new IllegalStateException("Active directory cannot handle paging.");
                    }
                } catch (CursorException e) {
                    LOG.error("Exception while traversing the search results for '" + wildcard + "' with filter '"
                            + filterName + "'.", e);
                } finally {
                    if (usernameEntries != null) {
                        try {
                            usernameEntries.close();
                        } catch (IOException e) {
                            // ignore
                        }
                    }
                }

                if (Strings.isEmpty(pagedSearchControl.getCookie())) {
                    break;
                }
                pagedSearchControl.setSize(searchPageSize);
            }
        } catch (LdapException e) {
            // connection might be dropped, while doing the search. Switch to alternative LDAP
            if (TIMEOUT.equals(e.getMessage()) && firstAttempt) {
                connectionFactory.switchConnection();
                return doPagedSearch(filterName, wildcard, userInfos, false);
            } else {
                throw e;
            }
        }

        return userInfos;
    }

    /**
     * Makes a direct search using the given filter and wildcard. If the LDAP server has a page limit, the number of
     * results are truncated to that limit. This method is synchronised and locked in order for synchronisation to work
     * in JEE and JSE.
     *
     * @param filterName the filter used for searching
     * @param wildcard the searched parameter
     * @param userInfos the collection into which the results will be stored as well
     * @param firstAttempt true if this is the first attempt to search or false if the second. On first attempt a
     *        connection switch is made.
     * @return the collection of retrieved user info objects
     * @throws LdapException is connection failed even after the connection switch
     */
    @Lock(LockType.WRITE)
    private synchronized Collection<UserInfo> doBruteForceSearch(String filterName, String wildcard,
            Collection<UserInfo> userInfos, boolean firstAttempt) throws LdapException {
        String filter = "(&" + MessageFormat.format(filterName, wildcard) + valueSearchFilterObject + ")";
        SearchRequest request = new SearchRequestImpl().setBase(new Dn(valueSearchName)).setScope(SearchScope.SUBTREE)
                .setFilter(filter).setTimeLimit((int) (LDAPConnectionFactory.TIMEOUT / 1000));

        SearchCursor usernameEntries = connectionFactory.getDefaultConnection().search(request);

        try {
            try {
                while (usernameEntries.next()) {
                    if (usernameEntries.isEntry()) {
                        userInfos.add(userInfoFromSearchEntry(usernameEntries.getEntry()));
                    }
                }
            } catch (CursorException e) {
                LOG.error("Exception while traversing the search results for '" + wildcard + "' with filter '"
                        + filterName + "'.", e);
            }
        } catch (LdapException e) {
            // connection might be dropped, while doing the search. Switch to alternative LDAP
            if (TIMEOUT.equals(e.getMessage()) && firstAttempt) {
                connectionFactory.switchConnection();
                return doBruteForceSearch(filterName, wildcard, userInfos, false);
            } else {
                throw e;
            }
        } finally {
            if (usernameEntries != null) {
                try {
                    usernameEntries.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
        return userInfos;

    }

    /**
     * Closes the LDAP directory service access. It unbinds and closes the default LDAP connection.
     */
    @Override
    @PreDestroy
    public void close() {
        connectionFactory.close();
    }

    /**
     * Searches for the provided username and returns all valid search results in a list. Search uses default LDAP
     * connection and is conducted using LDAP property {@value #KEY_SEARCH_NAME} as base search name and
     * {@value #KEY_SEARCH_FILTER_USERNAME} as search filter. This method has a locking mechanism and is synchronised at
     * the same time in order for synchronisation to work in JEE and JSE.
     *
     * @param username to search for
     * @param firstAttempt true if this is the first attempt to search for the username, or false if the second. In
     *        former case a connection switch will be made, in latter an exception thrown.
     * @return list of search results found, when searching for the provided username
     *
     * @throws LdapException if an exception is encountered while searching or if an exception is encountered while
     *         fetching LDAP connection
     */
    @Lock(LockType.WRITE)
    private synchronized List<Entry> searchForUsername(char[] username, boolean firstAttempt) throws LdapException {
        SearchRequest usernameRequest = new SearchRequestImpl().setBase(new Dn(valueSearchName))
                .setFilter(MessageFormat.format(valueSearchFilterUsername, String.valueOf(username)))
                .setScope(SearchScope.SUBTREE).addAttributes(interestingAttributes)
                .setTimeLimit((int) (LDAPConnectionFactory.TIMEOUT / 1000));
        SearchCursor usernameEntries = connectionFactory.getDefaultConnection().search(usernameRequest);

        List<Entry> results = new ArrayList<>();
        try {
            try {
                while (usernameEntries.next()) {
                    if (usernameEntries.isEntry()) {
                        results.add(usernameEntries.getEntry());
                    }
                }
            } catch (CursorException e) {
                LOG.error("Exception while traversing username search entries.", e);
            }
        } catch (LdapException e) {
            // connection might be dropped, while doing the search. Switch to alternative LDAP server
            if (TIMEOUT.equals(e.getMessage()) && firstAttempt) {
                connectionFactory.switchConnection();
                return searchForUsername(username, false);
            } else {
                throw e;
            }
        } finally {
            if (usernameEntries != null) {
                try {
                    usernameEntries.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
        return results;
    }

    /**
     * Constructs a {@link LDAPUserInfo} from a LDAP search entry. Gathered information contains username, first, last
     * and middle name, group name, email address, phone number and user's location. If a specific attribute could not
     * be found in the search entry, its value is set to <code>null</code>. If the username, first name or last name do
     * not exist, null is returned.
     *
     * @param searchResult LDAP search result from which user info should be gathered
     *
     * @return user info derived from the provided LDAP search result
     */
    private LDAPUserInfo userInfoFromSearchEntry(Entry searchEntry) {
        Collection<Attribute> userAttributes = searchEntry.getAttributes();
        Map<String, String> attrMap = new HashMap<>();
        for (Attribute attribute : userAttributes) {
            if (attribute.get() != null) {
                if (attribute.size() == 1) {
                    attrMap.put(attribute.getId(), INVALID_CHARACTER_PATTERN.matcher(String.valueOf(attribute.get()))
                            .replaceAll(EMPTY_STRING));
                } else {
                    // Handle each ambiguous case individually and only if necessary.
                    // Group filtering.
                    if (attribute.getId().equals(valueAttributeGroup)) {
                        String s = filterAttributeValues(attribute, valueAttributeGroupFilter);
                        if (s != null) {
                            attrMap.put(attribute.getId(), INVALID_CHARACTER_PATTERN.matcher(s)
                                    .replaceAll(EMPTY_STRING));
                        }
                    }
                }
            }
        }

        if (!attrMap.containsKey(valueAttributeUsername) || !attrMap.containsKey(valueAttributeFirstName)
                || !attrMap.containsKey(valueAttributeLastName)) {
            return null;
        }

        return new LDAPUserInfo(attrMap.get(valueAttributeUsername), attrMap.get(valueAttributeLastName),
                attrMap.get(valueAttributeFirstName), attrMap.get(valueAttributeMiddleName),
                attrMap.get(valueAttributeGroup), attrMap.get(valueAttributeEmail), attrMap.get(valueAttributePhone),
                attrMap.get(valueAttributeMobile), attrMap.get(valueAttributeLocation));
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            close();
        } finally {
            super.finalize();
        }
    }

    private static String filterAttributeValues(Attribute attribute, String filter) {
        Iterator<Value<?>> iter = attribute.iterator();
        while (iter.hasNext()) {
            String value = iter.next().toString();
            if (value.contains(filter)) {
                return value;
            }
        }
        return null;
    }

    private static boolean areParametersFaulty(char[] username, char[] password) {
        return username == null || username.length == 0 || password == null || password.length == 0;
    }
}
