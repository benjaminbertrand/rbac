/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.util.Arrays;

import javax.xml.bind.JAXB;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.IConnectionFactory;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * 
 * <code>AbstractSecurityFacadeTest</code> provides common setup methods for the {@link SecurityFacade} testing.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public abstract class AbstractSecurityFacadeBase implements SecurityFacadeListener {

    protected static final Charset CHARSET = Charset.forName("UTF-8");

    protected TokenInfo info = new TokenInfo("tokenID", "johndoe", "John", "Doe", System.currentTimeMillis(),
            System.currentTimeMillis() + 10000, "192.168.1.2", Arrays.asList("Role1", "Role2"),
            new byte[] { 1, 2, 3, 4 });

    protected HttpURLConnection loginConnection;
    protected HttpURLConnection logoutConnection;
    protected IConnectionFactory factory;
    protected ISecurityFacade facade;

    protected int loggedIn = 0;
    protected Token loggedInToken;
    protected Token loggedOutToken;
    protected int loggedOut = 0;

    @Override
    public void loggedIn(Token token) {
        loggedIn++;
        loggedInToken = token;
    }

    @Override
    public void loggedOut(Token token) {
        loggedOut++;
        loggedOutToken = token;
    }

    @BeforeClass
    public static void init() throws Exception {
        System.setProperty(SecurityCallbackAdapter.DEFAULT_ACCEPT_CERTIFICATE, "true");
        Field f = RBACProperties.class.getDeclaredField("instance");
        f.setAccessible(true);
        f.set(null, null);

        System.setProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL, "primary");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_HOST, "primary");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_PORT, "1");
        System.setProperty(RBACProperties.KEY_SECONDARY_SERVICES_URL, "secondary");
        System.setProperty(RBACProperties.KEY_SECONDARY_SSL_HOST, "secondary");
        System.setProperty(RBACProperties.KEY_SECONDARY_SSL_PORT, "1");
        System.setProperty(RBACProperties.KEY_INACTIVITY_RESPONSE_GRACE_PERIOD, "2");
        System.setProperty(RBACProperties.KEY_SHOW_ROLE_SELECTOR, "false");
        System.setProperty(RBACProperties.KEY_VERIFY_SIGNATURE, "false");
    }

    @AfterClass
    public static void destroy() throws Exception {
        Field f = RBACProperties.class.getDeclaredField("instance");
        f.setAccessible(true);
        f.set(null, null);

        System.clearProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SSL_HOST);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SSL_PORT);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SERVICES_URL);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SSL_HOST);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SSL_PORT);
        System.clearProperty(RBACProperties.KEY_INACTIVITY_RESPONSE_GRACE_PERIOD);
        System.clearProperty(RBACProperties.KEY_SHOW_ROLE_SELECTOR);
        System.clearProperty(RBACProperties.KEY_VERIFY_SIGNATURE);
        System.clearProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION);
        System.clearProperty(RBACProperties.KEY_CERTIFICATE_STORE);
        System.clearProperty(RBACProperties.KEY_INACTIVITY_TIMEOUT_DEFAULT);
    }

    /**
     * Construct a new SecurityFacade that is to be used in the test. This method should always construct the default
     * facade.
     * 
     * @return the facade instance
     */
    protected ISecurityFacade createFacade() {
        return new SecurityFacade(factory);
    }

    @Before
    public void setUp() throws Exception {
        factory = mock(IConnectionFactory.class);
        
        facade = createFacade();
        facade.setToken(null);
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials("johndoe", "pass".toCharArray(), "Role", "192.168.1.2");
            }
        });
        facade.addSecurityFacadeListener(this);
        loggedIn = 0;
        loggedOut = 0;
        loggedInToken = null;
        loggedOutToken = null;
    }

    /**
     * Cleanup after each test.
     * 
     * @throws Exception on error
     */
    @After
    public void tearDown() throws Exception {
        setUpForAuthentication();
        try {
            facade.logout();
        } catch (NullPointerException e) {
            // ignore, the mockup is maybe not ready
        }
        facade.removeSecurityFacadeListener(this);
        facade.destroy();
    }

    protected void setUpForAuthentication() throws Exception {
        loginConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/"), eq("POST"))).thenReturn(
                loginConnection);
        when(loginConnection.getResponseCode()).thenReturn(201);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(info, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(loginConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(loginConnection.getErrorStream()).thenReturn(stream2);

        logoutConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID"), eq("DELETE"))).thenReturn(
                logoutConnection);
        when(logoutConnection.getResponseCode()).thenReturn(204);

        InputStream stream3 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(logoutConnection.getErrorStream()).thenReturn(stream3);
    }
}
