/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXB;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.IConnectionFactory;
import se.esss.ics.rbac.access.RBACConnector;
import se.esss.ics.rbac.access.RBACConnectorException;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.ResourcesInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.UsersInfo;

/**
 * 
 * <code>RBACConnectorTest</code> tests the methods of the {@link RBACConnector}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class RBACConnectorTest {

    protected static final Charset CHARSET = Charset.forName("UTF-8");

    private RolesInfo rolesInfo = new RolesInfo(null, Arrays.asList("Role1", "Role2"));
    private ResourcesInfo resourcesInfo = new ResourcesInfo(Arrays.asList("Res1", "Res2"));
    private UsersInfo usersRoleInfo = new UsersInfo("role", Arrays.asList("user1","user2"));
    private UsersInfo usersPermissionInfo = new UsersInfo("resource", "permission", Arrays.asList("user1","user2"));
    
    private PublicKey key;
    private IConnectionFactory factory;
    private HttpURLConnection connection;

    @Before
    public void setUp() throws Exception {
        Field f = RBACProperties.class.getDeclaredField("instance");
        f.setAccessible(true);
        f.set(null, null);
        System.setProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL, "primary");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_HOST, "primary");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_PORT, "1");
        System.setProperty(RBACProperties.KEY_INACTIVITY_RESPONSE_GRACE_PERIOD, "2");

        factory = mock(IConnectionFactory.class);
        f = RBACConnector.class.getDeclaredField("connectionFactory");
        f.setAccessible(true);
        f.set(RBACConnector.getInstance(), factory);
    }

    private void setUpForGetPublicKey() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/publickey"), eq("GET"))).thenReturn(
                connection);
        when(connection.getResponseCode()).thenReturn(200);

        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(512, new SecureRandom());
        key = keyGenerator.genKeyPair().getPublic();

        InputStream stream = new ByteArrayInputStream(key.getEncoded());
        when(connection.getInputStream()).thenReturn(stream);
        InputStream stream2 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(connection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForGetACFFile() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/pvaccess/group/group1,group2"), eq("GET")))
                .thenReturn(connection);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/pvaccess/group/group|group1,group2"),
                eq("GET"))).thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);

        InputStream stream = new ByteArrayInputStream("Content".getBytes(CHARSET));
        when(connection.getInputStream()).thenReturn(stream);
        InputStream stream2 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(connection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForGetRoles() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/roles"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(rolesInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);
    }
    
    private void setUpForGetResources() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/resources"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(resourcesInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);
    }
    
    private void setUpForGetUsersWithRole() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/rrr/users"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(404);
        InputStream stream2 = new ByteArrayInputStream("Error".getBytes());
        when(connection.getInputStream()).thenReturn(stream2);
        
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/role/users"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(usersRoleInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);
    }
    
    private void setUpForGetUsersWithPermission() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/rrr/permission/users"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(404);
        InputStream stream2 = new ByteArrayInputStream("Error".getBytes());
        when(connection.getInputStream()).thenReturn(stream2);
        
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/resource/permission/users"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(usersPermissionInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);
    }
    
    private void setUpForGetPermissions() throws Exception {
        connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/res1/permissions"), eq("GET")))
                .thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);
        HttpURLConnection connection2 = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/res2/permissions"),
                eq("GET"))).thenReturn(connection2);
        when(connection2.getResponseCode()).thenReturn(404);

        Map<String, Boolean> map = new LinkedHashMap<>();
        map.put("perm1", Boolean.TRUE);
        map.put("perm2", Boolean.TRUE);
        PermissionsInfo permissionsInfo = new PermissionsInfo("res1", map);
        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(permissionsInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);
        
        InputStream stream2 = new ByteArrayInputStream("Error".getBytes());
        when(connection2.getInputStream()).thenReturn(stream2);
    }
    
    
    /**
     * Test the getRoles method.
     * 
     * @throws Exception
     */
    @Test
    public void testGetRoles() throws Exception {
        setUpForGetRoles();
        String[] roles = RBACConnector.getInstance().getRoles();
        assertEquals(rolesInfo.getRoleNames().size(), roles.length);
        assertEquals("Role1", roles[0]);
        assertEquals("Role2", roles[1]);
    }
    
    /**
     * Test the getResources method.
     * 
     * @throws Exception
     */
    @Test
    public void testGetResources() throws Exception {
        setUpForGetResources();
        String[] resources = RBACConnector.getInstance().getResources();
        assertEquals(resourcesInfo.getResourceNames().size(), resources.length);
        assertEquals("Res1", resources[0]);
        assertEquals("Res2", resources[1]);
    }
    
    /**
     * Test the getResources method.
     * 
     * @throws Exception
     */
    @Test
    public void testGetPermissions() throws Exception {
        setUpForGetPermissions();
        String[] permissions = RBACConnector.getInstance().getPermissions("res1");
        assertEquals(2, permissions.length);
        assertEquals("perm1", permissions[0]);
        assertEquals("perm2", permissions[1]);
        
        try {
            permissions = RBACConnector.getInstance().getPermissions("res2");
            fail("Exception should occur");
        } catch (RBACConnectorException e) {
            assertEquals("Service responded unexpectedly: Error",e.getMessage());
        }
        try {
            permissions = RBACConnector.getInstance().getPermissions(null);
            fail("Exception should occur");
        } catch (RBACConnectorException e) {
            assertEquals("Resource parameter must not be null or empty.",e.getMessage());
        }
    }
    
    /**
     * Test the getUsersWithRole method.
     * 
     * @throws Exception
     */
    @Test
    public void testGetUsersWithRole() throws Exception {
        setUpForGetUsersWithRole();
        String[] users = RBACConnector.getInstance().getUsersWithRole("role");
        assertEquals(2, users.length);
        assertEquals("user1", users[0]);
        assertEquals("user2", users[1]);
        
        try {
            users = RBACConnector.getInstance().getUsersWithRole("rrr");
            fail("Exception should occur");
        } catch (RBACConnectorException e) {
            assertEquals("Service responded unexpectedly: Error",e.getMessage());
        }
        try {
            users = RBACConnector.getInstance().getUsersWithRole(null);
            fail("Exception should occur");
        } catch (IllegalArgumentException e) {
            assertEquals("Role name must not be null or empty.",e.getMessage());
        }
    }
    
    /**
     * Test the getUsersWithPermission method.
     * 
     * @throws Exception
     */
    @Test
    public void testGetUsersWithPermission() throws Exception {
        setUpForGetUsersWithPermission();
        String[] users = RBACConnector.getInstance().getUsersWithPermission("resource", "permission");
        assertEquals(2, users.length);
        assertEquals("user1", users[0]);
        assertEquals("user2", users[1]);
        
        try {
            users = RBACConnector.getInstance().getUsersWithPermission("rrr","permission");
            fail("Exception should occur");
        } catch (RBACConnectorException e) {
            assertEquals("Service responded unexpectedly: Error",e.getMessage());
        }
        try {
            users = RBACConnector.getInstance().getUsersWithPermission(null,"permission");
            fail("Exception should occur");
        } catch (IllegalArgumentException e) {
            assertEquals("Resource name must not be null or empty.",e.getMessage());
        }
        try {
            users = RBACConnector.getInstance().getUsersWithPermission("resource",null);
            fail("Exception should occur");
        } catch (IllegalArgumentException e) {
            assertEquals("Permission name must not be null or empty.",e.getMessage());
        }
    }

    /**
     * Test if default instance of connector facade can be created.
     */
    @Test
    public void testRBACConnectorConstructor() {
        RBACConnector connector = RBACConnector.getInstance();
        assertNotNull("Connector should not be null", connector);
        assertSame("Default instance should always return the same instance", connector, RBACConnector.getInstance());
    }

    /**
     * Test the method {@link RBACConnector#getPublicKey()}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetPublicKey() throws Exception {
        setUpForGetPublicKey();
        PublicKey receivedKey = RBACConnector.getInstance().getPublicKey();
        assertEquals("Received key is equal to stored key", key, receivedKey);

        when(connection.getInputStream()).thenReturn(new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 }));
        try {
            RBACConnector.getInstance().getPublicKey();
            fail("Exception expected");
        } catch (RBACConnectorException e) {
            assertEquals("Exception expected", "Could not decode the public key.", e.getMessage());
        }

        ByteArrayInputStream stream = new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 }) {
            @Override
            public int read(byte[] b) throws IOException {
                throw new IOException("Test");
            }
        };
        when(connection.getInputStream()).thenReturn(stream);
        try {
            RBACConnector.getInstance().getPublicKey();
            fail("Exception expected");
        } catch (RBACConnectorException e) {
            assertEquals("Exception expected", "Service response error: Error", e.getMessage());
            assertEquals("Exception cause should be IO", IOException.class, e.getCause().getClass());
            assertEquals("Exception cause message should be correct", "Test", e.getCause().getMessage());
        }

        setUpForGetPublicKey();
        when(connection.getResponseCode()).thenReturn(400);
        try {
            RBACConnector.getInstance().getPublicKey();
            fail("Exception expected");
        } catch (RBACConnectorException e) {
            assertEquals("Exception expected", "Service responded unexpectedly: Error", e.getMessage());
        }
    }

    /**
     * Test the method {@link RBACConnector#getACFFileForGroups(String...)}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetACFFile() throws Exception {
        setUpForGetACFFile();
        String[] groups = { "group1", "group2" };
        String receivedFile = RBACConnector.getInstance().getACFFileForGroups(null, groups);
        assertEquals("Content should match", "Content", receivedFile);

        setUpForGetACFFile();
        receivedFile = RBACConnector.getInstance().getACFFileForGroups("group", groups);
        assertEquals("Content should match", "Content", receivedFile);

        try {
            RBACConnector.getInstance().getACFFileForGroups(null, null);
            fail("Exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("Exception expected",
                    "Groups must not be empty or null and must not contain any empty or null name.", e.getMessage());
        }

        when(connection.getResponseCode()).thenReturn(400);
        try {
            RBACConnector.getInstance().getACFFileForGroups(null, groups);
            fail("Exception expected");
        } catch (RBACConnectorException e) {
            assertEquals("Exception expected", "Service responded unexpectedly: Error", e.getMessage());
        }
    }

}
