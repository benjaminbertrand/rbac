/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXB;

import org.junit.Test;
import org.mockito.Mockito;

import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.PermissionsInfo;

/**
 * 
 * <code>SecurityFacadeAutoLogoutTest</code> tests auto logout mechanism. This tests take a bit longer to execute,
 * because they value the minimum auto logout time, which is 1 minute.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SecurityFacadeAutoLogoutTest extends AbstractSecurityFacadeBase {

    /**
     * 
     * <code>Callback</code> is a callback that is used for auto logout testing.
     * 
     * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
     * 
     */
    protected class Callback extends SecurityCallbackAdapter {

        boolean returnImmediately = false;
        boolean returnValue = true;

        public Callback(boolean returnImmediately, boolean returnValue) {
            this.returnImmediately = returnImmediately;
            this.returnValue = returnValue;
        }

        public Token token;
        public int timeout;

        @Override
        public boolean autoLogoutConfirm(Token token, int timeoutInSeconds) {
            this.token = token;
            this.timeout = timeoutInSeconds;
            if (returnImmediately) {
                return returnValue;
            } else {
                try {
                    Thread.sleep(4000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return returnValue;
            }
        }
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        setUpForAuthentication();
        facade.setAutoLogoutTimeout(15);
        facade.authenticate();
        setUpAutoLogout();
    }

    /**
     * Forcefully set the auto logout features, which cannot be set in a standard way. This is necessary to make the
     * test shorter, so one doesn't need to wait for long timeouts.
     * 
     * @throws Exception on error
     */
    protected void setUpAutoLogout() throws Exception {
        // force autoLogout of 1000 milliseconds to avoid excessive waiting during this test
        Field field = SecurityFacade.class.getDeclaredField("autoLogout");
        field.setAccessible(true);
        AutoLogout logout = (AutoLogout) field.get(facade);
        Field f = AutoLogout.class.getDeclaredField("logoutTimeout");
        f.setAccessible(true);
        f.set(logout, Long.valueOf(1000));
    }

    protected void setUpForPermissionCheck() throws Exception {
        HttpURLConnection connection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/permission"),
                eq("GET"))).thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Map<String, Boolean> permissions = new HashMap<>(1);
        permissions.put("permission", Boolean.TRUE);
        PermissionsInfo pInfo = new PermissionsInfo("resource", permissions);
        JAXB.marshal(pInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);
    }

    /**
     * Test if callback answers immediately and confirms logout.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAutoLogoutImmediateConfirm() throws Exception {
        Token token = facade.getToken();
        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);
        // first let's check a little bit earlier
        synchronized (cb) {
            cb.wait(500);
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(2000);
        }

        assertNotNull("Auto logout should be called", cb.token);
        assertSame("The tokens should be the same", token, cb.token);
        assertEquals("Grace period should match", 2, cb.timeout);

        assertEquals("Logout should be called", 1, loggedOut);
        assertSame("Tokens should match", token, loggedOutToken);

        assertNull("Current token should be null", facade.getToken());

    }

    /**
     * Test if callback answers immediately and denies logout.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAutoLogoutImmediateDeny() throws Exception {
        Token token = facade.getToken();
        Callback cb = new Callback(true, false);
        facade.setDefaultSecurityCallback(cb);

        // wait to be logged out automatically
        // logged out after 1 minute - grace period, because logout confirm returns immediately
        synchronized (cb) {
            cb.wait(2500);
        }

        assertNotNull("Auto logout should be called", cb.token);
        assertSame("The tokens should be the same", token, cb.token);
        assertEquals("Grace period should match", 2, cb.timeout);

        assertEquals("Logout should not be called", 0, loggedOut);
        assertSame("Tokens should match", facade.getToken(), token);

        cb.token = null;
        cb.timeout = 0;

        // wait for another logout
        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
        assertSame("The tokens should be the same", token, cb.token);
        assertEquals("Grace period should match", 2, cb.timeout);

        assertEquals("Logout should not be called", 0, loggedOut);
        assertSame("Tokens should match", facade.getToken(), token);

    }

    /**
     * Tests if auto logout happens even if callback delays the answer.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAutoLogoutDelayedDeny() throws Exception {
        Token token = facade.getToken();
        Callback cb = new Callback(false, false);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            cb.wait(2500);
        }

        assertNotNull("Auto logout should be called", cb.token);
        assertSame("The tokens should be the same", token, cb.token);
        assertEquals("Grace period should match", 2, cb.timeout);

        synchronized (cb) {
            cb.wait(3000);
        }

        assertEquals("Logout should be called", 1, loggedOut);
        assertSame("Tokens should match", token, loggedOutToken);

        // wait for autoLogoutConfirm to return;
        synchronized (cb) {
            cb.wait(3000);
        }
    }

    /**
     * Test if auto logout happen even if the callback delays the answer and then confirms.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAutoLogoutDelayedConfirm() throws Exception {
        Token token = facade.getToken();
        Callback cb = new Callback(false, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            cb.wait(2500);
        }

        assertNotNull("Auto logout should be called", cb.token);
        assertSame("The tokens should be the same", token, cb.token);
        assertEquals("Grace period should match", 2, cb.timeout);

        synchronized (cb) {
            cb.wait(3000);
        }

        assertEquals("Logout should be called", 1, loggedOut);
        assertSame("Tokens should match", token, loggedOutToken);

        // wait for autoLogoutConfirm to return;
        synchronized (cb) {
            cb.wait(3000);
        }
    }

    /**
     * Test if auto logout is extended if permissions are checked.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAutoLogoutWhenUpdating() throws Exception {
        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            for (int i = 0; i < 10; i++) {
                setUpForPermissionCheck();
                facade.hasPermission("resource", "permission");
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test the boundaries for the timeout.
     */
    @Test
    public void testForInvalidTimeout() {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        // test if less than 15 minutes can be set
        for (int i = 1; i < 15; i++) {
            try {
                facade.setAutoLogoutTimeout(i);
                fail("Exception should occur if setting timeout of " + i);
            } catch (Exception e) {
                assertEquals("Exception expected", "Auto logout timeout shorter than 15 minutes is not allowed.",
                        e.getMessage());
            }
        }

        // test if more than 15 minutes can be set
        for (int i = 15; i < 200; i++) {
            facade.setAutoLogoutTimeout(i);
            assertEquals("Timeout is the same as the set one", i, facade.getAutoLogoutTimeout());
        }
        // test for maximum
        facade.setAutoLogoutTimeout(Integer.MAX_VALUE);
        // test for infinity
        facade.setAutoLogoutTimeout(0);
    }

    /**
     * Test that auto logout is properly initialised when set.
     */
    @Test
    public void testAutoLogoutSetting() {
        AutoLogout al = Mockito.mock(AutoLogout.class);
        facade.setAutoLogout(al);
        Mockito.verify(al, Mockito.times(1)).setLogoutConfirmationGrace(2);
        Mockito.verify(al, Mockito.times(1)).setLogoutTimeout(facade.getAutoLogoutTimeout());
        Mockito.verify(al, Mockito.times(1)).start();
        Mockito.verify(al, Mockito.never()).stop();

        AutoLogout al2 = Mockito.mock(AutoLogout.class);
        facade.setAutoLogout(al2);
        Mockito.verify(al, Mockito.times(1)).stop();

        facade.setAutoLogoutTimeout(0);
        Mockito.verify(al2, Mockito.times(1)).stop();

        facade.setAutoLogout(al);
        Mockito.verify(al, Mockito.times(2)).stop();
    }
}
