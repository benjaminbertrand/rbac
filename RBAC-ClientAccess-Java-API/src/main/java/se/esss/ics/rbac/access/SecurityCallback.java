/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.security.cert.X509Certificate;
import java.util.Map;

import se.esss.ics.rbac.access.localservice.LocalAuthServiceDetails;

/**
 * {@link SecurityCallback} is used for requesting information required by the {@link SecurityFacade} from the client
 * application (for example username and password) and to report the results of asynchronous actions.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public interface SecurityCallback {

    /**
     * Called when authentication of the user was successful. The token of the user is provided as a parameter.
     * 
     * @param token the token received during authentication
     */
    void authenticationCompleted(Token token);

    /**
     * Called when authentication fails. The reason for failure is provided as a parameter together with the username of
     * the user, for whom the authentication failed.
     * 
     * @param reason the reason for the authentication failure
     * @param username the username of the user which could not be authenticated
     * @param error the exception that caused the failure
     */
    void authenticationFailed(String reason, String username, Throwable error);

    /**
     * Called when logout was completed successfully. The token of the logged out user is provided as a parameter.
     * 
     * @param token the token of the user that was logged out
     */
    void logoutCompleted(Token token);

    /**
     * Called when logout could not be completed. The reason for failure is provided as a parameter together with the
     * token of the user, who could not be logged out.
     * 
     * @param reason the reason for the logout failure
     * @param token the token of the user who could not be logged out
     * @param error the exception that caused the failure
     */
    void logoutFailed(String reason, Token token, Throwable error);

    /**
     * Called when authorisation is completed. The token used for authorisation is provided as a parameter. The second
     * parameter provides <code>permission name and grant pairs</code>. If a permission is granted the value is
     * <code>true</code>, if denied the value is <code>false</code>.
     * 
     * @param token the token used for authorisation
     * @param permissions the result of authorisation. The keys are permission names are and values are the permission
     *            grants (<code>true</code> for granted and <code>false</code> for denied).
     */
    void authorisationCompleted(Token token, Map<String, Boolean> permissions);

    /**
     * Called when authorisation attempt failed. The reason for failure is provided as a parameter, together with the
     * array of permissions that were checked and the token used for authorisation.
     * 
     * @param reason the reason for the authorisation failure
     * @param permissions the permissions array, containing the names of all checked permissions
     * @param token the token used for authorisation
     * @param error the exception that caused the failure
     */
    void authorisationFailed(String reason, String[] permissions, Token token, Throwable error);

    /**
     * Called during the authentication process when the SecurityFacade requires username and password. The method may
     * show an input dialog, where the user can enter his username and password.
     * 
     * @return credentials containing the required authentication information
     */
    Credentials getCredentials();

    /**
     * Called when the roles for a specific username are requested and the call completes successfully.
     * 
     * @param username the username for which the roles have been requested
     * @param roles the array containing the names of all roles assigned to the specified username
     */
    void rolesLoadingCompleted(String username, String[] roles);

    /**
     * Called when the roles for a specific username could not be loaded. The reason parameter provides the details, why
     * the roles could not be loaded.
     * 
     * @param reason the reason why the loading of roles for the specified user failed
     * @param username the username for which loading of roles failed
     * @param error the exception that caused the failure
     */
    void rolesLoadingFailed(String reason, String username, Throwable error);

    /**
     * Called when an exclusive access has been requested for a specific permission and the call is completed
     * successfully. The token used for authorisation is provided as a parameter and so are the name of the permission
     * and the name of the resource on which exclusive access has been granted.
     * 
     * @param token the token used for requesting exclusive access
     * @param exclusiveAccess the exclusive access that contains the information about the permission for which
     *            exclusive access was requested and the expiration date for the exclusive access
     */
    void requestExclusiveAccessCompleted(Token token, ExclusiveAccess exclusiveAccess);

    /**
     * Called when exclusive access for a permission could not be granted. The reason parameter provides the details,
     * why exclusive access could not be granted.
     * 
     * @param reason the reason why exclusive access for the specified permission could not be granted
     * @param permission the permission name for which exclusive access could not be granted
     * @param resource the resource which the permission belongs to
     * @param token the token used for authorisation
     * @param error the exception that caused the failure
     */
    void requestExclusiveAccessFailed(String reason, String permission, String resource, Token token, Throwable error);

    /**
     * Called when a removal of exclusive access for a permission has been requested and the call is completed
     * successfully. The token used for authorisation is provided as a parameter and so are the name of the permission
     * and the name of the resource on which exclusive access has been granted.
     * 
     * @param token the token used for authorisation
     * @param permission the name of the permission for which exclusive access has been released
     * @param resource the name of the resource that the permission belongs to
     */
    void releaseExclusiveAccessCompleted(Token token, String permission, String resource);

    /**
     * Called when exclusive access for a permission could not be released. The reason parameter provides the details,
     * why exclusive access could not be released.
     * 
     * 
     * @param reason the reason why exclusive access for the specified permission could not be released
     * @param permission the permission name for which exclusive access could not be released
     * @param resource the name of the resource that the permission belongs to
     * @param token the token used for authorisation
     * @param error the exception that caused the failure
     */
    void releaseExclusiveAccessFailed(String reason, String permission, String resource, Token token, Throwable error);

    /**
     * Called when token was successfully renewed.
     * 
     * @param token the renewed token
     */
    void tokenRenewalCompleted(Token token);

    /**
     * Called when token renewal was not successful. The reason for failure is provided as a parameter, together with
     * the token, which could not be renewed.
     * 
     * @param reason the reason for failure
     * @param token the token that could not be renewed
     * @param error the exception that caused the failure
     */
    void tokenRenewalFailed(String reason, Token token, Throwable error);

    /**
     * Called when the token validity is checked.
     * 
     * @param token the token that was checked
     * @param valid true if the token is valid or false otherwise
     */
    void tokenValidationCompleted(Token token, boolean valid);

    /**
     * Called when the token validity check failed.
     * 
     * @param reason the reason for failure
     * @param token the token that was checked
     * @param error the exception that caused the failure
     */
    void tokenValidationFailed(String reason, Token token, Throwable error);

    /**
     * Called when the facade attempts to automatically logout the user due to inactivity. The method should return
     * <code>true</code> if the facade should logout the user or <code>false</code> if the user should remain logged in.
     * If there is no response to this call within <code>timeoutInSeconds</code>, the facade will assume the answer was
     * <code>true</code> .
     * 
     * @param token the token of the user that will be logged out
     * @param timeoutInSeconds the timeout in seconds after which the facade will assume the answer was true
     * @return true if the facade can logout the user or false otherwise
     */
    boolean autoLogoutConfirm(Token token, int timeoutInSeconds);

    /**
     * Called when the facade receives an unknown certificate from the server, which needs to be accepted, before any
     * further action is possible. If the certificate should be accepted by the facade, this method should return true,
     * if the certificate should be rejected, the method should return false.
     * 
     * @param host the host from which the certificate was received
     * @param certificate the certificate
     * @return true if the certificate should be accepted or false if rejected
     */
    boolean acceptCertificate(String host, X509Certificate certificate);
    
    /**
     * Called when the facade attempts to retrieve token from local authentication service, delete token from local
     * authentication service or save token on local authentication service. Method returns local authentication
     * service and client request details (IP address and port number).
     * 
     * @return local authentication service and client request details (IP address and port number).
     */
    LocalAuthServiceDetails getLocalAuthServiceDetails();
}
