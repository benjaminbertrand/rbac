/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.impl.RBACConnectionFactory;

/**
 * <code>RBACProperties</code> provides access to properties that guide the connection to the RBAC services. It provides
 * the URL to the service and other properties that define the behaviour of the client.
 * <p>
 * Properties are loaded from <code>rbac.properties</code> file, that has to be placed somewhere on the classpath. All
 * values can be overridden by setting the system properties, but that has to be done before this class is loaded.
 * Convenience methods are provided for accessing the properties.
 * </p>
 * <p>
 * RBAC properties are implemented as a singleton. Use {@link #getInstance()} to get an instance of the properties.
 * </p>
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public final class RBACProperties {

    /** Primary RBAC service URL */
    public static final String KEY_PRIMARY_SERVICES_URL = "rbac.primaryServicesURL";
    /** Primary RBAC service host */
    public static final String KEY_PRIMARY_SSL_HOST = "rbac.primarySSLHost";
    /** Primary RBAC service port */
    public static final String KEY_PRIMARY_SSL_PORT = "rbac.primarySSLPort";
    /** Secondary RBAC service URL */
    public static final String KEY_SECONDARY_SERVICES_URL = "rbac.secondaryServicesURL";
    /** Secondary RBAC service host */
    public static final String KEY_SECONDARY_SSL_HOST = "rbac.secondarySSLHost";
    /** Secondary RBAC service port */
    public static final String KEY_SECONDARY_SSL_PORT = "rbac.secondarySSLPort";
    /** Certificate store location */
    public static final String KEY_CERTIFICATE_STORE = "rbac.certificateStore";
    /** Default inactivity timeout */
    public static final String KEY_INACTIVITY_TIMEOUT_DEFAULT = "rbac.inactivityTimeoutDefault";
    /** Inactivity response time */
    public static final String KEY_INACTIVITY_RESPONSE_GRACE_PERIOD = "rbac.inactivityResponseGrace";
    /** A hint to the UI to show the role picker when credentials are requested */
    public static final String KEY_SHOW_ROLE_SELECTOR = "rbac.showRoleSelector";
    /** A flag whether the facade should verify the signature of each token */
    public static final String KEY_VERIFY_SIGNATURE = "rbac.verifySignature";
    /** Path to the location of the public key */
    public static final String KEY_PUBLICK_KEY_LOCATION = "rbac.publicKeyLocation";
    /** A flag whether handshake with the server should be performed or not */
    public static final String KEY_PERFORM_HANDHSAKE = "rbac.performHandshake";
    /** A flag for the connection timeout when contacting the RBAC for the first time to perform handshake */
    public static final String KEY_HANDSHAKE_TIMEOUT = "rbac.handshakeTimeout";
    /** Local service port **/
    public static final String KEY_LOCAL_SERVICES_PORT = "rbac.localServicesPort";
    /** Use local service **/
    public static final String KEY_USE_LOCAL_SERVICE = "rbac.useLocalService";

    private static final String FILE_RBAC_PROPERTIES = "rbac.properties";
    

    private static final Logger LOGGER = LoggerFactory.getLogger(RBACProperties.class);
    private static final String PRIMARY_HOST_PORT_ERROR = "Could not identify the primary SSL host and port.";
    private static final String SECONDARY_HOST_PORT_ERROR = "Could not identify the secondary SSL host and port.";
    private static final String LOADING_FAILED = "Loading properties from file " + FILE_RBAC_PROPERTIES
            + " failed. Using default.";
    
    private static RBACProperties instance;

    private final Properties properties;

    private RBACProperties() {
        properties = new Properties();
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(FILE_RBAC_PROPERTIES)) {
            properties.load(stream);
        } catch (IOException e) {
            LOGGER.error(LOADING_FAILED, e);
            properties.setProperty(KEY_PRIMARY_SERVICES_URL, "https://localhost:8443/service");
            properties.setProperty(KEY_SECONDARY_SERVICES_URL, "https://localhost:8443/service");
            properties.setProperty(KEY_INACTIVITY_TIMEOUT_DEFAULT, String.valueOf(1800));
            properties.setProperty(KEY_INACTIVITY_RESPONSE_GRACE_PERIOD, String.valueOf(30));
            properties.setProperty(KEY_SHOW_ROLE_SELECTOR, Boolean.FALSE.toString());
            properties.setProperty(KEY_VERIFY_SIGNATURE, Boolean.FALSE.toString());
            properties.setProperty(KEY_PERFORM_HANDHSAKE, Boolean.TRUE.toString());
            properties.setProperty(KEY_LOCAL_SERVICES_PORT, String.valueOf(9421));
            properties.setProperty(KEY_HANDSHAKE_TIMEOUT, String.valueOf(2000));
        }
        properties.putAll(System.getProperties());

        verifyHostDefined();
    }

    private void verifyHostDefined() {
        String host = getPrimarySSLHostname();
        String port = properties.getProperty(KEY_PRIMARY_SSL_PORT);
        if (host == null || port == null) {
            try {
                URL url = new URL(getPrimaryServicesBaseURL());
                properties.setProperty(KEY_PRIMARY_SSL_HOST, url.getHost());
                properties.setProperty(KEY_PRIMARY_SSL_PORT, String.valueOf(url.getPort()));
            } catch (MalformedURLException e) {
                LOGGER.error(PRIMARY_HOST_PORT_ERROR, e);
            }
        }
        host = getSecondarySSLHostname();
        port = properties.getProperty(KEY_SECONDARY_SSL_PORT);
        if (host == null || port == null) {
            try {
                URL url = new URL(getSecondaryServicesBaseURL());
                properties.setProperty(KEY_SECONDARY_SSL_HOST, url.getHost());
                properties.setProperty(KEY_SECONDARY_SSL_PORT, String.valueOf(url.getPort()));
            } catch (MalformedURLException e) {
                LOGGER.error(SECONDARY_HOST_PORT_ERROR, e);
            }
        }
    }

    /**
     * Returns the singleton instance of {@link RBACConnectionFactory} properties. Properties are read from the
     * properties file when the instance is first created. If properties could not be read, default values are put in
     * their place. Properties are always overridden by the system properties.
     * <p>
     * The default values are:
     * <ul>
     * <li>{@value #KEY_PRIMARY_SERVICES_URL} -&gt; https://localhost:8443/auth-services</li>
     * <li>{@value #KEY_PRIMARY_SSL_HOST} -&gt; localhost</li>
     * <li>{@value #KEY_PRIMARY_SSL_PORT} -&gt; 8443</li>
     * <li>{@value #KEY_SECONDARY_SERVICES_URL} -&gt; https://localhost:8443/auth-services</li>
     * <li>{@value #KEY_SECONDARY_SSL_HOST} -&gt; localhost</li>
     * <li>{@value #KEY_SECONDARY_SSL_PORT} -&gt; 8443</li>
     * <li>{@value #KEY_INACTIVITY_TIMEOUT_DEFAULT} -&gt; 1800</li>
     * <li>{@value #KEY_INACTIVITY_RESPONSE_GRACE_PERIOD} -&gt; 30</li>
     * <li>{@value #KEY_SHOW_ROLE_SELECTOR} -&gt; false</li>
     * <li>{@value #KEY_VERIFY_SIGNATURE} -&gt; false</li>
     * <li>{@value #KEY_LOCAL_SERVICES_PORT} -&gt; 9638</li>
     * </ul>
     * 
     * @return singleton instance of RBAC properties.
     */
    public static synchronized RBACProperties getInstance() {
        if (instance == null) {
            instance = new RBACProperties();
        }
        return instance;
    }

    /**
     * Searches for the property with the specified key amongst the RBAC properties. The method returns null if the
     * property is not found.
     * 
     * @param key the property key.
     * 
     * @return the value of the RBAC property with the specified key value.
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * Returns the value of the {@value #KEY_PRIMARY_SERVICES_URL} property.
     * 
     * @return the value of the {@value #KEY_PRIMARY_SERVICES_URL} property.
     */
    public String getPrimaryServicesBaseURL() {
        return properties.getProperty(KEY_PRIMARY_SERVICES_URL);
    }

    /**
     * Returns the value of the {@value #KEY_PRIMARY_SSL_HOST} property.
     * 
     * @return the host name of the primary RBAC services
     */
    public String getPrimarySSLHostname() {
        return properties.getProperty(KEY_PRIMARY_SSL_HOST);
    }

    /**
     * Returns the value of the {@value #KEY_PRIMARY_SSL_PORT} property as an integer.
     * 
     * @return the port on which the primary RBAC services are accessible
     */
    public int getPrimarySSLPort() {
        return Integer.parseInt(properties.getProperty(KEY_PRIMARY_SSL_PORT));
    }

    /**
     * Returns the value of the {@value #KEY_SECONDARY_SERVICES_URL} property.
     * 
     * @return the value of the {@value #KEY_SECONDARY_SERVICES_URL} property.
     */
    public String getSecondaryServicesBaseURL() {
        return properties.getProperty(KEY_SECONDARY_SERVICES_URL);
    }

    /**
     * Returns the value of the {@value #KEY_SECONDARY_SSL_HOST} property.
     * 
     * @return the host name of the secondary RBAC services
     */
    public String getSecondarySSLHostname() {
        return properties.getProperty(KEY_SECONDARY_SSL_HOST);
    }

    /**
     * Returns the value of the {@value #KEY_SECONDARY_SSL_PORT} property as an integer.
     * 
     * @return the port on which the secondary RBAC services are accessible
     */
    public int getSecondarySSLPort() {
        return Integer.parseInt(properties.getProperty(KEY_SECONDARY_SSL_PORT));
    }

    /**
     * Returns the value of the {@value #KEY_CERTIFICATE_STORE} property.
     * 
     * @return the location of the certificate store.
     */
    public String getCertificateStore() {
        String s = properties.getProperty(KEY_CERTIFICATE_STORE);
        if (s == null || s.indexOf("%HOME%") < 0) {
            return s;
        } else {
            //if %HOME% is provided replace it by the user's home folder
            return s.replace("%HOME%", System.getProperty("user.home"));
        }
    }

    /**
     * Returns the value of the {@value #KEY_INACTIVITY_TIMEOUT_DEFAULT} property as an integer.
     * 
     * @return the default number of seconds after which the user should be considered as inactive.
     */
    public int getInactivityDefaultTimeout() {
        return Integer.parseInt(properties.getProperty(KEY_INACTIVITY_TIMEOUT_DEFAULT));
    }

    /**
     * Returns the value of the {@value #KEY_INACTIVITY_RESPONSE_GRACE_PERIOD} property as an integer.
     * 
     * @return the number of seconds that a {@link SecurityFacade} should wait for a {@link SecurityCallback} to respond
     *         to an auto logout confirmation.
     */
    public int getInactivityResponseGracePeriod() {
        return Integer.parseInt(properties.getProperty(KEY_INACTIVITY_RESPONSE_GRACE_PERIOD));
    }

    /**
     * Returns the value of the {@value #KEY_SHOW_ROLE_SELECTOR} property as a boolean. If true the facade should show
     * the role selector when credentials are requested. If false, the role selector is not necessary.
     * 
     * @return true if role selector is requested or false if not
     */
    public boolean isShowRoleSelector() {
        return Boolean.parseBoolean(properties.getProperty(KEY_SHOW_ROLE_SELECTOR, "false"));
    }

    /**
     * Returns the value of the {@value #KEY_VERIFY_SIGNATURE} property as a boolean. If true the facade will verify the
     * token signature when the token is obtained. If false, the signature will not be verified. The facade will verify
     * the signature using the public key store in the {@link #KEY_PUBLICK_KEY_LOCATION}.
     * 
     * @return true if the signature is verified or false otherwise
     */
    public boolean isVerifySignature() {
        return Boolean.parseBoolean(properties.getProperty(KEY_VERIFY_SIGNATURE, "false"));
    }

    /**
     * Returns the path to the file containing the public key that is used to verify the token signature.
     * 
     * @return the public key
     */
    public String getPublicKeyLocation() {
        return properties.getProperty(KEY_PUBLICK_KEY_LOCATION);
    }

    /**
     * Returns the value of the {@value #KEY_PERFORM_HANDHSAKE} property. If true the API will perform handshake with
     * the server before any other communication takes place. If false no handshake will be performed. This might result
     * in all communication being rejected if the server certificate is not trusted.
     * 
     * @return true if handshake should be performed or false otherwise
     */
    public boolean isPerformHandshake() {
        return Boolean.parseBoolean(properties.getProperty(KEY_PERFORM_HANDHSAKE, "true"));
    }
    
    /**
     * Returns the timeout in milliseconds to be used when establishing the first connection with the
     * RBAC server to perform the handshake. The value of the timeout is stored under the key
     * {@value #KEY_HANDSHAKE_TIMEOUT}.
     *  
     * @return the timeout in milliseconds
     */
    public int getHandhsakeTimeout() {
        return Integer.parseInt(properties.getProperty(KEY_HANDSHAKE_TIMEOUT,"2000"));
    }

    /**
     * Returns the value of the {@value #KEY_LOCAL_SERVICES_PORT} property as an integer.
     * 
     * @return the port on which the local authentication services are accessible
     */
    public int getLocalServicesPort() {
        return Integer.parseInt(properties.getProperty(KEY_LOCAL_SERVICES_PORT));
    }

    /**
     * Returns the value of the {@value #KEY_USE_LOCAL_SERVICE} property as an boolean value.
     * 
     * @return true if use local service otherwise false.
     */
    public boolean useLocalService() {
        return Boolean.parseBoolean(properties.getProperty(KEY_USE_LOCAL_SERVICE, "false"));
    }
}
