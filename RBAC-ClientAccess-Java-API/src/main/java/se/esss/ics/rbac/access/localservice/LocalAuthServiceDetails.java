/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.localservice;

import se.esss.ics.rbac.access.RBACProperties;

/**
 * <code>LocalAuthServiceDetails</code> carry the local authentication service and client request informations (IP
 * address and port number).
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class LocalAuthServiceDetails {

    private static final String DEFAULT_LOCAL_SERVICES_HOST = "127.0.0.1";
    private String localServiceIP;
    private int localServicePort;
    private String requestIP;
    private int requestPort;

    /**
     * Constructs <code>LocalAuthServiceDetails</code> object with fields set to the provided values. The local service
     * IP is set to the localhost IP (127.0.0.1), the local service port is retrieved from properties file. Client
     * request IP address is set to null and client request port to 0.
     */
    public LocalAuthServiceDetails() {
        this(DEFAULT_LOCAL_SERVICES_HOST, RBACProperties.getInstance().getLocalServicesPort(), null, 0);
    }

    /**
     * Constructs <code>LocalAuthServiceDetails</code> object with fields set to the provided values. The local service
     * port is retrieved from properties file.
     * 
     * @param localServiceIP the IP address of the local authentication service
     * @param requestIP the IP address of the client request
     * @param requestPort the IP source port of the client request
     */
    public LocalAuthServiceDetails(String localServiceIP, String requestIP, int requestPort) {
        this(localServiceIP, RBACProperties.getInstance().getLocalServicesPort(), requestIP, requestPort);
    }

    /**
     * Constructs <code>LocalAuthServiceDetails</code> object with fields set to the provided values.
     * 
     * @param localServiceIP the IP address of the local authentication service
     * @param localServicePort the IP source port of the local authentication service
     * @param requestIP the IP address of the client request
     * @param requestPort the IP source port of the client request
     */
    public LocalAuthServiceDetails(String localServiceIP, int localServicePort, String requestIP, int requestPort) {
        this.localServiceIP = localServiceIP;
        this.localServicePort = localServicePort;
        this.requestIP = requestIP;
        this.requestPort = requestPort;
    }

    /**
     * @return the IP address of the local authentication service.
     */
    public String getLocalServiceIP() {
        return localServiceIP;
    }

    /**
     * @return the IP source port of the local authentication service.
     */
    public int getLocalServicePort() {
        return localServicePort;
    }

    /**
     * @return the IP address of the client request.
     */
    public String getRequestIP() {
        return requestIP;
    }

    /**
     * @return the IP source port of the client request.
     */
    public int getRequestPort() {
        return requestPort;
    }
}
