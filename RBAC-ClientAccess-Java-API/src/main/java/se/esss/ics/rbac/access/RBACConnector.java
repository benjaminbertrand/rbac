/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;

import se.esss.ics.rbac.access.IConnectionFactory.Service;
import se.esss.ics.rbac.access.impl.RBACConnectionFactory;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.ResourcesInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.UsersInfo;

/**
 * 
 * <code>RBACConnector</code> provides access to the methods of the RBAC services, which are not directly related to
 * authentication and authorisation or are not to be used by general clients of RBAC.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class RBACConnector implements Serializable {

    private static final long serialVersionUID = 4199893198075704162L;

    private static final RBACConnector INSTANCE = new RBACConnector(RBACConnectionFactory.getInstance());

    /**
     * Returns the singleton instance of {@link RBACConnector}.
     * 
     * @return the singleton instance of {@link RBACConnector}.
     */
    public static RBACConnector getInstance() {
        return INSTANCE;
    }

    private final IConnectionFactory connectionFactory;
    private transient SecurityCallback securityCallback;

    private RBACConnector(IConnectionFactory factory) {
        this.connectionFactory = factory;
        this.securityCallback = new SecurityCallbackAdapter() {
        };
    }

    /**
     * Sets the security callback to use when contacting the RBAC service. The callback is used only during the 
     * handshaking process with the service to provide mechanisms to accept and reject certificates.
     * 
     * @param securityCallback the callback to use.
     */
    public void setSecurityCallback(SecurityCallback securityCallback) {
        if (securityCallback == null) {
            this.securityCallback = new SecurityCallbackAdapter() {
            };
        } else {
            this.securityCallback = securityCallback;
        }
    }
    
    /**
     * Returns the public key that can be used to verify the token signature.
     * 
     * @return the public key
     * 
     * @throws RBACConnectorException if there is an error while reading web service response.
     */
    public PublicKey getPublicKey() throws RBACConnectorException {
        return execute("/auth/publickey", new ConnectionExecutionTask<PublicKey, RBACConnectorException>() {
            @Override
            public PublicKey execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    try (InputStream stream = connection.getInputStream()) {
                        byte[] buffer = new byte[stream.available()];
                        stream.read(buffer);
                        X509EncodedKeySpec spec = new X509EncodedKeySpec(buffer);
                        KeyFactory keyFactory = KeyFactory.getInstance(FacadeUtilities.KEY_FACTORY_ALGORITHM);
                        return keyFactory.generatePublic(spec);
                    } catch (GeneralSecurityException e) {
                        throw new RBACConnectorException("Could not decode the public key.", e);
                    }
                } else {
                    return throwException(connection);
                }
            }
        });
    }

    /**
     * Returns generated content for access security file. {@link ConnectionException} is thrown in case of an error.
     * 
     * @param defaultGroup the name of the group, which will be used as default group
     * @param groups access security group names
     * 
     * @return string which contains generated content.
     * 
     * @throws RBACConnectorException if there is an error while reading web service response
     * @throws IllegalArgumentException if the parameter was null or invalid
     */
    public String getACFFileForGroups(String defaultGroup, String[] groups) throws RBACConnectorException,
            IllegalArgumentException {
        if (!FacadeUtilities.isArrayValid(groups)) {
            throw new IllegalArgumentException(
                    "Groups must not be empty or null and must not contain any empty or null name.");
        }
        StringBuilder urlBuilder = new StringBuilder(45 + groups.length * 28).append("/auth/pvaccess/group/");
        if (defaultGroup != null) {
            urlBuilder.append(defaultGroup).append('|');
        }
        urlBuilder.append(groups[0]);
        for (int i = 1; i < groups.length; i++) {
            urlBuilder.append(',');
            urlBuilder.append(groups[i]);
        }
        return execute(urlBuilder.toString(), new ConnectionExecutionTask<String, RBACConnectorException>() {
            @Override
            public String execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    return readACFContent(connection.getInputStream());
                } else {
                    return throwException(connection);
                }
            }
        });
    }
    
    /**
     * Returns the array of all usernames that are assigned the given role in RBAC.
     * 
     * @param role the role that assignees are requested for
     * @return the array of all usernames
     * @throws RBACConnectorException if the usernames could not be retrieved
     */
    public String[] getUsersWithRole(String role) throws RBACConnectorException {
        if (role == null || role.isEmpty()) {
            throw new IllegalArgumentException("Role name must not be null or empty.");
        }
        String url = new StringBuilder(12 + role.length()).append("/auth/").append(role).append("/users").toString();
        return execute(url, new ConnectionExecutionTask<String[], RBACConnectorException>() {
            @Override
            public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    UsersInfo users = JAXB.unmarshal(connection.getInputStream(), UsersInfo.class);
                    return users.getUsers().toArray(new String[users.getUsers().size()]);
                } else {
                    return throwException(connection);
                }
            }
        });
    }
    
    /**
     * Returns the array of all usernames in RBAC that are assigned a role with the given permission.
     * 
     * @param resource the resource that owns the permission
     * @param permission the name of the permissions that assignees are requested for
     * @return the array of all usernames
     * @throws RBACConnectorException if the usernames could not be retrieved
     */
    public String[] getUsersWithPermission(String resource, String permission) throws RBACConnectorException {
        if (resource == null || resource.isEmpty()) {
            throw new IllegalArgumentException("Resource name must not be null or empty.");
        }
        if (permission == null || permission.isEmpty()) {
            throw new IllegalArgumentException("Permission name must not be null or empty.");
        }
        String url = new StringBuilder(13 + resource.length() + permission.length())
                .append("/auth/").append(resource).append('/').append(permission).append("/users").toString();
        return execute(url, new ConnectionExecutionTask<String[], RBACConnectorException>() {
            @Override
            public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    UsersInfo users = JAXB.unmarshal(connection.getInputStream(), UsersInfo.class);
                    return users.getUsers().toArray(new String[users.getUsers().size()]);
                } else {
                    return throwException(connection);
                }
            }
        });
    }

    /**
     * Returns the array of all roles currently defined in RBAC.
     * 
     * @return the array of all roles
     * @throws RBACConnectorException if the array could not be retrieved
     */
    public String[] getRoles() throws RBACConnectorException {
        return execute("/auth/roles", new ConnectionExecutionTask<String[], RBACConnectorException>() {
            @Override
            public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    RolesInfo rolesInfo = JAXB.unmarshal(connection.getInputStream(), RolesInfo.class);
                    return rolesInfo.getRoleNames().toArray(new String[rolesInfo.getRoleNames().size()]);
                } else {
                    return throwException(connection);
                }
            }
        });
    }

    /**
     * Returns the array of all resources currently defined in RBAC.
     * 
     * @return the array of all resource names
     * @throws RBACConnectorException if the array could not be retrieved
     */
    public String[] getResources() throws RBACConnectorException {
        return execute("/auth/resources", new ConnectionExecutionTask<String[], RBACConnectorException>() {
            @Override
            public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    ResourcesInfo resourcesInfo = JAXB.unmarshal(connection.getInputStream(), ResourcesInfo.class);
                    return resourcesInfo.getResourceNames().toArray(
                            new String[resourcesInfo.getResourceNames().size()]);
                } else {
                    return throwException(connection);
                }
            }
        });
    }

    /**
     * Returns the array of all permissions currently defined in RBAC for the specified resource.
     * 
     * @param resource the name of the resource, for which the permissions should be retrieved
     * @return the array of permissions for the resource
     * @throws RBACConnectorException if the array of permissisons could not be retrieved
     */
    public String[] getPermissions(String resource) throws RBACConnectorException {
        if (resource == null || resource.isEmpty())
            throw new RBACConnectorException("Resource parameter must not be null or empty.");
        String url = new StringBuilder(50).append("/auth/").append(resource).append("/permissions").toString();
        return execute(url, new ConnectionExecutionTask<String[], RBACConnectorException>() {
            @Override
            public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                    RBACConnectorException {
                if (connection.getResponseCode() == FacadeUtilities.OK) {
                    PermissionsInfo permissionsInfo = JAXB.unmarshal(connection.getInputStream(),
                            PermissionsInfo.class);
                    return permissionsInfo.getPermissions().keySet().toArray(
                            new String[permissionsInfo.getPermissions().size()]);
                } else {
                    return throwException(connection);
                }
            }
        });
    }

    /**
     * Creates a connection to the given URL and execute the provided task using that connection. If there was an error
     * during execution the method will catch that error and wrap it into an RBACConnectorException.
     * 
     * @param url the URL to connect
     * @param task the task to execute
     * @return the task execution result
     * @throws RBACConnectorException if there was an error during execution
     */
    private <T> T execute(String url, ConnectionExecutionTask<T, RBACConnectorException> task)
            throws RBACConnectorException {
        int numServices = Service.values().length;
        for (int i = 1; i <= numServices; i++) {
            HttpURLConnection connection = null;
            try {
                connection = connectionFactory.getConnection(securityCallback, url, FacadeUtilities.GET);
                return task.execute(connection);
            } catch (IOException | ConnectionException e) {
                if (i == numServices) {
                    return throwConnectionException(connection, e);
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            connectionFactory.switchService();
        }
        // Should never happen, because one of the other exceptions should occur prior to this line
        throw new RBACConnectorException("Could not connect to " + url + ".");
    }

    /**
     * Reads the content of the stream and returns it as a string.
     * 
     * @param stream the stream to read from
     * @return the content
     * @throws IOException if there was an error accessing the stream
     */
    private static String readACFContent(InputStream stream) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream, FacadeUtilities.CHARSET))) {
            StringBuilder stringBuilder = new StringBuilder(stream.available());
            String line = null;
            while (br.ready()) {
                line = br.readLine();
                if (line == null) {
                    break;
                }
                stringBuilder.append(line);
                stringBuilder.append('\r');
            }
            return stringBuilder.toString().trim();
        }
    }

    /**
     * Constructs a {@link RBACConnectorException} that contains a more precise cause for why the received HTTP response
     * from the services was not as expected.<br/>
     * If primary exception is relevant to the cause it will be included as the cause.
     * 
     * @param connection used for connecting to the RBAC services.
     * @param primaryException exception caught beforehand, that may be relevant to the cause.
     * 
     *            throws RBACConnectorException containing a more precise cause why the received HTTP response from the
     *            services was not as expected
     */
    private static <T> T throwConnectionException(HttpURLConnection connection, Exception primaryException)
            throws RBACConnectorException {
        if (connection != null) {
            try {
                throw new RBACConnectorException(FacadeUtilities.ERROR_RESPONSE_MESSAGE
                        + FacadeUtilities.readResponseMessage(connection.getErrorStream()), primaryException);
            } catch (IOException e) {
                throw new RBACConnectorException(FacadeUtilities.ERROR_READING_RESPONSE, e);
            }
        }
        throw new RBACConnectorException(FacadeUtilities.ERROR_CONNECTING_TO_SERVICE, primaryException);
    }

    /**
     * Generates and throws an exception composed from the content of the error or input stream.
     * 
     * @param connection the connection that provides the input or error stream
     * 
     * @throws RBACConnectorException the generated exception
     * @throws IOException if there was an error reading the content of the streams
     */
    private static <T> T throwException(HttpURLConnection connection) throws RBACConnectorException, IOException {
        InputStream stream = connection.getErrorStream();
        if (stream == null) {
            stream = connection.getInputStream();
        }
        throw new RBACConnectorException(FacadeUtilities.INCORRECT_RESPONSE_MESSAGE
                + FacadeUtilities.readResponseMessage(stream));
    }
}
