/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * <code>ISecurityFacade</code> is the interface that provides access to all the actions that user can execute on the
 * RBAC authentication and authorisation services.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public interface ISecurityFacade extends Serializable {

    /**
     * Authenticates the user by calling the appropriate method in the web service. Prior to contacting the web service
     * it requests the credentials from the default SecurityCallback. The method returns the Token if authentication was
     * successful or throws a {@link SecurityFacadeException} if authentication failed. Token from the latest
     * authentication may later be retrieved by calling {@link #getToken()}.
     * 
     * @return token for the currently authenticated user.
     * 
     * @throws SecurityFacadeException if security callback or credentials are missing, if there is an error while
     *             reading web service response, or if the authentication failed for some reason.
     */
    Token authenticate() throws SecurityFacadeException;

    /**
     * Same as {@link #authenticate()}, but the action is executed asynchronously in order not to block the calling
     * thread.
     * 
     * Upon completing the authentication process the SecurityCallback is notified.
     * 
     * @param callback the security callback to be notified when the action is completed (if null the default callback
     *            is used)
     * 
     * @see #authenticate()
     * @see SecurityCallback#authenticationCompleted(Token)
     * @see SecurityCallback#authenticationFailed(String, String, Throwable)
     */
    void authenticate(SecurityCallback callback);

    /**
     * Logs the user out and invalidates the token synchronously. If logout was successful, the method returns true,
     * otherwise it returns false (e.g. no user logged in). If there was an error during the logout a
     * {@link SecurityFacadeException} is thrown.
     * 
     * @return <code>true</code> if the logout was successful.
     * 
     * @throws SecurityFacadeException if there is an error while reading or connecting to the web services.
     */
    boolean logout() throws SecurityFacadeException;

    /**
     * Same as {@link #logout()}, but the action is executed asynchronously. Upon completing the logout the
     * SecurityCallback is notified.
     * 
     * @param callback the security callback to be notified when the action is completed (if null the default callback
     *            is used)
     * 
     * @see #logout()
     * @see SecurityCallback#logoutCompleted(Token)
     * @see SecurityCallback#logoutFailed(String, Token, Throwable)
     */
    void logout(SecurityCallback callback);

    /**
     * Returns the token of the currently logged in user. If the token does not exist, but only the token ID exists, the
     * token is fetched from the service. If neither the token or token id is set, null is returned.
     * 
     * @return the locally stored token of the currently logged in user.
     * 
     * @throws AccessDeniedException if the token is no longer valid
     * @throws SecurityFacadeException if there is an error in communication or the service throws an exception
     */
    Token getToken() throws AccessDeniedException, SecurityFacadeException;

    /**
     * Sets the token ID to be used for authentication and authorisation.
     * 
     * @param tokenID the tokenID to be used for authentication and authorisation
     */
    void setToken(char[] tokenID);

    /**
     * Returns the locally stored token. This is either the actual token or token created by from the set tokenID. This
     * method does not make any round trip to the services.
     * 
     * @return the token
     */
    Token getLocalToken();

    /**
     * Checks if the logged in user is granted the permission provided as parameter from the IP embedded in the token.
     * If the access is granted, method returns <code>true</code>, if not it returns <code>false</code>. If there was an
     * error a {@link SecurityFacadeException} is thrown.
     * 
     * @param resource the name of the resource
     * @param permission the name of the permission
     * 
     * @return <code>true</code> if the currently logged in user has the specified permission
     * 
     * @throws AccessDeniedException if the token is no longer valid
     * @throws SecurityFacadeException if token is missing, or if there was an error while reading or connecting to web
     *             services
     * @throws IllegalArgumentException if one of the parameters <code>resource</code> or <code>permission</code> is
     *             null or empty
     */
    boolean hasPermission(String resource, String permission) throws AccessDeniedException, SecurityFacadeException,
            IllegalArgumentException;

    /**
     * Checks if the logged in user is granted the permissions provided as parameters from the IP embedded in the token.
     * Method returns a map of permission-grant pairs. For every permission, which was granted, value true is returned;
     * for every permission, which was denied, value false is returned. {@link SecurityFacadeException} is thrown in
     * case of an error.
     * 
     * @param resource the name of the resource
     * @param permissions the names of the permission
     * 
     * @return map of permission name - permission grant pairs.
     * 
     * @throws AccessDeniedException if the token is no longer valid
     * @throws SecurityFacadeException if token is missing, or if there was an error while reading or connecting to web
     *             services.
     * @throws IllegalArgumentException if one of the parameters <code>resource</code> or <code>permission</code> is
     *             null.
     */
    Map<String, Boolean> hasPermissions(String resource, String... permissions) throws AccessDeniedException,
            SecurityFacadeException, IllegalArgumentException;

    /**
     * Same as {@link #hasPermissions(String, String...)}, but the action is executed asynchronously. Upon completion
     * the SecurityCallback is notified.
     * 
     * @param callback the security callback to be notified when the action is completed (if null the default callback
     *            is used)
     * @param resource name of the resource
     * @param permissions name of the permissions
     * 
     * @see #hasPermissions(String, String...)
     * @see SecurityCallback#authorisationCompleted(Token, Map)
     * @see SecurityCallback#authorisationFailed(String, String[], Token, Throwable)
     */
    void hasPermissions(SecurityCallback callback, String resource, String... permissions);

    /**
     * Checks if the current token is valid and returns true if the token is valid or false otherwise. The method
     * delegates the checking to the service. Execution of this method should be as fast as possible and does not need
     * to modify the token in any way (e.g. increase the expiration period).
     * 
     * @return true if the token is valid or false otherwise
     * 
     * @throws SecurityFacadeException if there was an error during the check
     */
    boolean isTokenValid() throws SecurityFacadeException;

    /**
     * Asynchronous check if the token is valid or not. Upon completion the security callback is notified.
     * 
     * @param callback the callback to notify about the result
     * 
     * @see #isTokenValid()
     * @see SecurityCallback#tokenValidationCompleted(Token, boolean)
     * @see SecurityCallback#tokenValidationFailed(String, Token, Throwable)
     */
    void isTokenValid(SecurityCallback callback);

    /**
     * Returns the list of all roles for the provided username. {@link SecurityFacadeException} is thrown in case of an
     * error.
     * 
     * @param username the username of the user to get assigned roles for
     * 
     * @return array of role names assigned to the user
     * 
     * @throws SecurityFacadeException if there is an error while reading web service response.
     * @throws IllegalArgumentException if the parameter is null or empty
     */
    String[] getRolesForUser(String username) throws SecurityFacadeException, IllegalArgumentException;

    /**
     * Same as {@link #getRolesForUser(String)}, but the action is executed asynchronously. Upon completion the
     * SecurityCallback is notified.
     * 
     * @param callback the security callback to be notified when the action is completed (if null the default callback
     *            is used)
     * @param username the username of the user to get the assigned roles for
     * 
     * @see #getRolesForUser(String)
     * @see SecurityCallback#rolesLoadingCompleted(String, String[])
     * @see SecurityCallback#rolesLoadingFailed(String, String, Throwable)
     */
    void getRolesForUser(SecurityCallback callback, String username);

    /**
     * Requests exclusive access to the specified permission for the currently logged in user, on the specified
     * resource. If the access is granted, method returns <code>true</code>. If there was an error a
     * {@link SecurityFacadeException} is thrown.
     * 
     * @param resource name of the resource
     * @param permission name of the permission
     * @param durationInMinutes the duration of exclusive access in minutes, if less than 1 minute, default value will
     *            be used (defined by the service)
     * 
     * @return ExclusiveAccess containing information about the requested permission and the expiration date of the
     *         exclusive access, if the request was successful or null, if not successful
     * 
     * @throws AccessDeniedException if the token is no longer valid
     * @throws SecurityFacadeException if token is missing, or if there was an error while reading or connecting to web
     *             services
     * @throws IllegalArgumentException if resource of permission are null or empty
     */
    ExclusiveAccess requestExclusiveAccess(String resource, String permission, int durationInMinutes)
            throws AccessDeniedException, SecurityFacadeException, IllegalArgumentException;

    /**
     * Same as {@link #requestExclusiveAccess(String, String, int)}, but the action is executed asynchronously. Upon
     * completion the SecurityCallback is notified.
     * 
     * @param callback the security callback to be notified when the action is completed (if null the default callback
     *            is used)
     * @param resource name of the resource
     * @param permission name of the permission
     * @param durationInMinutes the duration of exclusive access in minutes, if less than 1 minute, default value will
     *            be used (defined by the service)
     * 
     * @see #requestExclusiveAccess(String, String, int)
     * @see SecurityCallback#requestExclusiveAccessCompleted(Token, ExclusiveAccess)
     * @see SecurityCallback#requestExclusiveAccessFailed(String, String, String, Token, Throwable)
     */
    void requestExclusiveAccess(SecurityCallback callback, String resource, String permission, int durationInMinutes);

    /**
     * Releases exclusive access to the specified permission for the currently logged in user, on the specified
     * resource. If the access is released, method returns <code>true</code>. If there was an error a
     * {@link SecurityFacadeException} is thrown.
     * 
     * @param resource name of the resource.
     * @param permission name of the permission.
     * 
     * @return <code>true</code> if exclusive access has been released successfully.
     * 
     * @throws AccessDeniedException if the token is no longer valid
     * @throws SecurityFacadeException if token is missing, or if there was an error while reading or connecting to web
     *             services
     * @throws IllegalArgumentException if one of the parameters is invalid (e.g. null)
     */
    boolean releaseExclusiveAccess(String resource, String permission) throws AccessDeniedException,
            SecurityFacadeException, IllegalArgumentException;

    /**
     * Same as {@link #releaseExclusiveAccess(String, String)}, but the action is executed asynchronously. Upon
     * completion the SecurityCallback is notified.
     * 
     * @param callback the security callback to be notified when the action is completed (if null the default callback
     *            is used)
     * @param resource name of the resource
     * @param permission name of the permission
     * 
     * @see #releaseExclusiveAccess(String, String)
     * @see SecurityCallback#releaseExclusiveAccessCompleted(Token, String, String)
     * @see SecurityCallback#releaseExclusiveAccessFailed(String, String, String, Token, Throwable)
     */
    void releaseExclusiveAccess(SecurityCallback callback, String resource, String permission);

    /**
     * Returns the version of the RBAC services.
     * 
     * @return the version of RBAC services
     * 
     * @throws SecurityFacadeException if there is an error while reading web service response.
     */
    String getRBACVersion() throws SecurityFacadeException;

    /**
     * Renew the token that is currently owned by this {@link SecurityFacade}. If the facade does not own a token an
     * exception is thrown. If the token could not be renewed, because it already expired, null is returned.
     * 
     * @return the renewed token if renewal was successful or null otherwise
     * 
     * @throws AccessDeniedException if the token is no longer valid
     * @throws SecurityFacadeException if the facade does not own a token
     */
    Token renewToken() throws AccessDeniedException, SecurityFacadeException;

    /**
     * Renews the token asynchronously. Upon completion the SecurityCallback is notified.
     * 
     * @param callback the callback to use upon completion or failure
     * 
     * @see #renewToken()
     * @see SecurityCallback#tokenRenewalCompleted(Token)
     * @see SecurityCallback#tokenRenewalFailed(String, Token, Throwable)
     */
    void renewToken(SecurityCallback callback);

    /**
     * Sets the auto logout timeout in minutes. If there was no user activity for the specified duration the facade will
     * notify the user and request confirmation through {@link SecurityCallback#autoLogoutConfirm(Token, int)}. Based on
     * the response or if there is no response for a specific duration, the user will be logged out.
     * 
     * @param timeoutInMinutes the timeout in minutes after which the user will be logged out if inactive
     */
    void setAutoLogoutTimeout(int timeoutInMinutes);

    /**
     * Sets the auto logout implementation that will be used by this facade.
     * 
     * @param autoLogout the auto logout used by this facade
     */
    void setAutoLogout(AutoLogout autoLogout);

    /**
     * Returns the auto logout timeout in seconds. This is the time after which the facade will call
     * {@link SecurityCallback#autoLogoutConfirm(Token, int)} if there was no user activity.
     * 
     * @return the auto logout timeout in seconds
     */
    int getAutoLogoutTimeout();

    /**
     * Returns the default security callback. If none is set a default one is created.
     * 
     * @return the default security callback
     */
    SecurityCallback getDefaultSecurityCallback();

    /**
     * Sets the security callback that is used by the facade. If none is set, default implementation (
     * {@link SecurityCallbackAdapter}) is used.
     * 
     * @param callback to be set as the security callback.
     */
    void setDefaultSecurityCallback(SecurityCallback callback);

    /**
     * Checks if use local service flag is true. If true, it tries to retrieve the token from the local service 
     * and register local service notification listener. If false the method does not do anything
     */
    void initLocalServiceUsage();

    /**
     * Adds a security facade listener, which is notified every time, when a user is logged in our out.
     * 
     * @param listener to be added.
     */
    void addSecurityFacadeListener(SecurityFacadeListener listener);

    /**
     * Removes the security facade listener.
     * 
     * @param listener to be removed.
     */
    void removeSecurityFacadeListener(SecurityFacadeListener listener);

    /**
     * Destroys this security facade and releases any resources that it might have allocated. There is no guarantee that
     * the facade is usable after this method has been called, nor is the implementor obliged to offer any recover
     * possibilities after the facade has been destroyed. After the facade has been destroyed it should no longer be
     * used. Calling this method several times in a row does not have any effect.
     */
    void destroy();

    /**
     * A convenience method which tells if this facade has been destroyed or not. Generally a facade is considered to be
     * destroyed if destroy was called at least once in its life cycle.
     * 
     * @return true if the facade has been destroyed or false otherwise
     */
    boolean isDestroyed();
}
