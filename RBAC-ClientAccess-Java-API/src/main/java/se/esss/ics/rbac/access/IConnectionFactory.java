/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.util.Map;

/**
 * 
 * <code>IConnectionFactory</code> defines the factory that creates connections to the requested URLs.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public interface IConnectionFactory extends Serializable {

    /**
     * <code>Service</code> identifies the service that is being used by the connection factory.
     */
    public static enum Service {
        PRIMARY, SECONDARY
    }

    /**
     * Creates a new HTTP connection to the provided URL using the given request method. The URL is given as a relative
     * path to the base server URL, which means that the provided URL should be appended to the currently used base URL.
     * No HTTP request headers are set when creating the connection. This is equivalent to calling
     * {@link #getConnection(SecurityCallback, String, String, Map)} with null or empty map.
     * 
     * @param callback the security callback which is notified if a certificate needs to be accepted
     * @param urlString URL to connect to
     * @param requestMethod HTTP request method (GET, POST, DELETE, etc.)
     * 
     * @return HTTP connection to the provided URL
     * 
     * @throws ConnectionException if there was an error creating the connection
     * 
     * @see #getConnection(SecurityCallback, String, String, Map)
     */
    HttpURLConnection getConnection(SecurityCallback callback, String urlString, String requestMethod)
            throws ConnectionException;

    /**
     * Creates a new HTTP connection to the provided URL. URL is given as a relative path and should be appended to the
     * server base URL. The connection is set to use the given HTTP request method (GET, POST, DELETE, etc.) and has
     * additional request properties (headers) set, as defined in the <code>requestProperties</code> parameter. When a
     * connection is created its verifier is set to only allow connections to the RBAC authentication server. It is
     * expected that the connection to the URL is verified and that any certificate (that might be issued by the server)
     * is trusted before the connection is returned. The provided callback can be used to accept or deny certificate.
     * 
     * @see SecurityCallback#acceptCertificate(String, java.security.cert.X509Certificate)
     * 
     * @param callback the security callback which is notified in case a certificate needs to be accepted
     * @param urlString URL to connect to
     * @param requestMethod HTTP request method (GET, POST, DELETE, etc.)
     * @param requestProperties map of additional HTTP request properties (headers)
     * 
     * @return HTTP connection to the provided URL
     * 
     * @throws ConnectionException if there was an error creating the connection
     */
    HttpURLConnection getConnection(SecurityCallback callback, String urlString, String requestMethod,
            Map<String, String> requestProperties) throws ConnectionException;

    /**
     * Switch the currently used service from primary to secondary or vice versa. The method should always switch from
     * one service to another.
     * 
     * @return the service that is being used after this call
     */
    Service switchService();

    /**
     * Returns the service that is being used for all connections.
     * 
     * @return the currently used service
     */
    Service getService();

}
