/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

/**
 * 
 * <code>RBACConnectorException</code> is an exception thrown by {@link RBACConnector} when it fails to parse the
 * incoming data or there is some other kind of error.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class RBACConnectorException extends Exception {

    private static final long serialVersionUID = 4759158690966702553L;

    /**
     * Constructs a new exception.
     * 
     * @param message the exception message
     */
    public RBACConnectorException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception.
     * 
     * @param message the exception message
     * @param cause the cause of the exception
     */
    public RBACConnectorException(String message, Throwable cause) {
        super(message, cause);
    }
}
