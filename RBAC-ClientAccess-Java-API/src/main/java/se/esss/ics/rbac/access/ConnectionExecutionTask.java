/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.xml.bind.DataBindingException;

/**
 * 
 * <code>ConnectionExecutionTask</code> is a task that makes that executes the HTTP method and returns whatever the
 * service method is supposed to return. The purpose of this class is to handle its exceptions in a common way.
 * 
 * @see SecurityFacade
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 * @param <T> the type of the object returned by this task
 * @param <E> the type of exception thrown by the method of this interface
 */
public interface ConnectionExecutionTask<T, E extends Exception> {

    /**
     * Executes the task. This method is called synchronously, which means that the caller is blocked until the method
     * returns. The implementor should execute the connection method by calling
     * {@link HttpURLConnection#getResponseCode()} and based on the answer return the appropriate object or throw one of
     * the possible exceptions.
     * 
     * @param connection the connection to act upon
     * @return the object that is generated from the connection's response
     * @throws IOException if there was an error reading the connection's response
     * @throws DataBindingException if the response could not be unmarshalled
     * @throws E if there was an error during connection, other than the above
     */
    T execute(HttpURLConnection connection) throws IOException, DataBindingException, E;
}
