/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.impl.RBACThreadFactory;

/**
 * 
 * <code>AbstractAutoLogout</code> provides the common implementation that takes care of automatic logout. The
 * implementation will trigger a call to {@link SecurityCallback#autoLogoutConfirm(Token, int)} if there has been no
 * update made for the specified {@link #setLogoutTimeout(int)}. In addition it will also schedule a logout action in
 * case that the callback does not respond in time.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public abstract class AutoLogout {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutoLogout.class);
    private static final String AUTO_LOGOUT_ERROR = "Could not auto-logout user.";

    private int logoutConfirmationGraceInSeconds;

    private long logoutTimeout;
    private int setLogoutTime;
    private boolean stopped = true;
    private long lastActionTime;
    private final ISecurityFacade facade;

    private class ForceLogoutRunnable implements Runnable {

        private final Token token;

        ForceLogoutRunnable(Token token) {
            this.token = token;
        }

        @Override
        public void run() {
            if (token == null) {
                return;
            }
            Token t = facade.getLocalToken();
            if (t != null && !Arrays.equals(t.getTokenID(), token.getTokenID())) {
                // do not log out, if there was a new user logged in in between
                return;
            }
            try {
                facade.logout();
            } catch (SecurityFacadeException e) {
                LOGGER.error(AUTO_LOGOUT_ERROR, e);
            }
        }
    }

    private final Runnable logoutChecker = new Runnable() {
        @Override
        public void run() {
            if (stopped) {
                return;
            }
            if (System.currentTimeMillis() - lastActionTime > logoutTimeout) {
                Token t = facade.getLocalToken();
                if (t == null) {
                    return;
                }
                doJob(t);
            }
        }

        private void doJob(Token t) {
            ScheduledFuture<?> future = executor.schedule(new ForceLogoutRunnable(t),
                    logoutConfirmationGraceInSeconds + 1, TimeUnit.SECONDS);
            if (facade.getDefaultSecurityCallback().autoLogoutConfirm(t, logoutConfirmationGraceInSeconds)) {
                try {
                    facade.logout();
                } catch (SecurityFacadeException e) {
                    LOGGER.error(AUTO_LOGOUT_ERROR, e);
                }
            }
            update();
            if (!future.isDone()) {
                future.cancel(false);
            }
        }
    };

    private ScheduledExecutorService executor;

    /**
     * Constructs a new auto logout.
     * 
     * @param facade the facade to use for logout, callbacks etc.
     */
    protected AutoLogout(ISecurityFacade facade) {
        this.lastActionTime = System.currentTimeMillis();
        this.facade = facade;
        setLogoutTimeout(15);
    }

    /**
     * Stops this auto logout.
     */
    public synchronized void stop() {
        stopped = true;
        if (executor != null) {
            executor.shutdownNow();
        }
        unRegisterForInputEvents();
    }

    /**
     * Starts this auto logout.
     */
    public synchronized void start() {
        if (!stopped) {
            return;
        }
        stopped = false;
        registerForInputEvents();
        if (executor != null) {
            executor.shutdownNow();
        }
        executor = Executors.newScheduledThreadPool(2, new RBACThreadFactory("RBAC-AutoLogout", false, true));
        executor.scheduleWithFixedDelay(logoutChecker, 1000, 1000, TimeUnit.MILLISECONDS);
    }

    /**
     * Sets the logout timeout.
     * 
     * @param logoutTimeoutInMinutes the timeout in minutes
     */
    public void setLogoutTimeout(int logoutTimeoutInMinutes) {
        if (logoutTimeoutInMinutes < 0) {
            throw new IllegalArgumentException("Timeout cannot be negative (" + logoutTimeoutInMinutes + ").");
        }
        this.setLogoutTime = logoutTimeoutInMinutes;
        this.logoutTimeout = logoutTimeoutInMinutes * 60000L - logoutConfirmationGraceInSeconds * 1000L;
        if (this.logoutTimeout < 0) {
            this.logoutTimeout = logoutTimeoutInMinutes * 60000L;
        }
    }

    /**
     * Sets the timeout in seconds how much time the callback has to respond to auto logout confirm call. After the
     * given time elapses since the call to the method, the auto logout will automatically logout the user.
     * 
     * @param logoutConfirmationGraceInSeconds the time in seconds
     */
    public void setLogoutConfirmationGrace(int logoutConfirmationGraceInSeconds) {
        this.logoutConfirmationGraceInSeconds = logoutConfirmationGraceInSeconds;
        setLogoutTimeout(setLogoutTime);
    }

    /**
     * Update the last action time to now.
     */
    protected void update() {
        lastActionTime = System.currentTimeMillis();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            stop();
        } finally {
            super.finalize();
        }
    }

    /**
     * Unregister from the mouse and keyboard input events.
     */
    protected abstract void unRegisterForInputEvents();

    /**
     * Register for mouse and keyboard input events. The implementation of this method depends on the UI framework that
     * uses the {@link SecurityFacade}.
     */
    protected abstract void registerForInputEvents();
}
