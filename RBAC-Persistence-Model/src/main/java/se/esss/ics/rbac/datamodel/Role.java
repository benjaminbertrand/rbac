/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule;

/**
 * <code>Role</code> is a definition of a user role and is uniquely identified by its name. Each user in the system can
 * have any number of roles (including none) assigned to him. By assigning the user a role, that user becomes the
 * representative of the role and automatically inherits everything that the role is entitled to do.
 * <p>
 * Role is associated with a set of {@link Permission}s. Those permissions define, what each particular role is allowed
 * to do. If the role contains a permissions <code>A</code> than every user that is assigned this role is allowed to
 * execute the action associated with permission <code>A</code>. If a role does not contain permission <code>B</code>
 * than the user is not allowed to execute the action associated with permission <code>B</code>. Assigned permissions
 * can only grant access; there are no permissions, which can deny access.
 * </p>
 * <p>
 * Each role is also associated with a set of designated users, who can manage this role. These are called Role Managers
 * and are provided by {@link #getManagers()}. Only the managers can modify the settings of this role (who is assigned
 * this role and which permissions are associated with this role).
 * </p>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "role")
@Table(name = "role", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_ROLE_name", columnNames = { "name" }) },
    indexes = { 
        @Index(name = "INDEX_ROLE_name", columnList = ("name")) })
@NamedQueries({
        @NamedQuery(name = "Role.selectAll",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Role r ORDER BY r.name"),
        @NamedQuery(name = "Role.findByName",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Role r WHERE r.name = :name"),
        @NamedQuery(name = "Role.findByUserId",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Role r, se.esss.ics.rbac.datamodel.UserRole ur"
                        + " WHERE ur.role = r AND ur.userId = :userId ORDER by r.name"),
        @NamedQuery(name = "Role.findValidByUserId",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Role r, se.esss.ics.rbac.datamodel.UserRole ur"
                        + " WHERE ur.role = r AND ur.userId = :userId AND"
                        + " (ur.assignment = 0 OR (NOW() BETWEEN ur.startTime AND ur.endTime))"),
        @NamedQuery(name = "Role.findByWildcard",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Role r"
                        + " WHERE r.name LIKE :wildcard ORDER BY r.name") })
public final class Role implements Serializable, NamedEntity {

    private static final long serialVersionUID = -1368486268613424849L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 30, nullable = false)
    private String name;
    @Column(name = "description", length = 1000)
    private String description;
    @ElementCollection(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_ROLE_MANAGERS_role"), 
                name = "managers", nullable = false, referencedColumnName = "id")
    private Set<String> managers;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(foreignKey = @ForeignKey(name = "FK_ROLE_PERMISSION_role"), 
                inverseForeignKey = @ForeignKey(name = "FK_ROLE_PERMISSION_permission"))
    private Set<Permission> permissions;
    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<UserRole> users;
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private Set<AccessSecurityRule> rules;

    /**
     * @return the primary key of this role
     */
    public int getId() {
        return id;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name set the unique name of this role (limited to 30 characters)
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the detailed description of this role
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the detailed description of this role
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the users that are assigned this role
     */
    public Set<UserRole> getUsers() {
        return users;
    }

    /**
     * @return the managers that are allowed to manage this role (add/remove permissions etc.)
     */
    public Set<String> getManagers() {
        return managers;
    }

    /**
     * @param managers the managers that are allowed to manage this role
     */
    public void setManagers(Set<String> managers) {
        this.managers = managers;
    }

    /**
     * @return the permissions that this role has
     */
    public Set<Permission> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions for this role
     */
    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
    
    /**
     * @return the rules
     */
    public Set<AccessSecurityRule> getRules() {
        return rules;
    }
    
    /**
     * @param rules the rules to set
     */
    public void setRules(Set<AccessSecurityRule> rules) {
        this.rules = rules;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Role other = (Role) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(description, other.description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        return new StringBuilder(description == null ? 100 : description.length() + 100)
                .append("ROLE [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("Description: ").append(description).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
