/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.EntityDescriptor;
import se.esss.ics.rbac.Util;

/**
 * <code>Token</code> contains information about all currently valid tokens. These are the tokens that have been issued
 * to clients and have not yet expired. Each token has a creation and expiration date, it is related to a specific IP
 * and user and it carries the RBAC digital signature. The token also provides a list of roles which requested the
 * token.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "token")
@Table(name = "token", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_TOKEN_token_id", columnNames = { "token_id" }) },
    indexes = { 
        @Index(name = "INDEX_TOKEN_token_id", columnList = ("token_id")),
        @Index(name = "INDEX_TOKEN_user_id", columnList = ("user_id")),
        @Index(name = "INDEX_TOKEN_expiration_date", columnList = ("expiration_date")) })
@NamedQueries({
        @NamedQuery(name = "Token.selectAll", 
                query = "SELECT t FROM se.esss.ics.rbac.datamodel.Token t ORDER BY t.userId, t.creationDate"),
        @NamedQuery(name = "Token.findByTokenId", 
                query = "SELECT t FROM se.esss.ics.rbac.datamodel.Token t"
                        + " WHERE t.tokenId = :tokenId AND t.expirationDate > NOW()"),
        @NamedQuery(name = "Token.findExpired", 
                query = "SELECT t FROM se.esss.ics.rbac.datamodel.Token t WHERE t.expirationDate < NOW()"),
        // deleteExpired is not to be used, unless there is an explicit ON CASCADE DELETE set on the token_role
        @NamedQuery(name = "Token.deleteExpired", 
                query = "DELETE FROM se.esss.ics.rbac.datamodel.Token t WHERE t.expirationDate < NOW()") })
public final class Token implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = -1124766546041757001L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "token_id", nullable = false)
    private String tokenId;
    @Column(name = "user_id", nullable = false)
    private String userId;
    @Column(name = "creation_date", nullable = false)
    private Timestamp creationDate;
    @Column(name = "expiration_date", nullable = false)
    private Timestamp expirationDate;
    @Column(name = "ip", nullable = false)
    private String ip;
    // collection itself is not serializable, but most of the implementations are, including
    // hibernate's, so we do not worry about non-serializable class
    @OneToMany(mappedBy = "token", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<TokenRole> role;

    /**
     * @return the primary key
     */
    public int getId() {
        return id;
    }

    /**
     * @return the unique tokenId of this token
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     * @param tokenId the tokenId to set
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    /**
     * @return the userId of the user whom this token has been issued to
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the date of creation of this token
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the date of expiration of this token
     */
    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return the ip of the client for which the token was issued
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip of the client for which the token was issued
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the roles that have been used to request the token
     */
    public Set<TokenRole> getRole() {
        return role;
    }

    /**
     * @param role the roles that have been used to request the token
     */
    public void setRole(Set<TokenRole> role) {
        this.role = role;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, userId, tokenId, ip, creationDate, expirationDate);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Token other = (Token) obj;
        return id == other.id && Objects.equals(tokenId, other.tokenId)
                && Objects.equals(creationDate, other.creationDate)
                && Objects.equals(expirationDate, other.expirationDate) && Objects.equals(ip, other.ip)
                && Objects.equals(userId, other.userId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        String roles = Util.toString(role, level + 1);
        return new StringBuilder(200 + roles.length())
                .append("TOKEN [").append(tab)
                .append("ID: ").append(tokenId).append(tab)
                .append("User: ").append(userId).append(tab)
                .append("Creation Date: ").append(creationDate).append(tab)
                .append("Expiration Date: ").append(expirationDate).append(tab)
                .append("IP: ").append(ip).append(tab)
                .append("Roles: ").append(roles).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
