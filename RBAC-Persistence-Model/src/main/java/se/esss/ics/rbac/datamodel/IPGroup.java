/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;

/**
 * <code>IPGroup</code> defines a group of IPs that commonly appears in the {@link Expression}s. An example of such a
 * group, is the group of all IPs that are in the main control room.
 * <p>
 * The group is uniquely identified by its name. This name should be used in the definition of the expression and should
 * only contain letters, numbers and underscores.
 * </p>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "ip_group")
@Table(name = "ip_group", 
    uniqueConstraints = {
        @UniqueConstraint(name = "UK_IP_GROUP_name", columnNames = { "name" }) },
    indexes = { 
        @Index(name = "INDEX_IP_GROUP_name", columnList = ("name")) })
@NamedQueries({
        @NamedQuery(name = "IPGroup.selectAll",
                query = "SELECT ipg FROM se.esss.ics.rbac.datamodel.IPGroup ipg ORDER BY ipg.name"),
        @NamedQuery(name = "IPGroup.findByWildcard",
                query = "SELECT ipg FROM se.esss.ics.rbac.datamodel.IPGroup ipg WHERE ipg.name LIKE :wildcard"), 
        @NamedQuery(name = "IPGroup.findByName",
                query = "SELECT ipg FROM se.esss.ics.rbac.datamodel.IPGroup ipg WHERE ipg.name = :name") })
public final class IPGroup implements Serializable, NamedEntity {

    private static final long serialVersionUID = 4615411592079098378L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "description")
    private String description;
    @ElementCollection(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_IP_GROUP_IP_ip_group"), name = "ip", nullable = false,
            referencedColumnName = "id")
    private Set<String> ip;

    /**
     * @return the unique id of this entity
     */
    public int getId() {
        return id;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the new name of this group
     */
    public void setName(String name) {
        if (name != null && name.indexOf(' ') > -1) {
            throw new IllegalArgumentException("IP Group name should not contain white spaces.");
        }
        this.name = name;
    }

    /**
     * @return the description of the group
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description of this group
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the set of IPs or domain names that belong to this group
     */
    public Set<String> getIp() {
        return ip;
    }

    /**
     * @param ip the set of IPs or domain names that belong to this group
     */
    public void setIp(Set<String> ip) {
        if (ip != null) {
            for (String s : ip) {
                if (s == null || s.indexOf(' ') > -1) {
                    throw new IllegalArgumentException("IP should not be null and should not contain white spaces ("
                            + s + ").");
                }
            }
        }
        this.ip = ip;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        IPGroup other = (IPGroup) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(description, other.description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        String ips = Util.toString(ip, level + 1);
        return new StringBuilder(ips.length() + 50)
                .append("IPGROUP [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("IPs: ").append(ips).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
