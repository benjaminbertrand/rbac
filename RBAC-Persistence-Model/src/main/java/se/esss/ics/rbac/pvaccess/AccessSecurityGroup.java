/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.pvaccess;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;

/**
 * <code>AccessSecurityGroup</code> defines the access security group (ASG) as recognised by the IOC. The group consists
 * of a list of input PVs and a list of rules. The input PVs specify which PVs contribute to the security (e.g. machine
 * state) and the rules define the actual access: which users, from which IPs and at which input values.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "access_security_group")
@Table(name = "access_security_group", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_ACCESS_SECURITY_GROUP_name", columnNames = { "name" }) },
    indexes = { 
        @Index(name = "INDEX_ACCESS_SECURITY_GROUP_name", columnList = ("name")) })
@NamedQueries({
        @NamedQuery(name = "AccessSecurityGroup.findByNames", 
                query = "SELECT asg FROM se.esss.ics.rbac.pvaccess.AccessSecurityGroup asg"
                        + " WHERE asg.name IN :names ORDER BY asg.name"),
        @NamedQuery(name = "AccessSecurityGroup.selectAll", 
                query = "SELECT asg FROM se.esss.ics.rbac.pvaccess.AccessSecurityGroup asg ORDER BY asg.name"),
        @NamedQuery(name = "AccessSecurityGroup.findByWildcard",
                query = "SELECT asg FROM se.esss.ics.rbac.pvaccess.AccessSecurityGroup asg"
                        + " WHERE asg.name LIKE :wildcard") })
public final class AccessSecurityGroup implements Serializable, NamedEntity {

    private static final long serialVersionUID = 1666810369377835175L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description")
    private String description;
    // set itself is not serializable, but most of the implementations are, including hibernate's,
    // so we do not worry about non-serializable class
    @OneToMany(mappedBy = "asGroup", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<AccessSecurityInput> inputs;
    @OneToMany(mappedBy = "asGroup", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<AccessSecurityRule> rules;

    /**
     * @return the unique id of this table
     */
    public int getId() {
        return id;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name of the ASG group
     */
    public void setName(String name) {
        if (name.indexOf(' ') > -1) {
            throw new IllegalArgumentException("Name must not contain white spaces.");
        }
        this.name = name;
    }

    /**
     * @return the description of this group
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description of this group
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the inputs that contribute to this access security group
     */
    public Set<AccessSecurityInput> getInputs() {
        return inputs;
    }

    /**
     * @param inputs the inputs that contribute to the evaluation of rules
     */
    public void setInputs(Set<AccessSecurityInput> inputs) {
        this.inputs = inputs;
    }

    /**
     * @return the rules which define the access to the PVs that belong to this group
     */
    public Set<AccessSecurityRule> getRules() {
        return rules;
    }

    /**
     * @param rules the rules that define the access to the PVs
     */
    public void setRules(Set<AccessSecurityRule> rules) {
        this.rules = rules;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(description, id, name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        AccessSecurityGroup other = (AccessSecurityGroup) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(description, other.description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        int descLength = description == null ? 4 : description.length();
        String rulesStr = Util.toString(rules, level + 1);
        String inputsStr = Util.toString(inputs, level + 1);
        return new StringBuilder(100 + descLength + rulesStr.length() + inputsStr.length())
                .append("ACCESS SECURITY GROUP [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("Description: ").append(description).append(tab)
                .append("Rules: ").append(rulesStr).append(tab)
                .append("Inputs: ").append(inputsStr).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }

}
