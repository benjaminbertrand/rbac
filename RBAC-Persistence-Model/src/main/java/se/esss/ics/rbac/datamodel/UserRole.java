/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.EntityDescriptor;
import se.esss.ics.rbac.Util;

/**
 * <code>UserRole</code> describes user-role pairs. In general, it provides information, which user (defined by
 * {@link #userId}) is assigned the role (defined by {@link #role} ). Besides simple pairing of users and roles, this
 * entity provides also other options associates with the role to user assignment.
 * <p>
 * There are different ways how the role could be assigned to a specific user. These three possibilities are defined by
 * {@link AssignmentType}.
 * </p>
 * <ul>
 * <li>{@link AssignmentType#NORMAL}: a role is assigned to a user permanently (or until a manager decides to unassign
 * it)</li>
 * <li>{@link AssignmentType#LIMITED}: user is assigned the role for a limited period of time (starting at
 * {@link #startTime} and ending at {@link #endTime}). During that period the user has same access as any other user,
 * who is normally assigned that role. Outside of that time period, user has no access that is granted to that role
 * (unless he has other roles that grant him the same access): the end effect in such times is the same as if the user
 * is not assigned that role.</li>
 * <li>{@link AssignmentType#DELEGATED}: user is assigned the role for a limited period of time (similar to the LIMITED
 * case), but in this situation the role has been assigned to him by another user, who delegated his role to this user.
 * In this situation the {@link #delegator} always needs to be defined - this is the user who delegated his role. User
 * has the delegated role for the specified time period or until the delegator undelegates his role.</li>
 * </ul>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "user_role")
@Table(name = "user_role", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_USER_ROLE_user_id_role", 
                          columnNames = { "user_id", "role", "start_time", "end_time" }) },
    indexes = {
        @Index(name = "INDEX_USER_ROLE_user_id", columnList = ("user_id")),
        @Index(name = "INDEX_USER_ROLE_role", columnList = ("role")),
        @Index(name = "INDEX_USER_ROLE_assignment", columnList = ("assignment")),
        @Index(name = "INDEX_USER_ROLE_start_time", columnList = ("start_time")),
        @Index(name = "INDEX_USER_ROLE_end_time", columnList = ("end_time")) })
@NamedQueries({
        @NamedQuery(
                name = "UserRole.findNonExpiredByUserId",
                query = "SELECT ur FROM se.esss.ics.rbac.datamodel.Role r, se.esss.ics.rbac.datamodel.UserRole ur"
                        + " WHERE ur.role = r AND ur.userId = :userId AND (ur.assignment = 0 OR (NOW() < ur.endTime))"),
        @NamedQuery(name = "UserRole.selectUserIds",
                query = "SELECT ur.userId FROM se.esss.ics.rbac.datamodel.UserRole ur WHERE ur.role.id = :roleId"),
        @NamedQuery(name = "UserRole.deleteExpired", query = "DELETE FROM se.esss.ics.rbac.datamodel.UserRole ur"
                + " WHERE ur.assignment IN (2, 3) AND ur.endTime < NOW()"),
        @NamedQuery(
                name = "UserRole.findValidByNamesAndUserId",
                query = "SELECT ur FROM se.esss.ics.rbac.datamodel.Role r, se.esss.ics.rbac.datamodel.UserRole ur"
                        + " WHERE r.name IN :names AND ur.role = r AND ur.userId = :userId"
                        + " AND (ur.assignment = 0 OR (NOW() BETWEEN ur.startTime AND ur.endTime))"),
        @NamedQuery(
                name = "UserRole.findValidByUserId",
                query = "SELECT ur FROM se.esss.ics.rbac.datamodel.Role r, se.esss.ics.rbac.datamodel.UserRole ur"
                        + " WHERE ur.role = r AND ur.userId = :userId"
                        + " AND (ur.assignment = 0 OR (NOW() BETWEEN ur.startTime AND ur.endTime))") })
public final class UserRole implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = -1475396551288448161L;

    /**
     * <code>AssignmentType</code> describes three possible types of assignment of a role to the user: NORMAL, when the
     * user is selected to be added to the role; DELEGATER, when another user (who has this role) delegated the role to
     * this user, LIMITED, when the role is assigned to the user for a limited time.
     */
    public static enum AssignmentType implements Serializable {
        NORMAL, DELEGATED, LIMITED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "user_id", nullable = false)
    private String userId;
    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_USER_ROLE_role"), name = "role", nullable = false,
            referencedColumnName = "id")
    private Role role;
    @Column(name = "assignment", nullable = false)
    private AssignmentType assignment = AssignmentType.NORMAL;
    @Column(name = "delegator")
    private String delegator;
    @Column(name = "start_time")
    private Timestamp startTime;
    @Column(name = "end_time")
    private Timestamp endTime;
    // collection itself is not serializable, but most of the implementations are, including
    // hibernate's, so we do not worry about non-serializable class
    // for cascade purposes only, do not assign any tokens to roles directly
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "role")
    private List<TokenRole> token;

    /**
     * @return the tokens associated with this role
     */
    public List<TokenRole> getToken() {
        return token;
    }

    /**
     * @param token the tokens associated with this role
     */
    public void setToken(List<TokenRole> token) {
        this.token = token;
    }

    /**
     * @return the primary key of this entity
     */
    public int getId() {
        return id;
    }

    /**
     * @return the user id, who is assigned the role
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the id of the user, who is assigned the role
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the role, which this user assignment belongs to
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role, which this user assignment belongs to
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the type of assignment the role to the user
     */
    public AssignmentType getAssignment() {
        return assignment;
    }

    /**
     * @param assignment the type of assignment of the role to the user
     */
    public void setAssignment(AssignmentType assignment) {
        if (assignment == null) {
            this.assignment = AssignmentType.NORMAL;
        } else {
            this.assignment = assignment;
        }
    }

    /**
     * @return the user who might have delegated the role to the {@link #userId}, can be null if the role has not been
     *         delegated, but assigned permanently
     */
    public String getDelegator() {
        return delegator;
    }

    /**
     * @param delegator the id of the delegator
     */
    public void setDelegator(String delegator) {
        this.delegator = delegator;
    }

    /**
     * @return the start time of the delegated or limited assignment
     */
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the start time of the delegated or limited assignment
     */
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the end time of the delegated or limited assignment
     */
    public Timestamp getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the end time of the delegated or limited assignment
     */
    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, userId, role, startTime, endTime, delegator, assignment);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        UserRole other = (UserRole) obj;
        return id == other.id && assignment == other.assignment && Objects.equals(role, other.role)
                && Objects.equals(userId, other.userId) && Objects.equals(delegator, other.delegator)
                && Objects.equals(endTime, other.endTime) && Objects.equals(startTime, other.startTime);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        StringBuilder sb = new StringBuilder(200)
                .append("USER ROLE [").append(tab)
                .append("Role: ").append((role == null) ? "null" : role.toString(level + 1)).append(tab)
                .append("User: ").append(userId).append(tab)
                .append("Assignment: ").append(assignment);
        if (assignment != AssignmentType.NORMAL) {
            sb.append(tab)
            .append("Start Time: ").append(startTime).append(tab)
            .append("End Time: ").append(endTime);
        }
        if (assignment == AssignmentType.DELEGATED) {
            sb.append(tab)
            .append("Delegator: ").append(delegator);
        }
        sb.append(endTab).append(']');
        return sb.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
