/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 
 * <code>ManagementStudioLog</code> describes a log entry in the Management Studio. The table holds information about
 * all user actions that occurred in the management studio (add role, remove role, assign permissions etc.). Each entry
 * holds information about the user that made the change, time, severity of the action, and the content, which
 * is a description of what was done.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Entity(name = "management_studio_log")
@Table(name = "management_studio_log",
    indexes = {
        @Index(name = "INDEX_MANAGEMENT_STUDIO_LOG_timestamp", columnList = ("timestamp")),
        @Index(name = "INDEX_MANAGEMENT_STUDIO_LOG_timestamp_user_id", columnList = ("user_id")) })
@NamedQueries({ 
    @NamedQuery(name = "ManagementStudioLog.filterLogs", 
            query = "SELECT l FROM se.esss.ics.rbac.ManagementStudioLog l"
                    + " WHERE severity IN :severities AND user_id IN :users AND timestamp"
                    + " BETWEEN :startTime AND :endTime ORDER BY timestamp DESC") })
public class ManagementStudioLog implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = 7281962779118681021L;

    /**
     * Denotes the severity of the log entry. In ascending order of severity, enumerated values are:
     * <ul>
     * <li><code>INFO</code> - entry carries general information.</li>
     * <li><code>WARNING</code> - entry carries a warning.</li>
     * <li><code>ERROR</code> - entry carries information about an error.</li>
     * </ul>
     */
    public static enum Severity {
        INFO, WARNING, ERROR;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "timestamp", nullable = false)
    private Timestamp timestamp;
    @Column(name = "user_id", nullable = false)
    private String userID;
    @Column(name = "severity", nullable = false)
    @Enumerated(EnumType.STRING)
    private Severity severity;
    @Column(name = "content", length = 1000)
    private String content;

    /**
     * @return the primary key
     */
    public int getId() {
        return id;
    }

    /**
     * @return time stamp of the log entry
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the time stamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the userId of the user that triggered this log entry creation
     */
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userId of the user that triggered this log entry creation
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * @return the severity of this log entry
     */
    public Severity getSeverity() {
        return severity;
    }

    /**
     * @param severity the severity of this log entry
     */
    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    /**
     * @return additional message containing more details about this log entry
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content additional message containing more details about this log entry
     */
    public void setContent(String content) {
        this.content = content;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, userID, severity, timestamp, content);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        ManagementStudioLog other = (ManagementStudioLog) obj;
        return id == other.id && Objects.equals(timestamp, other.timestamp) && Objects.equals(content, other.content)
                && Objects.equals(severity, other.severity) && Objects.equals(userID, other.userID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        int contentLength = content == null ? 4 : content.length();
        return new StringBuilder(contentLength + 150)
                .append("MANAGEMENT STUDIO LOG [").append(tab)
                .append("Severity: ").append(severity).append(tab)
                .append("Time: ").append(timestamp).append(tab)
                .append("User: ").append(userID).append(tab)
                .append("Content: ").append(content).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
