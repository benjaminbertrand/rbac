/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.EntityDescriptor;
import se.esss.ics.rbac.Util;

/**
 * 
 * <code>TokenRole</code> is a utility entity which takes care of proper cascading of entity removal. When a role is
 * removed from the database, the token role should be removed as well, but the token should still be there. When a
 * token is removed from the system, the token role should be removed and the role left intact.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Entity(name = "token_role")
@Table(name = "token_role", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_TOKEN_ROLE_token_role", columnNames = { "token", "role" }) },
    indexes = { 
        @Index(name = "INDEX_TOKEN_ROLE_token", columnList = ("token")) })
public class TokenRole implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = 2063711738571606606L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_TOKEN_ROLE_token"), name = "token", nullable = false,
            referencedColumnName = "id")
    private Token token;
    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_TOKEN_ROLE_role"), name = "role", nullable = false,
            referencedColumnName = "id")
    private UserRole role;

    /**
     * Construct new TokenRole from the given parameters.
     * 
     * @param token the token
     * @param role the role
     */
    public TokenRole(Token token, UserRole role) {
        setToken(token);
        setRole(role);
    }

    /**
     * Construct new TokenRole.
     */
    public TokenRole() {

    }

    /**
     * @return the primary key
     */
    public int getId() {
        return id;
    }

    /**
     * @return the token associated with the role
     */
    public Token getToken() {
        return token;
    }

    /**
     * @param token the token that is associated with the role
     */
    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * @return the role that is associated with the token
     */
    public UserRole getRole() {
        return role;
    }

    /**
     * @param role the role that is associated with the token
     */
    public void setRole(UserRole role) {
        this.role = role;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, role, token);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        TokenRole other = (TokenRole) obj;
        return id == other.id && Objects.equals(token, other.token) && Objects.equals(role, other.role);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        return new StringBuilder(100)
                .append("TOKEN ROLE [").append(tab)
                .append("Token: ").append((token == null) ? "null" : token.getTokenId()).append(tab)
                .append("Role: ").append((role == null) ? "null" : role.getRole().getName()).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
