/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 
 * <code>RBACLog</code> represents a chronological information about general usage of the authentication and
 * authorisation services. In addition to timestamp, each entry carries information about the severity of the entry,
 * action taken by the user that triggered the entry creation, userId of the user at that time, IP address of that user
 * and an arbitrary additional message (content). Log entry severity is limited to <code>INFO</code>,
 * <code>WARNING</code>, and <code>ERROR</code>. Global actions recorded in log entries are: <code>AUTHORIZE</code>,
 * <code>ROLES</code>, <code>CLEANUP</code>, <code>LOGIN</code>, <code>LOGOUT</code>, <code>EXCLUSIVE</code>,
 * <code>RENEW</code>, and <code>TOKEN</code>.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Entity(name = "rbac_log")
@Table(name = "rbac_log", 
    indexes = { 
        @Index(name = "INDEX_RBAC_LOG_severity", columnList = ("severity")),
        @Index(name = "INDEX_RBAC_LOG_action", columnList = ("action")),
        @Index(name = "INDEX_RBAC_LOG_timestamp", columnList = ("timestamp")),
        @Index(name = "INDEX_RBAC_LOG_user_id", columnList = ("user_id")) })
@NamedQueries({
        @NamedQuery(name = "RBACLog.countEntries", 
                query = "SELECT COUNT(l.id) FROM se.esss.ics.rbac.RBACLog l"
                        + " WHERE action IN :actions AND severity IN :severities AND user_id IN :users"
                        + " AND timestamp BETWEEN :startTime AND :endTime"),
        @NamedQuery(name = "RBACLog.filterLogs", 
                query = "SELECT l FROM se.esss.ics.rbac.RBACLog l"
                        + " WHERE action IN :actions AND severity IN :severities AND user_id IN :users"
                        + " AND timestamp BETWEEN :startTime AND :endTime ORDER BY timestamp DESC"),
        @NamedQuery(name = "RBACLog.filterLogsWithoutUsers", 
                query = "SELECT l FROM se.esss.ics.rbac.RBACLog l"
                        + " WHERE action IN :actions AND severity IN :severities"
                        + " AND timestamp BETWEEN :startTime AND :endTime ORDER BY timestamp DESC")})
public class RBACLog implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = 7281962779118681021L;

    /**
     * Denotes the severity of the log entry. In ascending order of severity, enumerated values are:
     * <ul>
     * <li><code>INFO</code> - entry carries general information.</li>
     * <li><code>WARNING</code> - entry carries a warning.</li>
     * <li><code>ERROR</code> - entry carries information about an error.</li>
     * </ul>
     */
    public static enum Severity {
        INFO, WARNING, ERROR;
    }

    /**
     * Indicates a global action taken by a user that triggered the creation of an entry. Supported actions are:
     * <ul>
     * <li><code>AUTHORIZE</code> - entry has been created while a user was using authorisation services.</li>
     * <li><code>ROLES</code> - entry has been created while a user was trying to retrieve the roles list.</li>
     * <li><code>RESOURCES</code> - entry has been created while a user was trying to retrieve the resources list.</li>
     * <li><code>PERMISSIONS</code> - entry has been created while a user was trying to retrieve the 
     *                              permissions list.</li>
     * <li><code>LOGIN</code> - entry has been created while a user was trying to login.</li>
     * <li><code>LOGOUT</code> - entry has been created while a user was trying to log out.</li>
     * <li><code>EXCLUSIVE</code> - entry has been created while a user was managing exclusive access.</li>
     * <li><code>RENEW</code> - entry has been created while a user was renewing an existing token.</li>
     * <li><code>TOKEN</code> - entry has been created while a user was retrieving an existing token.</li>
     * <li><code>CLEANUP</code> - entry has been created while the services performed a cleanup of expired entries.</li>
     * </ul>
     */
    public static enum Action {
        AUTHORIZE, ROLES, RESOURCES, PERMISSIONS, LOGIN, LOGOUT, EXCLUSIVE, RENEW, TOKEN, CLEANUP;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "timestamp", nullable = false)
    private Timestamp timestamp;
    @Column(name = "user_id", nullable = false)
    private String userID;
    @Column(name = "ip", nullable = false)
    private String ip;
    @Column(name = "severity", nullable = false)
    @Enumerated(EnumType.STRING)
    private Severity severity;
    @Column(name = "action", nullable = false)
    @Enumerated(EnumType.STRING)
    private Action action;
    @Column(name = "content", length = 1000)
    private String content;

    /**
     * @return the primary key
     */
    public int getId() {
        return id;
    }

    /**
     * @return time stamp of the log entry
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the time stamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the userId of the user that triggered this log entry creation
     */
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userId of the user that triggered this log entry creation
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * @return the ip of the client that triggered this log entry creation
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip of the client that triggered this log entry creation
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the severity of this log entry
     */
    public Severity getSeverity() {
        return severity;
    }

    /**
     * @param severity the severity of this log entry
     */
    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    /**
     * @return primary action taken by the client to trigger this log entry creation
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action primary action taken by the client to trigger this log entry creation
     */
    public void setAction(Action action) {
        this.action = action;
    }

    /**
     * @return additional message containing more details about this log entry
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content additional message containing more details about this log entry
     */
    public void setContent(String content) {
        this.content = content;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, userID, ip, severity, timestamp, action, content);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        RBACLog other = (RBACLog) obj;
        return id == other.id && action == other.action && Objects.equals(timestamp, other.timestamp)
                && Objects.equals(content, other.content) && Objects.equals(ip, other.ip)
                && Objects.equals(severity, other.severity) && Objects.equals(userID, other.userID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        int contentLength = content == null ? 4 : content.length();
        return new StringBuilder(contentLength + 150)
                .append("RBAC LOG [").append(tab)
                .append("Action: ").append(action).append(tab)
                .append("Severity: ").append(severity).append(tab)
                .append("Time: ").append(timestamp).append(tab)
                .append("User: ").append(userID).append(tab)
                .append("IP: ").append(ip).append(tab)
                .append("Content: ").append(content).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
