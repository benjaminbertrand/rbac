# Overview
This project includes all RBAC sources.

All projects are configured to use and to be build with Maven. Projects are interdependent.

## Projects
- RBAC-Persistence-Model
	Data persistence model contains the JPA 2.1 model for the RBAC data that needs to be persisted.

- RBAC-DSAccess-API
	Directory Service Access api defines the api to access the directory service and fetch the data required by rbac (authentication, usernames, user info etc.).

- RBAC-DSAccess-LDAP
	Implementation of the RBAC-DSAccess-API that uses LDAP. The project is configured to work with ESS LDAP. It depends on the RBAC-DSAccess-API.

- RBAC-DSAccess-Dummy
	Implementation of the RBAC-DSAccess-API that provides some dummy data. The project can replace the RBAC-DSAccess-LDAP during development to allow accessing data in a faster way. The project can also be used, if access to LDAP is not available.

- RBAC-JAXB
	Provides JAXB implementation of POJOs used by the services. These are the elements that are returned by the service when the service response is XML.

- RBAC-PV
	Provides interfaces to access remote system end points (e.g. control system channels). The base of this code was Control System Studio plug-in.

- RBAC-PV-EPICS
	Provides implementation of the RBAC-PV, which can access EPICS PVs. The base of this code was Control System Studio plug-in.

- RBAC-AuthServices
	The REST services for authentication and authorization.

- RBAC-ManagementStudio
	The user interfaces for management and administration of RBAC.

- RBAC-ClientAccess-Java-API
	The client api to access the rbac services. It provides the facilities to login, logout, to check permissions, generate EPICS access security files, retrieve public key etc.

- RBAC-ClientAccess-Java-Swing
	An extension of the API that uses Swing components to show the login dialog and to display messages.

- RBAC-SSO
    The Single Sign On server implementation that stores the tokens used by the clients on the same host.

- RBAC-TestPlan
	Provides the TestCases, which automate the execution of the RBAC Test Plan (several of the TestCases in the official document are automated). The tests are written as JUnit tests. They expect a full RBAC setup to be available (services, database, LDAP, management studio, client access). During the execution, user needs to provide his username and password as described in the official document.

# Build Projects

All projects are configured to be build by maven. The projects extend the ess-java-config pom.xml, which has to be available in the maven repository, before RBAC can be build. You can get it here:
[https://bitbucket.org/europeanspallationsource/ess-java-config](https://bitbucket.org/europeanspallationsource/ess-java-config)
First, install the ess-java-config by running command **mvn install**. This will install the ess-java-config pom.xml into your maven repository. Now you can build the RBAC code. You can do that by executing **mvn install** in each individual project, or execute **mvn install** in the RBAC root. The root pom.xml contains references to all RBAC project and will build and install them in proper order. The output of the build is located in the *target* folder of each individual project.

# Help

User's manual is provided by https://bitbucket.org/europeanspallationsource/serviceshelp

# Development Environment

The following tools are required to setup development environment:
- Wildfly Application Server 8.1.0
- Java SDK 1.7 or later
- Eclipse IDE for Java EE developers 4.3 (Kepler) with JBoss Tools installed
- PostgreSQL (or other) database server.

For details on how to setup development environment see Setup.docx.

# Docker

The application can be built and run as a Docker container. The image is based on the jboss/wildfly image from Docker Hub.

### How to use this image

```
$ docker run registry.esss.lu.se/ics-software/rbac
```

Environment variables that can be set when running a container based on this image:

- available through [standalone.xml](https://gitlab.esss.lu.se/ics-software/rbac/tree/master/standalone.xml) file (system properties)

| Environment variable     | Default    | Description |
| -------------------------|------------|-------------|
| `RBAC_DEPLOYMENT_CONTEXT_MANAGEMENTSTUDIO` | / | Context root used for Management Studio |
| `RBAC_DEPLOYMENT_CONTEXT_AUTHSERVICES` | /service | Context root used for Auth Services |
| `RBAC_DATABASE_HOST` | localhost | Host used for the database connection |
| `RBAC_DATABASE_PORT` | 5432 | Port used for the database connection |
| `RBAC_DATABASE_NAME` | rbac | Database name used for the database connection |
| `RBAC_DATABASE_USERNAME` | rbac | Username used for the database connection |
| `RBAC_DATABASE_PASSWORD` | rbac | Password used for the database connection |
| `RBAC_PRIMARY_URL` | https://rbac.esss.lu.se:8443/service | URL for primary RBAC service |
| `RBAC_PRIMARY_SSL_HOST` | rbac.esss.lu.se | SSL host for primary RBAC service |
| `RBAC_PRIMARY_SSL_PORT` | 8443 | SSL port for primary RBAC service |
| `RBAC_SECONDARY_URL` | https://localhost:8443/service | URL for secondary RBAC service |
| `RBAC_SECONDARY_SSL_HOST` | localhost | SSL host for secondary RBAC service |
| `RBAC_SECONDARY_SSL_PORT` | 8443 | SSL port for secondary RBAC service |
| `RBAC_HANDSHAKE` | true | Perform SSL handshake with RBAC |
| `RBAC_HANDSHAKE_TIMEOUT` | 2000 | Timeout for SSL handshake with RBAC |
| `RBAC_INACTIVITY_TIMEOUT_DEFAULT` | 900 | Inactivity timeout for RBAC client |
| `RBAC_INACTIVITY_RESPONSE_GRACE` | 30 | Inactivity response grace period for RBAC client |
| `RBAC_SHOW_ROLE_SELECTOR` | false | Show role selector in RBAC client |
| `RBAC_VERIFY_SIGNATURE` | false | Verify signature of RBAC |
| `RBAC_PUBLIC_KEY_LOCATION` | ~/.rbac/rbac.key | Public key for RBAC |
| `RBAC_LOCAL_SERVICES_PORT` | 9421 | Port for local RBAC service |
| `RBAC_USE_LOCAL_SERVICE` | false | Use local RBAC service |
| `RBAC_CERTIFICATE_KEYSTORE_PATH` | /etc/rbac/certs | SSL certificate store for RBAC client |
| `RBAC_SINGLE_SIGNON` | false | Use single sign-on |
| `LDAP_PRIMARY_HOST` | dc01.esss.lu.se | Primary LDAP host |
| `LDAP_PRIMARY_PORT` | 389 | Primary LDAP port |
| `LDAP_SECONDARY_HOST` | dc02.esss.lu.se | Secondary LDAP host |
| `LDAP_SECONDARY_PORT` | 389 | Secondary LDAP port |
| `LDAP_TIMEOUT` | 7000 | Timeout for LDAP connections |
| `LDAP_SECURITY_PRINCIPAL` | CN=ldapreadonly,CN=Users,DC=esss,DC=lu,DC=se | Bind DN for LDAP connections |
| `LDAP_SECURITY_CREDENTIALS` |  | Bind password for LDAP connections |
| `LDAP_SECURITY_METHOD` | TLS | Security method for LDAP connections |
| `LDAP_SEARCH_NAME` | DC=esss,DC=lu,DC=se | LDAP user search base |
| `LDAP_SEARCH_FILTER_USERNAME` | (sAMAccountName={0}) | LDAP username search filter |
| `LDAP_SEARCH_FILTER_FIRSTNAME` | (givenName={0}) | LDAP firstname search filter |
| `LDAP_SEARCH_FILTER_LASTNAME` | (sn={0}) | LDAP lastname search filter |
| `LDAP_SEARCH_FILTER_OBJECT` | (objectCategory=CN=Person,CN=Schema,CN=Configuration,DC=esss,DC=lu,DC=se)(objectClass=user) | LDAP user search filter |
| `LDAP_SEARCH_PAGESIZE` | 500 | LDAP page size |
| `LDAP_ATTR_USERNAME` | samaccountname | LDAP username attribute |
| `LDAP_ATTR_FIRSTNAME` | givenname | LDAP firstname attribute |
| `LDAP_ATTR_MIDDLENAME` |  | LDAP middlename attribute |
| `LDAP_ATTR_LASTNAME` | sn | LDAP lastname attribute |
| `LDAP_ATTR_GROUP` | department | LDAP group attribute |
| `LDAP_ATTR_GROUP_FILTER` | OU=ESS Organisational Units,DC=esss,DC=lu,DC=se | LDAP group filter |
| `LDAP_ATTR_EMAIL` | mail | LDAP email attribute |
| `LDAP_ATTR_PHONE` | telephonenumber | LDAP phone attribute |
| `LDAP_ATTR_MOBILE` | mobile | LDAP mobile attribute |
| `LDAP_ATTR_LOCATION` |  | LDAP location attribute |

### Docker Compose

For convenience, the application comes with a `docker-compose.yml` file, which can be used to run the application with required services and configuration:

```
$ docker-compose up
```

For convenience, additional docker compose files have been added to `.gitignore`. These can be used to run the application with required services and configuration but without content being committed, e.g. sensitive data such as passwords and tokens, or volatile information such as local links.

- `docker-compose-local-cceco.yml`
- `docker-compose-local-database.yml`
- `docker-compose-local.yml`
