@echo off
setlocal enabledelayedexpansion
@set port=%1
@set ip=%2
for /f "tokens=5 delims= " %%P in ('netstat -a -n -o ^| findstr /r "\<%ip%:%port%\>"') do (
    call :innerloop %%P
    goto :break
)
:innerloop
set /a counter=0
for /f "tokens=8 delims= " %%U in ('tasklist /FI "PID eq %1" /v') do (
    set /a counter=!counter!+1
    if !counter!==3 (
        echo %%U
    )
)
:break