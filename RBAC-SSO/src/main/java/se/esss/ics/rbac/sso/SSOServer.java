/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.message.MessageProperties;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.sso.restservices.SSOResources;

/**
 * <code>SSOServer</code> is a single sign on server on which local authentication services are running.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public final class SSOServer {

    private static final String HOSTNAME_FLAG = "-h";
    private static final String PORT_FLAG = "-p";
    private static final String ACCEPTED_IP_FLAG = "-f";
    private static final String INTERACTIVE_FLAG = "-i";

    private static final Logger LOGGER = LoggerFactory.getLogger(SSOServer.class);

    private static HttpServer ssoServer;

    private SSOServer() {
    }

    /**
     * Starts single sign on server.
     *
     * @param baseUri URI on which server is started
     */
    private static boolean startServer(URI baseUri) {
        ResourceConfig resourceConfig = new ResourceConfig(SSOResources.class).register(RequestFilter.class);
        resourceConfig.property(MessageProperties.XML_SECURITY_DISABLE, Boolean.TRUE);
        try {
            ssoServer = GrizzlyHttpServerFactory.createHttpServer(baseUri, resourceConfig);
            return true;
        } catch (Exception e) {
            LOGGER.error("Exception occured while starting/running SSO server", e);
            return false;
        }
    }

    /**
     * Stops single sign on server.
     */
    private static void stopServer() {
        ssoServer.shutdown();
    }

    /**
     * Main method in which host and port are read and server is started.
     *
     * @param args arguments (-h host and -p port)
     * @throws InterruptedException if the main thread was interrupted, which should never really happen
     */
    public static void main(String[] args) throws InterruptedException {
        String host = SSOProperties.getInstance().getLocalServicesHostname();
        int port = SSOProperties.getInstance().getLocalServicesPort();
        String acceptedFile = SSOProperties.getInstance().getLocalServicesAcceptedIPsFilePath();
        boolean interactive = true;

        for (int i = 0; i < args.length; i++) {
            if (HOSTNAME_FLAG.equals(args[i])) {
                host = args[i + 1];
                i++;
            } else if (PORT_FLAG.equals(args[i])) {
                port = Integer.parseInt(args[i + 1]);
                i++;
            } else if (ACCEPTED_IP_FLAG.equals(args[i])) {
                acceptedFile = args[i + 1];
                i++;
            } else if (INTERACTIVE_FLAG.equals(args[i])) {
                interactive = Boolean.parseBoolean(args[i + 1]);
                i++;
            }
        }

        if (!interactive) {
            System.setOut(new NullPrintStream());
            System.setErr(new NullPrintStream());
            System.setIn(new InputStream() {
                @Override
                public int read() throws IOException {
                    synchronized(this) {
                        try {
                            while(true) {
                                wait(1000);
                            }
                        } catch (InterruptedException e) {
                            //ignore
                        }
                    }
                    return 0;
                }
            });
        }

        SSOProperties.getInstance().setLocalServicesHostname(host);
        SSOProperties.getInstance().setLocalServicesPort(port);
        SSOProperties.getInstance().setLocalServicesAcceptedIPsFilePath(acceptedFile);

        URI uri = UriBuilder.fromUri("http://" + host).port(port).build();
        LOGGER.info("Server started on " + uri + ".");
        if (!startServer(uri)) {
            System.err.println("Could not start the server.");
            return;
        }

        parseLine(null);

        try {
            BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String line = bf.readLine();
                if (parseLine(line))
                    break;
            }
        } catch (IOException e) {
            if (e.getMessage().contains("The handle is invalid")) {
                // console is not available
                while (true) {
                    synchronized (uri) {
                        uri.wait(5000);
                    }
                }
            } else {
                LOGGER.error("Error reading system input stream.", e);
            }
        }
    }

    private static boolean parseLine(String lineToParse) {
        String line = lineToParse;
        if (line == null) {
            line = "";
        }
        line = line.trim().toLowerCase();
        if ("help".equals(line)) {
            System.out.println();
            System.out.println("The following commands are available:");
            System.out.println("    help - print this help page");
            System.out.println("    exit - stop the server gracefully");
            System.out.println("    quit - stop the server gracefully");
            System.out.println("    tokens - display the currently owned tokens");
            System.out.println("    url - display the url this server is accessible on");
            System.out.println();
            System.out.println("The following startup parameters are available:");
            System.out.println("    -h <host> - the host on which this server is reachable");
            System.out.println("    -p <port> - the port on which this server is reachable");
            System.out.println("    -f <path> - the path to the file that contains the accepted IP addresses");
            System.out.println("    -i <true|false> - interactive console (default = true)");
        } else if ("quit".equals(line) || "exit".equals(line)) {
            stopServer();
            return true;
        } else if ("url".equals(line)) {
            System.out.println("http://" + SSOProperties.getInstance().getLocalServicesHostname() + ":"
                    + SSOProperties.getInstance().getLocalServicesPort());
        } else if ("tokens".equals(line)) {
            Map<String, TokenInfo> tokens = TokenMap.getInstance().getTokens();
            System.out.println("There are " + tokens.size() + " tokens registered on this server.");
            for (Map.Entry<String, TokenInfo> e : tokens.entrySet()) {
                System.out.println("Owner: " + e.getKey());
                System.out.println("Username: " + e.getValue().getUserID());
                System.out.println("Creation Time: " + new Date(e.getValue().getCreationTime()));
                System.out.println("Expiration Time: " + new Date(e.getValue().getExpirationTime()));
                System.out.println("IP: " + e.getValue().getIp());
                System.out
                        .print("Roles: " + Arrays.toString(e.getValue().getRoleNames().toArray(new String[0])));
                System.out.println("--------------------------------------");
            }
        } else {
            System.out.println("Type 'help' to see available commands.");
        }
        return false;
    }
}
